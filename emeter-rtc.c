//--------------------------------------------------------------------------
//
//  Software for MSP430 based e-meters.
//
//  THIS PROGRAM IS PROVIDED "AS IS". TI MAKES NO WARRANTIES OR
//  REPRESENTATIONS, EITHER EXPRESS, IMPLIED OR STATUTORY, 
//  INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS 
//  FOR A PARTICULAR PURPOSE, LACK OF VIRUSES, ACCURACY OR 
//  COMPLETENESS OF RESPONSES, RESULTS AND LACK OF NEGLIGENCE. 
//  TI DISCLAIMS ANY WARRANTY OF TITLE, QUIET ENJOYMENT, QUIET 
//  POSSESSION, AND NON-INFRINGEMENT OF ANY THIRD PARTY 
//  INTELLECTUAL PROPERTY RIGHTS WITH REGARD TO THE PROGRAM OR 
//  YOUR USE OF THE PROGRAM.
//
//  IN NO EVENT SHALL TI BE LIABLE FOR ANY SPECIAL, INCIDENTAL, 
//  CONSEQUENTIAL OR INDIRECT DAMAGES, HOWEVER CAUSED, ON ANY 
//  THEORY OF LIABILITY AND WHETHER OR NOT TI HAS BEEN ADVISED 
//  OF THE POSSIBILITY OF SUCH DAMAGES, ARISING IN ANY WAY OUT 
//  OF THIS AGREEMENT, THE PROGRAM, OR YOUR USE OF THE PROGRAM. 
//  EXCLUDED DAMAGES INCLUDE, BUT ARE NOT LIMITED TO, COST OF 
//  REMOVAL OR REINSTALLATION, COMPUTER TIME, LABOR COSTS, LOSS 
//  OF GOODWILL, LOSS OF PROFITS, LOSS OF SAVINGS, OR LOSS OF 
//  USE OR INTERRUPTION OF BUSINESS. IN NO EVENT WILL TI'S 
//  AGGREGATE LIABILITY UNDER THIS AGREEMENT OR ARISING OUT OF 
//  YOUR USE OF THE PROGRAM EXCEED FIVE HUNDRED DOLLARS 
//  (U.S.$500).
//
//  Unless otherwise stated, the Program written and copyrighted 
//  by Texas Instruments is distributed as "freeware".  You may, 
//  only under TI's copyright in the Program, use and modify the 
//  Program without any charge or restriction.  You may 
//  distribute to third parties, provided that you transfer a 
//  copy of this license to the third party and the third party 
//  agrees to these terms by its first use of the Program. You 
//  must reproduce the copyright notice and any other legend of 
//  ownership on each copy or partial copy, of the Program.
//
//  You acknowledge and agree that the Program contains 
//  copyrighted material, trade secrets and other TI proprietary 
//  information and is protected by copyright laws, 
//  international copyright treaties, and trade secret laws, as 
//  well as other intellectual property laws.  To protect TI's 
//  rights in the Program, you agree not to decompile, reverse 
//  engineer, disassemble or otherwise translate any object code 
//  versions of the Program to a human-readable form.  You agree 
//  that in no event will you alter, remove or destroy any 
//  copyright notice included in the Program.  TI reserves all 
//  rights not specifically granted under this license. Except 
//  as specifically provided herein, nothing in this agreement 
//  shall be construed as conferring by implication, estoppel, 
//  or otherwise, upon you, any license or other right under any 
//  TI patents, copyrights or trade secrets.
//
//  You may not use the Program in non-TI devices.
//
//  File: emeter-rtc.c
//
//  Steve Underwood <steve-underwood@ti.com>
//  Texas Instruments Hong Kong Ltd.
//
//  $Id: emeter-rtc.c,v 1.5 2009/04/23 07:32:08 a0754793 Exp $
//
/*! \file emeter-structs.h */
//
//--------------------------------------------------------------------------
#include <stdint.h>
#include <stdlib.h>
#include <io.h>
#include <emeter-toolkit.h>
#include "emeter-structs.h"
#include "Modem_Identify.h"
#include "emeter-communication.h"
#include "Telit.h"
#include "OTAUpdate.h"
#include "emeter-rtc.h"
#include "emeter-fifo.h"
#include "mb.h"
#include "debug.h"
//#define ONE_MINUTE_SAMPLING

#if !defined(NULL)
  #define NULL    (void *) 0
#endif

rtc_t rtc;
uint16_t gQueueDataFlag;
uint16_t gSendStatusFlag;
uint16_t gUploadReportFlag;
uint16_t gCheckConfigFlag;

uint16_t DisableDebugTimer = FALSE;


#if defined(CORRECTED_RTC_SUPPORT)
  int32_t rtc_correction;
#endif

  /* We need a small seconds counter, so we can do things like a display update every 2 seconds. */
uint8_t seconds;

void per_minute_activity(void);
void rtc_bumper(void);

uint16_t * gMdmTimerFlag;
uint16_t   gMdmTimerSecs;
uint16_t   gMdmTimerCnt;

uint16_t * gCommsTimerFlag;
uint16_t   gCommsTimerSecs;
uint16_t   gCommsTimerCnt;

void set_dummy_rtc(void)
{
    rtc.century = 20;
    rtc.year = 18;
    rtc.month = 1;
    rtc.day = 1;
    rtc.hour = 1;
    rtc.minute = 0;
    rtc.second = 0;
    set_rtc_sumcheck();
}

void start_modem_timer(uint16_t * flag, uint16_t secs)
{
	gMdmTimerCnt = 0;
	*flag = FALSE;
	gMdmTimerFlag = flag;
	gMdmTimerSecs = secs;
}

void start_comms_timer(uint16_t * flag, uint16_t secs)
{
	gCommsTimerCnt = 0;
	*flag = FALSE;
	gCommsTimerFlag = flag;
	gCommsTimerSecs = secs;
}

void per_minute_activity(void)
{
   if ( (rtc.minute % nv_sys_conf.s.sample_period) == 0)                         // Queue data at the sample rate
   {
	   if ((comms_meter_status & STATUS_ACTIVATED) != 0)
	   {
		  gQueueDataFlag = TRUE;
	   }
   }

   if (((rtc.hour + 1) * 60 + rtc.minute - nv_sys_conf.s.report_minute ) % nv_sys_conf.s.report_period  == 0)  // Send a report at the report rate
  {
	   if ((comms_meter_status & STATUS_ACTIVATED) != 0)
	   {
		   if ((FIFOTimeValid() == TRUE) && (getFIFODepth() > 0))
		   {
		     //  if (gModeSelect == HTTPOnly)
		       gUploadReportFlag = TRUE;    // Post a report
		   }
		   gCheckConfigFlag = TRUE;         // Get the config
	   }

   }

}


void per_second_activity(void)
{
	if(DisableDebugTimer++ % 3 == 2)
	{
	  EnableDebug();
	  EnableLCDPort();
	}

	if (gMdmTimerFlag != NULL)
	{
		if (gMdmTimerCnt++ >= gMdmTimerSecs)
		{
			*gMdmTimerFlag = TRUE;              // Set the flag
			gMdmTimerFlag = NULL;               // Clear the pointer
		}
	}

	if (gCommsTimerFlag != NULL)
	{
		if (gCommsTimerCnt++ >= gCommsTimerSecs)
		{
			*gCommsTimerFlag = TRUE;              // Set the flag
			gCommsTimerFlag = NULL;               // Clear the pointer
		}
	}
}


#if defined(RTC_SUPPORT)
void rtc_bumper(void)
{
    int i;

    i = bump_rtc();

    /* And now, a series of optional routines to get actions to take
       place at various intervals. Remember, we are in an interrupt
       routine. Do not do anything very complex here. If a complex action
       is needed set a flag in a simple routine and do the main work in
       the non-interrupt code's main loop. */
    #if defined(PER_YEAR_ACTIVITY_SUPPORT)
      if (i >= RTC_CHANGED_YEAR)
          per_year_activity();    
    #endif
    #if defined(PER_MONTH_ACTIVITY_SUPPORT)
      if (i >= RTC_CHANGED_MONTH)
          per_month_activity();
    #endif
    #if defined(PER_DAY_ACTIVITY_SUPPORT)  ||  defined(MULTI_RATE_SUPPORT)
    if (i >= RTC_CHANGED_DAY)
    {
        #if defined(PER_MINUTE_ACTIVITY_SUPPORT)
          per_day_activity();
        #endif
        #if defined(MULTI_RATE_SUPPORT)
          tariff_flags |= TARIFF_NEW_DAY;
        #endif
    }
    #endif
    #if defined(PER_HOUR_ACTIVITY_SUPPORT)
    if (i >= RTC_CHANGED_HOUR)
        per_hour_activity();
    #endif
    #if defined(PER_MINUTE_ACTIVITY_SUPPORT)  ||  defined(MULTI_RATE_SUPPORT)  ||  defined(BATTERY_MONITOR_SUPPORT)
    if (i >= RTC_CHANGED_MINUTE)
    {
        #if defined(PER_MINUTE_ACTIVITY_SUPPORT)
        per_minute_activity();
        #endif
        #if defined(MULTI_RATE_SUPPORT)
        tariff_flags |= TARIFF_NEW_MINUTE;
        #endif
        #if defined(BATTERY_MONITOR_SUPPORT)
        test_battery();
        #endif
    }
    #endif
    #if defined(PER_SECOND_ACTIVITY_SUPPORT)
    if (i >= RTC_CHANGED_SECOND)
        per_second_activity();
    #endif    
}
#endif

#if (defined(RTC_SUPPORT)  ||  defined(CUSTOM_RTC_SUPPORT))  &&  defined(CORRECTED_RTC_SUPPORT)
void correct_rtc(void)
{
    int32_t temp;

    /* Correct the RTC to allow for basic error in the crystal, and
       temperature dependant changes. This is called every two seconds,
       so it must accumulate two seconds worth of error at the current
       temperature. */
    if (nv_parms.seg_a.s.temperature_offset)
    {
        temp = temperature - nv_parms.seg_a.s.temperature_offset;
        temp *= nv_parms.seg_a.s.temperature_scaling;
        temp >>= 16;

        /* The temperature is now in degrees C. */
        /* Subtract the centre point of the crystal curve. */
        temp -= 25;
        /* Do the parabolic curve calculation, to find the current ppm of
           error due to temperature, and then the scaled RTC correction
           value for 2 seconds at this temperature. */
        temp = temp*temp*(2589L*4295L >> 5);
        temp >>= 11;
        temp = -temp;
    }
    else
    {
        temp = 0;
    }
    /* Allow for the basic manufacturing tolerance error of the crystal, found
       at calibration time. */
    temp += nv_parms.seg_a.s.crystal_base_correction;
    if (rtc_correction >= 0)
    {
        rtc_correction += temp;
        if (rtc_correction < 0)
        {
            rtc_correction -= 0x80000000;
            /* We need to add an extra second to the RTC */
    #if defined(CUSTOM_RTC_SUPPORT)
            custom_rtc();
    #else
            rtc_bumper();
    #endif
        }
    }
    else
    {
        rtc_correction += temp;
        if (rtc_correction >= 0)
        {
            rtc_correction += 0x80000000;
            /* We need to drop a second from the RTC */
            meter_status |= SKIP_A_SECOND;
        }
    }
}
#endif

#pragma vector=RTC_VECTOR
__interrupt void one_second_ticker(void)
{
    #if defined(__MSP430_HAS_RTC_C__) || defined(__MSP430_HAS_RTC_CE__)
   RTCPS1CTL &= ~RT1PSIFG;                  //  clear the flag
    #endif

   #if defined(RTC_SUPPORT)
        #if defined(CORRECTED_RTC_SUPPORT)
            /* Allow for RTC correction. */
            if ((meter_status & SKIP_A_SECOND))
                meter_status &= ~SKIP_A_SECOND;
            else
                rtc_bumper();
        #else
            rtc_bumper();
        #endif
    #endif
    #if defined(CUSTOM_RTC_SUPPORT)
        if ((meter_status & SKIP_A_SECOND))
            meter_status &= ~SKIP_A_SECOND;
        else
            custom_rtc();
    #endif
    if (++seconds & 1)
       meter_status |= TICKER;  /* Kick every 2 seconds */

    if (operating_mode == OPERATING_MODE_NORMAL)
    {

      ADC10CTL1 = ADC10DIV_3 | ADC10SHS_0 | ADC10CONSEQ_2 | ADC10SHP;       // SC trig., rpt, A10
      ADC10CTL0 &= ~(ADC10ENC | ADC10SC);
      ADC10MCTL0 = ADC10INCH_10 | ADC10SREF_1;

      ADC10CTL0 = ADC10SHT_8 | ADC10ON;
      ADC10IE = ADC10IE0;
      ADC10CTL0 |= ADC10ENC;
      ADC10CTL0 |= ADC10SC;
    }

}
