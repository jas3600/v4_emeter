/***********************************************************************************
 *   emeter-boot-reset.c
 *
 *   Handles the Resets from WDT and PMM
 *
 *   Author: JAS
 *
 ************************************************************************************/

#include <stdint.h>
#include <emeter-toolkit.h>
#include "emeter-structs.h"
#include "emeter-boot-reset.h"
#include "emeter-fifo.h"
#include "debug.h"
#include "hal_PMM.h"
#include "hal_UCS.h"

//#define DB_BOOT
#ifdef DB_BOOT
#define BOOT_DBPRINT(...) PrintDebugMessage(__VA_ARGS__)
#else
#define BOOT_DBPRINT(...)
#endif


/*************************************************************************************
 * InitPMM(void)
 *
 * TODO Description
 *
 *************************************************************************************/
unsigned int InitPMM(void)
{
	// Open PMM registers for write access
	PMMCTL0_H = PMMPW_H;

	// disable high side SVM
	SVSMHCTL = 0;

	// Clear the SVMHIFG
	PMMIFG &= ~SVMHIFG;
    SYSSNIV = 0;

	// Enable and Set SVSM Levels
	SVSMHCTL |= SVSHE | SVSHFP | SVMHE | SVMHFP | SVSHRVL_3 | SVSMHRRL_5;

	// Enable SVS Monitor High Level Intterrupt
	PMMRIE |= SVMHIE;
	// Lock PMM registers for write access
	PMMCTL0_H = 0x00;

	if ((PMMIFG & SVMHIFG) == SVMHIFG)
	return PMM_STATUS_ERROR;					 	// Highside is too low
	else return PMM_STATUS_OK;						// Return: OK

}

/*************************************************************************************
 * SystemResetQuery(void)
 *
 * TODO Description
 *
 *************************************************************************************/
void SystemResetQuery(void)
{
	switch ( __even_in_range( SYSRSTIV, SYSRSTIV_PMMKEY ) )
	{
	   case SYSRSTIV_NONE:
		   strcpy(gBootMsg,"BOOT: No Interrupt pending");
		   break;       // No Interrupt pending
	   case SYSRSTIV_BOR:
		   strcpy(gBootMsg,"BOOT: BOR(TurnedOn)");
		   break;       // SYSRSTIV : BOR
	   case SYSRSTIV_RSTNMI:
		   strcpy(gBootMsg,"BOOT: RST/NMI(VSpike)");
		   break;       // SYSRSTIV : RST/NMI
	   case SYSRSTIV_DOBOR:
		   strcpy(gBootMsg,"BOOT: SW BOR");
		   break;       // SYSRSTIV : Do BOR
	   case SYSRSTIV_LPM5WU:
		   strcpy(gBootMsg,"BOOT: Port LPM5 Wake Up");
		   break;       // SYSRSTIV : Port LPM5 Wake Up
	   case SYSRSTIV_SECYV:
		   strcpy(gBootMsg,"BOOT: Memory violation");
		   break;       // SYSRSTIV : Security violation
	   case SYSRSTIV_SVSL:
		   strcpy(gBootMsg,"BOOT: SVSL(VSpike)");
		   break;       // SYSRSTIV : SVSL
	   case SYSRSTIV_SVSH:
		   strcpy(gBootMsg,"BOOT: SVSH(VSpike)");
		   break;       // SYSRSTIV : SVSH
	   case SYSRSTIV_SVML_OVP:
		   strcpy(gBootMsg,"BOOT: SVML_OVP");
		   break;       // SYSRSTIV : SVML_OVP
	   case SYSRSTIV_SVMH_OVP:
		   strcpy(gBootMsg,"BOOT: SVMH_OVP");
		   break;       // SYSRSTIV : SVMH_OVP
	   case SYSRSTIV_DOPOR:
		   strcpy(gBootMsg,"BOOT: SW POR(OTA)");
		   break;       // SYSRSTIV : Do POR
	   case SYSRSTIV_WDTTO:
		   strcpy(gBootMsg,"BOOT:  WDT Time out(VSpikeOrFwError)");
		   break;       // SYSRSTIV : WDT Time out
	   case SYSRSTIV_WDTKEY:
		   strcpy(gBootMsg,"BOOT: WDTKEY violation");
		   break;       // SYSRSTIV : WDTKEY violation
	   case SYSRSTIV_KEYV:
		   strcpy(gBootMsg,"BOOT: Flash Key violation");
		   break;       // SYSRSTIV : Flash Key violation
	   case SYSRSTIV_PERF:
		   strcpy(gBootMsg,"BOOT: peripheral/config area fetch");
		   break;       // SYSRSTIV : peripheral/config area fetch
	   case SYSRSTIV_PMMKEY:
		   strcpy(gBootMsg,"BOOT: PMMKEY violation");
		   break;       // SYSRSTIV : PMMKEY violation
	   default:
		   strcpy(gBootMsg,"BOOT: default");
		   break;
	}

	sprintf(gDbgMsg,"%s\r\n",gBootMsg);
	BOOT_DBPRINT(gDbgMsg);
}

/*************************************************************************************
 * PowerDownSave(void)
 *
 * TODO Description
 *
 *************************************************************************************/
void PowerDownSave(void)
{
    struct operation_mem_s lOpVars;

	Set_Analog_Disable_Flag();

	if(gStartupCnt > 10)
	{
		lOpVars.s.consumed_energy = total_consumed_active_energy;
		lOpVars.s.produced_energy = total_produced_active_energy;
		lOpVars.s.wh_consumed_energy = wh_total_consumed_active_energy;
		lOpVars.s.wh_produced_energy = wh_total_produced_active_energy;

		lOpVars.s.op_config = comms_meter_status;
		//lOpVars.s.log[0].entry_time = rtc;
		//lOpVars.s.log[0].entry = 0x00FF & SYSRSTIV;

		flash_clr((int*) &nv_op_vars);
		flash_memcpy((char *) &nv_op_vars, (char *) lOpVars.x, sizeof(lOpVars));
		flash_secure();

		save_msg_fifo2();

	//	Init_FLL_Settle(SD_CLK*8388608/8/1000, SD_CLK*32768*32/32768);   	// set clock to 8Mhz
	//	SetVCore(0);														// Reduce Vcore

	}

	BOOT_DBPRINT("Shut Down");
    // Let voltages settle before rebooting
	kick_wdt_resetmode_16s();
    while(1);
}

/*************************************************************************************
 * WDT_handler(void)
 *
 * TODO Description
 *
 *************************************************************************************/
#pragma vector = WDT_VECTOR
__interrupt void WDT_handler(void)
{
	struct operation_mem_s lOpVars;

	Set_Analog_Disable_Flag();
	__disable_interrupt();

	lOpVars.s.consumed_energy = total_consumed_active_energy;
	lOpVars.s.produced_energy = total_produced_active_energy;
	lOpVars.s.wh_consumed_energy = wh_total_consumed_active_energy;
	lOpVars.s.wh_produced_energy = wh_total_produced_active_energy;

	lOpVars.s.op_config = comms_meter_status;
	//lOpVars.s.log[0].entry_time = rtc;
	//lOpVars.s.log[0].entry = 0x00FF & SYSRSTIV;

	flash_clr((int*) &nv_op_vars);
	flash_memcpy((char *) &nv_op_vars, (char *) lOpVars.x, sizeof(lOpVars));
	flash_secure();

	save_msg_fifo2();

	kick_wdt_resetmode_16s();

	// Immediate reboot with BOR
    PMMCTL0_H = PMMPW_H;
    PMMCTL0_L |= PMMSWBOR;
}

/*************************************************************************************
 * SYSNMI_handler(void)
 *
 * TODO Description
 *
 *************************************************************************************/
#pragma vector = SYSNMI_VECTOR
__interrupt void SYSNMI_handler(void)
{
	switch ( __even_in_range( SYSSNIV, SYSSNIV_VLRHIFG ) )
	{
		case  SYSSNIV_NONE:
		case  SYSSNIV_SVMLIFG:
			break;
		case  SYSSNIV_SVMHIFG:
		    PowerDownSave();
			break;
		case  SYSSNIV_DLYLIFG:
		case  SYSSNIV_DLYHIFG:
		case  SYSSNIV_VMAIFG:
		case  SYSSNIV_JMBINIFG:
		case  SYSSNIV_JMBOUTIFG:
		case  SYSSNIV_VLRLIFG:
		case  SYSSNIV_VLRHIFG:
		default:
			break;
	}

}

/*************************************************************************************
 * Save_AND_Reset(void)
 *
 * TODO Description
 *
 *************************************************************************************/
void Save_AND_Reset(void)
{
	struct operation_mem_s lOpVars;

	// save_msg_fifo2();
	BOOT_DBPRINT("Saving Cons and Prod Power");
	// Save the opvars
	lOpVars.s.consumed_energy = total_consumed_active_energy;
	lOpVars.s.produced_energy = total_produced_active_energy;
	lOpVars.s.wh_consumed_energy = wh_total_consumed_active_energy;
	lOpVars.s.wh_produced_energy = wh_total_produced_active_energy;

	lOpVars.s.op_config = comms_meter_status;

	flash_clr((int*) &nv_op_vars);
	flash_memcpy((char *) &nv_op_vars, (char *) lOpVars.x, sizeof(lOpVars));
	flash_secure();

	save_msg_fifo2();

	//Resetting the Processor by WDT
	BOOT_DBPRINT("Calling BSL INIT");

	// __delay_cycles(1000);
//	__disable_interrupt();
	// Immediate Reboot
    PMMCTL0_H = PMMPW_H;
    PMMCTL0_L |= PMMSWPOR;
}
