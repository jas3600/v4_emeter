/*
 * OTAUpdate.h
 *
 *  Created on: May 31, 2016
 *      Author: Afreen
 */

#ifndef V3_OTAUPDATE_H_
#define V3_OTAUPDATE_H_

#define LocalFW 0
#define SD_STARTING_BLOCK  	256

#define FW_UPDATE_FAILED 0
#define FW_UPDATE_SUCCESS 1
#define MAX_FW_LOAD_SECTORS 2000
#define MAX_FW_LOAD_SIZE 1000000  // Bytes

int UpdateFirmware(char* aServerFW); 									//++++++++++++++++++++++++++
//int DetectSDCard(void);
void DisplaySramContent(void);
void init_SD(void);

extern unsigned char OTAFailMsg[50];

#endif /* V3_OTAUPDATE_H_ */
