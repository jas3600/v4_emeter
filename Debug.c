/*
 * Debug.c
 *
 *  Created on: Dec 8, 2016
 *      Author: John
 *
 *      revisions:
 *      9/21/2017 - Made lTime longer to prevent overflow. Added counter to while loop to make safe.
 */

#include <emeter-toolkit.h>
#include "emeter-structs.h"
#include "emeter-communication.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <io.h>
#include "Debug.h"

int gDisableDebugFlag;
char gDbgMsg[800];
char gBootMsg[100];

void ClearDebugBuffer(void);

/**********************************************************************************
 *   EnableDebug(void)
 *
 *   TODO  Description
 *
 **********************************************************************************/
void EnableDebug(void)
{
	gDisableDebugFlag = FALSE;
}

/**********************************************************************************
 *   DisableDebug(void)
 *
 *   TODO  Description
 *
 **********************************************************************************/
void DisableDebug(void)
{
    gDisableDebugFlag = TRUE;
}

/**********************************************************************************
 *   Init_Debug_UART(unsigned int aClk, unsigned long int aBAUD)
 *
 *   TODO  Description
 *
 **********************************************************************************/
void Init_Debug_UART(unsigned int aClk, unsigned long int aBAUD)
{
	UCA2CTL0 = 0;                       /* 8-bit character */
	UCA2CTL1 |= UCSSEL__SMCLK | UCSWRST;          /* UCLK = SMCLK  place in reset */

	if (aClk == 24)
	{
		UCA2BR1 = 0x00;				// 9600 @ 24MHz
//		UCA2BR0 = 0x9E;
        UCA2BR0 = 0xA3;
		UCA2MCTLW = 0x0041;			// 9600 @ 24Mhz
	}
	else                           // 8 Mhz
	{
		UCA2BR1 = 0x00;				// 9600 @ 8 MHz
		UCA2BR0 = 0x35;
		UCA2MCTLW = 0x0011;			// 9600 @ 8 Mhz
	}

	ClearDebugBuffer();

	UCA2CTL1 &= ~UCSWRST;          // release from reset
	UCA2IE |= UCRXIE;              // Enable RX interrupt
//	UCA3IE |= UCRXIE;

	gDisableDebugFlag = FALSE;
}

/**********************************************************************************
 *   ClearDebugBuffer(void)
 *
 *   TODO  Description
 *
 **********************************************************************************/
void ClearDebugBuffer(void)
{
	tx_msg[DEBUG_PORT].ptr = 0;
	tx_msg[DEBUG_PORT].len = 0;
}

/**********************************************************************************
 *   PrintDebugMessage(char* ptr)
 *
 *   TODO  Description
 *
 **********************************************************************************/
void PrintDebugMessage(char* aPtr)
{
	char lTime[35];
	char * lStrPtr;
	int lMsgLen;

	if (gDisableDebugFlag == 1) return;

	sprintf(lTime,"\r\n(%d:%d:%d:%d:%d:%d) ",rtc.year,rtc.month,rtc.day,rtc.hour,rtc.minute,rtc.second);

	lStrPtr = lTime;

    while (*lStrPtr != '\0')
    {
        while (UCA2STATW & UCBUSY);
        UCA2TXBUF = *lStrPtr;
        lStrPtr++;
    }

	lMsgLen = strlen(aPtr);


	if (lMsgLen > MAX_MESSAGE_LENGTH)
	{
	    strcpy(aPtr, "DEBUG MESSAGE TOO LONG!");
	}
	else
	{
      if (aPtr[lMsgLen - 2] == '\r') aPtr[lMsgLen - 2] = '\0';       // remove endpoint carriage return
	}

	while (*aPtr != '\0')
    {
        while (UCA2STATW & UCBUSY);
        UCA2TXBUF = *aPtr;
        aPtr++;
    }

}



