/**************************************************************************
 * Emeter-display.c
 *
 *  Created on: Jan, 2017
 *      Author: JAS
 *
 **************************************************************************/
#include "emeter-display.h"
#include <emeter-toolkit.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include "emeter-structs.h"
#include "emeter-communication.h"
#include "Telit.h"


/***************************
   LCD Segment Mapping

        -A-
       F   B
        -G-
       E   C
        -D-

*************************/
const unsigned char LCD_Char_Map[] =
{
    SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F,                // '0'
    SEG_B | SEG_C,                                                // '1'
    SEG_A | SEG_B | SEG_D | SEG_E | SEG_G,                        // '2'
    SEG_A | SEG_B | SEG_C | SEG_D | SEG_G,                        // '3'
    SEG_B | SEG_C | SEG_F | SEG_G,                                // '4'
    SEG_A | SEG_C | SEG_D | SEG_F | SEG_G,                        // '5'
    SEG_C | SEG_D | SEG_E | SEG_F | SEG_G,                        // '6'
    SEG_A | SEG_B | SEG_C,                                        // '7'
    SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G,        // '8'
    SEG_A | SEG_B | SEG_C | SEG_D | SEG_F | SEG_G,                // '9'
    SEG_A | SEG_B | SEG_C | SEG_E | SEG_F | SEG_G,                // 'A'  10
    SEG_C | SEG_D | SEG_E | SEG_F | SEG_G,                        // 'b'
    SEG_A | SEG_D | SEG_E | SEG_F,                                // 'C'
    SEG_B | SEG_C | SEG_D | SEG_E | SEG_G,                        // 'd'
    SEG_A | SEG_D | SEG_E | SEG_F | SEG_G,                        // 'E'
    SEG_A | SEG_E | SEG_F | SEG_G,                                // 'F'
    SEG_A | SEG_B | SEG_C | SEG_D | SEG_F | SEG_G,                // 'g'
    SEG_B | SEG_C | SEG_E | SEG_F | SEG_G,                        // 'H'
    SEG_E | SEG_F,                                                // 'I'
    SEG_B | SEG_C | SEG_D | SEG_E,                                // 'J'
    SEG_B | SEG_C | SEG_E | SEG_F | SEG_G,                        // 'K'
    SEG_D | SEG_E | SEG_F,                                        // 'L'
    SEG_A | SEG_C | SEG_E,                                        // 'M'
    SEG_C | SEG_E | SEG_G,                                        // 'n'
    SEG_C | SEG_D | SEG_E | SEG_G,                                // 'o'
    SEG_A | SEG_B | SEG_E | SEG_F | SEG_G,                        // 'P'
    SEG_A | SEG_B | SEG_C | SEG_F | SEG_G,                        // 'q'
    SEG_E | SEG_G,                                                // 'r'
    SEG_A | SEG_C | SEG_D | SEG_F | SEG_G,                        // 'S'
    SEG_D | SEG_E | SEG_F | SEG_G,                                // 't'
    SEG_B | SEG_C | SEG_D | SEG_E | SEG_F,                        // 'U'
    SEG_C | SEG_D | SEG_E,                                        // 'v'
    SEG_B | SEG_D | SEG_F,                                        // 'W'
    SEG_B | SEG_C | SEG_E | SEG_F | SEG_G,                        // 'X'
    SEG_B | SEG_C | SEG_D | SEG_F | SEG_G,                        // 'y'
    SEG_A | SEG_B | SEG_D | SEG_E | SEG_G,                        // 'Z'
    SEG_G                                                         // '-'  36
};

#define charidx(x) ((x) - 'A' + 10)

int gLCDCounter;
unsigned int gLCDMessageState;
unsigned int gDisplayL1State;
uint16_t gLCD_Status;
int gDisableLcdFlag = FALSE;

// This struct stores the XX.YY,ZZ for printing firmware version.
//Its needed for displaying the emeter firmware version first and also in a loop
typedef struct
{
	uint16_t XX;
	uint16_t YY;
	uint16_t ZZ;

} FwVer;
FwVer FwData;

unsigned char chartoLCD(char digit);
void Prod_KWHInfo(char * LCDMessage,unsigned long Kwh);
void Consumed_KWHInfo(char * LCDMessage,unsigned long Kwh);
void VoltInfo(char* LCDMessage,float Volt);
void CurrInfo(char* LCDMessage,float Curr);
void LCDDispFloat(char* LCDMessage,float aNum);
void LCDDispUInt(char* LCDMessage,unsigned long aNum);
void LCDStatusSymbols(char* LCDMessage,unsigned int gLCDStatus,unsigned int DisplayL1State);
void LCD_Display_Text(char * LCDMessage);


void initLCD(void)
{
    gLCDCounter = 3;
    gDisableLcdFlag = FALSE;
}

void EnableLCDPort(void)
{
    gDisableLcdFlag = FALSE;
}

void DisableLCDPort(void)
{
    gDisableLcdFlag = TRUE;
}

//Display Produced KWH to LCD data array
void Prod_KWHInfo(char * aLCDMessage, unsigned long aKwh)
{
    aLCDMessage[0] = BIT0 | BIT4 | BIT5;
    LCDDispUInt(aLCDMessage, aKwh);
}

//Display Consumed KWH to LCD data array
void Consumed_KWHInfo(char * aLCDMessage, unsigned long aKwh)
{
    aLCDMessage[0] = BIT0 | BIT4 | BIT5;
    LCDDispUInt(aLCDMessage, aKwh);
}

//Display Volt to LCD Data array
void VoltInfo(char* aLCDMessage, float aVolt)
{
    aLCDMessage[0] = BIT6;
    LCDDispFloat(aLCDMessage, aVolt);
}

//Display Current to LCD Data Array
void CurrInfo(char* aLCDMessage, float aCurr)
{
    aLCDMessage[0] = BIT7;
    LCDDispFloat(aLCDMessage, aCurr);
}

void LCDDispFloat(char* aLCDMessage, float aNum)
{
 unsigned char i;
 char lStrNum[10];

	 sprintf(lStrNum, "%8.1f", aNum);

	 for (i = 1; i < 7; i++) aLCDMessage[i] = 0;

	 for (i = 0; i < 6; i++)
	 {
		 if ((lStrNum[i] >= '0') && (lStrNum[i] <= '9'))
		 {
		 	aLCDMessage[7-i] = LCD_Char_Map[lStrNum[i] - '0'];
		 }
	 }

	 // Write Decimal Char
	 aLCDMessage[1] &= ~LCD_Char_Map[8];
	 aLCDMessage[1] = LCD_Char_Map[lStrNum[7] - '0'] | SEG_DP1;
}

void LCDDispUInt(char* aLCDMessage, unsigned long aNum)
{
  unsigned char i;
  char lStrNum[10];

	  sprintf(lStrNum, "%6lu", aNum);

	  for (i = 1; i < 7; i++) aLCDMessage[i] = 0;

	  for (i = 0; i < 6; i++)
	  {
	    if ((lStrNum[i] >= '0') && (lStrNum[i] <= '9'))
	    {
	      aLCDMessage[7-i] = LCD_Char_Map[lStrNum[i] - '0'];
	    }
	  }
}

void LCDShowMsg(char* aLCDMessage,unsigned int aMsg)
{
	switch (aMsg)
	{
		case NO_CEL:
			aLCDMessage[13] = 0;
			aLCDMessage[12] = LCD_Char_Map[charidx('N')];
			aLCDMessage[11] = LCD_Char_Map[charidx('O')];
			aLCDMessage[10] = LCD_Char_Map[charidx('C')];
			aLCDMessage[9] =  LCD_Char_Map[charidx('E')];
			aLCDMessage[8] =  LCD_Char_Map[charidx('L')];
		break;
		case b4_EnA:
			aLCDMessage[13] = 0;
			aLCDMessage[12] = LCD_Char_Map[charidx('B')];
			aLCDMessage[11] = LCD_Char_Map[charidx('4')];
			aLCDMessage[10] = LCD_Char_Map[charidx('E')];
			aLCDMessage[9] =  LCD_Char_Map[charidx('N')];
			aLCDMessage[8] =  LCD_Char_Map[charidx('A')];
		break;
	}

}


void LCDStatusSymbols(char* aLCDMessage,unsigned int gLCDStatus,unsigned int aDisplayL1State)
{
	if(aDisplayL1State % 5 != 4 ) //Only in lcd state 4
	{
		if (!(gLCDStatus & STATUS_POST))
		{
			aLCDMessage[0] |= SEG_SKULL;                 // Skull
		}
		else if (!(gLCDStatus & COMMS_STATUS_OK))
		{
			LCDShowMsg(aLCDMessage, NO_CEL);
		}
		else if (!(gLCDStatus & STATUS_ACTIVATED))
		{
			LCDShowMsg(aLCDMessage, b4_EnA);
		}
		else
		{
			aLCDMessage[4] |= SEG_THUMB;                // Thumbs up
		}

		if (gLCDStatus & STATUS_DISCONNECT_CLOSED)
		{
			aLCDMessage[10] |= SEG_ARROW;               // Arrow
		}
	}

	//all LCD States
	if (!(gLCDStatus & STATUS_POST))
	{
		aLCDMessage[13] |= BIT0;					     // LED on
	}
	else if(!(gLCDStatus & COMMS_STATUS_OK))
	{
		aLCDMessage[13] |= BIT0;					     // LED on
	}

}

extern unsigned int comms_meter_status;
extern const char EmeterFwVersion[9];

void LCD_Display_Text(char * aLCDMessage)
{
	unsigned int temp;
	unsigned int SerialSize;
	unsigned int i, j;
	char Serialno[6];
	char lCompany[30];
	char lEnvironment[30];
	int index[6];
	int resp;
	unsigned char tch;
	unsigned char tempstr[19];

	if (gLCDMessageState == 4) gLCDMessageState = 0;  // Start scrolling from the beginning

	switch (gLCDMessageState)
	{
		case 0:
			//Code to display CEREG

			aLCDMessage[7] = LCD_Char_Map[charidx('C')];			//C
			aLCDMessage[6] = LCD_Char_Map[charidx('R')];			//r
			aLCDMessage[5] = LCD_Char_Map[charidx('E')];			//E
			aLCDMessage[4] = LCD_Char_Map[charidx('G')];	        //g

			temp = GetNetworkStatus();

			if (temp <= 9) aLCDMessage[2] = LCD_Char_Map[temp];
			else aLCDMessage[2] = LCD_Char_Map[charidx('E')];		// E

			//Display CSQ info
			aLCDMessage[12] = LCD_Char_Map[charidx('C')];			//C
			aLCDMessage[11] = LCD_Char_Map[charidx('S')];			//S

			temp = GetSignalQuality();
			if ((temp / 10 <= 9) && (temp % 10 <= 9))
			{
				aLCDMessage[9] = LCD_Char_Map[temp / 10];
				aLCDMessage[8] = LCD_Char_Map[temp % 10];
			}
			else
			{
				aLCDMessage[9] = LCD_Char_Map[charidx('E')];		//E
				aLCDMessage[8] = LCD_Char_Map[charidx('E')];		//E
			}
		break;

		case 1:
			// Message indicates E METER firmware on LCD display.
			// can only work if the function DisplayFwVer() is called from main() first

			sscanf(EmeterFwVersion,"%d_%d_%d",&FwData.XX, &FwData.YY, &FwData.ZZ);

            aLCDMessage[7] = LCD_Char_Map[FwData.XX / 10];
            aLCDMessage[6] = LCD_Char_Map[FwData.XX % 10];
            aLCDMessage[5] = BIT3 | LCD_Char_Map[FwData.YY / 10];
            aLCDMessage[4] = LCD_Char_Map[FwData.YY % 10];
            aLCDMessage[3] = BIT3 | LCD_Char_Map[FwData.ZZ / 10];
			aLCDMessage[2] = LCD_Char_Map[FwData.ZZ % 10];

            aLCDMessage[12] = LCD_Char_Map[charidx('F')];   //F
            aLCDMessage[11] = 0X00;
            aLCDMessage[10] = LCD_Char_Map[charidx('V')];   //v
            aLCDMessage[9] =  LCD_Char_Map[charidx('E')];   //E
            aLCDMessage[8] =  LCD_Char_Map[charidx('R')];   //r

		break;

		case 2:
			//LCDMessage[1] = LCD_Char_Map[2];
			//Message indicates emeter comms status on LCD
			aLCDMessage[12] = LCD_Char_Map[charidx('E')];   //E
			aLCDMessage[11] = LCD_Char_Map[charidx('S')]; 	//S
			aLCDMessage[10] = LCD_Char_Map[charidx('T')];   //t
			aLCDMessage[9] =  LCD_Char_Map[charidx('A')];   //A
			aLCDMessage[8] =  LCD_Char_Map[charidx('T')];   //t

			temp = comms_meter_status & 0xE6B3;

			aLCDMessage[3] = LCD_Char_Map[temp & 0x000F];
			temp >>= 4;
			aLCDMessage[4] = LCD_Char_Map[temp & 0x000F];
			temp >>= 4;
			aLCDMessage[5] = LCD_Char_Map[temp & 0x000F];
			temp >>= 4;
			aLCDMessage[6] = LCD_Char_Map[temp & 0x000F];
		break;

		case 3:
			//Display the Serial Num
		    memset(tempstr,0, 19);
            j = 0;
		    // Strip Special Characters out of the Property number
		    for (i = 0; i < 18; i++)
		    {
		        tch = nv_sys_conf.s.property_number[i];

		        if (((tch >= '0') && (tch <= '9')) || ((tch >= 'A') && (tch <= 'Z')) || ((tch >= 'a') && (tch <= 'z')))
		        {
		            tempstr[j++] = tch;
		        }

		    }

            SerialSize = strlen(tempstr);

            if (SerialSize >= 7) temp = 7;
            else temp = SerialSize;

		    for (i = 0; i < temp; i++) aLCDMessage[7-i] = chartoLCD(tempstr[i]);

            if (SerialSize > 7)
            {
                temp = SerialSize - 7;
                if (temp > 5) temp = 5;

                for (i = 0; i < temp; i++) aLCDMessage[12-i] = chartoLCD(tempstr[7+i]);
            }

            gLCDCounter = 2;

		break;

	/*	case 4:
			//Extract the TENANT And the Environment from the URL and Display on the LCD
			aLCDMessage[7] =  LCD_Char_Map[charidx('T')];				//t
			aLCDMessage[6] =  LCD_Char_Map[charidx('E')];				//e
			aLCDMessage[5] =  LCD_Char_Map[charidx('N')];				//n
			aLCDMessage[4] =  LCD_Char_Map[charidx('A')];				//a
			aLCDMessage[3] =  LCD_Char_Map[charidx('N')];				//n
			aLCDMessage[2] =  LCD_Char_Map[charidx('T')];				//t

			sscanf((char *) nv_sys_conf.s.report_URL,"/%30[^/]/%30[^/]",lEnvironment, lCompany);  // Extract the enviroment and company from the report URL

			if (strcmp(lEnvironment,"dev") == 0)
			{
				aLCDMessage[12] = LCD_Char_Map[charidx('D')];			//d
				aLCDMessage[11] = LCD_Char_Map[charidx('E')];			//e
			}
			else if (strcmp(lEnvironment,"prod") == 0)
			{
				aLCDMessage[12] = LCD_Char_Map[charidx('P')];			//p
				aLCDMessage[11] = LCD_Char_Map[charidx('R')];			//r
			}
			else
			{
				aLCDMessage[12] = LCD_Char_Map[charidx('E')];			//e
                aLCDMessage[11] = LCD_Char_Map[charidx('R')];           //r
                aLCDMessage[10] = LCD_Char_Map[charidx('R')];           //r
			}

			for (i = 0; i < MaxNumberOfTenants; i++)
			{
				resp = strcmp(lCompany, TenantNames[i]);
				if(resp == 0)
				{
					//display row number if found
					aLCDMessage[10] = 0x00;
					aLCDMessage[9] =  LCD_Char_Map[i / 10];
					aLCDMessage[8] =  LCD_Char_Map[i % 10];
					break;
				}
			}

			if (i == MaxNumberOfTenants)
			{
				//display error if Tenet not found
				aLCDMessage[12] =  LCD_Char_Map[charidx('E')];			//e
				aLCDMessage[11] =  LCD_Char_Map[charidx('R')];			//r
                aLCDMessage[10] =  LCD_Char_Map[charidx('R')];          //r
			}
		break; */

		default:
		break;
	}

	gLCDMessageState++;
}

//This function is extracting firmware version details to be printed on display.
void DisplayFwVer(char * aCh)
{
	int i;
    char lLCDMessage[17];

	FwData.XX = 0;
	FwData.YY = 0;
	FwData.ZZ = 0;

	sscanf(aCh, "%d_%d_%d", &FwData.XX, &FwData.YY, &FwData.ZZ);

	lLCDMessage[0] = LCD_STX;
	lLCDMessage[1] = 'D';
	lLCDMessage[2] = 0x00;

	lLCDMessage[3] = 0x00;
	lLCDMessage[4] = LCD_Char_Map[FwData.ZZ % 10];
    lLCDMessage[5] = BIT3 | LCD_Char_Map[FwData.ZZ / 10];
	lLCDMessage[6] = LCD_Char_Map[FwData.YY % 10];
	lLCDMessage[7] = BIT3 | LCD_Char_Map[FwData.YY / 10];
	lLCDMessage[8] = LCD_Char_Map[FwData.XX % 10];
	lLCDMessage[9] = LCD_Char_Map[FwData.XX / 10];

	lLCDMessage[10] = LCD_Char_Map[charidx('R')]; //r
	lLCDMessage[11] = LCD_Char_Map[charidx('E')]; //E
	lLCDMessage[12] = LCD_Char_Map[charidx('V')]; //V
	lLCDMessage[13] = 0x00;
	lLCDMessage[14] = LCD_Char_Map[charidx('E')]; //E
	lLCDMessage[15] = 0x00;

	lLCDMessage[16] = LCD_ETX;

	for (i = 0; i < 17; i++)
	{
		while (UCA3STATW & UCBUSY);
		UCA3TXBUF = lLCDMessage[i];
	}
}

void SetLCDLed()
{

}



/* update messages to indicate various stages
UPDATE 1: Download FW to the Sram
UPDATE 2: CRC verified
UPDATE 3: FW transferred to SD card
*/
/*extern unsigned int gStartupCounter;
void Update_FW_msg(int aNum )
{
	int i=0;
	char lLCDMessage[17];

	lLCDMessage[0] = 0x02;
	lLCDMessage[1] = 'D';

	lLCDMessage[2] = 0x00;
	lLCDMessage[3] = 0x00;
	lLCDMessage[10] = 0x00;
	lLCDMessage[11] = 0x00;
	lLCDMessage[12] = 0x00;

	lLCDMessage[9] = LCD_Char_Map[16];										//V
	lLCDMessage[8] = LCD_Char_Map[22];										//P
	lLCDMessage[7] = LCD_Char_Map[20];										//d
	lLCDMessage[6] = LCD_Char_Map[15];										//A
	lLCDMessage[5] = LCD_Char_Map[18];										//t
	lLCDMessage[4] = LCD_Char_Map[13];										//E

	lLCDMessage[13] = 0x00;
	lLCDMessage[15] = 0xfa;

	lLCDMessage[14] = LCD_Char_Map[aNum];
	lLCDMessage[16] = 0x03;

	for(i = 0;i<17;i++)
	{
		while(UCA3STATW & UCBUSY);
		UCA3TXBUF = lLCDMessage[i];
	}

	gLCDCounter=3;
}
*/

void Disp_Lcd_ota(int num)
{
	int i;
	char lLCDMessage[17];

	lLCDMessage[0] = LCD_STX;
	lLCDMessage[1] = 'D';

	lLCDMessage[2] = 0x00;
	lLCDMessage[3] = 0x00;

	switch (num)
	{
		case 1:
            lLCDMessage[9] =  LCD_Char_Map[charidx('F')];       // F
            lLCDMessage[8] =  LCD_Char_Map[charidx('W')];       // W
            lLCDMessage[7] =  0x00;                             //
            lLCDMessage[6] =  LCD_Char_Map[charidx('D')];       // d
            lLCDMessage[5] =  LCD_Char_Map[charidx('L')];       // L
			lLCDMessage[4] =  LCD_Char_Map[charidx('D')];		// d

            lLCDMessage[14] = 0x00;                             //
            lLCDMessage[13] = LCD_Char_Map[charidx('P')];       // P
            lLCDMessage[12] = LCD_Char_Map[charidx('A')];       // A
            lLCDMessage[11] = LCD_Char_Map[charidx('S')];       // S
            lLCDMessage[10] = LCD_Char_Map[charidx('S')];       // S
 		break;

		case 2:
            lLCDMessage[9] = LCD_Char_Map[charidx('E')];        // E
            lLCDMessage[8] = 0x00;                              //
            lLCDMessage[7] = LCD_Char_Map[charidx('C')];        // C
            lLCDMessage[6] = LCD_Char_Map[charidx('R')];        // r
			lLCDMessage[5] = LCD_Char_Map[charidx('C')];		// C

            lLCDMessage[14] = 0x00;                             //
            lLCDMessage[13] = LCD_Char_Map[charidx('P')];       // P
            lLCDMessage[12] = LCD_Char_Map[charidx('A')];       // A
            lLCDMessage[11] = LCD_Char_Map[charidx('S')];       // S
            lLCDMessage[10] = LCD_Char_Map[charidx('S')];       // S
		break;

		case 3:
            lLCDMessage[9] = LCD_Char_Map[charidx('S')];        // S
            lLCDMessage[8] = LCD_Char_Map[charidx('D')];        // d
            lLCDMessage[7] = 0x00;                              //
            lLCDMessage[6] = LCD_Char_Map[charidx('T')];        // t
			lLCDMessage[5] = LCD_Char_Map[charidx('F')];		// F
            lLCDMessage[4] = LCD_Char_Map[charidx('R')];        // r

            lLCDMessage[14] = 0x00;                             //
            lLCDMessage[13] = LCD_Char_Map[charidx('P')];       // P
            lLCDMessage[12] = LCD_Char_Map[charidx('A')];       // A
            lLCDMessage[11] = LCD_Char_Map[charidx('S')];       // S
            lLCDMessage[10] = LCD_Char_Map[charidx('S')];       // S
		break;

		case 4:
            lLCDMessage[9] = LCD_Char_Map[charidx('S')];        // S
            lLCDMessage[8] = LCD_Char_Map[charidx('D')];        // d
            lLCDMessage[7] = 0x00;                              //
            lLCDMessage[6] = LCD_Char_Map[charidx('C')];        // C
            lLCDMessage[5] = LCD_Char_Map[charidx('R')];        // r
            lLCDMessage[4] = LCD_Char_Map[charidx('C')];        // C

            lLCDMessage[14] = 0x00;                 //
            lLCDMessage[13] = LCD_Char_Map[charidx('P')];       // P
            lLCDMessage[12] = LCD_Char_Map[charidx('A')];       // A
            lLCDMessage[11] = LCD_Char_Map[charidx('S')];       // S
            lLCDMessage[10] = LCD_Char_Map[charidx('S')];       // S
		break;

		case 5:
            lLCDMessage[9] = LCD_Char_Map[charidx('S')];        // S
            lLCDMessage[8] = LCD_Char_Map[charidx('D')];        // d
            lLCDMessage[7] = 0x00;                              //
            lLCDMessage[6] = LCD_Char_Map[charidx('C')];        // C
            lLCDMessage[5] = LCD_Char_Map[charidx('R')];        // r
            lLCDMessage[4] = LCD_Char_Map[charidx('C')];        // C

            lLCDMessage[14] = 0x00;                             //
            lLCDMessage[13] =  LCD_Char_Map[charidx('F')];      // F
            lLCDMessage[12] =  LCD_Char_Map[charidx('A')];      // A
            lLCDMessage[11] =  LCD_Char_Map[charidx('I')];      // I
            lLCDMessage[10] =  LCD_Char_Map[charidx('L')];      // L
		break;

		case 6:
            lLCDMessage[9] = LCD_Char_Map[charidx('E')];        // E
            lLCDMessage[8] = 0x00;                              //
            lLCDMessage[7] = LCD_Char_Map[charidx('C')];        // C
            lLCDMessage[6] = LCD_Char_Map[charidx('R')];        // r
            lLCDMessage[5] = LCD_Char_Map[charidx('C')];        // C

            lLCDMessage[14] = 0x00;                             //
            lLCDMessage[13] =  LCD_Char_Map[charidx('F')];      // F
            lLCDMessage[12] =  LCD_Char_Map[charidx('A')];      // A
            lLCDMessage[11] =  LCD_Char_Map[charidx('I')];      // I
            lLCDMessage[10] =  LCD_Char_Map[charidx('L')];      // L
			break;

		case 7:
			break;

		default:
		break;
	}

	lLCDMessage[15] = 0xfa;
	lLCDMessage[16] = 0x03;

	for (i = 0; i < 17; i++)
	{
		while (UCA3STATW & UCBUSY);
		UCA3TXBUF = lLCDMessage[i];
	}

	gLCDCounter = 3;
}


// Display the Message "Sd PASS" or "Sd FAIL" if SD card is detected upon boot.
void sd_msg_ok(int aNum)
{
	int i;
	char lLCDMessage[17];

    lLCDMessage[0] = LCD_STX;
    lLCDMessage[1] = 'D';

	switch(aNum)
	{
		case 1:

            lLCDMessage[9] =  LCD_Char_Map[charidx('S')];      //S
            lLCDMessage[8] =  LCD_Char_Map[charidx('D')];      //d
			lLCDMessage[7] = 0x00;
			lLCDMessage[6] = 0x00;
			lLCDMessage[5] = 0x00;
			lLCDMessage[4] = 0x00;

            lLCDMessage[14] = 0x00;                             //
            lLCDMessage[13] = LCD_Char_Map[charidx('P')];       // P
            lLCDMessage[12] = LCD_Char_Map[charidx('A')];       // A
            lLCDMessage[11] = LCD_Char_Map[charidx('S')];       // S
            lLCDMessage[10] = LCD_Char_Map[charidx('S')];       // S
		break;

		case 2:

            lLCDMessage[9] =  LCD_Char_Map[charidx('S')];      //S
            lLCDMessage[8] =  LCD_Char_Map[charidx('D')];      //d
			lLCDMessage[7] = 0x00;
			lLCDMessage[6] = 0x00;
			lLCDMessage[5] = 0x00;
			lLCDMessage[4] = 0x00;

            lLCDMessage[14] = 0x00;                             //
            lLCDMessage[13] =  LCD_Char_Map[charidx('F')];      // F
            lLCDMessage[12] =  LCD_Char_Map[charidx('A')];      // A
            lLCDMessage[11] =  LCD_Char_Map[charidx('I')];      // I
            lLCDMessage[10] =  LCD_Char_Map[charidx('L')];      // L

		break;

		default:
		break;

	}

    lLCDMessage[15] =  0xda;
    lLCDMessage[16] =  LCD_ETX;

	for(i = 0; i < 17; i++)
	{
		while(UCA3STATW & UCBUSY);
		UCA3TXBUF = lLCDMessage[i];
	}

    gLCDCounter = 3;

}

//Display the message as "FLIPPEd" on the LCD if the Unit is Upside down.
void Display_Flipped()
{
	int i;
	char lLCDMessage[17];


	lLCDMessage[0] = LCD_STX;
	lLCDMessage[1] = 'D';

    lLCDMessage[9] = LCD_Char_Map[charidx('F')];  //F
    lLCDMessage[8] = LCD_Char_Map[charidx('L')];  //L
    lLCDMessage[7] = LCD_Char_Map[charidx('I')];  //I
    lLCDMessage[6] = LCD_Char_Map[charidx('P')];  //P
    lLCDMessage[5] = LCD_Char_Map[charidx('P')];  //P
    lLCDMessage[4] = LCD_Char_Map[charidx('E')];  //E
    lLCDMessage[3] = LCD_Char_Map[charidx('D')];  //d on seg1

	lLCDMessage[10] = 0x00;
	lLCDMessage[11] = 0x00;

	lLCDMessage[12] = 0x00;
	lLCDMessage[13] = 0x00;

	lLCDMessage[14] = 0x00;
	lLCDMessage[15] = 0xda;

	lLCDMessage[16] = LCD_ETX;

	gLCDCounter=3;

	for(i = 0; i < 17; i++)
	{
		while(UCA3STATW & UCBUSY);
		UCA3TXBUF = lLCDMessage[i];
	}
}

unsigned char chartoLCD(char digit)
{
    unsigned char LCDdigit;

    if (digit >= '0' && digit <= '9')       // Numbers
    {
        LCDdigit = LCD_Char_Map[digit - '0'];
    }
    else if (digit >= 'A' && digit <= 'Z')  // Uppercase
    {
        LCDdigit = LCD_Char_Map[digit - 'A' + 10];
    }
    else if (digit >= 'a' && digit <= 'z')  // Lowercase
    {
        LCDdigit = LCD_Char_Map[digit - 'a' + 10];
    }
    else
    {
        LCDdigit = LCD_Char_Map[36];                      // Dash '-'
    }

return LCDdigit;
}

/*******************************************************************************
 *   send_LCD_Info(void)
 *
 *   TODO Description
 *
 *************************************************************************/
void send_LCD_Info(void)
{
    float lFTemp[3];
    uint32_t lDTemp;
    uint32_t lDTemp1;
    char * strbuf;
    int lMonitorBranch;
    int i;

    if (gLCDCounter-- > 0) return;
    if (gDisableLcdFlag == TRUE) return;

    for (i = 0; i < 256; i++) tx_msg[LCD_PORT].buf.uint8[i] = 0;

    strbuf = (char *)(tx_msg[LCD_PORT].buf.uint8);

    lFTemp[0] = current_rms_voltage(4);
    lFTemp[0] =  lFTemp[0]/100;
    //   lFTemp[0] = (float) (dummycnt++ % 128);

    if (gIR_LED_Output == BRANCH_MONITORING)
    {
        lMonitorBranch = BRANCH_PHASE_TOTAL;
        lFTemp[1] = current_rms_current(0);

    }
    else
    {
        lMonitorBranch = WHOLE_HOUSE_PHASE_TOTAL;
        lFTemp[1] = current_rms_current(2);
    }


    lFTemp[1] = current_rms_current(0);
    lFTemp[1] = lFTemp[1]/1000;

    if (current_consumed_active_energy(lMonitorBranch) == 0xFFFFFFFF)
    {
        lDTemp = 0.0;
    }
    else
    {
        lDTemp = current_consumed_active_energy(lMonitorBranch)/1000;
    }

    if (current_produced_active_energy(lMonitorBranch) == 0xFFFFFFFF)
    {
        lDTemp1 = 0.0;
    }
    else
    {
        lDTemp1 = current_produced_active_energy(lMonitorBranch)/1000;
    }

   strbuf[0] = 0x02;    // STX
   strbuf[1] = 'D';     // Display Message
   gLCD_Status = 0;
   gLCD_Status |= (comms_meter_status & 0xE6B3);

   // State machine for top line information and Mem 0
   switch (gDisplayL1State % 5)
   {
       case 0:
           Prod_KWHInfo(&strbuf[2],lDTemp1);
           strbuf[3] =BIT4|BIT6|BIT0|BIT5|BIT1  ;     // Display "P" Message
           break;

       case 1:
           Consumed_KWHInfo(&strbuf[2],lDTemp);
           strbuf[3] = SEG_A | SEG_D | SEG_E | SEG_F;     // Display "C" Message
           break;
       case 2:
           VoltInfo(&strbuf[2],lFTemp[0]);
           break;

       case 3:
           CurrInfo(&strbuf[2],lFTemp[1]);
           break;

       case 4:
       //case 5:
           LCD_Display_Text(&strbuf[2]);
           break;

   }

   //Lcd data function to send data on second line on LCD display
   LCDStatusSymbols(&strbuf[2],gLCD_Status,gDisplayL1State);

   gDisplayL1State++;
   strbuf[16] = 0x03;       // ETX

   //LCD v3.1 accepts packet size 17 characters
   send_message(LCD_PORT, 17);
}

/*******************************************************************************
 *   test_send_lcd()
 *
 *   TODO Description
 *
 *************************************************************************/
void test_send_lcd()
{
    char str[18]={0x02,'D',0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0XCC,0x04,0x03,0X00};
    char* strbuf = (char *)(tx_msg[LCD_PORT].buf.uint8);
    strcpy(strbuf,str);

    send_message(LCD_PORT,18);
}

/*******************************************************************************
 *   send_lcd_test()
 *
 *   TODO Description
 *
 *************************************************************************/
void send_lcd_test()
{
    int i;
    char * strbuf;

    strbuf = (char *)(tx_msg[LCD_PORT].buf.uint8);

    strbuf[0] = 0x02;    // STX
    strbuf[1]='D';
    strbuf[2]=BIT6;
    strbuf[6]=BIT0|BIT1|BIT5|BIT6|BIT7;
    strbuf[5]=BIT4|BIT1|BIT5|BIT2;
    strbuf[4]=BIT0|BIT4|BIT1|BIT2|BIT7|BIT6;
    strbuf[3]=BIT0|BIT4|BIT1|BIT2|BIT7|BIT6|BIT3;

    for(i=7; i<16; i++)
    {
      strbuf[i]=0x00;
    }
    strbuf[16]=0x03;       // ETX

    send_message(LCD_PORT,17);
}

