/*
 * emeter-display.h
 *
 *  Created on: Mar 3, 2017
 *      Author: Lawrence
 */

#ifndef EMETER_DISPLAY_H_
#define EMETER_DISPLAY_H_

#include "msp430f6779.h"

#define LCD_STX  0x02
#define LCD_ETX  0x03

#define SEG_A  BIT0
#define SEG_B  BIT1
#define SEG_C  BIT2
#define SEG_D  BIT7
#define SEG_E  BIT6
#define SEG_F  BIT4
#define SEG_G  BIT5
#define SEG_DP1    BIT3
#define SEG_DP2    BIT3
#define SEG_DP3    BIT3
#define SEG_DP4    BIT3
#define SEG_THUMB  BIT3
#define SEG_SKULL  BIT3
#define SEG_ARROW  BIT3

enum {
   NO_CEL,
   b4_EnA
};


void initLCD(void);

//void LCD_LED_Indicator(char* LCDMessage,unsigned int gLCDStatus);

//void DisplayFwVer(char* ch);
//void Update_FW_msg(int num);
void Disp_Lcd_ota(int num);

void sd_msg_ok(int num);
//void Display_Flipped();
//unsigned int HEX_Char_Map(unsigned int decimal);
void send_LCD_Info(void);
void EnableLCDPort(void);
void DisableLCDPort(void);
void test_send_lcd();
void send_lcd_test();

#endif /* EMETER_DISPLAY_H_ */
