/*
 * emeter-relay.h
 *
 *  Created on: Dec 15, 2016
 *      Author: John
 */

#ifndef EMETER_RELAY_H_
#define EMETER_RELAY_H_


#define RELAY_SHUT  1
#define RELAY_OPEN  2
#define RELAY_DENERGIZE  0

enum{
   RLY_CHARGING,
   RLY_READY,
   RLY_ACTIVATED
};

void init_relay(void);
void relay_tick(void);
void switch_relay(int aRequest);
int getRelayState(void);

#endif /* EMETER_RELAY_H_ */
