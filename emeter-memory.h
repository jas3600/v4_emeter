#if !defined(_METER_MEMORY_H_)
#define _METER_MEMORY_H_

#define LOBYTE 0
#define HIBYTE 1

int test_psram(void);
char read_psram_byte(uint32_t aAddress);
void write_psram_byte(uint32_t aAddress, char aData);
uint32_t read_psram_block(uint32_t aAddress, char * data, uint16_t len);
uint32_t write_psram_block(uint32_t aAddress, char * data, uint16_t len);

#endif
