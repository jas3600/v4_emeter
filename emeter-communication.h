//--------------------------------------------------------------------------
//
//  Software for MSP430 based e-meters.
//
//  THIS PROGRAM IS PROVIDED "AS IS". TI MAKES NO WARRANTIES OR
//  REPRESENTATIONS, EITHER EXPRESS, IMPLIED OR STATUTORY,
//  INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//  FOR A PARTICULAR PURPOSE, LACK OF VIRUSES, ACCURACY OR
//  COMPLETENESS OF RESPONSES, RESULTS AND LACK OF NEGLIGENCE.
//  TI DISCLAIMS ANY WARRANTY OF TITLE, QUIET ENJOYMENT, QUIET
//  POSSESSION, AND NON-INFRINGEMENT OF ANY THIRD PARTY
//  INTELLECTUAL PROPERTY RIGHTS WITH REGARD TO THE PROGRAM OR
//  YOUR USE OF THE PROGRAM.
//
//  IN NO EVENT SHALL TI BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
//  CONSEQUENTIAL OR INDIRECT DAMAGES, HOWEVER CAUSED, ON ANY
//  THEORY OF LIABILITY AND WHETHER OR NOT TI HAS BEEN ADVISED
//  OF THE POSSIBILITY OF SUCH DAMAGES, ARISING IN ANY WAY OUT
//  OF THIS AGREEMENT, THE PROGRAM, OR YOUR USE OF THE PROGRAM.
//  EXCLUDED DAMAGES INCLUDE, BUT ARE NOT LIMITED TO, COST OF
//  REMOVAL OR REINSTALLATION, COMPUTER TIME, LABOR COSTS, LOSS
//  OF GOODWILL, LOSS OF PROFITS, LOSS OF SAVINGS, OR LOSS OF
//  USE OR INTERRUPTION OF BUSINESS. IN NO EVENT WILL TI'S
//  AGGREGATE LIABILITY UNDER THIS AGREEMENT OR ARISING OUT OF
//  YOUR USE OF THE PROGRAM EXCEED FIVE HUNDRED DOLLARS
//  (U.S.$500).
//
//  Unless otherwise stated, the Program written and copyrighted
//  by Texas Instruments is distributed as "freeware".  You may,
//  only under TI's copyright in the Program, use and modify the
//  Program without any charge or restriction.  You may
//  distribute to third parties, provided that you transfer a
//  copy of this license to the third party and the third party
//  agrees to these terms by its first use of the Program. You
//  must reproduce the copyright notice and any other legend of
//  ownership on each copy or partial copy, of the Program.
//
//  You acknowledge and agree that the Program contains
//  copyrighted material, trade secrets and other TI proprietary
//  information and is protected by copyright laws,
//  international copyright treaties, and trade secret laws, as
//  well as other intellectual property laws.  To protect TI's
//  rights in the Program, you agree not to decompile, reverse
//  engineer, disassemble or otherwise translate any object code
//  versions of the Program to a human-readable form.  You agree
//  that in no event will you alter, remove or destroy any
//  copyright notice included in the Program.  TI reserves all
//  rights not specifically granted under this license. Except
//  as specifically provided herein, nothing in this agreement
//  shall be construed as conferring by implication, estoppel,
//  or otherwise, upon you, any license or other right under any
//  TI patents, copyrights or trade secrets.
//
//  You may not use the Program in non-TI devices.
//
//  File: emeter-communications.h
//
//  Steve Underwood <steve-underwood@ti.com>
//  Texas Instruments Hong Kong Ltd.
//
//  $Id: emeter-communication.h,v 1.2 2009/04/27 09:46:31 a0754793 Exp $
//
/*! \file emeter-structs.h */
//
//--------------------------------------------------------------------------
//
#ifndef __COMMS
#define __COMMS

#define MAX_IEC1107_MSG_BODY        66
#define MAX_MESSAGE_LENGTH          800

#define DLT645_PORT  2
#define LCD_PORT     3
#define MODEM_PORT   0
#define SCRATCH_PAD  1
#define DEBUG_PORT   2

#define DLT645_MSG_RX_START_BODY    10
#define DLT645_MSG_RX_START_BODY_W  5
#define DLT645_MSG_TX_START_BODY    14
#define DLT645_MSG_TX_START_BODY_W  7
#define MAX_DLT645_MSG_BODY         66

#define BUFFER_EMPTY                0
#define BUFFER_FILLING              1
#define MESSAGE_RECEIVED            2
#define BUFFER_OVERFLOW             4


// Comms States
typedef enum{
       W4_MDM_HTTP_MODE,
	   HTTP_MODE,
	   W4_CONFIG_UPDATE,
	   W4_STATUS_RESPONSE,
       W4_REPORT_RESPONSE,
       W4_CONFIG_RESPONSE,
       UPDATING_FW,
       W4_MDM_MODBUS_MODE,
       MODBUS_MODE
} commstate;

// Comm responses
typedef enum{
	NO_RESPONSE=0,
    REPORT_OK,
    CONFIG_OK,
	FW_UPDATE_FAIL,
	CONFIG_ERR,
	REPORT_ERR
} commresp;


typedef union
{
    uint8_t uint8[MAX_MESSAGE_LENGTH];
    uint16_t uint16[(MAX_MESSAGE_LENGTH)/2];
} serial_msg_t;

/* Incoming or outgoing serial message buffer */
typedef struct
{
    serial_msg_t buf;
    uint16_t ptr;
    uint16_t len;
    uint8_t inter_char_timeout;
} serial_msg_buf_t;


typedef struct
{
	uint8_t BootLsb;
	uint8_t BootMsb;
	uint8_t Dev_or_Prod[5];
	uint8_t Company_Name[10];
	uint8_t IP_Address[16];
} Seg_Struct_1880;

typedef enum {
	HTTPOnly=0,
	MODBUSEnabled
} CommsModeControl;

extern serial_msg_buf_t tx_msg[4];
extern serial_msg_buf_t rx_msg[4];
extern uint16_t gFWDownloadFlag;
extern unsigned char gNewFWVersion[10];
extern uint16_t gSentReadings;
extern char gSendConstFlag;
extern char gOTAFailed;

void comms_init(void);
void comms_tick(void);
uint32_t queue_readings(void);

void set_comms_mode(CommsModeControl aMode);
CommsModeControl get_comms_mode(void);

void Set_SD_Error();
int clear_1900();
void Clear_SD_Error();

void SET_CRC_Error();

void Clear_CRC_Error();

#endif
