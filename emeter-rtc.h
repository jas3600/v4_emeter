/*
 * emeter-rtc.h
 *
 *  Created on: Dec 12, 2016
 *      Author: John
 */


#ifndef EMETER_RTC_H_
#define EMETER_RTC_H_

extern uint16_t gCheckConfigFlag;
extern uint16_t gQueueDataFlag;
extern uint16_t gSendStatusFlag;
extern uint16_t gUploadReportFlag;
extern uint16_t gCheckConfigFlag;

void start_modem_timer(uint16_t * flag, uint16_t secs);
void start_comms_timer(uint16_t * flag, uint16_t secs);
void set_dummy_rtc(void);

#endif /* EMETER_RTC_H_ */
