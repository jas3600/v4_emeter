/*
 * emeter-relay.c
 *
 *  Created on: Dec 15, 2016
 *      Author: John
 */
#include <emeter-toolkit.h>
#include "emeter-relay.h"
#include "emeter-structs.h"
#include "emeter-communication.h"
#include "emeter-rtc.h"
#include "debug.h"

//#define DB_RLY
#ifdef DB_RLY
#define RLY_DBPRINT(...) PrintDebugMessage(__VA_ARGS__)
#else
#define RLY_DBPRINT(...)
#endif

#define RLY_PORT_OUT P1OUT
#define RLY_CUTOUT_OPEN  BIT6
#define RLY_CUTOUT_SHUT  BIT7

int gRelayState;
int gChargeTimer;
int gAction;

void init_relay(void)
{
    // Denergize the relay coils
    RLY_PORT_OUT &= ~(RLY_CUTOUT_OPEN);
    RLY_PORT_OUT &= ~(RLY_CUTOUT_SHUT);

    // Set the Capacitor Charge Timer
    gChargeTimer = 10;                    // assuming 2 second ticks this gives 20 seconds of charging time.
    gAction = RELAY_DENERGIZE;
    gRelayState = RLY_CHARGING;
}

void relay_tick(void)
{
	switch (gRelayState)
	{
		case RLY_CHARGING:
			if (gChargeTimer-- <= 0)
			{
				gRelayState = RLY_READY;
			}
		break;
		case RLY_READY:
			if (gAction == RELAY_SHUT)
			{
			    RLY_PORT_OUT |= RLY_CUTOUT_SHUT;
				gRelayState = RLY_ACTIVATED;
				comms_meter_status |= STATUS_DISCONNECT_CLOSED;              // Close relay

				if (get_comms_mode() == HTTPOnly) gSendStatusFlag = TRUE;
				RLY_DBPRINT("RLY SHUT");
			}
			else if (gAction == RELAY_OPEN)
			{
			    RLY_PORT_OUT |= RLY_CUTOUT_OPEN;
				gRelayState = RLY_ACTIVATED;
				comms_meter_status &= ~STATUS_DISCONNECT_CLOSED;             // Open relay

				if (get_comms_mode() == HTTPOnly) gSendStatusFlag = TRUE;
				RLY_DBPRINT("RLY OPEN");
			}
		break;
		case RLY_ACTIVATED:
		default:
            RLY_PORT_OUT &= ~(RLY_CUTOUT_OPEN);
            RLY_PORT_OUT &= ~(RLY_CUTOUT_SHUT);

            gAction = RELAY_DENERGIZE;
            gChargeTimer = 15;
            gRelayState = RLY_CHARGING;
		break;
	}
}

void switch_relay(int aRequest)
{
	gAction = aRequest;
}

int getRelayState(void)
{
   return  gRelayState;
}

