/*
 * BSLUpdate.c
 *
 *  Created on: Jun 5, 2018
 *      Author: Johnz PC
 */

#include <msp430.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <io.h>
#include <emeter-toolkit.h>

#include "BSLUpdate.h"
#include "emeter-3ph-neutral-6779(A).h"
#include "Debug.h"
#include "emeter-memory.h"

//#define DB_OTA
#ifdef DB_BSL_UPDATE
#define BSL_UPDATE_DBPRINT(...) PrintDebugMessage(__VA_ARGS__)
#else
#define BSL_UPDATE_DBPRINT(...)
#endif


void flash_bank_clr(int *ptr);
unsigned int FlashBSL(unsigned long int aFWLength);

void UpdateBSL(unsigned long int aFWLength)
{
    unsigned char buffer[512];
    unsigned int lBSLBootVector;

    // Copy the boot vector sector
    memcpy(buffer, (unsigned char *) 0xfe00, 512);

    // Disable all interrupts
    __disable_interrupt();

    // replace boot vector with the Emeter vector
    buffer[510] = *(unsigned char *) 0x1880;
    buffer[511] = *(unsigned char *) 0x1881;

    flash_clr((unsigned char *) 0xfe00);                            // clear the sector
    flash_memcpy((unsigned char *) 0xfe00, buffer, 512);   // replace the boot vector sector

    // write the BSL Areas
    lBSLBootVector = FlashBSL(aFWLength);

    // write BSL boot vector
    if (lBSLBootVector > 0)
    {
       buffer[510] = lBSLBootVector & 0xFF;
       buffer[511] = (lBSLBootVector >> 8) & 0xFF;

       flash_clr((unsigned char *) 0xfe00);                            // clear the sector
       flash_memcpy((unsigned char *) 0xfe00, buffer, 512);   // replace the boot vector sector

       // Reboot
       BSL_UPDATE_DBPRINT("Rebooting!!");
       PMMCTL0_H = PMMPW_H;
       PMMCTL0_L |= PMMSWPOR;

    }

}

/*********************************************************************************
 *   FlashBSL()
 *
 *********************************************************************************/
unsigned int FlashBSL(unsigned long int aFWLength)
{
    int i;
    unsigned long int j;
    char ch;
    unsigned char bsl_store_ffff;
    unsigned char bsl_store_fffe;
    int OtaState;
    int destaddptr;
    char spinstr[3] = {"0\r"};
    char ch1[2];
    char bytebuffer[3];
    char bytestate;
    unsigned char destinationaddressstring[7];
    unsigned long destinationaddressdecimal;
    unsigned char firmwaredecimal;
    unsigned int lTemp;
    unsigned long lBSLBootVector;
    char lDBMessage[60];
    int *ptr;
    char *ptr1;


    OtaState = FIND_FIRST_ADDRESS;
    lBSLBootVector = 0;

    for (j = 0; j < aFWLength; j++)
    {
        ch = read_psram_byte(j);

        switch (OtaState)
        {
        case FIND_FIRST_ADDRESS:                                                    // this state is for HEADER mode
            if (ch == '@')                                                          // if '@' is detected in the header mode change the state to adddress mode
            {
                destinationaddressstring[0] = '\0';
                destaddptr = 0;                       // reset the destination address string
                OtaState = READ_ADDRESS;
           }
            else if (ch == 'q')
            {
                //  OtaState=state4;
            }
        break;

        case READ_ADDRESS:                                                              // this state is for ADDRESS mode
            if((ch == '\r') || (ch == '\n'))  // if eol is detected change the state to firmware mode
            {
                sscanf(destinationaddressstring, "%lx", &destinationaddressdecimal);
                sprintf(lDBMessage, "Code Address Detected: %s", destinationaddressstring);
                BSL_UPDATE_DBPRINT(lDBMessage);
                destinationaddressstring[0] = '\0';                                     // reset the destination address string
                destaddptr = 0;

                //clear here after you sscanf the address
                ptr = (int*) destinationaddressdecimal;                                 //here ptr is given a value

                if ( destinationaddressdecimal == 0x1000)         // this is BSL Memory area
                {
                    ptr = (int*) 0x1000;
                    flash_clr(ptr);                      // clearing BSL 0
                    ptr = (int*) 0x1200;
                    flash_clr(ptr);                      // clearing BSL 1
                    ptr = (int*) 0x1400;
                    flash_clr(ptr);                      // clearing BSL 2
                    ptr = (int*) 0x1600;
                    flash_clr(ptr);                      // clearing BSL 3

                    bytestate = FIRSTHALF;
                    OtaState = READ_CODE_TEXT;
               }
                else if ( destinationaddressdecimal == 0x6C000)   // this is BSL firmware area in Bank3
                {
                    ptr = (int*) 0x6C000;                 // clearing Bank 3
                    flash_bank_clr(ptr);

                    bytestate = FIRSTHALF;
                    OtaState = READ_CODE_TEXT;

                }
                else if ( destinationaddressdecimal == 0x8bff0)   // this is BSL firmware version string
                {
                    // already cleared
                    bytestate = FIRSTHALF;
                    OtaState = READ_CODE_TEXT;

                }
                else if ( destinationaddressdecimal == 0xffc8)    // boot vector area
                {
                    bytestate = FIRSTHALF;
                    OtaState = READ_CODE_TEXT;
                }
                else
                {
                    BSL_UPDATE_DBPRINT("Skipping Emeter Area");
                    OtaState = SKIP_SECTION;
                }

            }
            else
            {
                // check if the character is 0 to F only
                if (((ch >= '0') && (ch <='9')) || ((ch >= 'A') && (ch <= 'F')) || ((ch >= 'a') && (ch <= 'f')))
                {
                    destinationaddressstring[destaddptr++] = ch;       // adds char to destinationaddressstring
                    destinationaddressstring[destaddptr] = '\0';
                }
            }
            break;

        case READ_CODE_TEXT:                                                                // this state is for FIRMWARE mode

            if ((ch == 'r') || (ch == '\\') || (ch == 'n') || (ch == ' ') || (ch == '\r') || (ch == '\n') )
            {
                                                                                    // Do Nothing
            }
            else if (ch == '@')
            {
                OtaState = READ_ADDRESS;                                            // go to the address mode
                destinationaddressstring[0] = '\0';                                 // reset the destination address string
                destaddptr = 0;
            }
            else if(ch == 'q')
            {
                BSL_UPDATE_DBPRINT("BSL Flashing Finished");
                return lBSLBootVector;
               // OtaState = OTA_FINISHED;                                            //go to the end of firmware mode
            }
            else
            {
                if (bytestate == FIRSTHALF)
                {
                    bytebuffer[0] = ch;                                             // putting the first ch byte into the bytebuffer[0]
                    bytestate = SECONDHALF;
                }
                else if (bytestate == SECONDHALF)
                {
                    bytebuffer[1] = ch;                                             //putting the second ch byte into  the bytebuffer[1]
                    bytebuffer[2] = '\0';                                           //converting the ch to string using '\0'
                    bytestate = FIRSTHALF;                        //sscanf(bytebuffer,"%x",&firmwaredecimal);                     // converting/extracting the bytebuffer hex to decimal using sscanf function
                    sscanf(bytebuffer,"%x", &firmwaredecimal);


                    if (destinationaddressdecimal == 0xfffe)                            // if destaddressdecimal is eqaul to 0xfffe (emeters project 0xfffe)
                    {
                        lBSLBootVector = firmwaredecimal;
                    }
                    else if (destinationaddressdecimal == 0xffff)                       // if destaddressdecimal is eqaul to 0xfffe (emeters project 0xffff)
                    {
                        lTemp = firmwaredecimal;
                        lBSLBootVector |= lTemp << 8;

                        sprintf(lDBMessage, "BSL Boot Vector Found: %lX", lBSLBootVector);
                        BSL_UPDATE_DBPRINT(lDBMessage);
                        // disregard the rest of the vector section
                        OtaState = SKIP_SECTION;
                    }
                    else
                    {
                        ptr1 = (char*) destinationaddressdecimal;                     // ptr1 is pointing to  the destrinationaddressdecimal
                        flash_write_int8(ptr1, firmwaredecimal);                      // write  firmware decimal on destinationaddressdecimal
                    }
                    destinationaddressdecimal++;                                            //incremenenting the destinationaddressdecimal
                }
            }
            break;

        case OTA_FINISHED:                                                              // this state is for detecting 'q' as the end of the firmware
        {
            return lBSLBootVector;
        }
        break;

        case SKIP_SECTION:
            if(ch == '@')
            {
                destinationaddressstring[0] = '\0';         // reset the destination address string
                destaddptr = 0;
                OtaState = READ_ADDRESS;
            }
            else if(ch == 'q')
            {
                return lBSLBootVector;
                //OtaState = OTA_FINISHED;
            }

            break;

        default:
            break;
        }
    }
}

/************************************************************************************
 *    flash_bank_clr(int *ptr)
 *
 *
 ************************************************************************************/
void flash_bank_clr(int *ptr)
{
    _DINT();
    FCTL3 = FWKEY;                      /* Lock = 0 */
    FCTL1 = FWKEY + MERAS;
    *((int *) ptr) = 0;                 /* Erase flash segment */
    _NOP();
    _NOP();
    while (FCTL3 & BUSY);               /* wait till done */
    _EINT();
}

