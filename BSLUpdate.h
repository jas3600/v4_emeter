/*
 * BSLUpdate.h
 *
 *  Created on: Jun 6, 2018
 *      Author: Johnz PC
 */

#ifndef BSLUPDATE_H_
#define BSLUPDATE_H_

#define FIND_FIRST_ADDRESS  0
#define READ_ADDRESS        1
#define READ_CODE_TEXT      2
#define OTA_FINISHED        3
#define SKIP_SECTION        4
#define CRC_FINISHED        5

#define FIRSTHALF 0
#define SECONDHALF 1

void UpdateBSL(unsigned long int aFWLength);

#endif /* BSLUPDATE_H_ */
