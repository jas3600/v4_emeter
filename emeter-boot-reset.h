/*
 * emeter-boot-reset.h
 *
 *  Created on: Sep 1, 2017
 *      Author: Lawrence
 */

#ifndef EMETER_BOOT_RESET_H_
#define EMETER_BOOT_RESET_H_

#define SD_CLK 8

unsigned int InitPMM(void);
void SystemResetQuery(void);
void PowerDownSave(void);
void InitLowPower(void);
void Save_AND_Reset(void);

#endif /* EMETER_BOOT_RESET_H_ */
