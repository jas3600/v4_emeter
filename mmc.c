/*******************************************************************************
 * mmc.c
 *
 * SD Card Interface
 *
 *
 ******************************************************************************/
#include "mmc.h"
#include <emeter-toolkit.h>
#include "emeter-structs.h"
#include "debug.h"
#include "stdio.h"


#ifdef DB_MMC
#define MMC_DBPRINT(...) PrintDebugMessage(__VA_ARGS__)
#else
#define MMC_DBPRINT(...)
#endif

#define DUMMY_CHAR 0xFF

struct {
	int sdtype;
} SDC;

unsigned char mmcGetResponse(void);
unsigned char mmcSendCmd (const unsigned char cmd, unsigned long data, unsigned char crc, int extra_byte_cnt, unsigned char * bytes_arr);
unsigned char mmcReadCSD(void);

// Buffer for mmc responses
int SD_Card_Installed = FALSE;

/*******************************************************************
 * int TestSDCard(void)
 *
 * Test the SD card
 *
 ******************************************************************/
int TestSDCard(void)
{
    int lRtv;

    lRtv = 0;

    return lRtv;                    //  0 -> good write

}

/*******************************************************************
 * sdcard_installed(void)
 *
 * Return state of the card
 ******************************************************************/
int sdcard_installed(void)
{
	return SD_Card_Installed;
}

/*******************************************************************
 * mmcInit(void)
 *
 * Initialize the MMC Card
 *
 ******************************************************************/
int mmcInit(void)
{
    int lRtv;

    lRtv = 0;

    return lRtv;                    //  0 -> good write

}


/*******************************************************************
 * send command to MMC
 *
 *
 ******************************************************************/
unsigned char mmcSendCmd(const unsigned char cmd, unsigned long data, unsigned char crc, int extra_byte_cnt, unsigned char * bytes_arr)
{
    int lRtv;

    lRtv = 0;

    return lRtv;                    //  0 -> good write

}  //mmcSendCmd


/***********************************************************************************************
 * Reads a single block
 * For our puposes we will set the block size to 512. For High Capacity cards (HC or XC), block addressing is used.
 * For Standard Capacity (SC) cards direct addressing is used.
 * This function should be called with the block addresses. It will be translated for SC cards.
 *
 ********************************************************************************************/
unsigned char mmcReadBlock(const unsigned long address, unsigned char *pBuffer)
{
    int lRtv;

    lRtv = 0;

    return lRtv;                    //  0 -> good write

}  // mmc_read_block


/***********************************************************************************************
 * Writes a single Block
 * For our puposes we will set the block size to 512. For High Capacity cards (HC or XC), block addressing is used.
 * For Standard Capacity (SC) cards direct addressing is used.
 * This function should be called with the block addresses. It will be translated for SC cards.
 *
 ****************************************************************************************/
int mmcWriteBlock (unsigned long address, unsigned char *pBuffer)
{
  int lRtv;

  lRtv = 0;

  return lRtv;                    //  0 -> good write

} // mmc_write_block

/***********************************************************************************************
 * Erases a Range of Blocks
 * For our puposes we will set the block size to 512. For High Capacity cards (HC or XC), block addressing is used.
 * For Standard Capacity (SC) cards direct addressing is used.
 * This function should be called with the block addresses. It will be translated for SC cards.
 *
 ****************************************************************************************/
int mmcEraseBlocks (unsigned long start_address, unsigned long end_address)
{
    int rtv;

    rtv = 0;

	return rtv;
} //mmcEraseBlocks






/***********************************************************************************************
 * Reads the Card Structure
 *
 ****************************************************************************************/
unsigned char mmcReadCSD(void)
{

	return 0;
} //mmcReadCSD


/***********************************************************************************************
 * Test the MMC
 *
 ****************************************************************************************/

int TestSD(unsigned long address, int numwrites)
{

	return 0;
}

