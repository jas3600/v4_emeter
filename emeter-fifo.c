/*
 * emeter-fifo.c
 *
 *  Created on: Dec 9, 2016
 *      Author: John
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <io.h>
#include <time.h>
#include <emeter-toolkit.h>
#include "emeter-structs.h"
#include "emeter-fifo.h"
#include "emeter-memory.h"
#include "OTAUpdate.h"
#include "mmc.h"
#include "Debug.h"

//#define DB_FIFO
#ifdef DB_FIFO
#define FIFO_DBPRINT(...) PrintDebugMessage(__VA_ARGS__)
#else
#define FIFO_DBPRINT(...)
#endif

//#pragma RETAIN(gLastBlock)
#pragma location = 0x6bc00                        // 2nd to last block in Bank 2
const char gLastBlock[512];

//#define TWENTY_BLOCKS  10240
//rtc_t old_rtc;

uint32_t gFIFOHead;
uint32_t gFIFOTail;
uint32_t gSDTail;

uint32_t gNumMessages;
int gTestCnt;
int gFIF0TimesValid;
int gClearMMCFlag;
int gMsgStatus;

void clear_msg_fifo(void);
void RewriteSD(void);

int FIFOTimeValid(void)
{
	return gFIF0TimesValid;
}

/**********************************************************************
  clear_msg_fifo()
  Initializes the message fifo

************************************************************************/
void clear_msg_fifo(void)
{
	// clear the FIFO
	gFIFOHead = 0;
	gFIFOTail = 0;
	gNumMessages = 0;
	gSDTail = 0;
	gTestCnt = 0;
	gFIF0TimesValid = TRUE;
}

/**********************************************************************
  init_msg_fifo()

  Initializes the message fifo

************************************************************************/
void init_msg_fifo(void)
{
	FIFO_DBPRINT("Init FIFO:");
	gClearMMCFlag = FALSE;
	// clear the FIFO
	clear_msg_fifo();

	// read SD card to determine if outstanding messages are waiting
	// Restore the FIFO from the SD CARD
	restore_msg_fifo2();
}

/**********************************************************************
 push_msg_fifo

  Adds message to the FIFO
    msg = char array with message
    aType = Message Type

    returns: number of messages in fifo
************************************************************************/

uint32_t push_msg_fifo(char * aMsg, uint8_t aType, uint8_t aLen)
{
	fifomsg_t  lfifomsg;

	uint32_t lRtv;

	lRtv = 0;


	if (gFIFOTail <= 200000)  // FIFO not Full
	{
		if (rtc.century < 20) gFIF0TimesValid = FALSE;

		lfifomsg.msgstate = MSG_STS_NEW;
		lfifomsg.sendattemps = 0;
		lfifomsg.msgtype = aType;
		lfifomsg.msglen = aLen;

		gFIFOTail += write_psram_block(gFIFOTail , (char *) &lfifomsg, sizeof(fifomsg_t));
		gFIFOTail += write_psram_block(gFIFOTail , aMsg, aLen);

		gNumMessages++;

		if (gFIFOTail >= gSDTail + 512)  // 1 Blocks
		{

			if (sdcard_installed() == TRUE)
			{
				memset(Sd_buffer, 0, 514);

				if ((gFIFOTail >> 9) == (gSDTail >> 9)) // in the same block
				{
					read_psram_block((gSDTail >> 9) * 512, (char *) Sd_buffer, gFIFOTail - ((gSDTail >> 9) * 512));  // fill the buffer with the remaining full blocks of data
					gMsgStatus = mmcWriteBlock (SD_CARD_START_BLOCK + (gSDTail >> 9), Sd_buffer);   // write the data to the SD card

					gSDTail = gFIFOTail;

				}
				else  // In Multiple Blocks
				{
					// Write the first block
					read_psram_block((gSDTail >> 9) * 512, (char *) Sd_buffer, 512);  // fill the buffer with the remaining full blocks of data
					gMsgStatus = mmcWriteBlock (SD_CARD_START_BLOCK + (gSDTail >> 9), Sd_buffer);   // write the data to the SD card

					gSDTail += 512;

					memset(Sd_buffer, 0, 514);

					// Write the second block
					read_psram_block((gSDTail >> 9) * 512, (char *) Sd_buffer, gFIFOTail - ((gSDTail >> 9) * 512));  // fill the buffer with the remaining full blocks of data
					gMsgStatus = mmcWriteBlock (SD_CARD_START_BLOCK + (gSDTail >> 9), Sd_buffer);   // write the data to the SD card

					gSDTail = gFIFOTail;

					memset(Sd_buffer, 0, 514);
					gMsgStatus = mmcWriteBlock (SD_CARD_START_BLOCK + (gSDTail >> 9) + 1, Sd_buffer); // Erase SD card next+1 block

				}
			}
		}

		sprintf(gDbgMsg,"PUSH gFIFOHead %ld gFIFOTail %ld gSDTail %ld",gFIFOHead,gFIFOTail,gSDTail);
		FIFO_DBPRINT(gDbgMsg);

		lRtv = gNumMessages;
	}

	return lRtv;
}

/**********************************************************************
 peek_msg_fifo

  Looks at the X message in the fifo
    aMsg = char array passed in to fill
    aFifomsg = Message Type
    aDepth = Depth of message to peak
    aSramLoc

    returns: length of the message in bytes
************************************************************************/
uint16_t peek_msg_fifo(char *aMsg, fifomsg_t * aFifomsg, uint16_t aDepth, uint32_t * aSramLoc)
{
	uint16_t lRtnVal;
	uint32_t lFIFOptr;
	uint16_t i;

	lRtnVal = 0;  // return 0 length if no message

	lFIFOptr = gFIFOHead;

	if ((gNumMessages > 0) && (aDepth <= gNumMessages))
	{
		for (i = 0; i < aDepth-1; i++)
		{
			lFIFOptr += read_psram_block(lFIFOptr, (char *) aFifomsg, sizeof(fifomsg_t));
			lFIFOptr += aFifomsg->msglen;
		}

		lFIFOptr += read_psram_block(lFIFOptr, (char *) aFifomsg, sizeof(fifomsg_t));
		lRtnVal = aFifomsg->msglen;

		read_psram_block(lFIFOptr, aMsg, lRtnVal);

		*aSramLoc = lFIFOptr;
	}

	return lRtnVal;
}

/**********************************************************************
 pop_msg_fifo

  removes a message from the fifo

    returns: number of remaining messages
************************************************************************/
uint32_t pop_msg_fifo(void)
{
	fifomsg_t  lfifomsg;
	//unsigned char lSDblkbuff[514];
	uint16_t lCurrBlk;
    unsigned int i;

	if (gFIFOTail != gFIFOHead)
	{
		gFIFOHead += read_psram_block(gFIFOHead, (char *) &lfifomsg, sizeof(fifomsg_t));
		gFIFOHead += lfifomsg.msglen;

		gNumMessages--;
	}

	sprintf(gDbgMsg,"POP gFIFOHead %ld gFIFOTail %ld gSDTail %ld", gFIFOHead, gFIFOTail, gSDTail);
	FIFO_DBPRINT(gDbgMsg);

	if (gFIFOTail == gFIFOHead) // if psram data is fully sent to server.
	{
		gFIFOHead = 0;
		gFIFOTail = 0;
		gNumMessages = 0;

		if ((sdcard_installed() == TRUE) && (gSDTail > 0) )  // write zero block to start
		{
			gSDTail = 0;
			memset(Sd_buffer, 0, 514);
			lCurrBlk = SD_CARD_START_BLOCK;

			mmcReadBlock(lCurrBlk, Sd_buffer);  // Read first block

			if (ZeroBlock(Sd_buffer) != TRUE)
			{
				ClearMMCBlocks();
			}
			else
			{
				FIFO_DBPRINT("SD card FIFO block 2048 empty");
			}
		}

		// if any data in the flash buffer.
		for (i = 0; i < 512; i++)
		{
		    if (gLastBlock[i] != 0xFF)
		    {
		        flash_clr((int *) gLastBlock);
		        _EINT();
		        i = 512;
		    }
		}
	}

  return gNumMessages;
}


/**********************************************************************
 pop_msg_fifo

  removes message from the fifo tail

    returns: number of remaining messages
************************************************************************/
uint32_t pop_msg_fifo_Tail(void)
{
	if (gFIFOTail != gFIFOHead)
	{

		gFIFOTail -= (sizeof(fifomsg_t) + sizeof(readings_t));

		gNumMessages--;
	}

	sprintf(gDbgMsg,"POP gFIFOHead %ld gFIFOTail %ld gSDTail %ld", gFIFOHead, gFIFOTail, gSDTail);
	FIFO_DBPRINT(gDbgMsg);

	if (gFIFOTail == gFIFOHead) // if psram data is fully sent to server.
	{
		gFIFOHead = 0;
		gFIFOTail = 0;
		gNumMessages = 0;
	}

  return gNumMessages;
}

/**************************************************************************
 * int getFIFODepth(void)
 *
 * REturns the number of messages in the fifo
 *
 ***********************************************************************/
int getFIFODepth(void)
{
  return gNumMessages;
}



/**********************************************************************
   save_msg_fifo2()

   saves last block of fifo to flash

************************************************************************/
void save_msg_fifo2(void)
{
	//unsigned char lSDblkbuff[514];

	   if (gFIFOTail > gSDTail)
	   {
	       memset(Sd_buffer, 0, 514);

		   read_psram_block((gSDTail >> 9) * 512, (char *) Sd_buffer, gFIFOTail - ((gSDTail >> 9) * 512));  // fill the buffer with the remaining full blocks of data
		   flash_memcpy(gLastBlock, Sd_buffer, 512);
	   }

}

/**********************************************************************
   restore_msg_fifo2()

   restores fifo from SIM card

************************************************************************/
void restore_msg_fifo2(void)
{
	fifomsg_t  lfifomsg;
//	unsigned char lSDblkbuff[514];
	uint16_t lCurrBlk;
	unsigned int i;
	readings_t lReading;

//	uint32_t lSramLoc;
//	readings_t lReading;
//	struct operation_mem_s lOpVars;
//	extern const struct operation_mem_s nv_op_vars ;

    if (sdcard_installed() == TRUE)
    {
    	FIFO_DBPRINT("Restore FIFO:");

		gNumMessages = 0;
		gFIFOTail = 0;

	    // set the first record in psram to 0
		lfifomsg.msgstate = 0;
	    lfifomsg.sendattemps = 0;
	    lfifomsg.msgtype = 0;
	    lfifomsg.msglen = 0;


		memset(Sd_buffer, 0, 514);
		write_psram_block(gFIFOTail , (char *) Sd_buffer,512);

		lCurrBlk = SD_CARD_START_BLOCK;
		if (mmcReadBlock(lCurrBlk, Sd_buffer) == TRUE)  // Read first block
		{

			if((Sd_buffer[0] == 0x01) && (Sd_buffer[1] == 0x00) && (Sd_buffer[2] == 0x01))
			{
				while (ZeroBlock(Sd_buffer) != TRUE)     // Keep reading blocks until zero block encountered
				{
					sprintf(gDbgMsg,"RESTF Found Block: %d", lCurrBlk);
					FIFO_DBPRINT(gDbgMsg);
					gFIFOTail += write_psram_block(gFIFOTail , (char *) Sd_buffer, 512);

					lCurrBlk++;
					memset(Sd_buffer, 0, 514);
					write_psram_block(gFIFOTail , (char *) Sd_buffer, 512); // fill the next PSRAM block

					if (mmcReadBlock(lCurrBlk, Sd_buffer) == FALSE)
					{
						FIFO_DBPRINT("Read Block Fail");
						//for (i = 0; i < 514; i++) Sd_buffer[i] = 0;     // fake a zero block
						memset(Sd_buffer, 0, 514);
						clear_msg_fifo();    // Fifo corrupt, dump it.
					}
				}

			}
			else if((Sd_buffer[0] == 0x00) && (Sd_buffer[1] == 0x00) && (Sd_buffer[2] == 0x00))
			{
				FIFO_DBPRINT("SD 1st block data empty");
			}
			else
			{
				FIFO_DBPRINT("SD 1st block data bad");
			}

			// Check for data in the Flash buffer
	        for (i = 0; i < 512; i++)
	        {
	            if (gLastBlock[i] != 0xFF)
	            {
	                write_psram_block(gFIFOTail , (char *) gLastBlock, 512);
	                FIFO_DBPRINT("Data Found in Flash");
	                i = 512;
	            }
	        }

			// Set the end of the Fifo
			gFIFOTail = 0;
			lfifomsg.msgstate = 1;

			while (lfifomsg.msgstate == 0x01) //makes sure no previous data from psram will be read
			{
				read_psram_block(gFIFOTail, (char *) &lfifomsg, sizeof(fifomsg_t));

				if (lfifomsg.msglen != sizeof(lReading)) break; // break out of restoring fifo if the lReadings size missmatch in SD and Firmware.

				read_psram_block(gFIFOTail + sizeof(fifomsg_t), (char *) &lReading, lfifomsg.msglen);

				if (lfifomsg.msglen > 0)
				{
					if ( IsReadingValid(&lReading) == TRUE)
					{
						gFIFOTail += sizeof(fifomsg_t) + lfifomsg.msglen;
						gNumMessages++; // count messages
					}
					else
					{
						lfifomsg.msgstate = 0;     // stop the loop
					}
				}
			}

		   //	if (gNumMessages > 0) gNumMessages--;     // subtract the zero message

			sprintf(gDbgMsg,"RESTF Msgs found: %lu", gNumMessages);
			FIFO_DBPRINT(gDbgMsg);

//			gFIFOTail -= ( sizeof(fifomsg_t) + lfifomsg.msglen);  // roll back the last structure length

			gSDTail = gFIFOTail & 0xFFFFFE00;                  //  SDTail = FIFOTail

		   if (FIFOTimeStampsValid() == FALSE)
		   {
			   // Bad time
			   ClearMMCBlocks();
			   clear_msg_fifo();
			   // These reading are lost
			   FIFO_DBPRINT("FIFO Time Bad, Readings lost:");
		   }
		}
		else
		{
			FIFO_DBPRINT("Restore FIFO not complete:");
		}

		sprintf(gDbgMsg,"RESTORE gFIFOHead %ld gFIFOTail %ld gSDTail %ld",gFIFOHead,gFIFOTail,gSDTail);
		FIFO_DBPRINT(gDbgMsg);
    }
    else
    {
    	FIFO_DBPRINT("FIFO SD Read Fail:");
    }
}

/**********************************************************************
   IsReadingValid(int gNumMessages)

   Checks if fifo data is valid

************************************************************************/
int IsReadingValid(readings_t * aReading)
{
	int rtv;

	rtv = TRUE;

	if(aReading->temperature == 0)
	{
		rtv = FALSE; // bad reading
	}

	if(aReading->rms_voltage == 0 || aReading->rms_voltage == 0xffffffff)
	{
		rtv = FALSE;		// bad reading
	}

	if(aReading->frequency == 0 || aReading->frequency == 0xffffffff)
	{
		rtv = FALSE;      //bad reading
	}

	if(aReading->century < 20)
	{
		rtv = FALSE; //bad date
	}

	if (rtv == FALSE) FIFO_DBPRINT("FIFO Bad Reading, Throwing out:");

	return rtv;
}

int FIFOTimeStampsValid(void)
{
	fifomsg_t  lfifomsg;
	uint32_t lSramLoc;
	readings_t lReading;
	int lRtv;

	lRtv = FALSE;

	if (gNumMessages > 0)
	{
	   // Get the first Reading time value
	   peek_msg_fifo((char *) &lReading, &lfifomsg, 1, &lSramLoc);

	   if (lReading.century > 19)
	   {
		   lRtv = TRUE;
	   }
	}
	else
	{
		lRtv = TRUE;
	}

return lRtv;
}

int ZeroBlock(char * aBlock)
{
	int rtv;
	int i;

	rtv = FALSE;

	i = 0;

	while ((i < 512) && ((aBlock[i] == 0) || (aBlock[i] == 0xFF))) i++;

	if (i >= 512) rtv = TRUE;

	return rtv;
}

void setClearMMC(void)
{
    gClearMMCFlag = TRUE;
}

int clearMMCQuery(void)
{
    return gClearMMCFlag;
}
void ClearMMCBlocks(void)
{
    unsigned int i;

//	unsigned char lSDblkbuff[514];
	gMsgStatus = mmcEraseBlocks (SD_CARD_START_BLOCK, SD_CARD_START_BLOCK + SD_BLOCKS_FOR_FIFO);

	if (gMsgStatus != 0)
	{
		mmcInit();
		gMsgStatus = mmcEraseBlocks (SD_CARD_START_BLOCK, SD_CARD_START_BLOCK + SD_BLOCKS_FOR_FIFO);
	}

	if (gMsgStatus == 0)
	{
		sprintf(gDbgMsg,"CLEARSD Erased Blocks: %x", gMsgStatus);
		FIFO_DBPRINT(gDbgMsg);

		memset(Sd_buffer, 0, 514);
		gMsgStatus = mmcWriteBlock (SD_CARD_START_BLOCK, Sd_buffer);
		gMsgStatus = mmcWriteBlock (SD_CARD_START_BLOCK+1, Sd_buffer);

		sprintf(gDbgMsg,"CLEARSD Write Block Zero: %x", gMsgStatus);
		FIFO_DBPRINT(gDbgMsg);
	}
	else
	{
		sprintf(gDbgMsg,"CLEARSD Erase Blocks Failed: %x", gMsgStatus);
		FIFO_DBPRINT(gDbgMsg);
	}

    // if any data in the flash buffer.
    for (i = 0; i < 512; i++)
    {
        if (gLastBlock[i] != 0xFF)
        {
            flash_clr((int *) gLastBlock);
            i = 512;
        }
    }

    gClearMMCFlag = FALSE;
}

void EraseMMCBlocks(void)
{

	if(sdcard_installed() == 1)
	{
		gMsgStatus = mmcEraseBlocks (256, 256 + 10);

		if (gMsgStatus != 0)
		{
			mmcInit();
			gMsgStatus = mmcEraseBlocks (256, 256 + 10);
		}

		sprintf(gDbgMsg,"CLEARSD Erase Blocks status: %x", gMsgStatus);
		FIFO_DBPRINT(gDbgMsg);
	}

}


void FixFIFOTime(rtc_t old_rtc)
{
   time_t old_time;
   time_t now_time;
   time_t diff_time;
   uint32_t lSramLoc;
   readings_t lReading;
   fifomsg_t lfifomsg;
   int i;
   int lRecordUpdated;

   struct tm tm_buf;
   struct tm * new_tm;

   FIFO_DBPRINT("Fixing FIFO Times");

   lRecordUpdated = FALSE;

   if (gNumMessages > 0)
   {
	   // determine time delta
	   tm_buf.tm_year = ((int) (old_rtc.century - 19) * 100) + (int) old_rtc.year;
	   tm_buf.tm_mon = old_rtc.month - 1;
	   tm_buf.tm_mday = old_rtc.day;
	   tm_buf.tm_hour = old_rtc.hour;
	   tm_buf.tm_min = old_rtc.minute;
	   tm_buf.tm_sec = old_rtc.second;
	   tm_buf.tm_isdst = -1;

	   old_time = mktime(&tm_buf);

	   tm_buf.tm_year = ((int) (rtc.century - 19) * 100) + (int) rtc.year;
	   tm_buf.tm_mon = rtc.month - 1;
	   tm_buf.tm_mday = rtc.day;
	   tm_buf.tm_hour = rtc.hour;
	   tm_buf.tm_min = rtc.minute;
	   tm_buf.tm_sec = rtc.second;
	   tm_buf.tm_isdst = -1;

	   now_time = mktime(&tm_buf);

	   diff_time = now_time - old_time;

       // update the data records
	   for (i = 0; i< gNumMessages; i++)
	   {
		   peek_msg_fifo((char *) &lReading, &lfifomsg, i+1, &lSramLoc);

		   if (lReading.century < 20)
		   {
				tm_buf.tm_year = ((int) (lReading.century - 19) * 100) + (int) lReading.year;
				tm_buf.tm_mon = lReading.month - 1;
				tm_buf.tm_mday = lReading.day;
				tm_buf.tm_hour = lReading.hour;
				tm_buf.tm_min = lReading.minute;
				tm_buf.tm_sec = lReading.second;
				tm_buf.tm_isdst = -1;

				old_time = mktime(&tm_buf);

				now_time = old_time + diff_time;

				new_tm = localtime(&now_time);

				lReading.century = (new_tm->tm_year/100) + 19;      // calculate century
				lReading.year = new_tm->tm_year % 100;              // calculate year
				lReading.month = new_tm->tm_mon + 1;
				lReading.day = new_tm->tm_mday;
				lReading.hour = new_tm->tm_hour;
				lReading.minute = new_tm->tm_min;
				lReading.second = new_tm->tm_sec;

				write_psram_block(lSramLoc , (char *) &lReading, sizeof(readings_t));

				lRecordUpdated = TRUE;
		   }

	   }

       if (lRecordUpdated == TRUE) RewriteSD();
   }

   gFIF0TimesValid = TRUE;
}

void RewriteSD(void)
{
	unsigned int i;

	FIFO_DBPRINT("Rewriting SD FIFO");

	//Set_Analog_Disable_Flag();


	for (i = 0; i < ((gFIFOTail >> 9) + 1); i++)
	{
	  memset(Sd_buffer, 0, 514);
	  read_psram_block(i << 9, (char *) Sd_buffer, 512);

	  gMsgStatus = mmcWriteBlock (SD_CARD_START_BLOCK + i, Sd_buffer);  // Write the block
	}

	memset(Sd_buffer, 0, 514);
	gMsgStatus = mmcWriteBlock (SD_CARD_START_BLOCK + i, Sd_buffer);    // Zero SD card next+1 block

	//enable_analog();
}

void Unprovision(void)
{
	char gDbgMsg[100];
//	unsigned char lSDblkbuff[514];
	uint16_t lCurrBlk;


	memset(Sd_buffer, 0, 514);
	lCurrBlk = SD_CARD_START_BLOCK;
	//disable_analog();
	//Set_Analog_Disable_Flag();

	if (mmcReadBlock(lCurrBlk, Sd_buffer) == TRUE)  // Read first block
	{
		if (ZeroBlock(Sd_buffer) != TRUE)
		{
			ClearMMCBlocks();
			FIFO_DBPRINT("Erase SD");
		}
		else
		{
			FIFO_DBPRINT("SD FIFO block 2048 empty");
		}
	}
	else
	{
		FIFO_DBPRINT("SD FIFO Unprov Read Fail");
	}

	if (clear_1900() != 0x01)
	{
		FIFO_DBPRINT("Clear Energy when unit is Unprov Done");
	}
	else
	{
		FIFO_DBPRINT("Energy is already empty");
	}

	if (gFIFOTail != 0 || gFIFOHead != 0)
	{
		sprintf(gDbgMsg,"RESTF Messages found: %lu", gNumMessages);
		FIFO_DBPRINT(gDbgMsg);
		gFIFOTail=0;
		gFIFOHead=0;
		FIFO_DBPRINT("FIFO msgs made to zero when unit is unprov");
	}
	else
	{
		FIFO_DBPRINT("FIFO is already empty");
	}

//	enable_analog();

}



#ifdef TEST_DATA_RETENTION

/*
 * this function will create 200+ samples FIFO to the PSRAM and call savefifo function
 * the save fifo moves the PSRAM FIFO to SD card FIFO 2048 block onwards
 * FOR TESTING PURPOSE ONLY
 */
int TestDataRetention(void)
{
	int lRtv;
	  uint32_t lRtnVal;
	  readings_t lData;
	  int i;

		lRtv = 0;

    	gTestCnt++;

	  // fill the fifo

    	 kick_watchdog();

    	if ((gTestCnt >= 2) && (gTestCnt <= 20))
    	{
    		sprintf(gDbgMsg,"Pushing Messages: %d\r\n", gTestCnt);
    		FIFO_DBPRINT(gDbgMsg);

    		for (i = 0; i < 10; i++)
    	    {
			  comms_meter_status = meter_status | (comms_meter_status & 0xE01B);
			  lData.status = comms_meter_status;
			  lData.century = rtc.century;
			  lData.year = rtc.year;
			  lData.month = rtc.month;
			  lData.day = rtc.day;
			  lData.hour = rtc.hour;
			  lData.minute = rtc.minute + (10 * gTestCnt + i);
			  lData.second = rtc.second;
			  lData.power_factor = 9999;
			  lData.produced_energy = 1000ul * (10ul * gTestCnt + i);
			  lData.consumed_energy = 2000ul * (10ul * gTestCnt + i);
			  lData.frequency = 6000;
			  lData.active_power = 1125;
			  lData.reactive_power = 345;
			  lData.temperature = 320;
			  lData.rms_voltage = 24000;
			  lData.rms_current = 4100;

			 lRtnVal = push_msg_fifo((char *) &lData, MSG_READINGS, sizeof(readings_t));
		 }
      }


      if (gTestCnt == 20)
      {
  		FIFO_DBPRINT("Saving Messages:\r\n");
       	 save_msg_fifo2();
      }


return lRtv;
}
#endif

