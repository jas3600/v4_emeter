#if !defined(_EMETER_HTTP_H_)
#define _EMETER_HTTP_H_

#define MAX_READINGS_PER_REPORT 4

#define FW_STILL_DOWNLOADING 0
#define FW_DOWNLOAD_COMPLETE 1
#define FW_DOWNLOAD_ERROR    2

#define HTTPSampleRate 15

//void clear_http_buf();
//void http_rx_byte(int port, uint8_t ch);
//int parse_config(char * aRecvbuf);
int parse_http_resp(char * aRecvbuf);

void Get_Config();
void Get_Firmware();
int Send_Status();
int Send_Report();
int Parse_V3_Config(char* GetConfig_RxBuff);
int Parse_Date(char * aRecvbuf);
void init_Dechunker(void);
int proc_FW_Chunks(char ch);
int ConfigCRC( char *GetConfig_RxBuff);
uint16_t GetModbusSampleRate();
#endif
