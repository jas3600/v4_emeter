/*
 * Debug.h
 *
 *  Created on: Dec 8, 2016
 *      Author: John
 */

#ifndef DEBUG_H_
#define DEBUG_H_
#include "stdio.h"
#include <stdbool.h>
#define DB_OTA

#define DB_FIFO

#define DB_COMMS

#define DB_HTTP

#define DB_SETUP

#define DB_MAIN

#define DB_MMC

#define DB_RLY

#define DB_BOOT

#define DB_MDM

#define DB_SPI

#define DB_Modbus

#define DB_ModbusRTU

#define DB_BSL_UPDATE

void EnableDebug(void);
void DisableDebug(void);
void Init_Debug_UART(unsigned int aClk, unsigned long int aBAUD);
void ClearDebugBuffer(void);
void PrintDebugMessage(char* ptr);
void ClearDebugBuffer(void);
void EnableDebug(void);
void EnableLCDPort(void);
void DisableLCDPort(void);

extern char gDbgMsg[];
extern char gBootMsg[];

#endif /* DEBUG_H_ */
