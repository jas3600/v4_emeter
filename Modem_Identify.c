/*
 * Modem_Identify.c
 *
 *  Created on: Nov 8, 2016
 *      Author: Afreen
 */
#include "Modem_Identify.h"
#include "Telit.h"
#include "msp430.h"

char Identify_Modem()
{
	return Telit;
}



void Initialize_Modem(char ch )
{

	if (ch==Telit)
	{
		Global_init_modem=&Telit_init_modem;
		Global_modem_tick=&Telit_modem_tick;
		Global_proc_modem_char=&proc_modem_char;

//		Global_wait_connected=&wait_connected;
//		Global_gRespReceived=&gRespReceived;
//		Global_gModemState=&gModemState;
//		Global_rx_msg_count=&rx_msg_count;
//		Global_rx_msg_timer_begin=&rx_msg_timer_begin;
	}
	UCA0IE |= UCRXIE;
}


