/* 
 * FreeModbus Libary: A portable Modbus implementation for Modbus ASCII/RTU.
 * Copyright (c) 2006 Christian Walter <wolti@sil.at>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * File: $Id: mbrtu.c,v 1.18 2007/09/12 10:15:56 wolti Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include "stdlib.h"
#include "string.h"
#include "stdio.h"

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbrtu.h"
#include "mbframe.h"

#include "mbcrc.h"
#include "mbport.h"


/* ----------------------- Static variables ---------------------------------*/
static volatile eMBSndState eXpicoSndState;
static volatile eMBRcvState eXpicoRcvState;

volatile UCHAR  ucXpicoRTUBuf[MB_SER_PDU_SIZE_MAX];

static volatile UCHAR *pucXpicoSndBufferCur;
static volatile USHORT usXpicoSndBufferCount;

volatile USHORT usXpicoRcvBufferPos;

/* ----------------------- Start implementation -----------------------------*/
eMBErrorCode
eMBXpicoRTUInit( UCHAR ucSlaveAddress, UCHAR ucPort, ULONG ulBaudRate, eMBParity eParity )
{
    eMBErrorCode    eStatus = MB_ENOERR;
    ULONG           usTimerT35_50us;

  //  ( void )ucSlaveAddress;
    ENTER_CRITICAL_SECTION(  );

    /* Modbus RTU uses 8 Databits. */
    if( xMBXpicoPortSerialInit( ucPort, ulBaudRate, 8, eParity ) != TRUE )
    {
        eStatus = MB_EPORTERR;
    }
    else
    {
        /* If baudrate > 19200 then we should use the fixed timer values
         * t35 = 1750us. Otherwise t35 must be 3.5 times the character time.
         */
        if( ulBaudRate > 19200 )
        {
            usTimerT35_50us = 2600;       /* 1750us. */
        }
        else
        {
            /* The timer reload value for a character is given by:
             *
             * ChTimeValue = Ticks_per_1s / ( Baudrate / 11 )
             *             = 11 * Ticks_per_1s / Baudrate
             *             = 220000 / Baudrate
             * The reload for t3.5 is 1.5 times this value and similary
             * for t3.5.
             */
            usTimerT35_50us = ( 7UL * 220000UL ) / ( 2UL * ulBaudRate );
        }
        if( xMBXpicoPortTimersInit( ( USHORT ) usTimerT35_50us ) != TRUE )
        {
            eStatus = MB_EPORTERR;
        }
    }
    EXIT_CRITICAL_SECTION(  );

//    printf("RTUInit: eStatus = %d \r\n",eStatus);

    return eStatus;
}

void
eMBXpicoRTUStart( void )
{
    ENTER_CRITICAL_SECTION(  );
    /* Initially the receiver is in the state STATE_RX_INIT. we start
     * the timer and if no character is received within t3.5 we change
     * to STATE_RX_IDLE. This makes sure that we delay startup of the
     * modbus protocol stack until the bus is free.
     */
    eXpicoRcvState = STATE_RX_INIT;
    eXpicoSndState = STATE_TX_IDLE;
    vMBXpicoPortSerialEnable( TRUE, FALSE );
    vMBXpicoPortTimersEnable(  );

    EXIT_CRITICAL_SECTION(  );

 //   printf("RTUStart: eRcvState = %d \r\n",eRcvState);

}

void
eMBXpicoRTUStop( void )
{
    ENTER_CRITICAL_SECTION(  );
    vMBXpicoPortSerialEnable( FALSE, FALSE );
    vMBXpicoPortTimersDisable(  );
    EXIT_CRITICAL_SECTION(  );
}

eMBErrorCode
eMBXpicoRTUReceive( UCHAR * pucXpicoRcvAddress, UCHAR ** pucXpicoFrame, USHORT * pusXpicoLength )
{
   // BOOL            xFrameReceived = FALSE;
    eMBErrorCode    eStatus = MB_ENOERR;

    ENTER_CRITICAL_SECTION(  );
 //   assert( usRcvBufferPos < MB_SER_PDU_SIZE_MAX );

    /* Length and CRC check */
    if( usXpicoRcvBufferPos >= MB_SER_PDU_SIZE_MIN )
   //     && ( usMBCRC16( ( UCHAR * ) ucRTUBuf, usRcvBufferPos ) == 0 ) )
    {
        /* Save the address field. All frames are passed to the upper layed
         * and the decision if a frame is used is done there.
         */
        *pucXpicoRcvAddress = ucXpicoRTUBuf[MB_SER_PDU_ADDR_OFF];

        /* Total length of Modbus-PDU is Modbus-Serial-Line-PDU minus
         * size of address field and CRC checksum.
         */
        *pusXpicoLength = ucXpicoRTUBuf[MB_LEN_PDU_ADDR_OFF] - 1;
     //   *pusXpicoLength = ( USHORT )( usXpicoRcvBufferPos - MB_SER_PDU_PDU_OFF - MB_SER_PDU_SIZE_CRC );

        /* Return the start of the Modbus PDU to the caller. */
        *pucXpicoFrame = ( UCHAR * ) & ucXpicoRTUBuf[MB_SER_PDU_PDU_OFF];
     //   xFrameReceived = TRUE;
    }
    else
    {
        eStatus = MB_EIO;
    }

    EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

eMBErrorCode
eMBXpicoRTUSend( UCHAR ucXpicoSlaveAddress, const UCHAR * pucXpicoFrame, USHORT usXpicoLength )
{
    eMBErrorCode    eStatus = MB_ENOERR;
    USHORT          usCRC16;

    ENTER_CRITICAL_SECTION(  );

    /* Check if the receiver is still in idle state. If not we where too
     * slow with processing the received frame and the master sent another
     * frame on the network. We have to abort sending the frame.
     */
    if( eXpicoRcvState == STATE_RX_IDLE )
    {

    	/* First byte before the Modbus-PDU is the slave address. */
        pucXpicoSndBufferCur = (( UCHAR * ) pucXpicoFrame );
        usXpicoSndBufferCount = 1;

        /* Now copy the Modbus-PDU into the Modbus-Serial-Line-PDU. */
        pucXpicoSndBufferCur[MB_XPICO_SER_PDU_ADDR_OFF] = ucXpicoSlaveAddress;
        usXpicoSndBufferCount += usXpicoLength;

        /* Calculate CRC16 checksum for Modbus-Serial-Line-PDU. */
        usCRC16 = usMBCRC16( ( UCHAR * ) pucXpicoSndBufferCur, usXpicoSndBufferCount );
        pucXpicoSndBufferCur[usXpicoSndBufferCount++] = ( UCHAR )( usCRC16 & 0xFF );
        pucXpicoSndBufferCur[usXpicoSndBufferCount++] = ( UCHAR )( (usCRC16 >> 8) & 0xFF);

        /* Activate the transmitter. */
        eXpicoSndState = STATE_TX_XMIT;
        vMBXpicoPortSerialEnable( FALSE, TRUE );
    }
    else
    {
        eStatus = MB_EIO;
    }
    EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

BOOL
xMBXpicoRTUReceiveFSM( void )
{
    BOOL            xTaskNeedSwitch = FALSE;
    UCHAR           ucByte;

    assert( eXpicoSndState == STATE_TX_IDLE );

    /* Always read the character. */
    ( void )xMBXpicoPortSerialGetByte( ( CHAR * ) & ucByte );

    switch ( eXpicoRcvState )
    {
        /* If we have received a character in the init state we have to
         * wait until the frame is finished.
         */
    case STATE_RX_INIT:
        vMBXpicoPortTimersEnable(  );
        break;

        /* In the error state we wait until all characters in the
         * damaged frame are transmitted.
         */
    case STATE_RX_ERROR:
        vMBXpicoPortTimersEnable(  );
        break;

        /* In the idle state we wait for a new character. If a character
         * is received the t1.5 and t3.5 timers are started and the
         * receiver is in the state STATE_RX_RCV.
         */
    case STATE_RX_IDLE:
        usXpicoRcvBufferPos = 0;
        ucXpicoRTUBuf[usXpicoRcvBufferPos++] = ucByte;
        eXpicoRcvState = STATE_RX_RCV;

        /* Enable t3.5 timers. */
        vMBXpicoPortTimersEnable(  );
        break;

        /* We are currently receiving a frame. Reset the timer after
         * every character received. If more than the maximum possible
         * number of bytes in a modbus frame is received the frame is
         * ignored.
         */
    case STATE_RX_RCV:
        if( usXpicoRcvBufferPos < MB_SER_PDU_SIZE_MAX )
        {
            ucXpicoRTUBuf[usXpicoRcvBufferPos++] = ucByte;
        }
        else
        {
            eXpicoRcvState = STATE_RX_ERROR;
        }
        vMBXpicoPortTimersEnable(  );
        break;
    }
    return xTaskNeedSwitch;
}

BOOL
xMBXpicoRTUTransmitFSM( void )
{
    BOOL            xNeedPoll = FALSE;

    switch ( eXpicoSndState )
    {
        /* We should not get a transmitter event if the transmitter is in
         * idle state.  */
    case STATE_TX_IDLE:
        /* enable receiver/disable transmitter. */
        vMBXpicoPortSerialEnable( TRUE, FALSE );
        break;

    case STATE_TX_XMIT:
        /* check if we are finished. */
        if( usXpicoSndBufferCount != 0 )
        {
            xMBXpicoPortSerialPutByte( ( CHAR )*pucXpicoSndBufferCur );
            pucXpicoSndBufferCur++;  /* next byte in sendbuffer. */
            usXpicoSndBufferCount--;
        }
        else
        {
            xNeedPoll = xMBPortEventPost( EV_FRAME_SENT );
            /* Disable transmitter. This prevents another transmit buffer
             * empty interrupt. */
            vMBXpicoPortSerialEnable( TRUE, FALSE );
            eXpicoSndState = STATE_TX_IDLE;
        }
        break;
    }

    return xNeedPoll;
}

BOOL
xMBXpicoRTUTimerT35Expired( void )
{
    BOOL            xNeedPoll = FALSE;

    switch ( eXpicoRcvState )
    {
        /* Timer t35 expired. Startup phase is finished. */
      case STATE_RX_INIT:
        xNeedPoll = xMBPortEventPost( EV_READY );
//		printf("Timer Expired: xNeedPoll = %d",xNeedPoll);
        break;

        /* A frame was received and t35 expired. Notify the listener that
         * a new frame was received. */
      case STATE_RX_RCV:
        xNeedPoll = xMBPortEventPost( EV_XPICO_DEV_RESP_RECEIVED );
        break;

        /* An error occured while receiving the frame. */
      case STATE_RX_ERROR:
        break;

        /* Function called in an illegal state. */
      default:
        assert( ( eXpicoRcvState == STATE_RX_INIT ) ||
                ( eXpicoRcvState == STATE_RX_RCV ) || ( eXpicoRcvState == STATE_RX_ERROR ) );
    }

    vMBXpicoPortTimersDisable(  );
    eXpicoRcvState = STATE_RX_IDLE;

    return xNeedPoll;
}
