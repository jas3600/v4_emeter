/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: portserial.c,v 1.1 2006/08/22 21:35:13 wolti Exp $
 */

#include "port.h"
#include <stdint.h>
#include <emeter-toolkit.h>
#include "../emeter-structs.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

#ifdef MBSERIALPORT
/* ----------------------- Start implementation -----------------------------*/

void
vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
    /* If xRXEnable enable serial receive interrupts. If xTxENable enable
     * transmitter empty interrupts.
     */
  
    if ((xRxEnable == TRUE) || (xTxEnable == TRUE))
    {
      UCA0CTL1 &= ~UCSWRST;     /* Start USCI */
      
      if (xRxEnable == TRUE)  
      {
        UCA0IE |= UCRXIE;
      }
      else  
      {
        UCA0IE &= ~UCRXIE;
      }
      
      if (xTxEnable == TRUE) 
      {
        UCA0IE |= UCTXIE;
        UCA0IFG |= UCTXIFG;
      }
      else 
      {
        while (UCA0STATW & UCBUSY);
        UCA0IE &= ~UCTXIE;
      }
    }
    else
    {
      while (UCA0STATW & UCBUSY);
      UCA0IE = 0;
      UCA0CTL1 |= UCSWRST;     /* Suspend USCI */
    }
}


BOOL
xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity )
{
    UCA0CTL0 = 0;                       /* 8-bit character */
    UCA0CTL1 |= UCSSEL__SMCLK;                     /* UCLK = ACLK */
    UCA0BR1 = 0x00;           // 9600 @ 24MHz
    UCA0BR0 = 0x9E;
    UCA0MCTLW = 0x0041;                                // No modulation

 return TRUE;
}
#else

void
vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
    if (xRxEnable == TRUE)
    {
        UCA0IE |= UCRXIE;
    }
    else
    {
        UCA0IE &= ~UCRXIE;
    }

    if (xTxEnable == TRUE)
    {
        UCA0IE |= UCTXIE;
        UCA0IFG |= UCTXIFG;
    }
    else
    {
        while (UCA0STATW & UCBUSY);
        UCA0IE &= ~UCTXIE;
    }

}

BOOL
xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity )
{
    return TRUE;
}
#endif

BOOL
xMBPortSerialPutByte( CHAR ucByte )
{
    /* Put a byte in the UARTs transmit buffer. This function is called
     * by the protocol stack if pxMBFrameCBTransmitterEmpty( ) has been
     * called. */
   UCA0TXBUF = ucByte;
   
   return TRUE;
}

BOOL
xMBPortSerialGetByte( CHAR * pucByte )
{
    /* Return the byte in the UARTs receive buffer. This function is called
     * by the protocol stack after pxMBFrameCBByteReceived( ) has been called.
     */
    *pucByte = UCA0RXBUF;
    return TRUE;
}

/* Create an interrupt handler for the transmit buffer empty interrupt
 * (or an equivalent) for your target processor. This function should then
 * call pxMBFrameCBTransmitterEmpty( ) which tells the protocol stack that
 * a new character can be sent. The protocol stack will then call 
 * xMBPortSerialPutByte( ) to send the character.
 */
//interrupt void prvvUARTTxReadyISR( void )
//{
//    pxMBFrameCBTransmitterEmpty(  );
//    SciaRegs.SCIFFTX.bit.TXINTCLR = 1;
//	PieCtrlRegs.PIEACK.all = PIEACK_GROUP9;	// Must acknowledge the PIE group
//}

/* Create an interrupt handler for the receive interrupt for your target
 * processor. This function should then call pxMBFrameCBByteReceived( ). The
 * protocol stack will then call xMBPortSerialGetByte( ) to retrieve the
 * character.
 */
//interrupt void prvvUARTRxISR( void )
//{
//    pxMBFrameCBByteReceived(  );
//	SciaRegs.SCIFFRX.bit.RXFFOVRCLR = 1;   // Clear Overflow flag
//	SciaRegs.SCIFFRX.bit.RXFFINTCLR = 1;
//	PieCtrlRegs.PIEACK.all = PIEACK_GROUP9;	// Must acknowledge the PIE group
//}

/* Interrupt routines to send and receive serial messages. */
/***************************************************************************
   ISR(USCI_A2, serial_interrupt1)

   Sends Serial messages
****************************************************************************/
#ifdef MBSERIALPORT
ISR(USCI_A2, serial_interrupt2)
{
  switch(__even_in_range(UCA2IV,USCI_UART_UCTXCPTIFG))
  {
  case USCI_UART_UCRXIFG:
     pxMBFrameCBByteReceived(  );
    break;
  case USCI_UART_UCTXIFG:
     pxMBFrameCBTransmitterEmpty(  );
   break;
  case USCI_UART_UCSTTIFG:
    break;
  case USCI_UART_UCTXCPTIFG:
    break;
  }
  
}

// USCI_A0 interrupt service routine
/***************************************************************************
   ISR(USCI_A0, serial_interrupt0)

   Sends Serial messages
****************************************************************************/
/*
#pragma vector=USCI_A0_VECTOR
__interrupt void serial_interrupt0(void)
{
    switch (__even_in_range(UCA0IV, 4))
    {
        case USCI_NONE: break;                    // No interrupt
        case USCI_SPI_UCRXIFG:                    // RXIFG
            pxMBFrameCBByteReceived(  );
       break;
        case USCI_SPI_UCTXIFG:                     // TXIFG
            pxMBFrameCBTransmitterEmpty(  );
        break;             
        default:
        	break;
    }
}

*/
#endif
