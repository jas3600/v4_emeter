/**********************************************************************
* File: mbcallbacks.c
*
* Author: John Schroeder, Infinite Invention
*
* Reference Documents:
*
*
*
* History:
*   10/21/14 - Original (JAS)
**********************************************************************/

#include "port.h"
#include <math.h>
#include <stdint.h>
#include <emeter-toolkit.h>
#include "../emeter-structs.h"
#include <time.h>
#include "../emeter-relay.h"
#include "../debug.h"
#include "../emeter-fifo.h"

#ifdef DB_Modbus
#define Modbus_DBPRINT(...) PrintDebugMessage(__VA_ARGS__)
#else
#define Modbus_DBPRINT(...)
#endif

//#define TEST_DATA
#undef TEST_DATA

#define ENABLE_FIFO

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"
#include "Sunspec.h"

/* ----------------------- Static variables ---------------------------------*/
extern uint16_t comms_meter_status;
extern uint16_t gSendStatusFlag;
uint16_t WaitInHTTPTimer;
extern const char EmeterFwVersion[9];

//variable counts number of modbus operations executed since its cleared in RTC.
uint16_t ModbusMessageCount;

struct{
 USHORT SunSpec_ID[2];
 COMMON_BLOCK cb;
 COMMS_INTERFACE_HDR_BLOCK cifb;
 IPV4_BLOCK ipv4b;
 CELLULAR_LINK_BLOCK clb;
 AGGREGATOR ab;
 METER_BLOCK branchmb;
 METER_BLOCK loadmb;
 CONNECTDER_BLOCK conderb;
 CONNECTDER_DATA_BLOCK condatab;
 END_BLOCK eb;
} gRegisters;

UCHAR gDiscState = 0x0;
UCHAR gLatchState = 0x0;

#define HIBYTE(x) (((x) >> 8)& 0x00FF)
#define LOBYTE(x) ((x)& 0x00FF)
#define HOLDINGREGSTART 40000

void FillInputRegs(void);
void InitHoldingRegs(void);
void UpdateVariableRegisters(void);
unsigned long FindOffSet(unsigned long* DataReg );

extern unsigned int  gSwitchToHTTPFlag;

eMBErrorCode ReadHoldingReg(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs);
eMBErrorCode SaveHoldingReg(void);
eMBErrorCode ReadInputReg(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs);
eMBErrorCode WriteHoldingReg(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs);

ULONG LByteSwap(ULONG aNum)
{
   ULONG temp1, temp2;

	temp1 = aNum;

	temp2 = temp1 >> 16;

	temp1 <<= 16;

	temp1 |= temp2;

	return temp1;
}


FLOAT32 FByteSwap(FLOAT32 aNum)
{
  union{
	FLOAT32 fnum;
	ULONG   inum;
   } temp1;

   ULONG temp2;

	temp1.fnum = aNum;

	temp2 = temp1.inum >> 16;

	temp1.inum <<= 16;

	temp1.inum |= temp2;

	return temp1.fnum;
}

/*****************************************************************************/
/* BufToReg                                                                  */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
int
BufToReg(UCHAR * aBuf, SHORT * aReg, USHORT aLen)
{
    int i;

    for(i=0; i < aLen; i++)
    {
      aReg[i] = aBuf[2*i];
      aReg[i] <<= 8;
      aReg[i] += aBuf[2*i+1];
    }
return 1;
}

/*****************************************************************************/
/* WordToBuf                                                                 */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
int
WordToBuf(UCHAR * aBuf, USHORT aVal)
{
    aBuf[0] = (UCHAR) HIBYTE(aVal);
    aBuf[1] = (UCHAR) LOBYTE(aVal);

    return 0;
}

/*****************************************************************************/
/* RegToBuf                                                                  */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
int
RegToBuf(UCHAR * pucRegBuffer, SHORT * gTmp_Regs, USHORT usAddress, USHORT usNRegs)
{
  int i;

    for (i = 0; i < usNRegs; i++)
    {
       WordToBuf(pucRegBuffer + (2*i), gTmp_Regs[i + usAddress]);
    }
  return 0;
}

/*****************************************************************************/
/* InitHoldingRegs                                                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
void InitHoldingRegs(void)
{
	int i;

	ModbusMessageCount = 0;

	gRegisters.SunSpec_ID[0] = 0x5375;
	gRegisters.SunSpec_ID[1] = 0x6e53;

    // Common Block Constants
	gRegisters.cb.ID = 1;
	gRegisters.cb.L = 66;
	BufToReg("ConnectDER", &(gRegisters.cb.Mn[0]), 5);
	for (i = 0; i < 11; i++) gRegisters.cb.Mn[i+5] = 0;
	for (i = 0; i < 8; i++) gRegisters.cb.Opt[i] = 0;
	for (i = 0; i < 8; i++) gRegisters.cb.Vr[i] = 0;
	gRegisters.cb.DA = 1;

	gRegisters.cifb.ID = 10;
	gRegisters.cifb.L = 4;

	gRegisters.ipv4b.ID = 12;
	gRegisters.ipv4b.L = 98;

	gRegisters.clb.ID = 18;
	gRegisters.clb.L = 22;

	gRegisters.ab.ID = 2;
	gRegisters.ab.L = 14;

	gRegisters.branchmb.ID = 212;
	gRegisters.branchmb.L = 124;

	gRegisters.loadmb.ID = 212;
	gRegisters.loadmb.L = 124;

	gRegisters.conderb.ID = 62003;
	gRegisters.conderb.L = 244;

	gRegisters.condatab.ID = 62002;
	gRegisters.condatab.L = 45;

	gRegisters.eb.ID = 0;
	gRegisters.eb.L = 0;
}

/*****************************************************************************/
/* eMBRegInputCB                                                             */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
eMBErrorCode
eMBRegInputCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs )
{

  eMBErrorCode  eStatus;
  eStatus = MB_ENOREG;
  
 /* if ((usAddress >= 30001) && (usAddress + usNRegs) <= 30019)
  {
  //  RegToBuf(pucRegBuffer, (SHORT *) &gInputRegs, usAddress - 30000, usNRegs);
  }
  else
  {
     eStatus = MB_ENOREG;
  } */

  return eStatus;
}

/*****************************************************************************/
/* eMBRegHoldingCB                                                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
eMBErrorCode
eMBRegHoldingCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs,
                 eMBRegisterMode eMode )
{
  eMBErrorCode eStatus;

  eStatus = MB_ENOERR;
  if ((usAddress >= HOLDINGREGSTART) && ((usAddress + usNRegs) <= HOLDINGREGSTART + sizeof(gRegisters)))
  {
   
    if (eMode == MB_REG_READ)
    {
        sprintf(gDbgMsg,"Read Holding Regs:%u , Count:%d",usAddress,usNRegs);
        Modbus_DBPRINT(gDbgMsg);
    	ReadHoldingReg(pucRegBuffer, usAddress, usNRegs);
    	RegToBuf(pucRegBuffer, (SHORT*) &gRegisters, usAddress - HOLDINGREGSTART, usNRegs);
    }
    else // write
    {
        sprintf(gDbgMsg,"Write Holding Regs:%u , Count:%d",usAddress,usNRegs);
        Modbus_DBPRINT(gDbgMsg);
    	WriteHoldingReg(pucRegBuffer, usAddress, usNRegs);
    }
    ModbusMessageCount ++ ;
  }
  else
  {
     eStatus = MB_ENOREG;
  }

return eStatus;
}

/*****************************************************************************/
/* eMBRegCoilsCB                                                             */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
UCHAR gResvCoils = 0;

eMBErrorCode
eMBRegCoilsCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils,
               eMBRegisterMode eMode )
{
  eMBErrorCode eStatus;
  UCHAR temp;
  int i;
  
     eStatus = MB_ENOERR;

  if ((usAddress >= 1) && ((usAddress + usNCoils) <= 8))
  {
     if (eMode == MB_REG_READ)
     {
          
         pucRegBuffer[0] = temp >> (usAddress - 1 );
     }
     else  // Write to coils
     {
          temp = pucRegBuffer[0] << (usAddress - 1);
         if (usAddress >= 1)
          {
            for ( i = usAddress - 1; i < usAddress - 1 + usNCoils; i++)
            {
              if( temp & (0x01 << i)) gResvCoils |= 0x01 << i;
              else gResvCoils &= ~(0x01 << i);
            }
          }
     }
  }
  else
  {
     eStatus = MB_ENOREG;
  }
     
      return  eStatus;
}

/*****************************************************************************/
/* eMBRegDiscreteCB                                                          */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
eMBErrorCode
eMBRegDiscreteCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete )
{
  eMBErrorCode eStatus;
  UCHAR temp;
  // Report the discrete status
  
     eStatus = MB_ENOERR;
     
     if ((usAddress >= 10001) && ((usAddress + usNDiscrete) <= 10011))
     {
        pucRegBuffer[0] = 0;
        pucRegBuffer[1] = 0;
        
        if (usAddress < 10008)
        {
          pucRegBuffer[0] = temp >> ( usAddress - 10001 );
        }
     }
     else
     {
        eStatus = MB_ENOREG;
     }
       
return eStatus;
}


void FillInputRegs(void)
{
  

return;  
}

eMBErrorCode ReadHoldingReg(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs)
{
	eMBErrorCode eStatus;
	time_t now_time;
	struct tm tm_buf;
	float lTemp,lTemp2;
    fifomsg_t  lfifomsg;
    uint32_t lSramLoc;
    readings_t lReadings;
    int i;
    unsigned int lRecordStartAddress;
    unsigned int lRecordEndAddress;

    eStatus = MB_ENOERR;

    // ******  Common Model ************************************************************************
	BufToReg(&(nv_sys_conf.s.model_number[0]), &(gRegisters.cb.Md[0]), 9);
	for (i=0; i < 9; i++) gRegisters.cb.Md[i + 9] = 0;                             // Fill the rest with 0
	BufToReg(&(nv_sys_conf.s.property_number[0]), &(gRegisters.cb.SN[0]), 9);
	for (i=0; i < 7; i++) gRegisters.cb.SN[i + 9] = 0;                             // Fill the rest with 0

    // *****  Communication Interface Header  ****************************************************
    gRegisters.cifb.St = 0;
    gRegisters.cifb.Ctl = 0;
    gRegisters.cifb.Typ = 0;
    gRegisters.cifb.Pad = 0;

	// ***** IPV4 *****************************************************************************
	for (i=0; i<4; i++) gRegisters.ipv4b.Nam[i] = 0;
	gRegisters.ipv4b.CfgSt = 0;
	gRegisters.ipv4b.ChngSt = 0;
	gRegisters.ipv4b.Cap = 0;
	gRegisters.ipv4b.Cfg = 0;
	gRegisters.ipv4b.Ctl = 0;
/*	gRegisters.ipv4b.IP[8];
	gRegisters.ipv4b.Msk[8];
	gRegisters.ipv4b.Gw[8];
	gRegisters.ipv4b.DNS1[8];
	gRegisters.ipv4b.DNS2[8];
	gRegisters.ipv4b.NTP1[12];
	gRegisters.ipv4b.NTP2[12];
	gRegisters.ipv4b.DomNam[12];
	gRegisters.ipv4b.HostNam[12]; */

	// ******************  Cellular Link  ******************************************************
/*	gRegisters.clb.Nam[4];
	gRegisters.clb.IMEI;
	gRegisters.clb.APN[4];
	gRegisters.clb.Num[6];
	gRegisters.clb.Pin[6]; */

	// *************** Basic Aggregator ******************************************************
	gRegisters.ab.AID = 212;
	gRegisters.ab.NN = 2;
	gRegisters.ab.UN = 0;
	gRegisters.ab.St = 0x0002;
	gRegisters.ab.StVnd = 0;
	gRegisters.ab.Evt[0] = 0; gRegisters.ab.Evt[1] = 0;
	gRegisters.ab.EvtVnd[0] = 0; gRegisters.ab.EvtVnd[1] = 0;
	gRegisters.ab.Ctl = 0;
/*	gRegisters.ab.CtlVnd[2];
	gRegisters.ab.CtlVl[2]; */

    // ************ BRANCH Split single phase (ABN) meter ************************************

    lTemp = current_rms_current(4); //total AC Current
    gRegisters.branchmb.A = FByteSwap(lTemp/1000.0);

    gRegisters.branchmb.AphA = FByteSwap((float)(current_rms_current(0))/1000);
    gRegisters.branchmb.AphB = 0;
    gRegisters.branchmb.AphC = FByteSwap((float)(current_rms_current(1))/1000);

    lTemp = (current_rms_voltage(0)+current_rms_voltage(1))/2;
    gRegisters.branchmb.PhV = FByteSwap(lTemp/100.0);

    gRegisters.branchmb.PhVphA = FByteSwap((float)current_rms_voltage(0)/100.0);
    gRegisters.branchmb.PhVphB = 0;
    gRegisters.branchmb.PhVphC = FByteSwap((float)current_rms_voltage(1)/100.0);
    gRegisters.branchmb.PPV = FByteSwap((float)current_rms_voltage(4)/100);
    gRegisters.branchmb.PPVphAB = 0;
    gRegisters.branchmb.PPVphBC = 0;
    gRegisters.branchmb.PPVphCA = 0;

    lTemp = current_mains_frequency(0);
    gRegisters.branchmb.Hz = FByteSwap(lTemp/100.0);

    lTemp = current_active_power(4);
    gRegisters.branchmb.W = FByteSwap(lTemp/100.0);

    gRegisters.branchmb.WphA = FByteSwap((float)current_active_power(0)/100.0);
    gRegisters.branchmb.WphB = 0;
    gRegisters.branchmb.WphC = FByteSwap((float)current_active_power(1)/100.0);
    gRegisters.branchmb.VA = 0;
    gRegisters.branchmb.VAphA = 0;
    gRegisters.branchmb.VAphB = 0;
    gRegisters.branchmb.VAphC = 0;

    lTemp = current_reactive_power(4);
    gRegisters.branchmb.VAR = FByteSwap(lTemp/100.0);

    gRegisters.branchmb.VARphA = FByteSwap((float)current_reactive_power(0)/100.0);
    gRegisters.branchmb.VARphB = 0;
    gRegisters.branchmb.VARphC = FByteSwap((float)current_reactive_power(1)/100.0);

    lTemp2 = current_power_factor(4);
    gRegisters.branchmb.PF = FByteSwap(lTemp2/10000.0);

    gRegisters.branchmb.PFphA = 0;
    gRegisters.branchmb.PFphB = 0;
    gRegisters.branchmb.PFphC = 0;

    lTemp = current_produced_active_energy(4);
    gRegisters.branchmb.TotWhExp = FByteSwap(lTemp/1000);

    gRegisters.branchmb.TotWhExpPhA = FByteSwap((float)current_produced_active_energy(0)/100.0);
    gRegisters.branchmb.TotWhExpPhB = 0;
    gRegisters.branchmb.TotWhExpPhC = FByteSwap((float)current_produced_active_energy(1)/100.0);

    lTemp = current_consumed_active_energy(4);
    gRegisters.branchmb.TotWhImp = FByteSwap(lTemp/1000);

    gRegisters.branchmb.TotWhImpPhA = FByteSwap((float)current_consumed_active_energy(0)/100.0);
    gRegisters.branchmb.TotWhImpPhB = 0;
    gRegisters.branchmb.TotWhImpPhC = FByteSwap((float)current_consumed_active_energy(1)/100.0);
    gRegisters.branchmb.TotVAhExp = 0;
    gRegisters.branchmb.TotVAhExpPhA = 0;
    gRegisters.branchmb.TotVAhExpPhB = 0;
    gRegisters.branchmb.TotVAhExpPhC = 0;
    gRegisters.branchmb.TotVAhImp = 0;
    gRegisters.branchmb.TotVAhImpPhA = 0;
    gRegisters.branchmb.TotVAhImpPhB = 0;
    gRegisters.branchmb.TotVAhImpPhC = 0;
    gRegisters.branchmb.TotVArhImpQ1 = 0;
    gRegisters.branchmb.TotVArhImpQ1PhA = 0;
    gRegisters.branchmb.TotVArhImpQ1PhB = 0;
    gRegisters.branchmb.TotVArhImpQ1PhC = 0;
    gRegisters.branchmb.TotVArhImpQ2 = 0;
    gRegisters.branchmb.TotVArhImpQ2PhA = 0;
    gRegisters.branchmb.TotVArhImpQ2PhB = 0;
    gRegisters.branchmb.TotVArhImpQ2PhC = 0;
    gRegisters.branchmb.TotVArhExpQ3 = 0;
    gRegisters.branchmb.TotVArhExpQ3PhA = 0;
    gRegisters.branchmb.TotVArhExpQ3PhB = 0;
    gRegisters.branchmb.TotVArhExpQ3PhC = 0;
    gRegisters.branchmb.TotVArhExpQ4 = 0;
    gRegisters.branchmb.TotVArhExpQ4PhA = 0;
    gRegisters.branchmb.TotVArhExpQ4PhB = 0;
    gRegisters.branchmb.TotVArhExpQ4PhC = 0;
    gRegisters.branchmb.Evt = 0;

    // ***************  LOAD Split single phase (ABN) meter ******************************************

    gRegisters.loadmb.A = FByteSwap((float)(current_rms_current(2)+current_rms_current(3))/1000.0);
    gRegisters.loadmb.AphA = FByteSwap((float)(current_rms_current(2))/1000);
    gRegisters.loadmb.AphB = 0;
    gRegisters.loadmb.AphC = FByteSwap((float)(current_rms_current(3))/1000);

    lTemp = current_rms_voltage(0)+current_rms_voltage(1);
    gRegisters.loadmb.PhV = FByteSwap(lTemp/100.0);

    gRegisters.loadmb.PhVphA = FByteSwap((float)current_rms_voltage(2)/100.0);
    gRegisters.loadmb.PhVphB = 0;
    gRegisters.loadmb.PhVphC = FByteSwap((float)current_rms_voltage(3)/100.0);
    gRegisters.loadmb.PPV = FByteSwap((float)current_rms_voltage(5)/100.0);
    gRegisters.loadmb.PPVphAB = 0;
    gRegisters.loadmb.PPVphBC = 0;
    gRegisters.loadmb.PPVphCA = 0;

    lTemp = current_mains_frequency(0);
    gRegisters.loadmb.Hz = FByteSwap(lTemp/100.0);

    lTemp = current_active_power(5);
    gRegisters.loadmb.W = FByteSwap(lTemp/100.0);

    gRegisters.loadmb.WphA = FByteSwap((float)current_active_power(2)/100.0);
    gRegisters.loadmb.WphB = 0;
    gRegisters.loadmb.WphC = FByteSwap((float)current_active_power(3)/100.0);
    gRegisters.loadmb.VA = 0;
    gRegisters.loadmb.VAphA = 0;
    gRegisters.loadmb.VAphB = 0;
    gRegisters.loadmb.VAphC = 0;

    lTemp = current_reactive_power(5);
    gRegisters.loadmb.VAR = FByteSwap(lTemp/100.0);

    gRegisters.loadmb.VARphA = FByteSwap((float)current_reactive_power(2)/100.0);
    gRegisters.loadmb.VARphB = 0;
    gRegisters.loadmb.VARphC = FByteSwap((float)current_reactive_power(3)/100.0);

    lTemp2 = current_power_factor(5);
    gRegisters.loadmb.PF = FByteSwap(lTemp2/10000.0);

    gRegisters.loadmb.PFphA = 0;
    gRegisters.loadmb.PFphB = 0;
    gRegisters.loadmb.PFphC = 0;

    lTemp = current_produced_active_energy(5);
    gRegisters.loadmb.TotWhExp = FByteSwap(lTemp/1000);

    gRegisters.loadmb.TotWhExpPhA = FByteSwap((float)current_produced_active_energy(2)/100.0);
    gRegisters.loadmb.TotWhExpPhB = 0;
    gRegisters.loadmb.TotWhExpPhC = FByteSwap((float)current_produced_active_energy(3)/100.0);

    lTemp = current_consumed_active_energy(5);
    gRegisters.loadmb.TotWhImp = FByteSwap(lTemp/1000);

    gRegisters.loadmb.TotWhImpPhA = FByteSwap((float)current_consumed_active_energy(2)/100.0);
    gRegisters.loadmb.TotWhImpPhB = 0;
    gRegisters.loadmb.TotWhImpPhC = FByteSwap((float)current_consumed_active_energy(3)/100.0);
    gRegisters.loadmb.TotVAhExp = 0;
    gRegisters.loadmb.TotVAhExpPhA = 0;
    gRegisters.loadmb.TotVAhExpPhB = 0;
    gRegisters.loadmb.TotVAhExpPhC = 0;
    gRegisters.loadmb.TotVAhImp = 0;
    gRegisters.loadmb.TotVAhImpPhA = 0;
    gRegisters.loadmb.TotVAhImpPhB = 0;
    gRegisters.loadmb.TotVAhImpPhC = 0;
    gRegisters.loadmb.TotVArhImpQ1 = 0;
    gRegisters.loadmb.TotVArhImpQ1PhA = 0;
    gRegisters.loadmb.TotVArhImpQ1PhB = 0;
    gRegisters.loadmb.TotVArhImpQ1PhC = 0;
    gRegisters.loadmb.TotVArhImpQ2 = 0;
    gRegisters.loadmb.TotVArhImpQ2PhA = 0;
    gRegisters.loadmb.TotVArhImpQ2PhB = 0;
    gRegisters.loadmb.TotVArhImpQ2PhC = 0;
    gRegisters.loadmb.TotVArhExpQ3 = 0;
    gRegisters.loadmb.TotVArhExpQ3PhA = 0;
    gRegisters.loadmb.TotVArhExpQ3PhB = 0;
    gRegisters.loadmb.TotVArhExpQ3PhC = 0;
    gRegisters.loadmb.TotVArhExpQ4 = 0;
    gRegisters.loadmb.TotVArhExpQ4PhA = 0;
    gRegisters.loadmb.TotVArhExpQ4PhB = 0;
    gRegisters.loadmb.TotVArhExpQ4PhC = 0;
    gRegisters.loadmb.Evt = 0;

	// *************************** ConnectDER Vendor Model (MODBUS ID: 62003) ************************************************

	BufToReg(&(nv_sys_conf.s.property_number[0]), &(gRegisters.conderb.Sn[0]), 9);
	for (i=0; i < 11; i++) gRegisters.conderb.Sn[i + 9] = 0;

	BufToReg(EmeterFwVersion, &(gRegisters.conderb.FwVer[0]), (strlen(EmeterFwVersion)/2));
	for (i=0;i < 5; i++) gRegisters.conderb.FwVer[i + 5] = 0;

//	comms_meter_status = meter_status | (comms_meter_status & 0xE2D3);
	gRegisters.conderb.Sts = (comms_meter_status | meter_status) & 0xE6B3;
	gRegisters.conderb.OpCfg = nv_sys_conf.s.op_config;
	gRegisters.conderb.SmpPer = nv_sys_conf.s.sample_period;
	gRegisters.conderb.RptPer = nv_sys_conf.s.report_period;
	gRegisters.conderb.RptMn = nv_sys_conf.s.report_minute;
	gRegisters.conderb.RlySts = (comms_meter_status & STATUS_DISCONNECT_CLOSED)?Rly_CONNECT:Rly_DISCONNECT;
	gRegisters.conderb.Rly = 8888;
	gRegisters.conderb.ClsDly = nv_sys_conf.s.close_delay;
	BufToReg(&(nv_sys_conf.s.report_URL), &(gRegisters.conderb.RptURL), 60);

	gRegisters.conderb.IsfL1B = nv_parms.s.chan[0].current.I_rms_scale_factor;
	gRegisters.conderb.PsfL1B = nv_parms.s.chan[0].current.P_scale_factor;
	gRegisters.conderb.PcL1B = nv_parms.s.chan[0].current.phase_correction;
	gRegisters.conderb.VsfL1B = nv_parms.s.chan[0].V_rms_scale_factor;
	gRegisters.conderb.QpsL1B = nv_parms.s.chan[0].current.quadrature_step;
	gRegisters.conderb.IpsL1B = nv_parms.s.chan[0].current.in_phase_step;

	gRegisters.conderb.IsfL2B  = nv_parms.s.chan[1].current.I_rms_scale_factor;
	gRegisters.conderb.PsfL2B = nv_parms.s.chan[1].current.P_scale_factor;
	gRegisters.conderb.PcL2B = nv_parms.s.chan[1].current.phase_correction;
	gRegisters.conderb.VsfL2B = nv_parms.s.chan[1].V_rms_scale_factor;
	gRegisters.conderb.QpsL2B = nv_parms.s.chan[1].current.quadrature_step;
	gRegisters.conderb.IpsL2B = nv_parms.s.chan[1].current.in_phase_step;

	gRegisters.conderb.IsfL1LS  = nv_parms.s.chan[2].current.I_rms_scale_factor;;
	gRegisters.conderb.PsfL1LS = nv_parms.s.chan[2].current.P_scale_factor;
	gRegisters.conderb.PcL1LS = nv_parms.s.chan[2].current.phase_correction;
	gRegisters.conderb.VsfL1LS = nv_parms.s.chan[2].V_rms_scale_factor;
	gRegisters.conderb.QpsL1LS = nv_parms.s.chan[2].current.quadrature_step;
	gRegisters.conderb.IpsL1LS = nv_parms.s.chan[2].current.in_phase_step;

	gRegisters.conderb.IsfL2LS  = nv_parms.s.chan[3].current.I_rms_scale_factor;;
	gRegisters.conderb.PsfL2LS = nv_parms.s.chan[3].current.P_scale_factor;
	gRegisters.conderb.PcL2LS = nv_parms.s.chan[3].current.phase_correction;
	gRegisters.conderb.VsfL2LS = nv_parms.s.chan[3].V_rms_scale_factor;
	gRegisters.conderb.QpsL2LS = nv_parms.s.chan[3].current.quadrature_step;
	gRegisters.conderb.IpsL2LS = nv_parms.s.chan[3].current.in_phase_step;

	gRegisters.conderb.TmpOffset = nv_parms.s.temperature_offset;
	gRegisters.conderb.TmpSf = nv_parms.s.temperature_scaling;
	gRegisters.conderb.StayInHTTPModeTimer = 9000 + gNumMessages;
	gRegisters.conderb.OTActl = 0;
	for (i=0; i<120; i++) gRegisters.conderb.OTAdat[i] = i;

#ifdef ENABLE_FIFO
	// ******************************** ConnectDER Data Model (MODBUS ID: 62001) **************************************

	gRegisters.condatab.FIFOSf = 0x0001; 						// indicate to UI that FIFO is enabled in this firmware onwards
	gRegisters.condatab.FIFODp = getFIFODepth();

	lRecordStartAddress = &gRegisters.condatab.FIFOSf - &gRegisters + HOLDINGREGSTART;
	lRecordEndAddress = lRecordStartAddress + sizeof(gRegisters.condatab)/2;

	if((usAddress <= lRecordEndAddress) && (usAddress + usNRegs >= lRecordStartAddress))     // Asking for registers in the data area
	{
		if((getFIFODepth() > 0) && ((gRegisters.condatab.FIFOCtl == FIFOCtl_PEEK) || (gRegisters.condatab.FIFOCtl == FIFOCtl_PEEK_POP)))
		{
			/* Return FIFO Reading */
		    peek_msg_fifo((char *) &lReadings, &lfifomsg, 1 , &lSramLoc);

			//tm_buf.tm_year = ((int) (lReadings.century - 19) * 100) + (int) lReadings.year;
			tm_buf.tm_year = ((int) (lReadings.century) * 100) + (int) lReadings.year - 1900;
			tm_buf.tm_mon = lReadings.month - 1;
			tm_buf.tm_mday = lReadings.day;
			tm_buf.tm_hour = lReadings.hour;
			tm_buf.tm_min = lReadings.minute;
			tm_buf.tm_sec = lReadings.second;
			tm_buf.tm_isdst = -1;

			now_time = mktime(&tm_buf) - 2208988800L;

			gRegisters.condatab.TmSt = LByteSwap(now_time);
			gRegisters.condatab.FIFOSf = 0x0002;				//Indicates that current poll is made from FIFO queue
			gRegisters.condatab.RSts = 0x00;
			gRegisters.condatab.BPf = FByteSwap((float)lReadings.power_factor/10000.0);
			gRegisters.condatab.BPKwh = FByteSwap((float)lReadings.produced_energy/1000.0);
			gRegisters.condatab.BCKwh = FByteSwap((float)lReadings.consumed_energy/1000.0);
			gRegisters.condatab.F = FByteSwap((float)lReadings.frequency/100.0);
			gRegisters.condatab.Bw = FByteSwap((float)lReadings.active_power/100.0);
			gRegisters.condatab.Bvar = FByteSwap((float)lReadings.reactive_power/100.0);
			gRegisters.condatab.T = FByteSwap((float)lReadings.temperature/16.0);
			gRegisters.condatab.BLLV = FByteSwap((float)lReadings.rms_voltage/100.0);
			gRegisters.condatab.BL1I = FByteSwap((float)lReadings.rms_current/1000.0);
			gRegisters.condatab.BL2I = FByteSwap((float)lReadings.BL2I/1000.0);
			gRegisters.condatab.BL1V = FByteSwap((float)lReadings.BL1V/100.0);
			gRegisters.condatab.BL2V = FByteSwap((float)lReadings.BL2V/100.0);

			gRegisters.condatab.LSPf = FByteSwap((float)lReadings.LSPf/10000.0);
			gRegisters.condatab.LSw = FByteSwap((float)lReadings.LSw/100.0);
			gRegisters.condatab.LSvar = FByteSwap((float)lReadings.LSvar/100.0);
			gRegisters.condatab.LSCKwh = FByteSwap((float)lReadings.LSCKwh/1000.0);
			gRegisters.condatab.LSPKwh = FByteSwap((float)lReadings.LSPKwh/1000.0);
			gRegisters.condatab.LSL1I = FByteSwap((float)lReadings.LSL1I/1000.0);
			gRegisters.condatab.LSL2I = FByteSwap((float)lReadings.LSL2I/1000.0);

			if(gRegisters.condatab.FIFOCtl == FIFOCtl_PEEK_POP)
			{
				pop_msg_fifo();
			}

			gRegisters.condatab.FIFOCtl = 0;

			return eStatus;
		}
		else
		{
		    /* Return Immediate Reading */
		    tm_buf.tm_year = ((int) (rtc.century) * 100) + (int) rtc.year - 1900;
		    tm_buf.tm_mon = rtc.month - 1;
		    tm_buf.tm_mday = rtc.day;
		    tm_buf.tm_hour = rtc.hour;
		    tm_buf.tm_min = rtc.minute;
		    tm_buf.tm_sec = rtc.second;
		    tm_buf.tm_isdst = -1;

		    now_time = mktime(&tm_buf)  - 2208988800L;

		    gRegisters.condatab.TmSt = LByteSwap(now_time);
		    gRegisters.condatab.FIFOSf &= ~0x0002;
		    gRegisters.condatab.RSts = 0x00;
		    gRegisters.condatab.BPf = gRegisters.branchmb.PF;
		    gRegisters.condatab.BPKwh = gRegisters.branchmb.TotWhExp;
		    gRegisters.condatab.BCKwh = gRegisters.branchmb.TotWhImp;
		    gRegisters.condatab.F = gRegisters.branchmb.Hz;
		    gRegisters.condatab.Bw = gRegisters.branchmb.W;
		    gRegisters.condatab.Bvar = gRegisters.branchmb.VAR;
		    gRegisters.condatab.T = FByteSwap((float)current_temperature()/16.0);
		    gRegisters.condatab.BLLV = gRegisters.branchmb.PPV;
		    gRegisters.condatab.BL1I = gRegisters.branchmb.AphA;
		    gRegisters.condatab.BL2I = gRegisters.branchmb.AphC;
		    gRegisters.condatab.BL1V = gRegisters.branchmb.PhVphA;
		    gRegisters.condatab.BL2V = gRegisters.branchmb.PhVphC;

		    gRegisters.condatab.LSPf = gRegisters.loadmb.PF;
		    gRegisters.condatab.LSw = gRegisters.loadmb.W;
		    gRegisters.condatab.LSvar = gRegisters.loadmb.VAR;
		    gRegisters.condatab.LSCKwh = gRegisters.loadmb.TotWhImp;
		    gRegisters.condatab.LSPKwh = gRegisters.loadmb.TotWhExp;
		    gRegisters.condatab.LSL1I = gRegisters.loadmb.AphA;
		    gRegisters.condatab.LSL2I = gRegisters.loadmb.AphC;

		}


	}
#else
	gRegisters.condatab.FIFOSf = 0x0000; 						// indicate to UI that FIFO is enabled in this firmware onwards
	gRegisters.condatab.FIFODp = 0;


	tm_buf.tm_year = ((int) (rtc.century) * 100) + (int) rtc.year -1970;
	tm_buf.tm_mon = rtc.month - 1;
	tm_buf.tm_mday = rtc.day + 1;
	tm_buf.tm_hour = rtc.hour;
	tm_buf.tm_min = rtc.minute;
	tm_buf.tm_sec = rtc.second;
	tm_buf.tm_isdst = -1;

	now_time = mktime(&tm_buf);

	gRegisters.condatab.TmSt = LByteSwap(now_time);
	gRegisters.condatab.RSts = 0x00;
	gRegisters.condatab.BPf = gRegisters.branchmb.PF;
	gRegisters.condatab.BPKwh = gRegisters.branchmb.TotWhExp;
	gRegisters.condatab.BCKwh = gRegisters.branchmb.TotWhImp;
	gRegisters.condatab.F = gRegisters.branchmb.Hz;
	gRegisters.condatab.Bw = gRegisters.branchmb.W;
	gRegisters.condatab.Bvar = gRegisters.branchmb.VAR;
	gRegisters.condatab.T = FByteSwap((float)current_temperature()/16);
	gRegisters.condatab.BLLV = gRegisters.branchmb.PPV;
	gRegisters.condatab.BL1I = gRegisters.branchmb.AphA;
	gRegisters.condatab.BL2I = gRegisters.branchmb.AphC;
	gRegisters.condatab.BL1V = gRegisters.branchmb.PhVphA;
	gRegisters.condatab.BL2V = gRegisters.branchmb.PhVphC;

	gRegisters.condatab.LSPf = gRegisters.loadmb.PF;
	gRegisters.condatab.LSw = gRegisters.loadmb.W;
	gRegisters.condatab.LSvar = gRegisters.loadmb.VAR;
	gRegisters.condatab.LSCKwh = gRegisters.loadmb.TotWhImp;
	gRegisters.condatab.LSPKwh = gRegisters.loadmb.TotWhExp;
	gRegisters.condatab.LSL1I = gRegisters.loadmb.AphA;
	gRegisters.condatab.LSL2I = gRegisters.loadmb.AphC;
#endif

	return eStatus;
}


eMBErrorCode WriteHoldingReg(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs)
{
	eMBErrorCode  eStatus;
    unsigned int lStartIdx;
    unsigned int lRealLen;
    unsigned int lRecordStartAddress;
    unsigned int lRecordEndAddress;

    eStatus = MB_ENOERR;

    lRecordStartAddress = &gRegisters.condatab.TmSt - &gRegisters + HOLDINGREGSTART;
    lRealLen = sizeof(gRegisters.condatab.TmSt)/2;
    lRecordEndAddress = lRecordStartAddress + lRealLen;

    // Update Time Stamp
	if ((usAddress <= lRecordStartAddress) && (usAddress + usNRegs >= lRecordEndAddress))
	{
		/*
		lStartIdx = lRecordStartAddress - usAddress;
		BufToReg(&pucRegBuffer[lStartIdx*2], &gRegisters.condatab.TmSt, lRealLen);

		now_time = gRegisters.condatab.TmSt;

		new_tm = localtime(&now_time);

		rtc.century = (new_tm->tm_year/100) + 19;      // calculate century
		rtc.year = new_tm->tm_year % 100;              // calculate year
		rtc.month = new_tm->tm_mon + 1;
		rtc.day = new_tm->tm_mday + 1;
		rtc.hour = new_tm->tm_hour;
		rtc.minute = new_tm->tm_min;
		rtc.second = new_tm->tm_sec;
		set_rtc_sumcheck();
		*/
	}

	// Update FIFO Control
    lRecordStartAddress = &gRegisters.condatab.FIFOCtl - &gRegisters + HOLDINGREGSTART;
    lRealLen =  sizeof(gRegisters.condatab.FIFOCtl)/2;
    lRecordEndAddress = lRecordStartAddress + lRealLen;

	if ((usAddress <= lRecordStartAddress) && (usAddress + usNRegs >= lRecordEndAddress))
	{
		lStartIdx = lRecordStartAddress - usAddress;
		BufToReg(&pucRegBuffer[lStartIdx*2], &gRegisters.condatab.FIFOCtl, lRealLen);

        sprintf(gDbgMsg,"FIFO Ctl = %X", gRegisters.condatab.FIFOCtl);
        Modbus_DBPRINT(gDbgMsg);

		if (gRegisters.condatab.FIFOCtl == FIFOCtl_POP) pop_msg_fifo();

		if (gRegisters.condatab.FIFOCtl == FIFOCtl_CLEAR)
		{
		    setClearMMC();
		}
	}

	// Update branch registers to clear stored results
    lRecordStartAddress = (int *) &gRegisters.condatab.BCKwh - &gRegisters + HOLDINGREGSTART;
    lRealLen = sizeof(gRegisters.condatab.BCKwh)/2;
    lRecordEndAddress = lRecordStartAddress + lRealLen - 1;

	if ((usAddress <= lRecordStartAddress) && (usAddress + usNRegs >= lRecordEndAddress))
	{
		lStartIdx = lRecordStartAddress - usAddress;
		BufToReg(&pucRegBuffer[lStartIdx*2], &gRegisters.condatab.BCKwh, lRealLen);

		sprintf(gDbgMsg,"Clear Branch CKwh %f",FByteSwap(gRegisters.condatab.BCKwh));
		Modbus_DBPRINT(gDbgMsg);
		total_consumed_active_energy = FByteSwap(gRegisters.condatab.BCKwh);
	}

    lRecordStartAddress = (int *) &gRegisters.condatab.BPKwh - &gRegisters + HOLDINGREGSTART;
    lRealLen = sizeof(gRegisters.condatab.BPKwh)/2;
    lRecordEndAddress = lRecordStartAddress + lRealLen - 1;

	if ((usAddress <= lRecordStartAddress) && (usAddress + usNRegs >= lRecordEndAddress))
	{
		lStartIdx = lRecordStartAddress - usAddress;
		BufToReg(&pucRegBuffer[lStartIdx*2], &gRegisters.condatab.BPKwh, lRealLen);

		sprintf(gDbgMsg,"Clear Branch Pkwh %f", FByteSwap(gRegisters.condatab.BPKwh));
		Modbus_DBPRINT(gDbgMsg);
		total_produced_active_energy = FByteSwap(gRegisters.condatab.BPKwh);
	}

	// Update load registers to clear stored results
    lRecordStartAddress = (int *) &gRegisters.condatab.LSCKwh - &gRegisters + HOLDINGREGSTART;
    lRealLen = sizeof(gRegisters.condatab.LSCKwh)/2;
    lRecordEndAddress = lRecordStartAddress + lRealLen - 1;

	if ((usAddress <= lRecordStartAddress) && (usAddress + usNRegs >= lRecordEndAddress))
	{
		lStartIdx = lRecordStartAddress - usAddress;
		BufToReg(&pucRegBuffer[lStartIdx*2], &gRegisters.condatab.LSCKwh, lRealLen);

		sprintf(gDbgMsg,"Clear load CKwh %f",FByteSwap(gRegisters.condatab.LSCKwh));
		Modbus_DBPRINT(gDbgMsg);
		wh_total_consumed_active_energy = FByteSwap(gRegisters.condatab.LSCKwh);
	}

    lRecordStartAddress = (int *) &gRegisters.condatab.LSPKwh - &gRegisters + HOLDINGREGSTART;
    lRealLen = sizeof(gRegisters.condatab.LSPKwh)/2;
    lRecordEndAddress = lRecordStartAddress + lRealLen - 1;

	if ((usAddress <= lRecordStartAddress) && (usAddress + usNRegs >= lRecordEndAddress))
	{
		lStartIdx = lRecordStartAddress - usAddress;
		BufToReg(&pucRegBuffer[lStartIdx*2], &gRegisters.condatab.LSPKwh, lRealLen);

		sprintf(gDbgMsg,"Clear load Pkwh %f",FByteSwap(gRegisters.condatab.LSPKwh));
		Modbus_DBPRINT(gDbgMsg);
		wh_total_produced_active_energy = FByteSwap(gRegisters.condatab.LSPKwh);
	}

    // Update Relay
    lRecordStartAddress = &gRegisters.conderb.Rly - &gRegisters + HOLDINGREGSTART;
    lRealLen = sizeof(gRegisters.conderb.Rly)/2;
    lRecordEndAddress = lRecordStartAddress + lRealLen;

	if ((usAddress <= lRecordStartAddress) && (usAddress + usNRegs >= lRecordEndAddress))
	{
		lStartIdx = lRecordStartAddress - usAddress;
		BufToReg(&pucRegBuffer[lStartIdx*2], &gRegisters.conderb.Rly, lRealLen);
		if (gRegisters.conderb.Rly == Rly_CONNECT) switch_relay(RELAY_SHUT);
		else if(gRegisters.conderb.Rly == Rly_DISCONNECT) switch_relay(RELAY_OPEN);

		sprintf(gDbgMsg,"Relay Switch: %d",gRegisters.conderb.Rly);
		Modbus_DBPRINT(gDbgMsg);
	}

	// Switch from MODBUS to HTTP mode
    lRecordStartAddress = &gRegisters.conderb.StayInHTTPModeTimer - &gRegisters + HOLDINGREGSTART;
    lRealLen = sizeof(gRegisters.conderb.StayInHTTPModeTimer)/2;
    lRecordEndAddress = lRecordStartAddress + lRealLen;

	if ((usAddress <= lRecordStartAddress) && (usAddress + usNRegs >= lRecordEndAddress))
	{
		lStartIdx = lRecordStartAddress - usAddress;
		BufToReg(&pucRegBuffer[lStartIdx*2], &gRegisters.conderb.StayInHTTPModeTimer, lRealLen);
		if (gRegisters.conderb.StayInHTTPModeTimer > 0)
		{
		    gSwitchToHTTPFlag = TRUE;
			Modbus_DBPRINT("Switch 2 HTTP mode commanded");
		}

	}

	// Update Vendor Model
    lRecordStartAddress = &gRegisters.conderb - &gRegisters + HOLDINGREGSTART;
    lRecordEndAddress = lRecordStartAddress + sizeof(gRegisters.conderb)/2;

	if ((usAddress <= lRecordStartAddress) && (usAddress + usNRegs >= lRecordEndAddress))
	{
/*
		lStartIdx = (unsigned int)(&gRegisters.conderb.Sn[0] - &gRegisters) + mbHoldRegStrtAdr - usAddress;
		lRealLen = (unsigned int)(&gRegisters.conderb.TmpSf-&gRegisters.conderb.Sn[0]);
		lRealLen = (lRealLen>0)?lRealLen:1;
		BufToReg(&pucRegBuffer[lStartIdx*2], &gRegisters.conderb.Sn, lRealLen);

	    for (tempptr=127;tempptr>=0;tempptr--)
	    {
	    	lTempCalConst.x[tempptr] = nv_parms.x[tempptr]; //copy all existing factors to new variable
	    }

	    for (tempptr=127;tempptr>=0;tempptr--)
	    {
	    	lTempConfig.x[tempptr] = nv_sys_conf.x[tempptr]; //copy all existing factors to new variable
	    }

		// gRegisters.conderb.Sn[18];
		lTempConfig.s.op_config = gRegisters.conderb.OpCfg;
		lTempConfig.s.sample_period = gRegisters.conderb.SmpPer;
		lTempConfig.s.report_period = gRegisters.conderb.RptPer;
		lTempConfig.s.report_minute = gRegisters.conderb.RptMn;
		lTempConfig.s.close_delay = gRegisters.conderb.ClsDly;

		// gRegisters.conderb.RptURL[60];
		lTempCalConst.s.chan[0].current.I_rms_scale_factor = gRegisters.conderb.IsfL1B;
		lTempCalConst.s.chan[0].current.P_scale_factor[0] = gRegisters.conderb.PsfL1B;
		lTempCalConst.s.chan[0].current.phase_correction[0] = gRegisters.conderb.PcL1B;
		lTempCalConst.s.chan[0].V_rms_scale_factor = gRegisters.conderb.VsfL1B;
		lTempCalConst.s.chan[0].current.quadrature_step = gRegisters.conderb.QpsL1B;
		lTempCalConst.s.chan[0].current.in_phase_step = gRegisters.conderb.IpsL1B;

		lTempCalConst.s.chan[1].current.I_rms_scale_factor = gRegisters.conderb.IsfL2B;
		lTempCalConst.s.chan[1].current.P_scale_factor[0] = gRegisters.conderb.PsfL2B;
		lTempCalConst.s.chan[1].current.phase_correction[0] = gRegisters.conderb.PcL2B;
		lTempCalConst.s.chan[1].V_rms_scale_factor = gRegisters.conderb.VsfL2B;
		lTempCalConst.s.chan[1].current.quadrature_step = gRegisters.conderb.QpsL2B;
		lTempCalConst.s.chan[1].current.in_phase_step = gRegisters.conderb.IpsL2B;

		lTempCalConst.s.chan[2].current.I_rms_scale_factor = gRegisters.conderb.IsfL1LS;
		lTempCalConst.s.chan[2].current.P_scale_factor[0] = gRegisters.conderb.PsfL1LS;
		lTempCalConst.s.chan[2].current.phase_correction[0] = gRegisters.conderb.PcL1LS;
		lTempCalConst.s.chan[2].V_rms_scale_factor = gRegisters.conderb.VsfL1LS;
		lTempCalConst.s.chan[2].current.quadrature_step = gRegisters.conderb.QpsL1LS;
		lTempCalConst.s.chan[2].current.in_phase_step = gRegisters.conderb.IpsL1LS;

		lTempCalConst.s.chan[3].current.I_rms_scale_factor = gRegisters.conderb.IsfL2LS;
		lTempCalConst.s.chan[3].current.P_scale_factor[0] = gRegisters.conderb.PsfL2LS;
		lTempCalConst.s.chan[3].current.phase_correction[0] = gRegisters.conderb.PcL2LS;
		lTempCalConst.s.chan[3].V_rms_scale_factor = gRegisters.conderb.VsfL2LS;
		lTempCalConst.s.chan[3].current.quadrature_step = gRegisters.conderb.QpsL2LS;
		lTempCalConst.s.chan[3].current.in_phase_step = gRegisters.conderb.IpsL2LS;


		lTempCalConst.s.temperature_offset = gRegisters.conderb.TmpOffset;
		lTempCalConst.s.temperature_scaling = gRegisters.conderb.TmpSf;

		flash_clr((int*) &nv_parms);
		flash_memcpy((char *) &nv_parms, lTempCalConst.x, sizeof(struct info_mem_s));
		flash_secure();

		flash_clr((int*) &nv_sys_conf);
		flash_memcpy((char *) &nv_sys_conf, lTempConfig.x, sizeof(struct system_mem_s));
		flash_secure();
		*/
	}

	// Update OTA Control
    lRecordStartAddress = &gRegisters.conderb.OTActl - &gRegisters + HOLDINGREGSTART;
    lRecordEndAddress = lRecordStartAddress + sizeof(gRegisters.conderb.OTActl)/2;

	if ((usAddress <= lRecordStartAddress) && (usAddress + usNRegs >= lRecordEndAddress))
	{
	 // gRegisters.conderb.OTActl;
	 // gRegisters.conderb.OTAdat[120];
	}

	return eStatus;
}

/*
void TestMBWriteFunc()
{
	UCHAR tempdata[500];
	//unsigned int usAddress= &gRegisters.conderb.Sn[0]- &gRegisters  + mbHoldRegStrtAdr;
	//unsigned int usLen =  &gRegisters.conderb.TmpSf - &gRegisters.conderb.Sn[0];
	unsigned int usAddress= FindOffSet(&gRegisters.condatab.BPKwh);
	//unsigned int usLen =  (unsigned int)(&gRegisters.condatab.LSPKwh - &gRegisters.condatab.LSCKwh);
	unsigned int usLen = sizeof(gRegisters.condatab.BPKwh)/2;
	usLen = (usLen > 0)?usLen:1;
	eMBRegHoldingCB( tempdata, usAddress ,  usLen, MB_REG_READ );
	WriteHoldingReg(tempdata, usAddress ,  usLen);

}
*/

/*
unsigned long FindOffSet(unsigned long* DataReg )
{
	return((((unsigned long)DataReg - (unsigned long)&gRegisters)>>1)+mbHoldRegStrtAdr);
}
*/

#ifdef TEST_DATA
    // Branch Split single phase (ABN) meter
//  lTemp = current_rms_current(0)+current_rms_current(1); //total AC Current
    gRegisters.branchmb.A = FByteSwap(5.1);
    gRegisters.branchmb.AphA = FByteSwap(2.5);
    gRegisters.branchmb.AphB = 0;
    gRegisters.branchmb.AphC = FByteSwap(2.6);
//  lTemp = (current_rms_voltage(0)+current_rms_voltage(1))/2;
    gRegisters.branchmb.PhV = FByteSwap(120.0);
    gRegisters.branchmb.PhVphA = FByteSwap(121.0);
    gRegisters.branchmb.PhVphB = 0;
    gRegisters.branchmb.PhVphC = FByteSwap(122.0);
    gRegisters.branchmb.PPV = FByteSwap(240.0);
    gRegisters.branchmb.PPVphAB = 0;
    gRegisters.branchmb.PPVphBC = 0;
    gRegisters.branchmb.PPVphCA = 0;
//  lTemp = current_mains_frequency(0);
    gRegisters.branchmb.Hz = FByteSwap(60.1);
//  lTemp = current_active_power(4);
    gRegisters.branchmb.W = FByteSwap(800.0);
    gRegisters.branchmb.WphA = FByteSwap(400.0);
    gRegisters.branchmb.WphB = 0;
    gRegisters.branchmb.WphC = FByteSwap(401.0);
    gRegisters.branchmb.VA = 0;
    gRegisters.branchmb.VAphA = 0;
    gRegisters.branchmb.VAphB = 0;
    gRegisters.branchmb.VAphC = 0;
    lTemp = current_reactive_power(4);
    gRegisters.branchmb.VAR = FByteSwap(10.0);
    gRegisters.branchmb.VARphA = FByteSwap(5.1);
    gRegisters.branchmb.VARphB = 0;
    gRegisters.branchmb.VARphC = FByteSwap(5.2);
//  lTemp2 = current_power_factor(4);
    gRegisters.branchmb.PF = FByteSwap(0.98);
    gRegisters.branchmb.PFphA = 0;
    gRegisters.branchmb.PFphB = 0;
    gRegisters.branchmb.PFphC = 0;
//  lTemp = current_produced_active_energy(4);
    gRegisters.branchmb.TotWhExp = FByteSwap(100.0);
    gRegisters.branchmb.TotWhExpPhA = FByteSwap(50.0);
    gRegisters.branchmb.TotWhExpPhB = 0;
    gRegisters.branchmb.TotWhExpPhC = FByteSwap(51.0);
//  lTemp = current_consumed_active_energy(4);
    gRegisters.branchmb.TotWhImp = FByteSwap(300.1);
    gRegisters.branchmb.TotWhImpPhA = FByteSwap(150.1);
    gRegisters.branchmb.TotWhImpPhB = 0;
    gRegisters.branchmb.TotWhImpPhC = FByteSwap(151.2);
    gRegisters.branchmb.TotVAhExp = 0;
    gRegisters.branchmb.TotVAhExpPhA = 0;
    gRegisters.branchmb.TotVAhExpPhB = 0;
    gRegisters.branchmb.TotVAhExpPhC = 0;
    gRegisters.branchmb.TotVAhImp = 0;
    gRegisters.branchmb.TotVAhImpPhA = 0;
    gRegisters.branchmb.TotVAhImpPhB = 0;
    gRegisters.branchmb.TotVAhImpPhC = 0;
    gRegisters.branchmb.TotVArhImpQ1 = 0;
    gRegisters.branchmb.TotVArhImpQ1PhA = 0;
    gRegisters.branchmb.TotVArhImpQ1PhB = 0;
    gRegisters.branchmb.TotVArhImpQ1PhC = 0;
    gRegisters.branchmb.TotVArhImpQ2 = 0;
    gRegisters.branchmb.TotVArhImpQ2PhA = 0;
    gRegisters.branchmb.TotVArhImpQ2PhB = 0;
    gRegisters.branchmb.TotVArhImpQ2PhC = 0;
    gRegisters.branchmb.TotVArhExpQ3 = 0;
    gRegisters.branchmb.TotVArhExpQ3PhA = 0;
    gRegisters.branchmb.TotVArhExpQ3PhB = 0;
    gRegisters.branchmb.TotVArhExpQ3PhC = 0;
    gRegisters.branchmb.TotVArhExpQ4 = 0;
    gRegisters.branchmb.TotVArhExpQ4PhA = 0;
    gRegisters.branchmb.TotVArhExpQ4PhB = 0;
    gRegisters.branchmb.TotVArhExpQ4PhC = 0;
    gRegisters.branchmb.Evt = 0;

    // Load Split single phase (ABN) meter
        gRegisters.loadmb.A = FByteSwap(30.0);
        gRegisters.loadmb.AphA = FByteSwap(15.0);
        gRegisters.loadmb.AphB = 0;
        gRegisters.loadmb.AphC = FByteSwap(15.1);

        lTemp = current_rms_voltage(0)+current_rms_voltage(1);
        gRegisters.loadmb.PhV = FByteSwap(120.0);
        gRegisters.loadmb.PhVphA = FByteSwap(120.1);
        gRegisters.loadmb.PhVphB = 0;
        gRegisters.loadmb.PhVphC = FByteSwap(120.2);
        gRegisters.loadmb.PPV = FByteSwap(240.4);
        gRegisters.loadmb.PPVphAB = 0;
        gRegisters.loadmb.PPVphBC = 0;
        gRegisters.loadmb.PPVphCA = 0;
//      lTemp = current_mains_frequency(0);
        gRegisters.loadmb.Hz = FByteSwap(60.2);
//      lTemp = current_active_power(5);
        gRegisters.loadmb.W = FByteSwap(500.1);
        gRegisters.loadmb.WphA = FByteSwap(250.1);
        gRegisters.loadmb.WphB = 0;
        gRegisters.loadmb.WphC = FByteSwap(250.4);
        gRegisters.loadmb.VA = 0;
        gRegisters.loadmb.VAphA = 0;
        gRegisters.loadmb.VAphB = 0;
        gRegisters.loadmb.VAphC = 0;
//      lTemp = current_reactive_power(5);
        gRegisters.loadmb.VAR = FByteSwap(10.8);
        gRegisters.loadmb.VARphA = FByteSwap(5.6);
        gRegisters.loadmb.VARphB = 0;
        gRegisters.loadmb.VARphC = FByteSwap(5.7);
//      lTemp2 = current_power_factor(5);
        gRegisters.loadmb.PF = FByteSwap(-0.99);
        gRegisters.loadmb.PFphA = 0;
        gRegisters.loadmb.PFphB = 0;
        gRegisters.loadmb.PFphC = 0;
//      lTemp = current_produced_active_energy(5);
        gRegisters.loadmb.TotWhExp = FByteSwap(2999.0);
        gRegisters.loadmb.TotWhExpPhA = FByteSwap(1500.0);
        gRegisters.loadmb.TotWhExpPhB = 0;
        gRegisters.loadmb.TotWhExpPhC = FByteSwap(1499.9);
//      lTemp = current_consumed_active_energy(5);
        gRegisters.loadmb.TotWhImp = FByteSwap(4000.0);
        gRegisters.loadmb.TotWhImpPhA = FByteSwap(2000.0);
        gRegisters.loadmb.TotWhImpPhB = 0;
        gRegisters.loadmb.TotWhImpPhC = FByteSwap(1999.0);
        gRegisters.loadmb.TotVAhExp = 0;
        gRegisters.loadmb.TotVAhExpPhA = 0;
        gRegisters.loadmb.TotVAhExpPhB = 0;
        gRegisters.loadmb.TotVAhExpPhC = 0;
        gRegisters.loadmb.TotVAhImp = 0;
        gRegisters.loadmb.TotVAhImpPhA = 0;
        gRegisters.loadmb.TotVAhImpPhB = 0;
        gRegisters.loadmb.TotVAhImpPhC = 0;
        gRegisters.loadmb.TotVArhImpQ1 = 0;
        gRegisters.loadmb.TotVArhImpQ1PhA = 0;
        gRegisters.loadmb.TotVArhImpQ1PhB = 0;
        gRegisters.loadmb.TotVArhImpQ1PhC = 0;
        gRegisters.loadmb.TotVArhImpQ2 = 0;
        gRegisters.loadmb.TotVArhImpQ2PhA = 0;
        gRegisters.loadmb.TotVArhImpQ2PhB = 0;
        gRegisters.loadmb.TotVArhImpQ2PhC = 0;
        gRegisters.loadmb.TotVArhExpQ3 = 0;
        gRegisters.loadmb.TotVArhExpQ3PhA = 0;
        gRegisters.loadmb.TotVArhExpQ3PhB = 0;
        gRegisters.loadmb.TotVArhExpQ3PhC = 0;
        gRegisters.loadmb.TotVArhExpQ4 = 0;
        gRegisters.loadmb.TotVArhExpQ4PhA = 0;
        gRegisters.loadmb.TotVArhExpQ4PhB = 0;
        gRegisters.loadmb.TotVArhExpQ4PhC = 0;
        gRegisters.loadmb.Evt = 0;
#endif

