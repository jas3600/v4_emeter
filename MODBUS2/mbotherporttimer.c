/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: porttimer.c,v 1.1 2006/08/22 21:35:13 wolti Exp $
 */
/* ----------------------- Platform includes --------------------------------*/
#include "port.h"
#include <stdint.h>
#include <emeter-toolkit.h>
#include "../emeter-structs.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

/* ----------------------- Start implementation -----------------------------*/
BOOL
xMBXpicoPortTimersInit( USHORT usTim1Timerout50us )
{
    TA1CCTL0 = CCIE;
    TA1CCR0 = usTim1Timerout50us;
    TA1CTL = TASSEL__ACLK | ID__8;                // 500 KHz / 8
    
    return TRUE;
}


void
vMBXpicoPortTimersEnable(  )
{
   TA1CTL |= MC__UP | TACLR;
}

void
vMBXpicoPortTimersDisable(  )
{
   TA1CTL &= ~MC__UP;
}

/* Create an ISR which is called whenever the timer has expired. This function
 * must then call pxMBPortCBTimerExpired( ) to notify the protocol stack that
 * the timer has expired.
 */
//interrupt void prvvTIMERExpiredISR( void )
//{
//
//  pxMBPortCBTimerExpired( );
//}

#pragma vector=TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void)
{
  pxMBXpicoPortCBTimerExpired( );
}
