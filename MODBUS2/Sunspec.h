#ifndef _SUNSPEC_H
#define _SUNSPEC_H

#include "port.h"

#define C_STATUS_NORMAL 0x00000000           // Operating Normally
#define C_STATUS_ERROR  0xFFFFFFFE            // Generic Failure
#define C_STATUS_UNK    0xFFFFFFFF              // Status Unknown

#define M_EVENT_Power_Failure 0x00000004   /* Loss of power or phase */
#define M_EVENT_Under_Voltage 0x00000008   /* Voltage below threshold (Phase
                                              Loss) */
#define M_EVENT_Low_PF        0x00000010   /* Power Factor below threshold (can
                                              indicate miss-associated voltage and
                                              current inputs in three phase
                                              systems) */
#define M_EVENT_Over_Current 0x00000020    /* Current Input over threshold (out of
                                              measurement range) */
#define M_EVENT_Over_Voltage 0x00000040    /* Voltage Input over threshold (out of
                                              measurement range) */

#define M_EVENT_Missing_Sensor 0x00000080 // Sensor not connected
#define M_EVENT_Reserved1 0x00000100      // Reserved for future
#define M_EVENT_Reserved2 0x00000200      // Reserved for future
#define M_EVENT_Reserved3 0x00000400      // Reserved for future
#define M_EVENT_Reserved4 0x00000800      // Reserved for future
#define M_EVENT_Reserved5 0x00001000 // Reserved for future
#define M_EVENT_Reserved6 0x00002000 // Reserved for future
#define M_EVENT_Reserved7 0x00004000 // Reserved for future
#define M_EVENT_Reserved8 0x00008000 // Reserved for future
#define M_EVENT_OEM1_15   0x7FFF000    // Reserved for OEMs

// Common Model
typedef struct {
  USHORT ID;                            // = 1;
  USHORT L;                             // = 66;
  USHORT Mn[16];
  USHORT Md[16];
  USHORT Opt[8];
  USHORT Vr[8];
  USHORT SN[16];
  USHORT DA;
  USHORT Pad;
} COMMON_BLOCK;

// Basic Aggregator
typedef struct {
	USHORT ID;                            // = 2;
	USHORT L;                             // = 14;
	USHORT AID;
	USHORT NN;
	USHORT UN;
	USHORT St;
	USHORT StVnd;
	USHORT Evt[2];
	USHORT EvtVnd[2];
	USHORT Ctl;
	USHORT CtlVnd[2];
	USHORT CtlVl[2];
} AGGREGATOR;

#define St_OFF   1
#define St_ON    2
#define St_FULL  3
#define St_FAULT 4

#define Evt_GROUND_FAULT			0
#define Evt_INPUT_OVER_VOLTAGE		1
#define Evt_RESERVED				11
#define Evt_DC_DISCONNECT			3
#define Evt_MANUAL_SHUTDOWN			6
#define Evt_OVER_TEMPERATURE		7
#define Evt_BLOWN_FUSE				12
#define Evt_UNDER_TEMPERATURE		13
#define Evt_MEMORY_LOSS				14
#define Evt_ARC_DETECTION			15
#define Evt_THEFT_DETECTION			16
#define Evt_OUTPUT_OVER_CURRENT		17
#define Evt_OUTPUT_OVER_VOLTAGE		18
#define Evt_OUTPUT_UNDER_VOLTAGE	19
#define Evt_TEST_FAILED				20

#define Ctl_NONE					0
#define Ctl_AUTOMATIC				1
#define Ctl_FORCE_OFF				2
#define Ctl_TEST					3
#define Ctl_THROTTLE				4

// Communication Interface Header
typedef struct {
	USHORT ID;                          // = 10;
	USHORT L;                           //  = 4;
	USHORT St;
	USHORT Ctl;
	USHORT Typ;
	USHORT Pad;
} COMMS_INTERFACE_HDR_BLOCK;

// Comms interface enums
#define St_COMM_INT_DOWN	0
#define St_COMM_INT_UP	    1
#define St_COMM_INT_FAULT   2

#define Typ_UNKNOWN			0
#define Typ_INTERNAL		1
#define Typ_TWISTED_PAIR	2
#define Typ_FIBER			3
#define Typ_WIRELESS		4

// IPV4
typedef struct {
  USHORT ID;							//12
  USHORT L;								//98
  USHORT Nam[4];
  USHORT CfgSt;
  USHORT ChngSt;
  USHORT Cap;
  USHORT Cfg;
  USHORT Ctl;
  USHORT IP[8];
  USHORT Msk[8];
  USHORT Gw[8];
  USHORT DNS1[8];
  USHORT DNS2[8];
  USHORT NTP1[12];
  USHORT NTP2[12];
  USHORT DomNam[12];
  USHORT HostNam[12];
  USHORT Pad;
} IPV4_BLOCK;

// IPV4 an IPV6 Enums and bit fields
#define CfgSt_NOT_CONFIGURED	0
#define CfgSt_VALID_SETTING		1
#define CfgSt_VALID_HW			2

#define ChgSt PENDING			0

#define Cfg_STATIC				0
#define Cfg_Cap_DHCP			1
#define Cfg_Cap_BOOTP			2
#define Cfg_Cap_ZEROCONF		3
#define Cap_DNS					3
#define Cap_CFG_SETTABLE		4
#define Cap_HW_CONFIG			5
#define Cap_NTP_CLIENT			6
#define Cap_RESET_REQUIRED		7

#define Ctl_ENABLE_DNS				0
#define Ctl_ENABLE_NTP				1

// IPV6
typedef struct {
  USHORT ID;
  USHORT L;
  USHORT Nam[4];
  USHORT CfgSt;
  USHORT ChgSte;
  USHORT Cap;
  USHORT Cfg;
  USHORT Ctl;
  USHORT Addr[20];
  USHORT CIDR[20];
  USHORT Gw[20];
  USHORT DNS1[20];
  USHORT DNS2[20];
  USHORT NTP1[20];
  USHORT NTP2[20];
  USHORT DomNam[12];
  USHORT HostNam[12];
  USHORT Pad;
} IPV6_BLOCK;

// Cellular Link
typedef struct {
	USHORT ID;						//18
	USHORT L;						//22
	USHORT Nam[4];
	ULONG IMEI;
	USHORT APN[4];
	USHORT Num[6];
	USHORT Pin[6];
} CELLULAR_LINK_BLOCK;


// Split single phase (ABN) meter
typedef struct {
  USHORT ID;      // Split Phase Meter				//212
  USHORT L;  // Length of Meter Block				//124
  FLOAT32 A;
  FLOAT32 AphA;
  FLOAT32 AphB;
  FLOAT32 AphC;
  FLOAT32 PhV;
  FLOAT32 PhVphA;
  FLOAT32 PhVphB;
  FLOAT32 PhVphC;
  FLOAT32 PPV;
  FLOAT32 PPVphAB;
  FLOAT32 PPVphBC;
  FLOAT32 PPVphCA;
  FLOAT32 Hz;
  FLOAT32 W;
  FLOAT32 WphA;
  FLOAT32 WphB;
  FLOAT32 WphC;
  FLOAT32 VA;
  FLOAT32 VAphA;
  FLOAT32 VAphB;
  FLOAT32 VAphC;
  FLOAT32 VAR;
  FLOAT32 VARphA;
  FLOAT32 VARphB;
  FLOAT32 VARphC;
  FLOAT32 PF;
  FLOAT32 PFphA;
  FLOAT32 PFphB;
  FLOAT32 PFphC;
  FLOAT32 TotWhExp;
  FLOAT32 TotWhExpPhA;
  FLOAT32 TotWhExpPhB;
  FLOAT32 TotWhExpPhC;
  FLOAT32 TotWhImp;
  FLOAT32 TotWhImpPhA;
  FLOAT32 TotWhImpPhB;
  FLOAT32 TotWhImpPhC;
  FLOAT32 TotVAhExp;
  FLOAT32 TotVAhExpPhA;
  FLOAT32 TotVAhExpPhB;
  FLOAT32 TotVAhExpPhC;
  FLOAT32 TotVAhImp;
  FLOAT32 TotVAhImpPhA;
  FLOAT32 TotVAhImpPhB;
  FLOAT32 TotVAhImpPhC;
  FLOAT32 TotVArhImpQ1;
  FLOAT32 TotVArhImpQ1PhA;
  FLOAT32 TotVArhImpQ1PhB;
  FLOAT32 TotVArhImpQ1PhC;
  FLOAT32 TotVArhImpQ2;
  FLOAT32 TotVArhImpQ2PhA;
  FLOAT32 TotVArhImpQ2PhB;
  FLOAT32 TotVArhImpQ2PhC;
  FLOAT32 TotVArhExpQ3;
  FLOAT32 TotVArhExpQ3PhA;
  FLOAT32 TotVArhExpQ3PhB;
  FLOAT32 TotVArhExpQ3PhC;
  FLOAT32 TotVArhExpQ4;
  FLOAT32 TotVArhExpQ4PhA;
  FLOAT32 TotVArhExpQ4PhB;
  FLOAT32 TotVArhExpQ4PhC;
  ULONG Evt;
} METER_BLOCK;

// Meter Bit Fields
#define Evt_M_EVENT_Power_Failure		2
#define Evt_M_EVENT_Under_Voltage		3
#define Evt_M_EVENT_Low_PF				4
#define Evt_M_EVENT_Over_Current		5
#define Evt_M_EVENT_Over_Voltage		6
#define Evt_M_EVENT_Missing_Sensor		7
#define Evt_M_EVENT_Reserved1			8
#define Evt_M_EVENT_Reserved2			9
#define Evt_M_EVENT_Reserved3			10
#define Evt_M_EVENT_Reserved4			11
#define Evt_M_EVENT_Reserved5			12
#define Evt_M_EVENT_Reserved6			13
#define Evt_M_EVENT_Reserved7			14
#define Evt_M_EVENT_Reserved8			15
#define Evt_M_EVENT_OEM01				16
#define Evt_M_EVENT_OEM02				17
#define Evt_M_EVENT_OEM03				18
#define Evt_M_EVENT_OEM04				19
#define Evt_M_EVENT_OEM05				20
#define Evt_M_EVENT_OEM06				21
#define Evt_M_EVENT_OEM07				22
#define Evt_M_EVENT_OEM08				23
#define Evt_M_EVENT_OEM09				24
#define Evt_M_EVENT_OEM10				25
#define Evt_M_EVENT_OEM11				26
#define Evt_M_EVENT_OEM12				27
#define Evt_M_EVENT_OEM13				28
#define Evt_M_EVENT_OEM14				29
#define Evt_M_EVENT_OEM15				30

typedef struct {
	USHORT ID;              // =62003
	USHORT L;               // = 244
	USHORT Sn[18];
	USHORT FwVer[10];
	USHORT Sts;
	USHORT OpCfg;
	USHORT SmpPer;
	USHORT RptPer;
	USHORT RptMn;
	USHORT RlySts;
	USHORT Rly;
	USHORT ClsDly;
	USHORT RptURL[60];
	USHORT IsfL1B;
	USHORT PsfL1B;
	USHORT PcL1B;
	USHORT VsfL1B;
	USHORT QpsL1B;
	USHORT IpsL1B;
	USHORT IsfL2B;
	USHORT PsfL2B;
	USHORT PcL2B;
	USHORT VsfL2B;
	USHORT QpsL2B;
	USHORT IpsL2B;
	USHORT IsfL1LS;
	USHORT PsfL1LS;
	USHORT PcL1LS;
	USHORT VsfL1LS;  /////not exist in sheet 1.1
	USHORT QpsL1LS;
	USHORT IpsL1LS;
	USHORT IsfL2LS;
	USHORT PsfL2LS;
	USHORT PcL2LS;
	USHORT VsfL2LS; /////not exist in sheet 1.1
	USHORT QpsL2LS;
	USHORT IpsL2LS;
	USHORT TmpOffset;
	USHORT TmpSf;
	USHORT StayInHTTPModeTimer;
	USHORT OTActl;
	USHORT OTAdat[120];
} CONNECTDER_BLOCK;


#define Rly_DISCONNECT		0
#define Rly_CONNECT			1

#define OpCfg_2S			1
#define OpCfg_12S			2

#define Sts_METER_STATUS	0
#define Sts_POWER_DIR		1
#define Sts_POWER_STATUS	2
#define Sts_PIGTAIL			3
#define Sts_RELAY_STATUS	4
#define Sts_ORIENTATION		5
#define Sts_PASSWORD		6
#define Sts_TBD				7
#define Sts_POLARITY		8
#define Sts_EARTHED			9
#define Sts_PHASE_VOLTAGE	10
#define Sts_BATTERY			11
#define Sts_MAG_FIELD		12
#define Sts_POST			13
#define Sts_PROVISION		14
#define Sts_ACTIVE			15


typedef struct {
	USHORT ID;              // =62002
	USHORT L;               // = 45
	USHORT FIFOSf;
	USHORT FIFODp;
	USHORT FIFOCtl;
	ULONG  TmSt;
	ULONG  RSts;
	FLOAT32 T;
	FLOAT32 F;
	FLOAT32 BPf;
	FLOAT32 Bw;
	FLOAT32 Bvar;
	FLOAT32 BCKwh;
	FLOAT32 BPKwh;
	FLOAT32 BLLV;
	FLOAT32 BL1V;
	FLOAT32 BL1I;
	FLOAT32 BL2V;
	FLOAT32 BL2I;
	FLOAT32 LSPf;
	FLOAT32 LSw;
	FLOAT32 LSvar;
	FLOAT32 LSCKwh;
	FLOAT32 LSPKwh;
	FLOAT32 LSL1I;
	FLOAT32 LSL2I;
} CONNECTDER_DATA_BLOCK;

#define FIFOCtl_POP		1
#define FIFOCtl_PEEK	2
#define FIFOCtl_CLEAR	3
#define FIFOCtl_PEEK_POP 4

typedef struct {
 USHORT ID;         //0
 USHORT L;			//0
} END_BLOCK;

extern uint32_t gNumMessages;
#endif
