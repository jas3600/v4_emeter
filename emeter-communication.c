//--------------------------------------------------------------------------
//
//  Software for MSP430 based e-meters.
//
//  THIS PROGRAM IS PROVIDED "AS IS". TI MAKES NO WARRANTIES OR
//  REPRESENTATIONS, EITHER EXPRESS, IMPLIED OR STATUTORY,
//  INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//  FOR A PARTICULAR PURPOSE, LACK OF VIRUSES, ACCURACY OR
//  COMPLETENESS OF RESPONSES, RESULTS AND LACK OF NEGLIGENCE.
//  TI DISCLAIMS ANY WARRANTY OF TITLE, QUIET ENJOYMENT, QUIET
//  POSSESSION, AND NON-INFRINGEMENT OF ANY THIRD PARTY
//  INTELLECTUAL PROPERTY RIGHTS WITH REGARD TO THE PROGRAM OR
//  YOUR USE OF THE PROGRAM.
//
//  IN NO EVENT SHALL TI BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
//  CONSEQUENTIAL OR INDIRECT DAMAGES, HOWEVER CAUSED, ON ANY
//  THEORY OF LIABILITY AND WHETHER OR NOT TI HAS BEEN ADVISED
//  OF THE POSSIBILITY OF SUCH DAMAGES, ARISING IN ANY WAY OUT
//  OF THIS AGREEMENT, THE PROGRAM, OR YOUR USE OF THE PROGRAM.
//  EXCLUDED DAMAGES INCLUDE, BUT ARE NOT LIMITED TO, COST OF
//  REMOVAL OR REINSTALLATION, COMPUTER TIME, LABOR COSTS, LOSS
//  OF GOODWILL, LOSS OF PROFITS, LOSS OF SAVINGS, OR LOSS OF
//  USE OR INTERRUPTION OF BUSINESS. IN NO EVENT WILL TI'S
//  AGGREGATE LIABILITY UNDER THIS AGREEMENT OR ARISING OUT OF
//  YOUR USE OF THE PROGRAM EXCEED FIVE HUNDRED DOLLARS
//  (U.S.$500).
//
//  Unless otherwise stated, the Program written and copyrighted
//  by Texas Instruments is distributed as "freeware".  You may,
//  only under TI's copyright in the Program, use and modify the
//  Program without any charge or restriction.  You may
//  distribute to third parties, provided that you transfer a
//  copy of this license to the third party and the third party
//  agrees to these terms by its first use of the Program. You
//  must reproduce the copyright notice and any other legend of
//  ownership on each copy or partial copy, of the Program.
//
//  You acknowledge and agree that the Program contains
//  copyrighted material, trade secrets and other TI proprietary
//  information and is protected by copyright laws,
//  international copyright treaties, and trade secret laws, as
//  well as other intellectual property laws.  To protect TI's
//  rights in the Program, you agree not to decompile, reverse
//  engineer, disassemble or otherwise translate any object code
//  versions of the Program to a human-readable form.  You agree
//  that in no event will you alter, remove or destroy any
//  copyright notice included in the Program.  TI reserves all
//  rights not specifically granted under this license. Except
//  as specifically provided herein, nothing in this agreement
//  shall be construed as conferring by implication, estoppel,
//  or otherwise, upon you, any license or other right under any
//  TI patents, copyrights or trade secrets.
//
//  You may not use the Program in non-TI devices.
//
//  File: emeter-communications.c
//
//  Steve Underwood <steve-underwood@ti.com>
//  Texas Instruments Hong Kong Ltd.
//
//  $Id: emeter-communication.c,v 1.18 2009/04/27 09:05:25 a0754793 Exp $
//
/*! \file emeter-structs.h */
//
//--------------------------------------------------------------------------
//
#include <stdint.h>
#include <io.h>
#include <stdio.h>
#include <string.h>
#include <emeter-toolkit.h>
#include "emeter-structs.h"
#include "Modem_Identify.h"
#include "emeter-communication.h"
#include "emeter-fifo.h"
#include "emeter-http.h"
#include "Telit.h"
#include "emeter-rtc.h"
#include "Debug.h"
#include "emeter-display.h"
#include "MMC.h"
#include "emeter-relay.h"
#include "emeter-rtc.h"
#include <mbport.h>

//#define DB_COMMS
#ifdef DB_COMMS
#define COMMS_DBPRINT(...) PrintDebugMessage(__VA_ARGS__)
#else
#define COMMS_DBPRINT(...)
#endif

uint32_t queue_readings(void);

char gOTAFailed;
char gSendConstFlag;
uint16_t gSentReadings;
uint16_t gFWDownloadFlag;
uint16_t printSMvarFlag;
commstate gCommsState;
commresp gCommsResponse;
uint16_t gCommsTOFlag;
uint16_t gModemRebootCount;
unsigned char gNewFWVersion[10];
volatile uint16_t gReqRespFlag;
CommsModeControl  gModeSelect;

#if defined(IEC62056_21_SUPPORT)
  #include "iec62056-21.h"
#endif

#if !defined(NULL)
#define NULL    (void *) 0
#endif

enum
{
    MEASURES_ACTIVE_POWER                       = 0x01,
    MEASURES_TRIGONOMETRIC_REACTIVE_POWER       = 0x02,
    MEASURES_APPARENT_POWER                     = 0x04,
    MEASURES_VRMS                               = 0x08,
    MEASURES_IRMS                               = 0x10,
    MEASURES_POWER_FACTOR                       = 0x20,
    MEASURES_MAINS_FREQUENCY                    = 0x40,
    MEASURES_QUADRATURE_REACTIVE_POWER          = 0x80
};

enum host_commands_e
{
    HOST_CMD_GET_METER_CONFIGURATION            = 0x56,
    HOST_CMD_SET_METER_CONSUMPTION              = 0x57,
    HOST_CMD_SET_RTC                            = 0x58,
    HOST_CMD_GET_RTC                            = 0x59,
    HOST_CMD_SET_PASSWORD                       = 0x60,
    HOST_CMD_GET_READINGS_PHASE_1               = 0x61,
    HOST_CMD_GET_READINGS_PHASE_2               = 0x62,
    HOST_CMD_GET_READINGS_PHASE_3               = 0x63,
    HOST_CMD_GET_READINGS_NEUTRAL               = 0x64,
    HOST_CMD_ERASE_FLASH_SEGMENT                = 0x70,
    HOST_CMD_SET_FLASH_POINTER                  = 0x71,
    HOST_CMD_FLASH_DOWNLOAD                     = 0x72,
    HOST_CMD_FLASH_UPLOAD                       = 0x73,
    HOST_CMD_ZAP_MEMORY_AREA                    = 0x74,
    HOST_CMD_SUMCHECK_MEMORY                    = 0x75,
    HOST_CMD_GET_RAW_ACTIVE_POWER_PHASE_1       = 0x91,
    HOST_CMD_GET_RAW_ACTIVE_POWER_PHASE_2       = 0x92,
    HOST_CMD_GET_RAW_ACTIVE_POWER_PHASE_3       = 0x93,
    HOST_CMD_GET_RAW_REACTIVE_POWER_PHASE_1     = 0x95,
    HOST_CMD_GET_RAW_REACTIVE_POWER_PHASE_2     = 0x96,
    HOST_CMD_GET_RAW_REACTIVE_POWER_PHASE_3     = 0x97,
    HOST_CMD_GET_RAW_ACTIVE_POWER_NEUTRAL       = 0x99,
    HOST_CMD_GET_RAW_REACTIVE_POWER_NEUTRAL     = 0x9D,
    HOST_CMD_CHECK_RTC_ERROR                    = 0xA0,
    HOST_CMD_RTC_CORRECTION                     = 0xA1,
    HOST_CMD_MULTIRATE_SET_PARAMETERS           = 0xC0,
    HOST_CMD_MULTIRATE_GET_PARAMETERS           = 0xC1,
    HOST_CMD_MULTIRATE_CLEAR_USAGE              = 0xC2,
    HOST_CMD_MULTIRATE_GET_USAGE                = 0xC3
};

#if defined(__MSP430__)  &&  (defined(IEC1107_SUPPORT)  ||  defined(SERIAL_CALIBRATION_SUPPORT))

#if defined(UART_PORT_3_SUPPORT)
serial_msg_buf_t tx_msg[4];
serial_msg_buf_t rx_msg[4];
#elif defined(UART_PORT_2_SUPPORT)
serial_msg_buf_t tx_msg[3];
serial_msg_buf_t rx_msg[3];
#elif defined(UART_PORT_1_SUPPORT)
serial_msg_buf_t tx_msg[2];
serial_msg_buf_t rx_msg[2];
#elif defined(UART_PORT_0_SUPPORT)
serial_msg_buf_t tx_msg[1];
serial_msg_buf_t rx_msg[1];
#endif
#endif


/*******************************************************************************
 *   get_comms_mode(void)
 *
 *   TODO Description
 *
 *************************************************************************/
CommsModeControl get_comms_mode(void)
{
    return gModeSelect;
}

/*******************************************************************************
 *   is_calibration_enabled(void)
 *
 *   TODO Description
 *
 *************************************************************************/
int is_calibration_enabled(void)
{
    return TRUE;
}

#if defined(__MSP430__)  &&  (defined(IEC62056_21_SUPPORT)  ||  defined(IEC1107_SUPPORT)  ||  defined(SERIAL_CALIBRATION_SUPPORT))
/* Interrupt routines to send serial messages. */
/*******************************************************************************
 *   serial_interrupt0(void)
 *
 *   TODO Description
 *
 *   Used for MODEM Communications
 *
 *************************************************************************/
#if defined(UART_PORT_0_SUPPORT)
#if defined(__MSP430_HAS_EUSCI_A0__)
#pragma vector=USCI_A0_VECTOR
__interrupt void serial_interrupt0(void)
{
    switch(__even_in_range(UCA0IV,USCI_UART_UCTXCPTIFG))
    {
		case USCI_UART_UCRXIFG:
		     proc_modem_char();
		break;
		case USCI_UART_UCTXIFG:
			if(get_modem_state() == MDM_LISTENSKT_OPEN)
			{
			    // Send the MODBUS frames
			    pxMBFrameCBTransmitterEmpty(  );
			}
			else
			{
			    // Send the HTTP frames
				UCA0TXBUF = tx_msg[0].buf.uint8[tx_msg[0].ptr++];

				if (tx_msg[0].ptr >= tx_msg[0].len)
				{
					// Stop transmission
					UCA0IE &= ~UCTXIE;
					tx_msg[0].ptr = 0;
					tx_msg[0].len = 0;
				}
			}
		break;
		case USCI_UART_UCSTTIFG:
		break;
		case USCI_UART_UCTXCPTIFG:
		break;
    }

}
#else
#error Device does not have a UART port 0
#endif
#endif

/*******************************************************************************
 *   serial_interrupt1(void)
 *
 *   TODO Decription
 *
 *   To be used for IR Communications
 *
 *************************************************************************/
#if defined(UART_PORT_1_SUPPORT)
#if defined(__MSP430_HAS_EUSCI_A1__)

#ifdef MDSERIALPORT
#pragma vector=USCI_A1_VECTOR
__interrupt void serial_interrupt1(void)
{
    uint8_t ch;
    switch(__even_in_range(UCA1IV,USCI_UART_UCTXCPTIFG))
    {
		case USCI_UART_UCRXIFG:
			ch = UCA1RXBUF;
		break;
		case USCI_UART_UCTXIFG:
			UCA1TXBUF = tx_msg[1].buf.uint8[tx_msg[1].ptr++];
			if (tx_msg[1].ptr >= tx_msg[1].len)
			{
				/* Stop transmission */
				UCA1IE &= ~UCTXIE;
				tx_msg[1].ptr = 0;
				tx_msg[1].len = 0;
			}
		break;
		case USCI_UART_UCSTTIFG:
		break;
		case USCI_UART_UCTXCPTIFG:
		break;
    }

}
#endif
#else
#error Device does not have a UART port 1
#endif
#endif

/**************************************************************************
 *   serial_interrupt2(void)
 *
 *   TODO Decription
 *
 *   Used for Debug Communications and Calibrator
 *
 *************************************************************************/
#if defined(UART_PORT_2_SUPPORT)
#if defined(__MSP430_HAS_EUSCI_A2__)
#pragma vector=USCI_A2_VECTOR
__interrupt void serial_interrupt2(void)

{
    uint8_t ch;
    switch(__even_in_range(UCA2IV,USCI_UART_UCTXCPTIFG))
    {
		case USCI_NONE:
		break;
		case USCI_UART_UCRXIFG:
		ch = UCA2RXBUF;

#if defined(UART2_DLT645_SUPPORT)
		DisableDebug();
		dlt645_rx_byte(DLT645_PORT, ch);
#endif
		break;
		case USCI_UART_UCTXIFG:
			UCA2TXBUF = tx_msg[2].buf.uint8[tx_msg[2].ptr++];

		if (tx_msg[2].ptr >= tx_msg[2].len)
		{
			/* Stop transmission */
			UCA2IE &= ~UCTXIE;
			tx_msg[2].ptr = 0;
			tx_msg[2].len = 0;
		}
		else if (tx_msg[2].ptr >= MAX_MESSAGE_LENGTH) tx_msg[2].ptr = 0;

		break;
		case USCI_UART_UCSTTIFG:
		break;
		case USCI_UART_UCTXCPTIFG:
		break;
	}

}
#else
   #error Device does not have a UART port 2
#endif
#endif

/*******************************************************************************
 *   serial_interrupt3(void)
 *
 *   TODO Decription
 *   Used for LCD display communications and calibrator if needed
 *
 *************************************************************************/
#if defined(UART_PORT_3_SUPPORT)
#if defined(__MSP430_HAS_EUSCI_A3__)
#pragma vector=USCI_A3_VECTOR
__interrupt void serial_interrupt3(void)
{
    uint8_t ch;
    switch(__even_in_range(UCA3IV,USCI_UART_UCTXCPTIFG))
    {
		case USCI_UART_UCRXIFG:
			ch = UCA3RXBUF;
			DisableLCDPort();
			dlt645_rx_byte(3, ch);
		break;
		case USCI_UART_UCTXIFG:
			UCA3TXBUF = tx_msg[3].buf.uint8[tx_msg[3].ptr++];
			if (tx_msg[3].ptr >= tx_msg[3].len)
			{
				/* Stop transmission */
				UCA3IE &= ~UCTXIE;
				tx_msg[3].ptr = 0;
				tx_msg[3].len = 0;
			}
		break;
		case USCI_UART_UCSTTIFG:
		break;
		case USCI_UART_UCTXCPTIFG:
		break;
    }
}

#else
   #error Device does not have a UART port 3
#endif
#endif

/*******************************************************************************
 *   send_message(int port, int len)
 *
 *   TODO Description
 *
 *************************************************************************/
void send_message(int port, int len)
{
  tx_msg[port].ptr = 0;
  tx_msg[port].len = len;

	switch (port)
	{
		#if defined(UART_PORT_1_SUPPORT)
			case 1:
				  UCA1IE |= UCTXIE;
			break;
		#endif
		#if defined(UART_PORT_2_SUPPORT)
			case 2:
			   UCA2IE |= UCTXIE;
			break;
		#endif
		#if defined(UART_PORT_3_SUPPORT)
		  case 3:
			 UCA3IE |= UCTXIE;
		  break;
		#endif
	}
}
#endif


/****************************************************************************
*   queue_readings(void)
*
*   Adds readings to FIFO
*
*   returns : number of messages in FIFO
****************************************************************************/
uint32_t queue_readings(void)
{
	uint32_t lRtnVal;
	readings_t lData;

#if(0)
	  // Test data
	  comms_meter_status = meter_status | (comms_meter_status & 0xE01B);
	  lData.status = comms_meter_status;
	  lData.century = rtc.century;
	  lData.year = rtc.year;
	  lData.month = rtc.month;
	  lData.day = rtc.day;
	  lData.hour = rtc.hour;
	  lData.minute = rtc.minute;
	  lData.second = rtc.second;
	  lData.power_factor = 9999;
	  lData.consumed_energy = 1234567;
	  lData.frequency = 6000;
	  lData.active_power = 1125;
	  lData.reactive_power = 345;
	  lData.temperature = 320;
	  lData.rms_voltage = 12000;
	  lData.rms_current = 4100;
#else

	//comms_meter_status = meter_status | (comms_meter_status & 0xE03B);
	comms_meter_status |= meter_status;
	lData.status = comms_meter_status;
	lData.century = rtc.century;
	lData.year = rtc.year;
	lData.month = rtc.month;
	lData.day = rtc.day;
	lData.hour = rtc.hour;
	lData.minute = rtc.minute;
	lData.second = rtc.second;
	lData.power_factor = current_power_factor(4);
	lData.produced_energy = current_produced_active_energy(4);
	lData.consumed_energy = current_consumed_active_energy(4);
	lData.frequency = current_mains_frequency(0);
	lData.active_power = current_active_power(4);
	lData.reactive_power = current_reactive_power(4);
	lData.temperature = current_temperature();
	lData.rms_voltage = current_rms_voltage(4);
	lData.rms_current = current_rms_current(4);

	lData.BL1V = current_rms_voltage(0);
	lData.BL2V = current_rms_voltage(1);
	lData.BL2I = current_rms_current(1);
	lData.LSPf = current_power_factor(5);
	lData.LSw = current_active_power(5);
	lData.LSvar = current_reactive_power(5);
	lData.LSCKwh = current_consumed_active_energy(5);
	lData.LSPKwh = current_produced_active_energy(5);
	lData.LSL1I = current_rms_current(2);
	lData.LSL2I = current_rms_current(3);
#endif

	lRtnVal = push_msg_fifo((char *) &lData, MSG_READINGS, sizeof(readings_t));

	return lRtnVal;
}

/**********************************************************************
*   void comms_init(void)
*
*   State Machine Init for Comms
*
************************************************************************/
void comms_init(void)
{
	COMMS_DBPRINT("Init Comms:");
	gSendConstFlag = TRUE;
	gQueueDataFlag = FALSE;
	gSendStatusFlag = FALSE;
	gUploadReportFlag = FALSE;
	gCheckConfigFlag = FALSE;
	gFWDownloadFlag = FALSE;
    gOTAFailed = FALSE;

	init_msg_fifo();

	gModeSelect = HTTPOnly;
	gCommsTOFlag = FALSE;
	modem_send_req(&gReqRespFlag, MDM_SWITCH_TO_HTTP);
	start_comms_timer(&gCommsTOFlag, 240);
    gCommsState = W4_MDM_HTTP_MODE;

}

/**********************************************************************
*   comms_tick(void)
*
*   TODO Describe State Machine for Comms
*
************************************************************************/
void comms_tick(void)
{
   int i;

    if ((gQueueDataFlag == TRUE) && ((comms_meter_status & STATUS_ACTIVATED) > 0) && (get_modem_state() != MDM_FW_DOWNLOAD))       // if ready for data and provisioned
     {
        COMMS_DBPRINT("FIFO Queue Readings");
        queue_readings();
        gQueueDataFlag = FALSE;
     }

    switch (gCommsState)
     {
       case W4_MDM_HTTP_MODE:
           if (gCommsTOFlag == TRUE)
           {
               if (gModeSelect == MODBUSEnabled)
               {
                   // Try MODBUS Mode
                   modem_send_req(&gReqRespFlag, MDM_SWITCH_TO_MODBUS);
                   start_comms_timer(&gCommsTOFlag, 30);
                   gModemRebootCount = 0;
                   gCommsState = W4_MDM_MODBUS_MODE;
               }
               else
               {
                 // Reboot the modem
                   modem_send_req(&gReqRespFlag, MDM_SWITCH_TO_HTTP);
                   //modem_send_req(&gReqRespFlag, MDM_REBOOT);   // reboot the modem
                   start_comms_timer(&gCommsTOFlag, 120);
               }
           }
           else
           {
               if (modem_send_req(&gReqRespFlag, MDM_QRY_MODE) == MDM_RESP_HTTP_MODE)
               {
                   COMMS_DBPRINT("MODEM IN HTTP MODE");
                   gCheckConfigFlag = TRUE;
                   gCommsState = HTTP_MODE;
               }
           }
       break;

       case HTTP_MODE:

           if (gUploadReportFlag == TRUE)
           {
               gSentReadings = 0;
               if (modem_send_req(&gReqRespFlag, HTTP_SEND_REPORT) == MDM_RESP_ACCEPT_REQUEST)
                {
                   COMMS_DBPRINT("Sending Report");
                   start_comms_timer(&gCommsTOFlag, 30);
                   gCommsState = W4_REPORT_RESPONSE;
                }
           }
           else if (gCheckConfigFlag == TRUE)
           {
               if (modem_send_req(&gReqRespFlag, HTTP_REQ_UNIT_INFO) == MDM_RESP_ACCEPT_REQUEST)
               {
                   COMMS_DBPRINT("Request Config");
                   start_comms_timer(&gCommsTOFlag, 30);
                   gCommsState = W4_CONFIG_RESPONSE;
               }
           }
           else if(gFWDownloadFlag == TRUE)
           {
               if (modem_send_req(&gReqRespFlag, HTTP_GETLATEST_FW) == MDM_RESP_ACCEPT_REQUEST)
               {
                  COMMS_DBPRINT("Request FW");
                  gOTAFailed = FALSE;
                  start_comms_timer(&gCommsTOFlag, 300);
                  gCommsState = UPDATING_FW;
               }
           }
           else if (gSendStatusFlag == TRUE)
           {
              if (getRelayState() == RLY_READY)
              {
                 if (modem_send_req(&gReqRespFlag, HTTP_SEND_STATUS) == MDM_RESP_ACCEPT_REQUEST)
                 {
                    COMMS_DBPRINT("Sending Status");
                    start_comms_timer(&gCommsTOFlag, 30);
                    gCommsState = W4_STATUS_RESPONSE;
                 }
              }
           }

           if (modem_send_req(&gReqRespFlag, MDM_QRY_MODE) == MDM_RESP_MODBUS_MODE)
           {
               modem_send_req(&gReqRespFlag, MDM_SWITCH_TO_HTTP);
               gCommsState = W4_MDM_HTTP_MODE;
           }

      break;

       case W4_STATUS_RESPONSE:
			if (gCommsTOFlag == TRUE)
			{
				comms_meter_status &= ~COMMS_STATUS_OK;
				COMMS_DBPRINT("* Status Created TO");

	            if (gModeSelect == MODBUSEnabled)
	            {
                    modem_send_req(&gReqRespFlag, MDM_SWITCH_TO_MODBUS);
                    start_comms_timer(&gCommsTOFlag, 120);
                    gModemRebootCount = 0;
                    gCommsState = W4_MDM_MODBUS_MODE;
	            }
	            else
	            {
	               modem_send_req(&gReqRespFlag, MDM_SWITCH_TO_HTTP);
//                   modem_send_req(&gReqRespFlag, MDM_REBOOT);   // reboot the modem
                   start_comms_timer(&gCommsTOFlag, 120);
                   gCommsState = W4_MDM_HTTP_MODE;
	            }
			}
			else if (gReqRespFlag == REPORT_OK)
			{
				comms_meter_status |= COMMS_STATUS_OK;
				COMMS_DBPRINT("Status Created");
				gSendStatusFlag = FALSE;
				gSendConstFlag = 0;
				gOTAFailed = FALSE;

                if (gModeSelect == MODBUSEnabled)
                {
                    modem_send_req(&gReqRespFlag, MDM_SWITCH_TO_MODBUS);
                    start_comms_timer(&gCommsTOFlag, 120);
                    gModemRebootCount = 0;
                    gCommsState = W4_MDM_MODBUS_MODE;
                }
                else
                {
                    gCommsState = HTTP_MODE;
                }
			}
       break;

       case W4_REPORT_RESPONSE:
         if (gCommsTOFlag == TRUE)
         {
        	comms_meter_status &= ~COMMS_STATUS_OK;
        	COMMS_DBPRINT("* Report Created TO\r\n");

            if (gModeSelect == MODBUSEnabled)
            {
                modem_send_req(&gReqRespFlag, MDM_SWITCH_TO_MODBUS);
                start_comms_timer(&gCommsTOFlag, 120);
                gModemRebootCount = 0;
                gCommsState = W4_MDM_MODBUS_MODE;
            }
            else
            {
               modem_send_req(&gReqRespFlag, MDM_SWITCH_TO_HTTP);
               //modem_send_req(&gReqRespFlag, MDM_REBOOT);   // reboot the modem
               start_comms_timer(&gCommsTOFlag, 120);
               gCommsState = W4_MDM_HTTP_MODE;
            }
         }
         else if (gReqRespFlag == REPORT_OK)
         {
        	comms_meter_status |= COMMS_STATUS_OK;
        	COMMS_DBPRINT("Report Created\r\n");
            // Pop Reported Values from FIFO
            for (i = 0; i < gSentReadings; i++) pop_msg_fifo();

            if (getFIFODepth() == 0 )   // FIFO Empty
            {
               gUploadReportFlag = FALSE;

               if (gModeSelect == MODBUSEnabled)
               {
                   modem_send_req(&gReqRespFlag, MDM_SWITCH_TO_MODBUS);
                   start_comms_timer(&gCommsTOFlag, 120);
                   gModemRebootCount = 0;
                   gCommsState = W4_MDM_MODBUS_MODE;
               }
            }

            gCommsState = HTTP_MODE;
          }
       break;

       case W4_CONFIG_RESPONSE:
         if (gCommsTOFlag == TRUE)
         {
        	 comms_meter_status &= ~COMMS_STATUS_OK;
        	 COMMS_DBPRINT("* Get Config TO");
        	 gCheckConfigFlag = FALSE;

        	 if ((gModeSelect == HTTPOnly) && (gSendConstFlag == TRUE)) gUploadReportFlag = TRUE;
        	 else gSendStatusFlag = TRUE;

             gCommsState = HTTP_MODE;
         }
         else if (gReqRespFlag == CONFIG_OK)
         {
      	     Parse_V3_Config((char*) rx_msg[1].buf.uint8);
        	 comms_meter_status |= COMMS_STATUS_OK;
          	 COMMS_DBPRINT("Config Received");
          	 gCheckConfigFlag = FALSE;

          //	 if ((gModeSelect == HTTPOnly) && (gConstFlag == TRUE)) gUploadReportFlag = TRUE;
         //    else gSendStatusFlag = TRUE;
          	 gSendStatusFlag = TRUE;
          	 gCommsState = HTTP_MODE;
         }
       break;

       case UPDATING_FW:
           if ((gCommsTOFlag == TRUE) || (gReqRespFlag == FW_UPDATE_FAIL))
           {
          	   COMMS_DBPRINT("* OTA Update Fail");
          	   gFWDownloadFlag = FALSE;
			   gSendStatusFlag = TRUE;
       		   gOTAFailed = TRUE;

               if (gModeSelect == MODBUSEnabled)
               {
                   modem_send_req(&gReqRespFlag, MDM_SWITCH_TO_MODBUS);
                   start_comms_timer(&gCommsTOFlag, 120);
                   gModemRebootCount = 0;
                   gCommsState = W4_MDM_MODBUS_MODE;
               }
               else
               {
                   gCommsState = HTTP_MODE;
               }
           }
    	break;

       case W4_MDM_MODBUS_MODE:
           if (gCommsTOFlag == TRUE)
           {
               modem_send_req(&gReqRespFlag, MDM_SWITCH_TO_HTTP);
               start_comms_timer(&gCommsTOFlag, 120);
               gCommsState = W4_MDM_HTTP_MODE;
           }
           else
           {
               if (modem_send_req(&gReqRespFlag, MDM_QRY_MODE) == MDM_RESP_MODBUS_MODE)
               {
                  // COMMS_DBPRINT("MODEM IN MODBUS MODE");
                   start_comms_timer(&gCommsTOFlag, nv_sys_conf.s.sample_period * 240);         // Set to 2 times the sampling period
                   gCommsState = MODBUS_MODE;
               }
           }
        break;

       case MODBUS_MODE:
           if (gCommsTOFlag == TRUE)                             // No command in > 2 sample periods
           {
/*               if (gModemRebootCount++ < 3)                      // three trys to reboot
               {
                    modem_send_req(&gReqRespFlag, MDM_REBOOT);   // reboot the modem
                    start_comms_timer(&gCommsTOFlag, 120);
                    gCommsState = W4_MDM_MODBUS_MODE;
               }
               else */
               {
                   modem_send_req(&gReqRespFlag, MDM_SWITCH_TO_HTTP);    // goto http mode
                   start_comms_timer(&gCommsTOFlag, 120);
                   gCommsState = W4_MDM_HTTP_MODE;
               }
           }
           else
           {
               if (modem_send_req(&gReqRespFlag, MDM_QRY_COMMAND_RECEIVED) == MDM_RESP_COMMAND_RECEIVED)  // if MODBUS Command Received
               {
                   start_comms_timer(&gCommsTOFlag, nv_sys_conf.s.sample_period * 240);    // Reset waiting for command timer
               }

               if (modem_send_req(&gReqRespFlag, MDM_QRY_MODE) == MDM_RESP_HTTP_MODE)       // Did MODBUS master request mode change
               {
                   start_comms_timer(&gCommsTOFlag, 120);
                   gCommsState = W4_MDM_HTTP_MODE;
               }
           }

         break;

       default:
    	   comms_init();
       break;
     }

}

/***********************************************************************
 *  Set_SD_Error()
 *
 *
 *
 **********************************************************************/
void Set_SD_Error()
{
	comms_meter_status |= SD_CARD_ERROR;
}

/*********************************************************************
 *   Clear_SD_Error()
 *
 *
 ********************************************************************/
void Clear_SD_Error()
{
	comms_meter_status &=~ SD_CARD_ERROR;
}

/*********************************************************************
 *  SET_CRC_Error()
 *
 *
 ********************************************************************/

void SET_CRC_Error()
{
	comms_meter_status |= STATUS_CRC_ERROR;
}

/*********************************************************************
 *  Clear_CRC_Error()
 *
 *
 ********************************************************************/
void Clear_CRC_Error()
{
	comms_meter_status &=~ STATUS_CRC_ERROR;
}

/********************************************************************
 * set_comms_mode(CommsModeControl aMode)
 *
 * ******************************************************************/
 void set_comms_mode(CommsModeControl aMode)
{
     gModeSelect = aMode;
}
