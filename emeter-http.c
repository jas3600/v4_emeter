/*
 * Telit.c
 *
 *  Created on: Nov 8, 2016
 *      Author: John
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <io.h>
#include <time.h>
#include <emeter-toolkit.h>
#include "emeter-structs.h"
#include "emeter-communication.h"
#include "emeter-http.h"
#include "emeter-fifo.h"
#include "Telit.h"
#include "Debug.h"
#include "OTAUpdate.h"
#include "emeter-relay.h"
#include "emeter-memory.h"
#include "MMC.h"
#include "emeter-rtc.h"

#define POLY 0x8408

uint16_t CRCtrial = 0;

char tempch;
int Update_Cal_Const(char * aCalConstStr);
void update_flash_config(struct system_mem_s * aSysconfig);
unsigned short crc16(char *data_p, unsigned short length);
unsigned short ModbusSampleRate;
int Parse_Config_Date(char * aRecvbuf);

#pragma RETAIN(EmeterFwVersion)
#pragma location=0x6bff0
const unsigned char EmeterFwVersion[9] = EMETER_FIRMWARE_VERSION;

#pragma RETAIN(BSLFwVersion)
#pragma location=0x8bff0
const unsigned char BSLFwVersion[9];

//#define DB_HTTP
#ifdef DB_HTTP
#define HTTP_DBPRINT(...) PrintDebugMessage(__VA_ARGS__)
#else
#define HTTP_DBPRINT(...)
#endif

const unsigned int table[256]={0,41121,41281,480,41601,544,960,41825,42241,
 		1440,1088,42209,1920,42785,42689,1632,43521,2720,2880,44001,2176,
			43041,43457,2400,3840,44961,44609,3808,44417,3360,3264,44129,46081,
			5280,5440,46561,5760,46625,47041,5984,4352,45473,45121,4320,45953,
			4896,4800,45665,7680,48801,48961,8160,48257,7200,7616,48481,47873,
			7072,6720,47841,6528,47393,47297,6240,34817,10400,10560,35297,10880,
			35361,35777,11104,11520,36257,35905,11488,36737,12064,11968,36449,
			8704,33441,33601,9184,32897,8224,8640,33121,34561,10144,9792,34529,
			9600,34081,33985,9312,15360,40097,40257,15840,40577,15904,16320,
			40801,39169,14752,14400,39137,15232,39713,39617,14944,38401,13984,
			14144,38881,13440,37921,38337,13664,13056,37793,37441,13024,37249,
			12576,12480,36961,61441,20640,20800,61921,21120,61985,62401,21344,
			21760,62881,62529,21728,63361,22304,22208,63073,23040,64161,64321,
			23520,63617,22560,22976,63841,65281,24480,24128,65249,23936,64801,
			64705,23648,17408,58529,58689,17888,59009,17952,18368,59233,57601,
			16800,16448,57569,17280,58145,58049,16992,60929,20128,20288,61409,
			19584,60449,60865,19808,19200,60321,59969,19168,59777,18720,18624,
			59489,30720,55457,55617,31200,55937,31264,31680,56161,56577,32160,
			31808,56545,32640,57121,57025,32352,53761,29344,29504,54241,28800,
			53281,53697,29024,30464,55201,54849,30432,54657,29984,29888,54369,
			52225,27808,27968,52705,28288,52769,53185,28512,26880,51617,51265,
			26848,52097,27424,27328,51809,26112,50849,51009,26592,50305,25632,
			26048,50529,49921,25504,25152,49889,24960,49441,49345,24672};

void Get_Config()
{
	uint16_t i;
	char * lGetMsgBuf;
    char lCompany[30];
    char lEnvironment[30];

//  Example:
//	    GET /dev/test/api/units/INTERNALsampleXS1.json HTTP/1.1
//      Host: test.dev.connectder.com
//      Accept: */*


    lGetMsgBuf = (char *) tx_msg[MODEM_PORT].buf.uint8;
    lGetMsgBuf[0]=0;

	sscanf((char *) nv_sys_conf.s.report_URL,"/%30[^/]/%30[^/]",lEnvironment, lCompany);  // Extract the enviroment and company from the report URL

	strcat(lGetMsgBuf,"GET ");
	strcat(lGetMsgBuf,(char *) nv_sys_conf.s.report_URL);
	strcat(lGetMsgBuf,".json?v=5 HTTP/1.1\r\n");

	strcat(lGetMsgBuf,"HOST: ");
	strcat(lGetMsgBuf,lCompany);
	strcat(lGetMsgBuf,".");
	strcat(lGetMsgBuf,lEnvironment);
	strcat(lGetMsgBuf,".connectder.com\r\n");
	strcat(lGetMsgBuf,"ACCEPT: */*\r\n");
	strcat(lGetMsgBuf,"\r\n");

	i = strlen(lGetMsgBuf);

    send_modem_message(i);
}

void Get_Firmware()
{
	uint16_t i;
	char * lGetMsgBuf;
    char lCompany[30];
    char lEnvironment[30];

// Example:
	// GET /dev/test/api/units/INTERNALsampleXS1.json HTTP/1.1
    // Host: test.dev.connectder.com
    // Accept: */*
//

    lGetMsgBuf = (char *) tx_msg[MODEM_PORT].buf.uint8;
    lGetMsgBuf[0]=0;

	sscanf((char *) nv_sys_conf.s.report_URL,"/%30[^/]/%30[^/]",lEnvironment, lCompany);  // Extract the environment and company from the report URL

	sprintf(lGetMsgBuf,"GET /%s/%s/firmwares/%s.json HTTP/1.1\r\n", lEnvironment, lCompany, gNewFWVersion);

	strcat(lGetMsgBuf,"HOST: ");
	strcat(lGetMsgBuf,lCompany);
	strcat(lGetMsgBuf,".");
	strcat(lGetMsgBuf,lEnvironment);
	strcat(lGetMsgBuf,".connectder.com\r\n");
	strcat(lGetMsgBuf,"ACCEPT: */*\r\n");
	strcat(lGetMsgBuf,"\r\n");

	i = strlen(lGetMsgBuf);

    send_modem_message(i);
}

/***************************************************************************
   BuildPOSTHeader()

   Packages and sends readings report for promptworks
****************************************************************************/
void BuildPOSTHeader()
{
	// Example:
		// POST /dev/test/api/units/INTERNALsampleXS1/report HTTP/1.1
	    // Host: test.dev.connectder.com
	    // Accept: */*
	    // Content-Type: application/json
	    // Content-Length:
	//

	char lCompany[30];
    char lEnvironment[30];
	char * lPostMsgBuf;

    lPostMsgBuf = (char *) tx_msg[MODEM_PORT].buf.uint8;
    lPostMsgBuf[0]=0;

	sscanf((char *) nv_sys_conf.s.report_URL,"/%30[^/]/%30[^/]", lEnvironment, lCompany);  // Extract the enviroment and company from the report URL

	strcat(lPostMsgBuf,"POST ");
	strcat(lPostMsgBuf,(char *) nv_sys_conf.s.report_URL);
	strcat(lPostMsgBuf,"/report HTTP/1.1\r\n");

	strcat(lPostMsgBuf,"HOST: ");
	strcat(lPostMsgBuf,lCompany);
	strcat(lPostMsgBuf,".");
	strcat(lPostMsgBuf,lEnvironment);
	strcat(lPostMsgBuf,".connectder.com\r\n");
    strcat(lPostMsgBuf, "Accept: */*\r\n");
    strcat(lPostMsgBuf, "Content-Type: application/json\r\n");
    strcat(lPostMsgBuf, "Content-Length: ");
 
return;
}

/***************************************************************************
   Send_Status()

   Packages and sends status json report
****************************************************************************/
int Send_Status()
{
    int i;
    char lContentBuff[850];
    char lContentLength[10];
    char * s;
	char * lPostMsgBuf;
	char lTemp[25];
	unsigned int lSts;

	const unsigned int CintAddress = 0xfffe;
	unsigned int BSLAddressValue = *(unsigned int *)CintAddress;
    lPostMsgBuf = (char *) tx_msg[MODEM_PORT].buf.uint8;

    BuildPOSTHeader();

    s = lContentBuff;

    s += sprintf(s,"{\"VER\":\"3\",");     // Record Version

    memset(lTemp, 0, 25);
    strncpy(lTemp, nv_sys_conf.s.property_number, 18);
    s += sprintf(s,"\"SNM\":\"%s\",", lTemp);          // Unit ID

    s += sprintf(s,"\"ST\":\"%d%02d-%02d-%02d %02d:%02d:%02dZ\",",                  // Report Time
                   rtc.century, rtc.year, rtc.month,
                   rtc.day, rtc.hour, rtc.minute,
                   rtc.second);

   // comms_meter_status = meter_status | (comms_meter_status & 0xE03B);
    lSts = (comms_meter_status | 0xC000 | meter_status) & 0xE6B3;
    s += sprintf(s,"\"SS\":\"%04X\",", lSts);                          // Current Status

    strncpy(lTemp, nv_sys_conf.s.model_number, 18);

    s += sprintf(s,"\"MDL\":\"%sA\",", lTemp);            // Model Number

    s += sprintf(s,"\"OC\":\"%d\",", nv_sys_conf.s.op_config);            // Operating Config

    s += sprintf(s,"\"IPA\":\"%u.%u.%u.%u\",", gIPA[0],gIPA[1],gIPA[2],gIPA[3]);    // IP Address

    strncpy(lTemp, EmeterFwVersion, 8);
	s += sprintf(s,"\"FWV\":\"%s\",", EmeterFwVersion);                            // Firmware Version

    s += sprintf(s,"\"CSQ\":\"%d\",", GetSignalQuality());                          // CSQ

    if (gSendConstFlag == TRUE)									// Calibration Constants Send Here
    {
        s += sprintf(s,"\"IMEI\":\"%s\",", gIMEI);                                      // IMEI

    	s += sprintf(s,"\"SIM\":\"%s\",", gICCID);                            // ICCID

    	s += sprintf(s,"\"MFW\":\"%s\",", gMFW);                            // Modem FW

    	s += sprintf(s,"\"CC\":\"");
    	for (i=0; i<128; i++)
    	{
    		s += sprintf(s,"%.2x",nv_parms.x[i]);
    	}
    	s += sprintf(s,"\",");
    }

    if	(gOTAFailed == TRUE || gSendConstFlag == TRUE)						// Status String
    {
    	s += sprintf(s,"\"CS\":\"");

    	if (gOTAFailed == TRUE)
    	{
    		s += sprintf(s, "OTA Fail:%s,", OTAFailMsg); //Send OTA failed due to SD CRC ERROR
    	}

    	if (gSendConstFlag == TRUE)
    	{
    		if((nv_parms.x[0] == 0xff) && (nv_parms.x[1] == 0xff))
    		{
    			s += sprintf(s," NoCal");
    		}
    		s += sprintf(s,", BootFw:%s", EmeterFwVersion);  //Drny boot FW , signal strength and boot reason to UI at first
    		if (get_comms_mode() == HTTPOnly)
    		{
    		    s += sprintf(s,", HTTPOnly");
    		}
    		else
    		{
    		    s += sprintf(s,", Modbus");
    		}
    		s += sprintf(s,", %s", gBootMsg);
    		s += sprintf(s,", SS:%04X", lSts);
    		memset(lTemp, 0, 25);
    		if (BSLFwVersion[0] == 0xFF)
    		{
    		    strcpy(lTemp, "NA");
    		}
    		else
    		{
    		    strncpy(lTemp, BSLFwVersion, 8);
    		}
    		s += sprintf(s,", BSL:%s", lTemp);
    	}
    	s += sprintf(s,"\",");
    }

    if (nv_sys_conf.s.op_config == 1)
    {
      s += sprintf(s,"\"form\":\"2S\"}");                            // FORM
    }
    else
    {
      s += sprintf(s,"\"form\":\"12S\"}");
    }

    i = strlen(lContentBuff);

    sprintf(lContentLength,"%d", i);    //

    strcat(lPostMsgBuf, lContentLength);
    strcat(lPostMsgBuf, "\r\n\r\n");

    strcat(lPostMsgBuf, lContentBuff);

    i = strlen(lPostMsgBuf);

 //   PrintDebugMessage(lPostMsgBuf);

    send_modem_message(i);

return 1;
}


/***************************************************************************
   Send_Report(void)

   Packages and sends multiple readings report for promptworks
****************************************************************************/

int Send_Report()
{
    uint16_t i,lNumReadings;
    uint32_t sramloc;
    readings_t lReadings[MAX_READINGS_PER_REPORT];
    float lTemp;
    double lTemp1;
    char lContentBuf[800];
    char lContentLength[10];
    fifomsg_t lfifomsg;
    char lTemp3[25];
    char lTemp2[10];
    unsigned int lSts;

    char *s;
	char * lPostMsgBuf;

	if (rtc.century < 20) return 0;

	lPostMsgBuf = (char *) tx_msg[MODEM_PORT].buf.uint8;

/*    for (i = 0; i < 4; i++)
    {
		lReadings[i].active_power = 1125;
		lReadings[i].century = 20;
		lReadings[i].consumed_energy = 1234567;
		lReadings[i].day = 24;
		lReadings[i].frequency = 6000;
		lReadings[i].hour = 15;
		lReadings[i].minute = 45;
		lReadings[i].month = 9;
		lReadings[i].power_factor = 9999;
		lReadings[i].reactive_power = 345;
		lReadings[i].rms_current = 4100;
		lReadings[i].rms_voltage = 12000;
		lReadings[i].second = 12;
		lReadings[i].status = 0x1234;
		lReadings[i].temperature = 320;
		lReadings[i].year = 16;
   } */

    // Build Content
    for (i = 0; ((i < MAX_READINGS_PER_REPORT) && (peek_msg_fifo((char *) &lReadings[i], &lfifomsg, i+1, &sramloc) > 0)); i++);

    lNumReadings = i;

    if (lNumReadings > 0)
    {

        BuildPOSTHeader();


		s = lContentBuf;

		s += sprintf(s,"{\"VER\":\"3\",");     // Record Version

		memset(lTemp3,0,25);
		strncpy(lTemp3, nv_sys_conf.s.property_number, 18);
	    s += sprintf(s,"\"SNM\":\"%s\",", lTemp3);          // Unit ID

		s += sprintf(s,"\"ST\":\"%d%02d-%02d-%02d %02d:%02d:%02dZ\",",               // Report Time
					   rtc.century, rtc.year, rtc.month,
					   rtc.day, rtc.hour, rtc.minute,
					   rtc.second);

	//	comms_meter_status = meter_status | (comms_meter_status & 0xE03B);
	    lSts = (comms_meter_status | 0xC000 | meter_status) & 0xE6B3;
		s += sprintf(s,"\"SS\":\"%04X\",", lSts);                           // Current Status

	    strncpy(lTemp3, nv_sys_conf.s.model_number, 18);
	    s += sprintf(s,"\"MDL\":\"%sA\",", lTemp3);            // Model Number

		s += sprintf(s,"\"IPA\":\"%u.%u.%u.%u\",", gIPA[0],gIPA[1],gIPA[2],gIPA[3]);       // IP Address

		strcpy(lTemp2,EmeterFwVersion);
		s += sprintf(s,"\"FWV\":\"%s\",", lTemp2 );                             // Firmware Version

		s += sprintf(s,"\"samples\":{\"ST\":[");         // Samples

		for (i = 0; i < lNumReadings; i++)
		{
		  s += sprintf(s,"\"%d%02d-%02d-%02d %02d:%02d:%02dZ\",",
					   lReadings[i].century, lReadings[i].year, lReadings[i].month,
					   lReadings[i].day, lReadings[i].hour, lReadings[i].minute,
					   lReadings[i].second);
		}
		*(s-1) = ']';

		s += sprintf(s,",\"PF\":[");   // Power Factor
		for (i = 0; i< lNumReadings; i++)
		{
		  lTemp = lReadings[i].power_factor;
		  s += sprintf(s,"%.2f,", lTemp/10000.0);
		}
		*(s-1) = ']';

		s += sprintf(s,",\"F\":[");   // Frequency
		for (i = 0; i< lNumReadings; i++)
		{
		  lTemp = lReadings[i].frequency;
		  s += sprintf(s,"%.2f,", lTemp/100.0);
		}
		*(s-1) = ']';

		s += sprintf(s,",\"W\":[");   // Active Power
		for (i = 0; i< lNumReadings; i++)
		{
		  lTemp = lReadings[i].active_power;
		  s += sprintf(s,"%.2f,", lTemp/100.0);
		}
		*(s-1) = ']';

		s += sprintf(s,",\"R\":[");   // Reactive Power
		for (i = 0; i< lNumReadings; i++)
		{
		  lTemp = lReadings[i].reactive_power;
		  s += sprintf(s,"%.2f,", lTemp/100.0);
		}
		*(s-1) = ']';

		s += sprintf(s,",\"T\":[");   // Temperature
		for (i = 0; i< lNumReadings; i++)
		{
		  lTemp = lReadings[i].temperature;
		  s += sprintf(s,"%.1f,", lTemp/16);
		}
		*(s-1) = ']';

		s += sprintf(s,",\"PWH\":[");   // Produced Energy
		for (i = 0; i< lNumReadings; i++)
		{
			lTemp1 = lReadings[i].produced_energy;
			lTemp1 = lTemp1 / 1000;
			 s += sprintf(s,"%.2f,", lTemp1);
		}

		*(s-1) = ']';
		s += sprintf(s,",\"CWH\":[");   // Consumed Energy
		for (i = 0; i< lNumReadings; i++)
		{
			lTemp1 = lReadings[i].consumed_energy;
			lTemp1 = lTemp1 / 1000;
			s += sprintf(s,"%.2f,", lTemp1);
		}
		*(s-1) = ']';

		s += sprintf(s,",\"V\":[");   // RMS Voltage
		for (i = 0; i< lNumReadings; i++)
		{
		  lTemp = lReadings[i].rms_voltage;
		  s += sprintf(s,"%.1f,", lTemp/100.0);
		}
		*(s-1) = ']';

		s += sprintf(s,",\"I\":[");   // RMS Current
		for (i = 0; i< lNumReadings; i++)
		{
		  lTemp = lReadings[i].rms_current;
		  s += sprintf(s,"%.3f,", lTemp/1000.0);
		}
		*(s-1) = ']';

		s += sprintf(s,",\"SS\":[");   // Status
		for (i = 0; i< lNumReadings; i++)
		{
		  lSts = (0xC000 | lReadings[i].status) & 0xE6B3;
		  s += sprintf(s,"\"%04X\",", lSts);
		}
		*(s-1) = ']';

		s+=sprintf(s,"}");

	/*	if(PsramFlag == TRUE)
		{
			s += sprintf(s,",\"CS\":");
			s += sprintf(s,"%s %s %s","\"Data Error: ",nv_sys_conf.s.property_number,"\"");
		} */

		s+=sprintf(s,"}");

		i = strlen(lContentBuf);
		sprintf(lContentLength,"%d", i);
		strcat(lContentBuf,"\r\n");


		strcat(lPostMsgBuf, lContentLength);
		strcat(lPostMsgBuf, "\r\n\r\n");

		strcat(lPostMsgBuf, lContentBuf);

		i = strlen(lPostMsgBuf);

	//	gSentReadings = lNumReadings;
	//    PrintDebugMessage(lPostMsgBuf);

		send_modem_message(i);
    }

return lNumReadings;
}


int gCreated;

/***************************************************************************
   Parses the POST response from HTTP server

   Packages and sends multiple readings report for promptworks
****************************************************************************/
int parse_http_resp(char * aRecvbuf)
{
  int lRtv;
  
  lRtv = 0;

  if (strstr(aRecvbuf, "Created") != NULL)
  {
	  gCreated = 1;
	  lRtv = REPORT_OK;
  }

  return lRtv;
}

/***************************************************************************
   Parses the Date from Config

   2019-08-23T17:06:34Z

****************************************************************************/
int Parse_Config_Date(char * aRecvbuf)
{
  int lNumConversions;
  int lyear;
  int sec,min,hour,day,mon,year,cent;
  int lRtv;
  time_t old_time;
  time_t now_time;
  struct tm tm_buf;
  rtc_t old_rtc;
  char lDbStr[50];

  lRtv = 0;

  old_rtc.century = rtc.century;
  old_rtc.year = rtc.year;
  old_rtc.month = rtc.month;
  old_rtc.day = rtc.day;
  old_rtc.hour = rtc.hour;
  old_rtc.minute = rtc.minute;
  old_rtc.second = rtc.second;

  lNumConversions = sscanf((char *) aRecvbuf,"%d-%d-%dT%d:%d:%d",&lyear, &mon, &day, &hour, &min, &sec);

  year = lyear - 2000;
  cent = (lyear - year) / 100;

  if (lNumConversions == 6)   // Update the RTC
  {
    if ( cent == 20 &&
            year >= 17 && year <= 99 &&
            mon >= 1 && mon <= 12 &&
            day>= 1 && day <= 31 &&
            hour >= 0 && hour <= 24 &&
            min >= 0 && min <= 60 &&
            sec >= 0 && sec <= 60)
    {
       // determine time delta
  /*     tm_buf.tm_year = ((int) (rtc.century - 19) * 100) + (int) rtc.year;
       tm_buf.tm_mon = rtc.month - 1;
       tm_buf.tm_mday = rtc.day;
       tm_buf.tm_hour = rtc.hour;
       tm_buf.tm_min = rtc.minute;
       tm_buf.tm_sec = rtc.second;
       tm_buf.tm_isdst = -1;

       old_time = mktime(&tm_buf);

       tm_buf.tm_year = ((int) (cent - 19) * 100) + (int) year;
       tm_buf.tm_mon = mon - 1;
       tm_buf.tm_mday = day;
       tm_buf.tm_hour = hour;
       tm_buf.tm_min = min;
       tm_buf.tm_sec = sec;
       tm_buf.tm_isdst = -1;

       now_time = mktime(&tm_buf);

       if (old_time < now_time)
       { */
            rtc.century = cent;
            rtc.year = year;
            rtc.month = mon;
            rtc.day = day;
            rtc.hour = hour;
            rtc.minute = min;
            rtc.second = sec;
            set_rtc_sumcheck();

           //   if (FIFOTimeStampsValid() == FALSE) FixFIFOTime();
           if (FIFOTimeValid() == FALSE) FixFIFOTime(old_rtc);
    //   }
           sprintf(lDbStr, "Set RTC to: %02d/%02d/%02d,%02d:%02d:%02d", year, mon, day, hour, min, sec);
           HTTP_DBPRINT(lDbStr);
    }
    else
    {
        HTTP_DBPRINT("Time Parse Error!");
    }
  }

  return  lRtv;
}

/***************************************************************************
   Parses the Date from HTTP server

   Packages and sends multiple readings report for promptworks
****************************************************************************/
int Parse_Date(char * aRecvbuf)
{
  int lNumConversions;
  int lyear;
  int sec,min,hour,day,mon,year,cent, i;
  char strmon[5];
  int lRtv;
  time_t old_time;
  time_t now_time;
  struct tm tm_buf;
  rtc_t old_rtc;
  
  lRtv = 0;

  old_rtc.century = rtc.century;
  old_rtc.year = rtc.year;
  old_rtc.month = rtc.month;
  old_rtc.day = rtc.day;
  old_rtc.hour = rtc.hour;
  old_rtc.minute = rtc.minute;
  old_rtc.second = rtc.second;

  const char months[12][4] = { "Jan","Feb","Mar","Apr","May","Jun",
                               "Jul","Aug","Sep","Oct","Nov","Dec" };

  lNumConversions = sscanf((char *) aRecvbuf,"%*[^,], %d %s %d %d:%d:%d",&day,strmon,&lyear,&hour,&min,&sec);

  year = lyear - 2000;
  cent = (lyear - year) / 100;

  mon = 0;
  for (i = 0; ((i < 12) && (mon == 0)); i++)
  {
    if (strncmp(strmon, months[i], 3) == 0)
    {
      mon = i+1;
    }
  }
  
  if ((lNumConversions == 6) && (mon > 0))   // Update the RTC
  {
        if ( cent == 20 &&
                year >= 17 && year <= 99 &&
                mon >= 1 && mon <= 12 &&
                day>= 1 && day <= 31 &&
                hour >= 0 && hour <= 24 &&
                min >= 0 && min <= 60 &&
                sec >= 0 && sec <= 60)
        {
           // determine time delta
           tm_buf.tm_year = ((int) (rtc.century - 19) * 100) + (int) rtc.year;
           tm_buf.tm_mon = rtc.month - 1;
           tm_buf.tm_mday = rtc.day;
           tm_buf.tm_hour = rtc.hour;
           tm_buf.tm_min = rtc.minute;
           tm_buf.tm_sec = rtc.second;
           tm_buf.tm_isdst = -1;

           old_time = mktime(&tm_buf);

           tm_buf.tm_year = ((int) (cent - 19) * 100) + (int) year;
           tm_buf.tm_mon = mon - 1;
           tm_buf.tm_mday = day;
           tm_buf.tm_hour = hour;
           tm_buf.tm_min = min;
           tm_buf.tm_sec = sec;
           tm_buf.tm_isdst = -1;

           now_time = mktime(&tm_buf);

           if (old_time < now_time)
           {
                rtc.century = cent;
                rtc.year = year;
                rtc.month = mon;
                rtc.day = day;
                rtc.hour = hour;
                rtc.minute = min;
                rtc.second = sec;
                set_rtc_sumcheck();

               //   if (FIFOTimeStampsValid() == FALSE) FixFIFOTime();
               if (FIFOTimeValid() == FALSE) FixFIFOTime(old_rtc);
           }
        }
  }

  return  lRtv;
}

/***************************************************************************
   Update_Cal_Const(char * lCalConstStr)

   Updates the Cal constants

****************************************************************************/
int Update_Cal_Const(char * aCalConstStr)
{
    char lByteStr[3];
    uint8_t lCalConst[128];
    uint8_t lbyte;
    int lConstChanged;
    int i;

    lConstChanged = FALSE;

    HTTP_DBPRINT("Cal Const Found");
    // convert the string to bytes
    for (i = 0; i < 128; i++)
    {
        // convert the string to bytes
        strncpy(lByteStr, aCalConstStr + (i * 2), 2);
        sscanf(lByteStr,"%x", &lbyte);
        lCalConst[i] = lbyte;
        // check if the cal consts are different
        if (lCalConst[i] != nv_parms.x[i]) lConstChanged = TRUE;
    }

    if (lConstChanged == TRUE)
    {
        // Update the flash
        flash_clr(nv_parms.x);
        flash_memcpy(nv_parms.x, lCalConst, 128);

        HTTP_DBPRINT("Cal Const Updated");
    }
    else
    {
        HTTP_DBPRINT("Cal Const No Change");
    }

    gSendConstFlag = TRUE;

return lConstChanged;
}

/***************************************************************************
   Parse_V3_Config(char* GetConfig_RxBuff)

   Parses the V3 Config response from HTTP server

   Packages and sends multiple readings report for promptworks
****************************************************************************/
uint16_t gNumConversions;

int Parse_V3_Config(char* GetConfig_RxBuff)
{
    int lRtv;
    struct system_mem_s lTempConfig;
    char lFWVersion[10]="0";
    //char gDbgMsg[30];
	char* tmpstr;
    uint16_t lRecord_Version;
    uint16_t lStatus;
    uint16_t lIPA[4];
    uint16_t lDNS1[4];
    uint16_t lDNS2[4];
    uint16_t i;
    uint32_t lConsumedWh;
    uint32_t lProducedWh;
    char lTime[20];
    char lCalConstStr[257];
    int crcOK;

    lRtv = CONFIG_OK;

    lRecord_Version = 5;

    memcpy(&lTempConfig, &nv_sys_conf, 128);

    crcOK = FALSE;

	tmpstr=strstr((char*)GetConfig_RxBuff,"VER");
	if(tmpstr != NULL)
	{
		sscanf(tmpstr,"VER\"%*[^\"]\"%d",&lRecord_Version);
	}

	if (lRecord_Version >= 3)
	{
	   crcOK=ConfigCRC((char*)GetConfig_RxBuff);

	}
	else
	{
		crcOK=FALSE;
	}

	if (crcOK == TRUE)
	{

	    if (lRecord_Version >= 4)
	    {
	        tmpstr=strstr((char*)GetConfig_RxBuff,"TM");
	        if(tmpstr != NULL)
	        {
	            sscanf(tmpstr,"TM\"%*[^\"]\"%60[^\"]", lTime);
	            if (strlen(lTime) == 20) Parse_Config_Date(lTime);
	        }

            tmpstr=strstr((char*)GetConfig_RxBuff,"CC");
            if(tmpstr != NULL)
            {
                sscanf(tmpstr,"CC\"%*[^\"]\"%256[^\"]", lCalConstStr);
                if (strlen(lCalConstStr) == 256) Update_Cal_Const(lCalConstStr);
            }
	    }

		tmpstr=strstr((char*)GetConfig_RxBuff,"SRT");
		if(tmpstr != NULL)
		{
			sscanf(tmpstr,"SRT\"%*[^\"]\"%d",&lTempConfig.s.sample_period);

			//ModbusSampleRate = lTempConfig.s.sample_period;
			//lTempConfig.s.sample_period = 15; //HTTP sample rate is set to 15 minutes, this variable is now being used by modbus

			if ( lTempConfig.s.sample_period < 1 )
			{
				return CONFIG_ERR;
			}
		}

		tmpstr=strstr((char*)GetConfig_RxBuff,"RMN");
		if(tmpstr != NULL)
		{
			sscanf(tmpstr,"RMN\"%*[^\"]\"%d",&lTempConfig.s.report_minute);

			if ( lTempConfig.s.report_minute > 59 )
			{
				return CONFIG_ERR;
			}
		}
		else
		{
			lTempConfig.s.report_minute = 2;
		}


		tmpstr=strstr((char*)GetConfig_RxBuff,"RRT");
		if(tmpstr != NULL)
		{
		   sscanf(tmpstr,"RRT\"%*[^\"]\"%d", &lTempConfig.s.report_period);



		   if ( lTempConfig.s.report_period < lTempConfig.s.sample_period )
		   {
				return CONFIG_ERR;
		   }

		  // lTempConfig.s.report_period = 5;
		}

		tmpstr=strstr((char*)GetConfig_RxBuff,"FW");
		if(tmpstr != NULL)
		{
			sscanf(tmpstr, "FW\"%*[^\"]\"%60[^\"]", lFWVersion);
			 // If the FW version is different set flag for FW update.
			if (strcmp(lFWVersion, "0") != 0)
			{
				 if (strcmp(lFWVersion, EmeterFwVersion)!=0)
				 {
					 // Save the new FW version the Comms State Machine Sees the change
					sprintf(gDbgMsg,"Update from %s --> %s",EmeterFwVersion,lFWVersion);
					HTTP_DBPRINT(gDbgMsg);
					if (sdcard_installed() == 1)
					{
						strcpy(gNewFWVersion,lFWVersion);
						gFWDownloadFlag = TRUE;
						HTTP_DBPRINT("SD  Detected");
					}
					else
					{
						HTTP_DBPRINT("SD Not Detected");
					}
				 }
			}
		}


		tmpstr=strstr((char*)GetConfig_RxBuff,"CD");
		if(tmpstr != NULL)
		{
			sscanf(tmpstr,"CD\"%*[^\"]\"%d", &lTempConfig.s.close_delay);
		}

        if (get_comms_mode() == HTTPOnly)
        {
            tmpstr=strstr((char*)GetConfig_RxBuff,"RLY");
            if(tmpstr != NULL)
            {
               if (strstr(tmpstr,"\"Shut\"") != NULL)
               {
                      comms_meter_status |= STATUS_DISCONNECT_CLOSED;              // Close relay
                      switch_relay(RELAY_SHUT);
               }
               else if (strstr(tmpstr,"\"Open\"") != NULL)
               {
                     comms_meter_status &= ~STATUS_DISCONNECT_CLOSED;             // Open relay
                     switch_relay(RELAY_OPEN);
               }
               else
               {
                  return CONFIG_ERR;
               }
            }
        }

		tmpstr = strstr((char*)GetConfig_RxBuff,"MODE");
		if(tmpstr != NULL)
		{
			if(strstr(tmpstr,"\"HTTP\"") != NULL)
			{
			    set_comms_mode(HTTPOnly);
				HTTP_DBPRINT("UI CONFIG HTTP Mode Only");
			}
			else if(strstr(tmpstr,"\"Modbus\"") != NULL)
			{
			    set_comms_mode(MODBUSEnabled);
				HTTP_DBPRINT("UI CONFIG Modbus Mode Enabled");

			}
		}
		else //if(strstr(tmpstr,"\"Modbus\"") != NULL)
		{
		    set_comms_mode(HTTPOnly);
			HTTP_DBPRINT("UI CONFIG Mode NA");
		}

		tmpstr=strstr((char*)GetConfig_RxBuff,"STS");
		if(tmpstr != NULL)
		{
		   sscanf(tmpstr,"STS\"%*[^\"]\"%x", &lStatus);

		   comms_meter_status &= ~(STATUS_AUTHORIZED | STATUS_ACTIVATED);     // Clear Provisioning States

		   if ((lStatus & STATUS_AUTHORIZED) > 0)
		   {
			   if ((comms_meter_status & ORIENTATION_OK) > 0) comms_meter_status |= (STATUS_AUTHORIZED | STATUS_ACTIVATED);   // If Authorized then make active
			   else  comms_meter_status |= STATUS_AUTHORIZED;
		   }
		   else
		   {
			   Unprovision();
		   }
		}
	/*	else
		{
		   lStatus |= 0x4000;
		}

		if ((lStatus & 0x4000) != 0) comms_meter_status |= 0x8000; */


		tmpstr=strstr((char*)GetConfig_RxBuff,"rURL");
		if(tmpstr != NULL)
		{
		  sscanf(tmpstr,"rURL\"%*[^\"]\"%60[^\"]", (char *) &lTempConfig.s.report_URL);
		}
		else
		{
			return CONFIG_ERR;
		}

		tmpstr=strstr((char*)GetConfig_RxBuff,"IPA");
		if(tmpstr != NULL)
		{
			  sscanf(tmpstr,"IPA\"%*[^\"]\"%u.%u.%u.%u", &lIPA[0],&lIPA[1],&lIPA[2],&lIPA[3]);
			  // TODO If we would like, we could compare to the Modem IP address for validation. But not today....

		}

		tmpstr=strstr((char*)GetConfig_RxBuff,"DNS");
		if(tmpstr != NULL)
		{
			  gNumConversions = sscanf(tmpstr,"DNS\"%*[^\"]\"%u.%u.%u.%u\"%*[^\"]\"%u.%u.%u.%u",
														 &lDNS1[0],&lDNS1[1],&lDNS1[2],&lDNS1[3],
														 &lDNS2[0],&lDNS2[1],&lDNS2[2],&lDNS2[3]);
			if (gNumConversions == 8)
			{
			  for (i=0; i < 4; i++)
			  {
				if ((lDNS1[i] < 256) && (lDNS2[i] < 256))
				{
				  lTempConfig.s.DNS_Address_1[i] = lDNS1[i];
				  lTempConfig.s.DNS_Address_2[i] = lDNS2[i];
				}
				else
				{
					return CONFIG_ERR;
				}
			  }

			}
			else
			{
				return CONFIG_ERR;
			}


		}

		tmpstr=strstr((char*)GetConfig_RxBuff,"SWH");
		if(tmpstr != NULL)
		{
			sscanf(tmpstr,"SWH\"%*[^\"]\"%lu\",\"%lu", &lConsumedWh, &lProducedWh);
			total_consumed_active_energy = lConsumedWh;
			total_produced_active_energy = lProducedWh;
		}

		tmpstr=strstr((char*)GetConfig_RxBuff,"WHSWH");
		if(tmpstr != NULL)
		{
			sscanf(tmpstr,"WHSWH\"%*[^\"]\"%lu\",\"%lu", &lConsumedWh, &lProducedWh);
			wh_total_consumed_active_energy = lConsumedWh;
			wh_total_produced_active_energy = lProducedWh;

		}


		 if ( lRtv == CONFIG_OK)
		 {
		   // Copy the items not parsed
			 strcpy(lTempConfig.s.model_number, nv_sys_conf.s.model_number);
			 strcpy(lTempConfig.s.property_number, nv_sys_conf.s.property_number);

			 lTempConfig.s.op_config = nv_sys_conf.s.op_config;
			 lTempConfig.s.config_version = nv_sys_conf.s.config_version;

			 lTempConfig.s.cfg_update_time.century = rtc.century;
			 lTempConfig.s.cfg_update_time.year = rtc.year;
			 lTempConfig.s.cfg_update_time.month = rtc.month;
			 lTempConfig.s.cfg_update_time.day = rtc.day;
			 lTempConfig.s.cfg_update_time.hour = rtc.hour;
			 lTempConfig.s.cfg_update_time.minute = rtc.minute;
			 lTempConfig.s.cfg_update_time.second = rtc.second;
			 lTempConfig.s.serial_number = nv_sys_conf.s.serial_number;

			 update_flash_config(&lTempConfig);
		 }
	}

	return lRtv;

}

void update_flash_config(struct system_mem_s * aSysconfig)
{
	flash_unlockA();
	_DINT();
	flash_clr((int*) nv_sys_conf.x);

    flash_memcpy((char*) nv_sys_conf.x, (char*) &(aSysconfig->x[0]), sizeof(struct system_mem_s));
    flash_lockA();
   _EINT();
}


/**********************************************************************************************************
 *
 *
 *
 *********************************************************************************************************/
enum {
	W4_FW_START,
	GET_CHUNK_LEN,
	GET_CHUNK,
	GET_ALL_FW
};

unsigned long int gChunkCnt;
int gChunked;
unsigned long int gChunkLen;
uint32_t gSRAM_Idx;
int gDechunkerState;

extern serial_msg_buf_t rx_msg[4];

void init_Dechunker(void)
{
	gDechunkerState = W4_FW_START;
	rx_msg[MODEM_PORT].ptr = 0;
	gSRAM_Idx = 0;
	gChunked = TRUE;
}

char dbgch;

int proc_FW_Chunks(char ch)
{

   int lRtv;

   lRtv = FW_STILL_DOWNLOADING;

   switch (gDechunkerState)
   {
	   case W4_FW_START:
			if (rx_msg[MODEM_PORT].ptr < (MAX_MESSAGE_LENGTH - 1))
			{
			    rx_msg[MODEM_PORT].buf.uint8[rx_msg[MODEM_PORT].ptr++] = ch;    // add the character to the buffer

				if (ch == '\n')                                                 // process the new line
				{
					if (rx_msg[MODEM_PORT].ptr == 2)                             //  Two chars = empty line /r/n  Empty Line before the FW
					{
						if (gChunked == TRUE)
						{
						    gDechunkerState = GET_CHUNK_LEN;
						}
						else
						{
							gChunkCnt = 0;
							gDechunkerState = GET_ALL_FW;
						}
					}
					else
					{
						if (strncmp((char*) rx_msg[MODEM_PORT].buf.uint8,"Content-Length:",15) == 0)             // if there is a content length then not chunked
						{
                            sscanf((char*) rx_msg[MODEM_PORT].buf.uint8, "Content-Length: %ld", &gChunkLen);
							gChunked = FALSE;
						}
					}

					rx_msg[MODEM_PORT].ptr = 0;                                   // clear the rx buffer for more lines
				}
			}
			else
			{
				rx_msg[MODEM_PORT].ptr = 0;
				lRtv = FW_DOWNLOAD_ERROR;                                            // Bad Response
			}
		   break;
	   case GET_CHUNK_LEN:                                                      // The next line is the chunk length
			if (rx_msg[MODEM_PORT].ptr < (MAX_MESSAGE_LENGTH - 1))
			{
			    rx_msg[MODEM_PORT].buf.uint8[rx_msg[MODEM_PORT].ptr++] = ch;    // add the character to the buffer

				if (ch == '\n')                                                 // process the new line
				{
					if (rx_msg[MODEM_PORT].ptr > 2)                             // dump empty lines
					{
						sscanf((char *) rx_msg[MODEM_PORT].buf.uint8,"%lx", &gChunkLen);     // get the chunk length

						if (gChunkLen > 0)
						{
					    	gChunkCnt = 0;
							gDechunkerState = GET_CHUNK;                            // Read the Chunk
						}
						else
						{
							// A zero length chunk indicates the end of the download
						//	UpdateFirmware();
					    	lRtv = FW_DOWNLOAD_COMPLETE;
						}
					}
					rx_msg[MODEM_PORT].ptr = 0;
				}
			}
			else
			{
				lRtv = FW_DOWNLOAD_ERROR;                  // didn't find the chunk so timed out
			}
		   break;
	   case GET_CHUNK:

		    dbgch = ch;
		    write_psram_byte(gSRAM_Idx++, ch);            // Write the char to SRAM

			if (gChunkCnt++ >= gChunkLen - 1)
		    {
				gDechunkerState = GET_CHUNK_LEN;           // get the next chunk length
		    }
		   break;
	   case GET_ALL_FW:
			write_psram_byte(gSRAM_Idx++, ch);            // Write the char to SRAM
		    if (gChunkCnt++ >= gChunkLen-1)
		    {
		    	//UpdateFirmware();                   // Update the firmware
		    	lRtv = FW_DOWNLOAD_COMPLETE;
		    }
		   break;
	   default:
		   lRtv = FW_DOWNLOAD_ERROR;
		 break;
   }

   return lRtv;
}

/*
//                                      16   12   5
// this is the CCITT CRC 16 polynomial X  + X  + X  + 1.
// This works out to be 0x1021, but the way the algorithm works
// lets us use 0x8408 (the reverse of the bit pattern).  The high
// bit is always assumed to be set, thus we only use 16 bits to
// represent the 17 bit value.
*/
/*
unsigned short crc16(char *data_p, unsigned short length)
{
	unsigned char i;
	unsigned int data;
	unsigned int crc = 0xffff;

	if (length == 0) return (~crc);

	do
	{
		for (i=0, data=(unsigned int)0xff & *data_p++; i < 8; i++, data >>= 1)
		{
			if ((crc & 0x0001) ^ (data & 0x0001))
				crc = (crc >> 1) ^ POLY;
			else  crc >>= 1;
		}
	} while (--length);

	crc = ~crc;
	data = crc;
	crc = (crc << 8) | (data >> 8 & 0xff);

	return (crc);
}
*/

int ConfigCRC( char *GetConfig_RxBuff)
{
	char* startcrcstr;
	char* stopcrcstr;
	unsigned int remotecrc;
	unsigned int  local_crc;
	unsigned short crclen;
	char* tmpstr;
	unsigned char index;
	unsigned char val1;
	int rtv;
	int i;

	rtv = FALSE;
	local_crc = 0;
	remotecrc = 0;

	tmpstr = strstr((char*)GetConfig_RxBuff,"CRC");

	if(tmpstr != NULL)
	{
		sscanf(tmpstr,"CRC\"%*[^\"]\"%x", &remotecrc);
		startcrcstr = strstr((char*)GetConfig_RxBuff,"RRT") - 1;
		stopcrcstr = tmpstr - 2;
	}

	crclen = stopcrcstr - startcrcstr;

	// calculate the local_crc
	local_crc = 0;
	for (i=0; i<crclen; i++)
	{
		val1 = startcrcstr[i];
		index = (char) (local_crc ^ val1);
		local_crc = (unsigned int) ((local_crc >> 8) ^ table[index]);
	}

	if (remotecrc == local_crc)
	{
		rtv = TRUE;
		HTTP_DBPRINT("Config CRC OK");
		gCheckConfigFlag = FALSE;
	}
	else
	{
		gCheckConfigFlag = TRUE;
		HTTP_DBPRINT("Config CRC Err");

		if (CRCtrial >= 3)
		{
			gCheckConfigFlag = FALSE;
			HTTP_DBPRINT("Config CRC Err-3 & Suspend");   //Suspend the Getconfig if CRC is wrong for 3 attempts
			CRCtrial = 0;
		}
		else
		{
			CRCtrial++;
		}
	}

	return rtv;
}


uint16_t GetModbusSampleRate()
{
	return 	nv_sys_conf.s.sample_period;
}

uint16_t GetHTTPSampleRate()
{
	return HTTPSampleRate;
}
