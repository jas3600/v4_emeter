#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <io.h>
#include <emeter-toolkit.h>

#include "emeter-structs.h"
#include "emeter-memory.h"
//test

char teststr1[20] = {'H','e','l','l','o',' ','P','s','r','a','m'};
char outstr[30];
char PsramFlag;
char tstchar[2];
char gByte[3];

#define RAM_WORKING

#define RAM_ZZ  BIT0
#define RAM_CE  BIT1
#define RAM_OE  BIT2
#define RAM_UB  BIT3
#define RAM_LB  BIT4
#define RAM_WE  BIT5

#define RAM_CTRL_PORT_DIR  P11DIR
#define RAM_CTRL_PORT_OUT  P11OUT
#define RAM_DATA_PORT_DIR  P10DIR
#define RAM_DATA_PORT_OUT  P10OUT
#define RAM_DATA_PORT_IN   P10IN

#define RAM_ADD_PORT_0TO7       P6OUT
#define RAM_ADD_PORT_8TO15      P7OUT
#define RAM_ADD_PORT_16TO21     P8OUT

unsigned long upperlimit= 51200;
int test_psram()
{
  uint32_t i;
  int lErr;
//  int lCount;

//  lCount = 0;
  lErr = 0;
  PsramFlag = 0;

  // write 2000*512 data to psram
  for(i=upperlimit; i>=1; i--)
  {
	  if(i%2==1)
	  {
		  write_psram_byte(i, 0xFF);
	  }
	  else
	  {
		  write_psram_byte(i,0x00);
	  }
  }

  // read 2000*512 data of psram
  for(i=upperlimit; i>=1; i--)
  {
	  if(i%2 == 1)
	  {
		  if(read_psram_byte(i) != 0xFF)
		  {
			  PsramFlag=TRUE;
			  break;
		  }
	  }
	  else
	  {
		  if(read_psram_byte(i) != 0x00)
		  {
			  PsramFlag=TRUE;
			  break;
		  }
	  }
  }

  lErr = PsramFlag;
  return lErr;
}

#ifdef RAM_WORKING


uint32_t read_psram_block(uint32_t aAddress, char * aData, uint16_t aLen)
{
   uint32_t i;
   uint32_t lAddress;
   
   lAddress = aAddress;
     
   for (i = 0; i < aLen; i++)
   {
     aData[i] = read_psram_byte(lAddress + i);
   }
  
  return aLen;
}

uint32_t gadd;

char read_psram_byte(uint32_t aAddress)
{
  char lData;
  uint32_t lAddress;
  
  gadd = aAddress;
  
  lAddress = aAddress >> 1;
  
  // Init Control & Data
  RAM_CTRL_PORT_DIR |= RAM_ZZ | RAM_CE | RAM_OE | RAM_UB | RAM_LB | RAM_WE;    // Control as output
  RAM_CTRL_PORT_OUT |= RAM_ZZ | RAM_CE | RAM_OE | RAM_UB | RAM_LB | RAM_WE;    // Control as HIGH
  RAM_DATA_PORT_DIR = 0x00;        // Data as Inputs
  
  // set up the address
  RAM_ADD_PORT_0TO7   = 0x000000FF & lAddress;
  RAM_ADD_PORT_8TO15  = 0x000000FF & (lAddress >> 8);
  RAM_ADD_PORT_16TO21 = 0x0000003F & (lAddress >> 16);

  // Select Byte
  if ((aAddress & 0x00000001) == 0) RAM_CTRL_PORT_OUT &= ~(RAM_CE + RAM_LB);   // low byte
  else RAM_CTRL_PORT_OUT &= ~(RAM_CE + RAM_UB);                                // high byte

  RAM_CTRL_PORT_OUT &= ~RAM_OE;       // Output Enable
  
  _delay_cycles(10);                  // Allow data settle
  
  lData = RAM_DATA_PORT_IN;           // Read Data
  
  RAM_CTRL_PORT_OUT |=  RAM_ZZ | RAM_CE | RAM_OE | RAM_UB | RAM_LB | RAM_WE;   // Control HIGH
  
 return lData; 
}

uint32_t write_psram_block(uint32_t aAddress, char * aData, uint16_t aLen)
{
   uint32_t i;
   uint32_t lAddress;
   
   lAddress = aAddress;
     
   for (i = 0; i < aLen; i++)
   {
     write_psram_byte(lAddress + i , aData[i]);
   }
  
  return aLen;
}

void write_psram_byte(uint32_t aAddress, char aData)
{
  uint32_t lAddress;
  
  lAddress = aAddress >> 1;
  
  // Init Data and Control
  RAM_DATA_PORT_DIR = 0xFF;    // Outputs
  RAM_DATA_PORT_OUT = aData;   // Output data

  RAM_CTRL_PORT_DIR |= RAM_ZZ | RAM_CE | RAM_OE | RAM_UB | RAM_LB | RAM_WE; 	//Direction Control as Output for Write_Psram_Byte
  RAM_CTRL_PORT_OUT |= RAM_ZZ | RAM_CE | RAM_OE | RAM_UB | RAM_LB | RAM_WE;

  // set up the address
  RAM_ADD_PORT_0TO7   = 0x000000FF & lAddress;
  RAM_ADD_PORT_8TO15  = 0x000000FF & (lAddress >> 8);
  RAM_ADD_PORT_16TO21 = 0x0000003F & (lAddress >> 16);

  // Chip Enable and Select Byte
  if ((aAddress & 0x00000001) == 0) RAM_CTRL_PORT_OUT &= ~(RAM_CE + RAM_LB);    // low byte
  else RAM_CTRL_PORT_OUT &= ~(RAM_CE + RAM_UB);                                 // high byte

  RAM_CTRL_PORT_OUT &= ~RAM_WE;  // Write Enable

  _delay_cycles(100);            // Data settle

  RAM_CTRL_PORT_OUT |= RAM_ZZ | RAM_CE | RAM_OE | RAM_UB | RAM_LB | RAM_WE;     // Latch Data
 
  // Reconfigure data bus
  RAM_DATA_PORT_DIR = 0x00;      // Inputs
}
#else

char RAM_buff[640];

uint32_t read_psram_block(uint32_t aAddress, char * aData, uint16_t aLen)
{
   uint32_t i;
   uint32_t lAddress;

   lAddress = aAddress;

   for (i = 0; i < aLen; i++)
   {
     aData[i] = RAM_buff[lAddress + i];
   }

  return aLen;
}

uint32_t write_psram_block(uint32_t aAddress, char * aData, uint16_t aLen)
{
   uint32_t i;
   uint32_t lAddress;

   lAddress = aAddress;

   for (i = 0; i < aLen; i++)
   {
	   RAM_buff[lAddress + i] = aData[i];
   }

  return aLen;
}


#endif
