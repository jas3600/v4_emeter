//--------------------------------------------------------------------------
//
//  Software for MSP430 based e-meters.
//
//  THIS PROGRAM IS PROVIDED "AS IS". TI MAKES NO WARRANTIES OR
//  REPRESENTATIONS, EITHER EXPRESS, IMPLIED OR STATUTORY,
//  INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//  FOR A PARTICULAR PURPOSE, LACK OF VIRUSES, ACCURACY OR
//  COMPLETENESS OF RESPONSES, RESULTS AND LACK OF NEGLIGENCE.
//  TI DISCLAIMS ANY WARRANTY OF TITLE, QUIET ENJOYMENT, QUIET
//  POSSESSION, AND NON-INFRINGEMENT OF ANY THIRD PARTY
//  INTELLECTUAL PROPERTY RIGHTS WITH REGARD TO THE PROGRAM OR
//  YOUR USE OF THE PROGRAM.
//
//  IN NO EVENT SHALL TI BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
//  CONSEQUENTIAL OR INDIRECT DAMAGES, HOWEVER CAUSED, ON ANY
//  THEORY OF LIABILITY AND WHETHER OR NOT TI HAS BEEN ADVISED
//  OF THE POSSIBILITY OF SUCH DAMAGES, ARISING IN ANY WAY OUT
//  OF THIS AGREEMENT, THE PROGRAM, OR YOUR USE OF THE PROGRAM.
//  EXCLUDED DAMAGES INCLUDE, BUT ARE NOT LIMITED TO, COST OF
//  REMOVAL OR REINSTALLATION, COMPUTER TIME, LABOR COSTS, LOSS
//  OF GOODWILL, LOSS OF PROFITS, LOSS OF SAVINGS, OR LOSS OF
//  USE OR INTERRUPTION OF BUSINESS. IN NO EVENT WILL TI'S
//  AGGREGATE LIABILITY UNDER THIS AGREEMENT OR ARISING OUT OF
//  YOUR USE OF THE PROGRAM EXCEED FIVE HUNDRED DOLLARS
//  (U.S.$500).
//
//  Unless otherwise stated, the Program written and copyrighted
//  by Texas Instruments is distributed as "freeware".  You may,
//  only under TI's copyright in the Program, use and modify the
//  Program without any charge or restriction.  You may
//  distribute to third parties, provided that you transfer a
//  copy of this license to the third party and the third party
//  agrees to these terms by its first use of the Program. You
//  must reproduce the copyright notice and any other legend of
//  ownership on each copy or partial copy, of the Program.
//
//  You acknowledge and agree that the Program contains
//  copyrighted material, trade secrets and other TI proprietary
//  information and is protected by copyright laws,
//  international copyright treaties, and trade secret laws, as
//  well as other intellectual property laws.  To protect TI's
//  rights in the Program, you agree not to decompile, reverse
//  engineer, disassemble or otherwise translate any object code
//  versions of the Program to a human-readable form.  You agree
//  that in no event will you alter, remove or destroy any
//  copyright notice included in the Program.  TI reserves all
//  rights not specifically granted under this license. Except
//  as specifically provided herein, nothing in this agreement
//  shall be construed as conferring by implication, estoppel,
//  or otherwise, upon you, any license or other right under any
//  TI patents, copyrights or trade secrets.
//
//  You may not use the Program in non-TI devices.
//
//  File: emeter-main.c
//
//  Steve Underwood <steve-underwood@ti.com>
//  Texas Instruments Hong Kong Ltd.
//
//  $Id: emeter-main.c,v 1.10 2009/04/27 06:21:22 a0754793 Exp $
//
/*! \file emeter-structs.h */
//
//--------------------------------------------------------------------------
//
//  MSP430 foreground (non-interrupt) routines for e-meters
//
//  This software is appropriate for single phase and three phase e-meters
//  using a voltage sensor plus a CT or shunt resistor current sensors, or
//  a combination of a CT plus a shunt.
// 
//    Foreground process includes:
//    -Using timer tick to wait
//    -Calculating the power per channel
//    -Determine if current channel needs scaling.
//    -Determine if needs to be in low power modes.
//    -Compensate reference from temperature sensor
//
#define DEFINE_GLOBALS
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <io.h>
#include <emeter-toolkit.h>
#define __MAIN_PROGRAM__


#include "emeter-structs.h"
#include "mmc.h"
//#include "hal_SPI.h"
#include "hal_MMC_hardware_board.h"
#include "emeter-memory.h"
#include "OTAUpdate.h"
#include "Debug.h"
#include "emeter-http.h"
#include "emeter-communication.h"
#include "emeter-relay.h"
#include "emeter-rtc.h"
#include "emeter-fifo.h"
#include "emeter-display.h"
#include "telit.h"
#include "emeter-boot-reset.h"
#include <mb.h>

#ifdef DB_MAIN
#define MAIN_DBPRINT(...) PrintDebugMessage(__VA_ARGS__)
#else
#define MAIN_DBPRINT(...)
#endif

void init_meter(void);
void display_status(void);
void flash_unlockA(void);
void flash_lockA(void);
void Save_AND_Reset();


#if defined(TOTAL_ACTIVE_ENERGY_SUPPORT)
typedef union
{
    unsigned char uint8[4];
    unsigned int uint16[2];
    uint32_t uint32;
} power_array;


uint8_t total_active_energy_pulse_remaining_time;
int32_t total_active_power;
power_array    total_active_power_array;
volatile uint32_t total_active_power_counter;
volatile uint32_t total_consumed_active_power_counter;
volatile uint32_t total_produced_active_power_counter;

#if defined(WHOLE_HOUSE_MONITORING_SUPPORT)
uint8_t wh_total_active_energy_pulse_remaining_time;
int32_t wh_total_active_power;
power_array wh_total_active_power_array;
volatile uint32_t wh_total_active_power_counter;
volatile uint32_t wh_total_consumed_active_power_counter;
volatile uint32_t wh_total_produced_active_power_counter;
#endif

    #if TOTAL_ENERGY_PULSES_PER_KW_HOUR < 1000
int16_t extra_total_active_power_counter;
    #endif
volatile uint32_t total_active_energy;
volatile uint32_t total_consumed_active_energy;
volatile uint32_t total_produced_active_energy;

#if defined(WHOLE_HOUSE_MONITORING_SUPPORT)
volatile uint32_t wh_total_active_energy;
volatile uint32_t wh_total_consumed_active_energy;
volatile uint32_t wh_total_produced_active_energy;
#endif

#endif

#if defined(TOTAL_REACTIVE_ENERGY_SUPPORT)
uint8_t total_reactive_energy_pulse_remaining_time;
volatile int32_t total_reactive_power;

volatile int32_t total_apparent_power;
volatile int32_t wh_total_apparent_power;
volatile int32_t total_power_factor;
volatile int32_t wh_total_power_factor;

volatile int32_t total_reactive_power_counter;
    #if TOTAL_ENERGY_PULSES_PER_KW_HOUR < 1000
int16_t extra_total_reactive_power_counter;
    #endif
volatile uint32_t total_consumed_reactive_energy;

uint8_t wh_total_reactive_energy_pulse_remaining_time;
volatile int32_t wh_total_reactive_power;
volatile int32_t wh_total_reactive_power_counter;
    #if TOTAL_ENERGY_PULSES_PER_KW_HOUR < 1000
int16_t wh_extra_total_reactive_power_counter;
    #endif
volatile uint32_t wh_total_consumed_reactive_energy;
#endif


#if defined(TEMPERATURE_SUPPORT)
uint16_t temperature;
#endif

/* Meter status flag bits. */
uint16_t meter_status;

/* Meter status flag bits. */
uint16_t comms_meter_status;

/* Current operating mode - normal, limp, power down, etc. */
int8_t operating_mode;

int gIR_LED_Output;

/* Persistence check counters for anti-tamper measures. */
#if defined(PHASE_REVERSED_DETECTION_SUPPORT)
int8_t current_reversed;
#endif
#if defined(POWER_BALANCE_DETECTION_SUPPORT)
int8_t current_unbalanced;
#endif

/* The main per-phase working parameter structure */
struct phase_parms_s chan[NUM_PHASES];
    #if defined(NEUTRAL_MONITOR_SUPPORT)
struct neutral_parms_s neutral;
    #endif

/* The main per-phase non-volatile parameter structure */
#pragma location=0x1800
const struct info_mem_s nv_parms =
{
    {
    {
        0xFFFF,
        {
            {
                {
                    {DEFAULT_I_RMS_SCALE_FACTOR_A},
                    0,
                    DEFAULT_P_SCALE_FACTOR_A_LOW,
					DEFAULT_QUADRATURE_PHASE_STEP_A,
                    DEFAULT_IN_PHASE_STEP_A,
                    DEFAULT_BASE_PHASE_A_CORRECTION_LOW,
                },
                DEFAULT_V_RMS_SCALE_FACTOR_A,
                DEFAULT_V_DC_ESTIMATE << 16,
            },
            {
                {
                   {DEFAULT_I_RMS_SCALE_FACTOR_B},
                   0,
                   DEFAULT_P_SCALE_FACTOR_B_LOW,
				   DEFAULT_QUADRATURE_PHASE_STEP_B,
				   DEFAULT_IN_PHASE_STEP_B,
                   DEFAULT_BASE_PHASE_B_CORRECTION_LOW,
                },
                DEFAULT_V_RMS_SCALE_FACTOR_B,
                DEFAULT_V_DC_ESTIMATE << 16,
            },
            {
                {
                    {DEFAULT_I_RMS_SCALE_FACTOR_C},
                    0,
                    DEFAULT_P_SCALE_FACTOR_C_LOW,
					DEFAULT_QUADRATURE_PHASE_STEP_C,
					DEFAULT_IN_PHASE_STEP_C,
                    DEFAULT_BASE_PHASE_C_CORRECTION_LOW,
                },
                DEFAULT_V_RMS_SCALE_FACTOR_C,
                DEFAULT_V_DC_ESTIMATE << 16,
            },
            {
                {
                    {DEFAULT_I_RMS_SCALE_FACTOR_D},
                    0,
                    DEFAULT_P_SCALE_FACTOR_D_LOW,
					DEFAULT_QUADRATURE_PHASE_STEP_D,
					DEFAULT_IN_PHASE_STEP_D,
                    DEFAULT_BASE_PHASE_D_CORRECTION_LOW,
                },
                DEFAULT_V_RMS_SCALE_FACTOR_D,
                DEFAULT_V_DC_ESTIMATE << 16,
            }
        },
        25,
        DEFAULT_TEMPERATURE_OFFSET,
        DEFAULT_TEMPERATURE_SCALING,
        {
            0,
            0,
            0,
            0,
            0,
            0,
			0,
			0
        }
    }
    }
};

#pragma location=0x1880
const Seg_Struct_1880 seg_data =
{
	0x00,
	0xCD
};


#pragma location=0x1900
const struct operation_mem_s nv_op_vars =
{
	0,      // Default Produced Energy
	0,      // Default Consumed Energy
	0xE433, // Default Operational config ie Relays Shut and right side up.
	0,      // Default Whole House Consumed Energy
	0       // Default Whole House Produced Energy
};


#pragma location=0x1980
const struct system_mem_s nv_sys_conf =
{
   DEFAULT_CONFIG_VERSION,
   DEFAULT_MODEL_NUMBER,
   DEFAULT_OP_CONFIG,
   DEFAULT_SERIAL_NUMBER,
   DEFAULT_SAMPLE_PERIOD,
   DEFAULT_REPORT_PERIOD,
   DEFAULT_REPORT_MINUTE,
   DEFAULT_CLOSE_DELAY,
   "VINCETEST",
//   DEFAULT_PROPERTY_NUMBER,
   DEFAULT_DNS1,
   DEFAULT_DNS2,
   "/dev/test/api/units/VINCETEST",
//   DEFAULT_REPORT_URL,
   DEFAULT_CFG_UPD_TIME
};

union
{
  uint32_t num;
  char str[4];
} gConsumed;

int gStartupCnt;
//unsigned int gLCDCounter;
int gClearMMC = 0;
extern char gConstFlag;

/**********************************************************************************************************************
 *    main()
 *
 *
 *
 *
 *******************************************************************************************************************/
void main(void)
{
    int ch=0;
    static struct phase_parms_s *phase;
    static struct phase_nv_parms_s const *phase_nv;
    static int32_t x;
    extern const char EmeterFwVersion[9];
    char dbgstr[256];
    float lV, lI, lAPCalc, lAPRept;

    initLCD();
    system_setup();
    enable_wdtie();

    __delay_cycles(2400000);   //delay for 1 second
    kick_watchdog();

    gIR_LED_Output = BRANCH_MONITORING;

    Init_Debug_UART(MCLK_DEF, 9600);

    MAIN_DBPRINT("*********************");
    SystemResetQuery();
    // Boot Message
    sprintf(gDbgMsg, "Emeter system boot : %s %02X%02X", EmeterFwVersion, seg_data.BootMsb, seg_data.BootLsb);
    MAIN_DBPRINT(gDbgMsg);

    init_meter();
    UpdateWHCoeffs();

    comms_meter_status |= STATUS_ACTIVATED;
    // initializes sd card during boot and sets global flag if sd is available
    init_SD();

    //test PSRAM during Boot
    if(test_psram() == 1)
    {
    	MAIN_DBPRINT("Psram Error");
    }
    else
    {
    	MAIN_DBPRINT("Psram OK");
    }

    // For Clearing the FIFO data out of the SD card after testing
 //   if (gClearMMC == 1) ClearMMCBlocks();

    init_relay();

    ClearDebugBuffer();

    //Display Firmware version on LCD Display
    DisplayFwVer(EmeterFwVersion);

    comms_meter_status |= 0x2001;                         // POST OK

    comms_init();

    Initialize_Modem(Identify_Modem());

    Telit_init_modem();

    eMBInit( MB_RTU, 1, 0, 9600, MB_PAR_NONE );
    eMBEnable();

    gStartupCnt = 0;

    if ((comms_meter_status & STATUS_DISCONNECT_CLOSED) > 0) switch_relay(RELAY_SHUT);
    else switch_relay(RELAY_OPEN);

    _EINT();

    for (;;)                 // Infinite Loop
    {
        kick_watchdog();


        phase = chan;
        phase_nv = nv_parms.s.chan;

        Telit_modem_tick();

		for (ch = 0;  ch < NUM_PHASES;  ch++)
		{

			/* Unless we are in normal operating mode, we should wait to be
			   woken by a significant event from the interrupt routines. */
			#if defined(POWER_DOWN_SUPPORT)
				if (operating_mode == OPERATING_MODE_POWERFAIL) switch_to_powerfail_mode();
			#endif
			if ((phase->status & NEW_LOG))
			{
						/* The background activity has informed us that it is time to
						   perform a block processing operation. */
				phase->status &= ~NEW_LOG;

				if (operating_mode == OPERATING_MODE_NORMAL)
				{
					x = active_power(phase, phase_nv);
                    if  (ch < 2)
                    {
                        x *= -1;    //L1 and L2 readings changed in 02_02_08, must invert to maintain positive scale factors
                    }
					#if defined(PRECALCULATED_PARAMETER_SUPPORT)
						#if defined(IRMS_SUPPORT)
							phase->readings.I_rms = current(phase, phase_nv, ch);
						#endif
						#if defined(VRMS_SUPPORT)
							phase->readings.V_rms = voltage(phase, phase_nv);
						#endif
					#endif
				}

				if (labs(x) < RESIDUAL_POWER_CUTOFF  ||  (phase->status & V_OVERRANGE))
				{
					x = 0;
					/* Turn off the LEDs, regardless of the internal state of the
					   reverse and imbalance assessments. */
					#if defined(PHASE_REVERSED_DETECTION_SUPPORT)
						meter_status &= ~STATUS_REVERSED;
						clr_reverse_current_indicator();
					#endif
					#if defined(POWER_BALANCE_DETECTION_SUPPORT)
						meter_status &= ~STATUS_EARTHED;
						clr_earthed_indicator();
					#endif
				 }
				else
				{
					if (operating_mode == OPERATING_MODE_NORMAL)
					{
						#if defined(PHASE_REVERSED_DETECTION_SUPPORT)  &&  defined(PHASE_REVERSED_IS_TAMPERING)
							if ((phase->status & PHASE_REVERSED))
							{
								meter_status |= STATUS_REVERSED;
								set_reverse_current_indicator();
							}
							else
							{
								meter_status &= ~STATUS_REVERSED;
								clr_reverse_current_indicator();
							}
						#endif
						#if defined(POWER_BALANCE_DETECTION_SUPPORT)
							if ((phase->status & PHASE_UNBALANCED))
							{
								meter_status |= STATUS_EARTHED;
								set_earthed_indicator();
							}
							else
							{
								meter_status &= ~STATUS_EARTHED;
								clr_earthed_indicator();
							}
						#endif
					}
				}

				if (ch < 2)  // Only Collect the first two phases of power production
				{
				    total_active_power += (x - phase->readings.active_power);
				}


				if (ch >= 2)  // Collect the last two phases of power production for the Whole House
				{
				    wh_total_active_power += (x - phase->readings.active_power);
				}

				phase->readings.active_power = x;

#if defined(PRECALCULATED_PARAMETER_SUPPORT)
		#if defined(REACTIVE_POWER_SUPPORT)
					    x = reactive_power(phase, phase_nv);
	                    if  (ch < 2)
	                    {
	                        x *= -1;    //L1 and L2 readings changed in 02_02_08, must invert to maintain positive scale factors
	                    }

					    if  (ch >= 2)
		                {
		                     x *= -1;    //Due to our conventions Phase 1 and 2 must be inverted.
		                }

					  	//total_reactive_power += (x - phase->readings.reactive_power);
						if (ch < 2)  // Only Collect the first two phases of power production
						{
							total_reactive_power += (x - phase->readings.reactive_power);
						}
						if (ch >= 2)  // Collect the last two phases of power production for the Whole House
						{
							wh_total_reactive_power += (x - phase->readings.reactive_power);
						}
					  phase->readings.reactive_power = x;
		#endif
		#if defined(APPARENT_POWER_SUPPORT)
					  //phase->readings.apparent_power = apparent_power(phase, phase_nv);
					  x = apparent_power(phase, phase_nv);
					  if (ch < 2)  // Only Collect the first two phases of power production
					  {
						  total_apparent_power += (x - phase->readings.apparent_power);
					  }
					  if (ch >= 2)  // Collect the last two phases of power production for the Whole House
					  {
						  wh_total_apparent_power += (x - phase->readings.apparent_power);
					  }
					  phase->readings.apparent_power = x;
		#endif
		#if defined(POWER_FACTOR_SUPPORT)
					  //phase->readings.power_factor = power_factor(phase, phase_nv);
					  phase->readings.power_factor = power_factor(phase->readings.apparent_power,  phase->readings.active_power, phase->readings.reactive_power);

                      if (ch < 2)  // Calc Branch PF after First two
                      {
                          total_power_factor = power_factor(total_apparent_power,  total_active_power, total_reactive_power);
                      }
                      if (ch >= 2)  // Collect the last two phases of power production for the Whole House
                      {
                          wh_total_power_factor = power_factor(wh_total_apparent_power,  wh_total_active_power, wh_total_reactive_power);
                      }
		#endif
#endif
				#if defined(PER_PHASE_ACTIVE_ENERGY_SUPPORT)
//                    phase->active_energy_counter += x*phase->metrology.dot_prod_logged.sample_count;
//                    while (phase->active_energy_counter > ENERGY_WATT_HOUR_THRESHOLD)
//                    {
//                          phase->active_energy_counter -= ENERGY_WATT_HOUR_THRESHOLD;
//                          phase->consumed_active_energy++;
//                    }
				#endif
				#if defined(PRECALCULATED_PARAMETER_SUPPORT)  && defined(MAINS_FREQUENCY_SUPPORT)
/*					 if (ch == 0) // only first phase needed for frequency
					 {
						lFrequency = frequency(phase, phase_nv);
					 }
					 phase->readings.frequency = lFrequency; */
					 phase->readings.frequency = frequency(phase, phase_nv);  // Need to calc frequency for every channel to
					                                                          // have correct quadrature calc
				#endif
			}

			phase++;
			phase_nv++;
		} // END OF CHANNEL FOR LOOP

	    /*** Do display and other housekeeping here ***/
		if ((meter_status & TICKER))
		{

			/* Two seconds have passed */
			 /* We have a 2 second tick */
			meter_status &= ~TICKER;           // Set in RTC.c

			if (gStartupCnt < 20) gStartupCnt++;  // Count to ensure startup complete

			comms_meter_status |= ORIENTATION_OK;                // Right side up

			relay_tick();
			comms_tick();

			send_LCD_Info();

			// Clear the SD card when the KWH is Cleared
			if(clearMMCQuery() == TRUE)
			{
				if (sdcard_installed() == TRUE)
				{
				    ClearMMCBlocks();
				}
			}

			#ifdef TEST_DATA_RETENTION
				TestDataRetention();
			#endif

			tgl_normal_indicator();               // I'm Alive!

			#if (defined(RTC_SUPPORT)  ||  defined(CUSTOM_RTC_SUPPORT))  &&  defined(CORRECTED_RTC_SUPPORT)
			   correct_rtc();
			#endif
		}

     } //End of the infinite FOR loop

} // END OF MAIN

/**************************************************************************
   display_status(void)

**************************************************************************/
void display_status(void)
{

	if ((meter_status & STATUS_POST))
	{
		if ((meter_status & STATUS_AUTHORIZED))
		{
			if ((meter_status & STATUS_ACTIVATED))
			{
				clr_warning_indicator();
				tgl_normal_indicator();
			}
			else
			{
				tgl_warning_indicator();
			}
		}
		else
		{
			set_warning_indicator();
		}
	}
	else
	{
		clr_warning_indicator();
		clr_normal_indicator();
	}
}

void init_meter(void)
{
    volatile int i;

    // Set rtc to a dummy value
    rtc.century = 19;
    rtc.year = 99;
    rtc.month = 1;
    rtc.day = 1;
    rtc.hour = 0;
    rtc.minute = 0;
    rtc.second = 0;
    set_rtc_sumcheck();


    total_produced_active_energy = nv_op_vars.s.produced_energy;
    total_consumed_active_energy = nv_op_vars.s.consumed_energy;

    wh_total_produced_active_energy = nv_op_vars.s.wh_produced_energy;
    wh_total_consumed_active_energy = nv_op_vars.s.wh_consumed_energy;

    comms_meter_status = nv_op_vars.s.op_config;

    if(comms_meter_status == 0xffff)
    {
    	// for WH Metering structure of 1900 was changed, and Comms meter status could get 0xffff in new position. Reset variable to ensure normal operation.
        	wh_total_produced_active_energy = 0;
        	wh_total_consumed_active_energy = 0;
        	comms_meter_status = 0xEC33;
        	MAIN_DBPRINT("LTS Values updated for WH Metering");

    }

    sprintf(gDbgMsg,"InitMeter B_Cons: %ld B_Prod: %ld WH_Cons: %ld WH_Prod: %ld Status: %x",
    		total_consumed_active_energy,    		total_produced_active_energy,
			wh_total_consumed_active_energy,			wh_total_produced_active_energy,
			comms_meter_status);
    MAIN_DBPRINT(gDbgMsg);
}

/*
 *
*/
#if defined(PRECALCULATED_PARAMETER_SUPPORT)
uint32_t current_produced_active_energy(int ph)
{
	#if defined(TOTAL_ACTIVE_ENERGY_SUPPORT)
		if (ph == BRANCH_PHASE_TOTAL)
		{
			if(total_produced_active_energy==0xFFFFFFFF)
			{
				return 0;
			}
			else
			{
				return total_produced_active_energy;
			}
		}

		if (ph == WHOLE_HOUSE_PHASE_TOTAL)
		{
			if(total_produced_active_energy==0xFFFFFFFF)
			{
				return 0;
			}
			else
			{
				//return wh_total_produced_active_energy;
				return wh_total_consumed_active_energy;
			}
		}
	#endif
	#if defined(PER_PHASE_ACTIVE_ENERGY_SUPPORT)
		return chan[ph].consumed_active_energy;
	#else
		return 0;
	#endif
}

uint32_t current_consumed_active_energy(int ph)
{
	#if defined(TOTAL_ACTIVE_ENERGY_SUPPORT)
		if (ph == BRANCH_PHASE_TOTAL)
		{
			if(total_consumed_active_energy==0xFFFFFFFF)
			{
				return 0;
			}
			else
			{
				return total_consumed_active_energy;
			}
		}
		if (ph == WHOLE_HOUSE_PHASE_TOTAL)
		{
			if(wh_total_consumed_active_energy==0xFFFFFFFF)
			{
				return 0;
			}
			else
			{
				//return wh_total_consumed_active_energy;
				return wh_total_produced_active_energy;
			}
		}

	#endif
	#if defined(PER_PHASE_ACTIVE_ENERGY_SUPPORT)
		return chan[ph].consumed_active_energy;
	#else
		return 0;
	#endif
}

int32_t current_active_power(int ph)
{
	if (ph == BRANCH_PHASE_TOTAL)
		return total_active_power;
	if (ph == WHOLE_HOUSE_PHASE_TOTAL)
		return wh_total_active_power;
	return chan[ph].readings.active_power;
}

#if defined(REACTIVE_POWER_SUPPORT)
int32_t current_consumed_reactive_energy(int ph)
{
	#if defined(TOTAL_REACTIVE_ENERGY_SUPPORT)
		if (ph == BRANCH_PHASE_TOTAL)
			return total_consumed_reactive_energy;
		if (ph == WHOLE_HOUSE_PHASE_TOTAL)
			return wh_total_consumed_reactive_energy;
	#endif
	#if defined(PER_PHASE_REACTIVE_ENERGY_SUPPORT)
		return chan[ph].consumed_reactive_energy;
	#else
		return 0;
	#endif
}


int32_t current_reactive_power(int ph)
{
	if (ph == BRANCH_PHASE_TOTAL) return total_reactive_power;
	if (ph == WHOLE_HOUSE_PHASE_TOTAL) return wh_total_reactive_power;
	return chan[ph].readings.reactive_power;
}
#endif

#if defined(APPARENT_POWER_SUPPORT)
int32_t current_apparent_power(int ph)
{
	return chan[ph].readings.apparent_power;
}
#endif

#if defined(POWER_FACTOR_SUPPORT)
int32_t current_power_factor(int ph)
{
	int32_t lTemp = 0;
	if(ph == 4) //Branch DER
	{
		if ((float)current_rms_current(0)/1000 >= 0.25 || (float)current_rms_current(0)/1000 <= -0.25)
		{
			lTemp =  total_power_factor;
		}
	}
	else if(ph == 5)//WH LOAD
	{
		if ((float)current_rms_current(2)/1000 >= 0.25 || (float)current_rms_current(2)/1000 <= -0.25)
		{
			lTemp =  wh_total_power_factor;
		}
	}
	return lTemp;
}


#endif

#if defined(VRMS_SUPPORT)
int32_t current_rms_voltage(int ph)
{
	int32_t x;
	float y;

	if (ph == 0 || ph == 1 || ph == 2 || ph == 3)
	{
		if (chan[ph].readings.V_rms == 0xFFFF)
		{
			x = -1;
		}
		else
		{
			x =  chan[ph].readings.V_rms;
		}
	}
	else
	{
		if (nv_sys_conf.s.op_config == 1)                           // If 2S
		{
			x = chan[0].readings.V_rms + chan[1].readings.V_rms;    // Use L1 and L2 voltages
		}
		else
		{
			y = (chan[0].readings.V_rms + chan[1].readings.V_rms);   // Use L1 and L2 voltage
			x = 0.8660254 * y;                                       // 0.8660254 = sqrt(3)/2
		}
	}

	return x;
}
#endif


#if defined(IRMS_SUPPORT)
int32_t current_rms_current(int ph)
{
	int32_t x;

    if (ph == 0 || ph == 1 || ph == 2 || ph == 3)
    {
	   x = chan[ph].readings.I_rms;
    }
    else
    {
        x = (chan[0].readings.I_rms + chan[1].readings.I_rms) >> 1;
    }

	if (x == 0xFFFF) x = -1;

	return x;
}
#endif

#if defined(MAINS_FREQUENCY_SUPPORT)
int32_t current_mains_frequency(int ph)
{
	return chan[0].readings.frequency;            // Only need one phase of Frequency
}
#endif

/*******************************************************
 * current_temperature(void)
 *
 * Returns the 16 times the temperture in degrees F
 *
 ********************************************************/
int32_t current_temperature(void)
{
	int32_t temp;

	temp = temperature;
	temp = (temp - nv_parms.s.temperature_offset);
	temp = temp * nv_parms.s.temperature_scaling;
	temp >>= 12;
	temp += 512;

	return temp;
}

#else
	/*
  int32_t current_consumed_active_energy(int ph)
  {
      return chan[ph].consumed_active_energy;
  } 

  int32_t current_active_power(int ph)
  {
      if (ph == FAKE_PHASE_TOTAL)
          return total_active_power;
      return active_power(&chan[ph], &nv_parms.seg_a.s.chan[ch]);
  }
  #if defined(REACTIVE_POWER_SUPPORT)
    int32_t current_consumed_reactive_energy(int ph)
    {
        return chan[ph].consumed_active_energy;
    }
  
    int32_t current_reactive_power(int ph)
    {
        if (ph == FAKE_PHASE_TOTAL)
            return total_reactive_power;
        return reactive_power(&chan[ph], &nv_parms.seg_a.s.chan[ch]);
    }
  #endif
  #if defined(APPARENT_POWER_SUPPORT)
    int32_t current_apparent_power(int ph)
    {
        return apparent_power(&chan[ph], &nv_parms.seg_a.s.chan[ch]);
    }
  #endif
  #if defined(POWER_FACTOR_SUPPORT)
    int32_t current_power_factor(int ph)
    {
        return power_factor(&chan[ph], &nv_parms.seg_a.s.chan[ch]);
    }
  #endif
  #if defined(VRMS_SUPPORT)
    int32_t current_rms_voltage(int ph)
    {
        return voltage(&chan[ph], &nv_parms.seg_a.s.chan[ch]);
    }
  #endif

  #if defined(IRMS_SUPPORT)
    int32_t current_rms_current(int ph)
    {
        return current(&chan[ph], &nv_parms.seg_a.s.chan[ch]);
    }
  #endif

  #if defined(MAINS_FREQUENCY_SUPPORT)
    int32_t current_mains_frequency(int ph)
    {
        return frequency(&chan[ph], &nv_parms.seg_a.s.chan[ch]);
    }
  #endif
    */
#endif


void flash_unlockA(void)
{
	_DINT();
	//FCTL4 = FWPW;
	if ((FCTL3 & LOCKA) != 0)
	{
		FCTL3 = FWPW | LOCKA;
	}
	_EINT();
}

void flash_lockA(void)
{
	_DINT();

	if ((FCTL3 & LOCKA) == 0)
	{
		FCTL3 = FWPW | LOCKA;
	}

	//FCTL4 = FWPW | LOCKINFO;
	_EINT();
}



int clear_1900()
{
	struct operation_mem_s lOpVars;
	int rtv=0;

	// Save the opvars
	if(nv_op_vars.s.consumed_energy!=0 || nv_op_vars.s.produced_energy!=0)
	{
		total_consumed_active_energy = 0;
		total_produced_active_energy = 0;
		wh_total_consumed_active_energy = 0;
		wh_total_produced_active_energy = 0;

		lOpVars.s.consumed_energy = total_consumed_active_energy;
		lOpVars.s.produced_energy = total_produced_active_energy;
		lOpVars.s.wh_consumed_energy = wh_total_consumed_active_energy;
		lOpVars.s.wh_produced_energy = wh_total_produced_active_energy;
		lOpVars.s.op_config = comms_meter_status;

		flash_clr((int*) &nv_op_vars);
		flash_memcpy((char *) &nv_op_vars, lOpVars.x, sizeof(lOpVars));
		flash_secure();
		rtv=0;
	}
	else
	{
		rtv=1;
	}

	return rtv;
}

void UpdateWHCoeffs()
{
    struct info_mem_s lTempCalConst;
    int tempptr;

    for (tempptr=0;tempptr<128;tempptr++)
    {
    	lTempCalConst.x[tempptr] = nv_parms.x[tempptr]; //copy all existing factors to new variable
    }

    // first step is to find if the units paramerts is not already fixed using calibrator app
	if(nv_parms.s.chan[0].current.quadrature_step == 0 && nv_parms.s.chan[0].current.in_phase_step == 0 &&
	   nv_parms.s.chan[1].current.quadrature_step == 0 && nv_parms.s.chan[1].current.in_phase_step == 0 &&
	   nv_parms.s.chan[2].current.quadrature_step == 0 && nv_parms.s.chan[2].current.in_phase_step == 0 && nv_parms.s.chan[2].current.phase_correction[0] == 0 &&
	   nv_parms.s.chan[3].current.quadrature_step == 0 && nv_parms.s.chan[3].current.in_phase_step == 0 && nv_parms.s.chan[3].current.phase_correction[0] == 0)
	{
		lTempCalConst.s.chan[0].current.quadrature_step = 17;

		lTempCalConst.s.chan[1].current.quadrature_step = 17;

		lTempCalConst.s.chan[2].V_rms_scale_factor = 9780;
		lTempCalConst.s.chan[2].current.I_rms_scale_factor = 12250;
		lTempCalConst.s.chan[2].current.P_scale_factor[0] = 3715;
		lTempCalConst.s.chan[2].current.in_phase_step = 19;
		lTempCalConst.s.chan[2].current.phase_correction[0] = 18;

		lTempCalConst.s.chan[3].V_rms_scale_factor = 9780;
		lTempCalConst.s.chan[3].current.I_rms_scale_factor = 12250;
		lTempCalConst.s.chan[3].current.P_scale_factor[0] = -3715;
		lTempCalConst.s.chan[3].current.in_phase_step = 19;
		lTempCalConst.s.chan[3].current.phase_correction[0] = 18;

		MAIN_DBPRINT("Cal Factors update for WH Metering");

		flash_clr((int*) &nv_parms);
		flash_memcpy((char *) &nv_parms, lTempCalConst.x, sizeof(lTempCalConst));
		flash_secure();

    	// for WH Metering structure of 1900 was changed, and Comms meter status could get 0xffff in new position. Reset variable to ensure normal operation.
		wh_total_produced_active_energy = 0;
    	wh_total_consumed_active_energy = 0;
    	comms_meter_status = 0xEE30;
    	MAIN_DBPRINT("LTS Values updated for WH Metering");

		Save_AND_Reset();
	}


}
