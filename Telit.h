/*
 * Telit.h
 *
 *  Created on: Nov 8, 2016
 *      Author: Afreen
 */

#ifndef TELIT_H_
#define TELIT_H_
#include <stdint.h>

//#define ATT3G 1
//#define SSL   1
extern char gICCID[30];
extern char gIMEI[30];
extern char gMFW[20];
extern uint16_t gIPA[4];

#define MDM_AT      "AT\r"
//#define MDM_SELINT  "AT#SELINT=2\r\n"
#define MDM_SELINT  "AT+CFUN=1\r\n"
#define MDM_ENS     "AT#ENS=1\r\n"
#define MDM_ECHO    "ATE0\r\n"
#define MDM_IPR_57600  "AT+IPR=57600\r\n"
#define MDM_IPR_115200  "AT+IPR=115200\r\n"
#define MDM_FLOW_CNT_OFF        "AT&K\r\n"
#define MDM_WRITE_PROFILE       "AT&W\r\n"
#define MDM_ICCID       "AT+ICCID\r\n"
//#define MDM_IMEISV      "AT+IMEISV\r\n"
#define MDM_IMEISV  "AT#FWSWITCH?\r\n"
#define MDM_IMEI        "AT+CGSN\r\n"
#define MDM_MODULE   "AT+GMM\r\n"
#define MDM_MFW      "AT+GMR\r\n"
#define MDM_SWPKGV   "AT#SWPKGV\r\n"
#define MDM_CLKMODE  "AT#CCLKMODE=1\r\n"
#define MDM_CCLK     "AT#CCLK?\r\n"
#define MDM_SKIPESC         "AT#SKIPESC=1\r\n"
#define MDM_ESCGT           "AT#E2ESC=0\r\n"
#define MDM_FWQ         "AT#FWSWITCH?\r\n"
#define MDM_FW_VZW      "AT#FWSWITCH=1\r\n"
#define MDM_FW_ATT      "AT#FWSWITCH=0\r\n"
#define MDM_ERR_VERBOSE "AT+CMEE=2\r\n"
#define MDM_SHUTDOWN    "AT#SHDN\r\n"
#define MDM_MDN         "AT+CNUM\r\n"
#define MDM_CONTEXT     "AT+CGDCONT?\r\n"
#define MDM_RDP         "AT+CGCONTRDP=3\r\n"


#define MDM_FOTA_CONT   "AT+CGDCONT=1,\"IP\",\"conetder.vzwentp\"\r\n"
#define MDM_FOTA_CFG    "AT#SCFG=1,1,1460,90,600,50\r\n"
#define MDM_FOTA_ACT    "AT#SGACT=1,1\r\n"
#define MDM_FOTA_DACT   "AT#SGACT=1,0\r\n"
#define MDM_OPN_FTP     "AT#FTPOPEN=\"10.0.1.5\",\"anonymous\",\"\",1,1\r\n"
#define MDM_FTP_CFG     "AT#FTPCFG=100,0,0,1\r\n"
#define MDM_CLS_FTP     "AT#FTPCLOSE\r\n"
#define MDM_GET_OTA     "AT#FTPGETOTA=\"/pub/update.bin\",0\r\n"
//#define MDM_GET_OTA     "AT#FTPGETOTA=\"/pub/FOTA_test.bin\",0\r\n"
//#define MDM_GET_OTA     "AT#FTPGETOTA=\"/pub/FOTA_test2.bin\",0\r\n"
//#define MDM_GET_OTA     "AT#FTPGETOTA=\"/pub/FOTA_test-large.bin\",0\r\n"
#define MDM_FOTA        "AT#OTAUP=0\r\n"


#ifdef ATT3G
#define MDM_CREG    "AT+CREG?\r"
#define MDM_CGDCONT "AT+CGDCONT=1,\"IP\",\"a105.2way.net\",\"0.0.0.0\",1,1\r"
#define MDM_DSGACT  "AT#SGACT=1,0\r"
#define MDM_SGACT   "AT#SGACT=1,1,\"\",\"\"\r"
#define MDM_SSLCFG  "AT#SSLCFG=1,1,500,90,100,50,0,0,0,0\r"
#else
#define MDM_CREG    "AT+CEREG?\r\n"
#define MDM_CGDCONT "AT+CGDCONT=3,\"IPV4V6\",\"conetder.vzwentp\",\"0.0.0.0\",1,1\r\n"
//#define MDM_CGDCONT "AT+CGDCONT=3,\"IP\",\"conetder.vzwentp\"\r\n"


//#define MDM_CGDCONT           "AT+CGDCONT=3,\"IP\",\"NE01.VZWSTATIC\",\"0.0.0.0\",1,1\r\n"

#define MDM_OPENFRWL        "AT#FRWL=1,\"1.1.1.1\",\"0.0.0.0\"\r\n"
#define MDM_CLOSEFRWL       "AT#FRWL=0,\"1.1.1.1\",\"0.0.0.0\"\r\n"
//#define MDM_OPENLISTCPSKT   "AT#SKTL=1,0,503,0\r\n"
//#define MDM_CLOSELISTCPSKT  "AT#SKTL=0,0,503,0\r\n"
//#define MDM_QRYLISTCP_SKT      "AT#SKTL?\r\n"

#define MDM_QRY_SL          "AT#SL?"
#define MDM_OPN_SL          "AT#SL=1,1,503,0\r\n"
#define MDM_CLS_SL          "AT#SL=1,0,503,0\r\n"
//#define MDM_CLOSELISTCPSKT    "+++"


#define MDM_DSGACT  "AT#SGACT=3,0\r\n"
#define MDM_SCFG    "AT#SCFG=1,3,300,90,600,1\r\n"
#define MDM_SCFGEXT "AT#SCFGEXT=1,0,0,240,1,0\r\n"
//#define MDM_SGACT   "AT#SGACT=1,1,\"\",\"\"\r\n"
#define MDM_SGACT   "AT#SGACT=3,1\r\n"
#define MDM_SSLCFG  "AT#SSLCFG=1,1,500,90,100,50,0,0,0,0\r\n"
#endif

#define MDM_CSQ     "AT+CSQ\r\n"
//#define MDM_QSGACT  "AT#SGACT?\r\n"
#define MDM_SGACTQ  "AT#SGACT?\r\n"

#ifdef SSL
#define MDM_SD      "AT#SSLD=1,443,\"smart.connectder.com\",0,0\r"
#define MDM_SH      "AT#SSLH=1,0\r"
#define MDM_SSLEN   "AT#SSLEN=1,1\r"
#define MDM_SECCFG   "AT#SSLSECCFG=1,0,0\r"
#else
    #ifdef ATT3G
#define MDM_SD      "AT#SD=1,0,80,\"connectder.herokuapp.com\",0,0,0\r"//att
    #else
//#define MDM_SD      "AT#SD=1,0,80,\"10.4.1.5\",0,0,0\r"//vzn apn
#define MDM_SD1      "AT#SD=1,0,80,\""              //0x4000 memory area
#define MDM_SD2      "\",0,0,0\r"                   //0x4000 memory area
//#define MDM_SD2      "\",0,0,0\r"//vzn apn
    #endif
#define MDM_SH      "AT#SH=1\r\n"
#endif

#define B9600 1
#define B14400 2
#define B19200 3
#define B38400 5
#define B57600 6
#define B115200 7

#define MAXBAUDTOGGLES 4

// Modem Modes
typedef enum {
    MDM_MODE_HTTP,
    MDM_MODE_MODBUS
} modemmode;

// Modem Responses
typedef enum  {
    MDM_RESP_NOT_READY = 0,
    MDM_RESP_DISREGARD,
    MDM_RESP_OK,
    MDM_RESP_ERROR,
    MDM_RESP_CONNECT,
    MDM_RESP_SG_ACTIVE,
    MDM_RESP_CREG_OK,
    MDM_RESP_CREG_FAIL,
    MDM_RESP_CSQ_OK,
    MDM_RESP_CSQ_FAIL,
    MDM_RESP_SG_NOT_ACTIVE,
    MDM_RESP_SG_ACTIVE_IP,
    MDM_RESP_NO_CARRIER,
    MDM_RESP_SG_STORED,
    MDM_RESP_ACCEPT_REQUEST,
    MDM_RESP_MODBUS_MODE,
    MDM_RESP_HTTP_MODE,
    MDM_RESP_COMMAND_RECEIVED,
} modemresponse;

// Modem Requests
typedef enum  {
   HTTP_NO_REQ = 0,
   HTTP_REQ_UNIT_INFO,
   HTTP_SEND_STATUS,
   HTTP_SEND_REPORT,
   HTTP_GETLATEST_FW,
   MDM_QRY_MODE,
   MDM_SWITCH_TO_MODBUS,
   MDM_SWITCH_TO_HTTP,
   MDM_QRY_COMMAND_RECEIVED,
   MDM_REBOOT,
   MDM_FOTA_UPDATE
} modemrequest;

//++++++++++++++++++++++++++

typedef enum  {
    MDM_CYCLE_POWER = 1,     // 1
    MDM_POWER_LOW_TO,        // 2
    MDM_W4_BOOT,             // 3
    MDM_SET_ECHO,            // 4
    MDM_SET_IPR,             // 5
    MDM_SET_FLOW,            // 6
    MDM_SAVE_PROFILE,        // 7
    MDM_SET_SELINT,          // 8
    MDM_RD_ICCID,            // 9
    MDM_RD_IMEI,             // 10
    MDM_RD_MFW,              //11
    MDM_RD_CSQ,              //12
    MDM_RD_CREG,             // 13
    MDM_SET_CGDCONT,         // 14
    MDM_SSL_ENABLE,                // Added SSL
    MDM_QRY_SGACT,
    MDM_SET_SKIPESC,
    MDM_SET_ESCGUARDTIME,
    MDM_SET_SCFG,
    MDM_ACT_SGACT,
    MDM_DACT_SGACT,
    MDM_OPN_FRWL,
    MDM_CLS_FRWL,
    //MDM_QRY_LIS_SKT,
    //MDM_OPN_LIS_SKT,
    //MDM_CLS_LIS_SKT,
    MDM_W4_HTTP_MSG,
    MDM_W4_HTTP_RESP,
    MDM_READY,
    MDM_LISTENSKT_READY,
    MDM_LISTENSKT_OPEN,
    MDM_SOCKOPEN,
    MDM_CONNECTED,
    MDM_SOCKSHUT,
    MDM_HANGUP,
    MDM_SET_SSLCFG,
    MDM_SET_SECCFG,
    MDM_FW_DOWNLOAD,
    MDM_RD_IPR,
    MDM_SET_W,
    MDM_SET_CLK_MODE,
    MDM_RD_CLK,
    MDM_QRY_SKT,
    MDM_OPN_SKT,
    MDM_CLS_SKT,
    MDM_SET_SCFGEXT,
    MDM_QRY_FW,
    MDM_SET_FWVZW,
    MDM_SET_FWATT,
    MDM_SET_VERBOSE,
    MDM_SHUT_DOWN,
    MDM_RETRY_DELAY,
    MDM_W8,
    MDM_NUMBER,
    MDM_RD_CNTXT,
    MDM_RD_SWPKGV,
    MDM_RD_MODULE,
    MDM_ACT_RDP,
    MDM_SET_FOTA_CONT,
    MDM_SET_FOTA_CFG,
    MDM_ACT_FOTA,
    MDM_DACT_FOTA,
    MDM_OPEN_FTP,
    MDM_SET_FTP_CFG,
    MDM_CLOSE_FTP,
    MDM_SET_FTP_DIR,
    MDM_GET_OTA_FILE,
    MDM_UPDATE,
    MDM_W4_FOTA,
    MDM_FOTA_DLD
} modemstate;

void Telit_init_modem(void);
void Telit_modem_tick(void);
void proc_modem_char(void);
modemstate  get_modem_state(void);
void send_modem_message(int len);
modemresponse  modem_send_req(unsigned int * flag, unsigned int aReq);
int Telit_ProcessResponse(void);
//void init_mdm_UART(void);
unsigned int GetNetworkStatus();
unsigned int GetSignalQuality();
#endif /* TELIT_H_ */
