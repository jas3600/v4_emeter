/*
 * Modem_Identify.h
 *
 *  Created on: Nov 8, 2016
 *      Author: Afreen
 */



#ifndef MODEM_IDENTIFY_H_
#define MODEM_IDENTIFY_H_

//#include "Telit.h"

#define Telit 1
#define Inhand 2

void Initialize_Modem(char ch );
char Identify_Modem();

void (*Global_init_modem)();
void (*Global_modem_tick)();
void (*Global_proc_modem_char)();


void disable_analog();
void enable_analog();

char *Global_wait_connected;
char *Global_wait_connected_timer;
unsigned char *Global_rx_msg_timer_begin;
unsigned char  *Global_rx_msg_count;
volatile unsigned int *Global_gModemState;
volatile unsigned int *Global_gRespReceived;



#endif /* MODEM_IDENTIFY_H_ */
