/*************************************************************************
 * OTAUpdate.c
 *
 *  Created on: Jan 20, 2017
 *      Authors: Afreen / JAS
 *************************************************************************/
#include "OTAUpdate.h"

#include <msp430.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <io.h>
#include <emeter-toolkit.h>

#include "emeter-3ph-neutral-6779(A).h"
#include "Telit.h"
#include "Debug.h"
#include "Modem_Identify.h"
#include "emeter-communication.h"
#include "emeter-display.h"
#include "emeter-memory.h"
#include "MMC.h"
#include "emeter-fifo.h"
#include "BSLUpdate.h"


//#define DB_OTA
#ifdef DB_OTA
#define OTA_DBPRINT(...) PrintDebugMessage(__VA_ARGS__)
#else
#define OTA_DBPRINT(...)
#endif

int SD_CheckCRC(void);
int SD_CompareCRC(char* ch, unsigned int aCRC);
int CheckFWCRC(unsigned long int * aFWSize);
int xferFWtoSDCard(unsigned long int aFWSize, char* aServerFW);
void DisplaySDCardContent(unsigned long int aBlockNum);
int Check_For_BSL_Update(unsigned long int aSramEndFW);

unsigned long int lFWBlocks;
extern const unsigned int table[256];
unsigned int SDCRCTrial;
unsigned char OTAFailMsg[50];
unsigned int  Disp_Flag_msg;
extern const unsigned char BSLFwVersion[9];

/**********************************************************************************
 * UpdateFirmware(char* aServerFW)
 *
 * TODO Description
 *
 *********************************************************************************/
int UpdateFirmware(char* aServerFW)
{
	int rtv;
	unsigned long int lFWSize;
	SDCRCTrial = 5;
	rtv = FW_UPDATE_FAILED;
	// Make sure SD card exists
	//	 DisplaySramContent();
	if (mmcInit() == TRUE)
	{
		// Check the CRC
		if (CheckFWCRC(&lFWSize) == TRUE)
		{
			// Transfer to the SD CARD
			if (xferFWtoSDCard(lFWSize, aServerFW) == TRUE)
			{
				//delay between write fw and read CRC
				__delay_cycles(1000);

				while(SDCRCTrial-- > 0)
				{
					if (SD_CheckCRC() == TRUE)							//CRC check After FW is saved to SD card
					{
						// Disp_Flag_msg=3 indicates SD card Transfer Completed
						Disp_Flag_msg = 3;

						//Send "UPDATE 3" message to the LCD, which indicates the FW is Transferred to the SD card
						Disp_Lcd_ota(3);

						Clear_CRC_Error();
						//Displaying the Saved SDcard firmware

						// DisplaySDCardContent(lFWSize);
						OTA_DBPRINT("FW Saved to SD:");
						sprintf(OTAFailMsg,"SD CRC OK");

		                if (sdcard_installed() == TRUE)
		                {
		                    ClearMMCBlocks();
		                }


						if (Check_For_BSL_Update(lFWSize) == TRUE)
						{
						   UpdateBSL(lFWSize);
						}
						else
						{
						    rtv = FW_UPDATE_SUCCESS;
						}
						break;
					}
					else
					{
						OTA_DBPRINT("SD Save CRC Error:");
						sprintf(OTAFailMsg,"SD CRC Error");
					}
				}

			}
			else
			{
			   OTA_DBPRINT("FW NOT Saved to SD:");
			}
		}
		else
		{
			SET_CRC_Error();
			sprintf(OTAFailMsg,"PSRAM CRC Error");
//			gModemState = MDM_HANGUP;
		}
	}
	else
	{
		sprintf(OTAFailMsg,"SD Init Fail");
	}

	return rtv;
}


/**********************************************************************************
 * xferFWtoSDCard(unsigned long int aFWSize, char* aServerFW)
 *
 * TODO Description
 *
 *********************************************************************************/


int xferFWtoSDCard(unsigned long int aFWSize, char* aServerFW)
{
	unsigned long int i;
	int lRtv;
	int lTrycnt;
	unsigned long int lFWStart;

	lRtv = TRUE;

	lFWBlocks = aFWSize >> 9;                               // divide by 512

	if ((aFWSize & 0x000001FF) > 0) lFWBlocks++;            // add one if some data in the last block

	mmcEraseBlocks (SD_STARTING_BLOCK, lFWBlocks + SD_STARTING_BLOCK + 1);     // erase the Index Block and

    lFWStart = SD_STARTING_BLOCK + 1;

    // Write the Firmware
	for (i = 0; i <= lFWBlocks; i++)                                      // write the blocks to the sdcard
	{
	    read_psram_block(i << 9, (char *) Sd_buffer, 512);
	    lTrycnt = 10;
		while ((mmcWriteBlock (i + lFWStart, Sd_buffer) != 0) && (lTrycnt-- > 0))
		{
	          OTA_DBPRINT("Block Write Retry:");
			  sprintf(OTAFailMsg,"FWxfer2SD fail Block: %ld", i + lFWStart);
		}

	    if (lTrycnt <= 0) return FALSE;

	}

	// Write the Index Block
	sprintf((char *) Sd_buffer, "SERVER_FW=%s,START_SD_SECTOR=%ld,END_SD_SECTOR=%ld\0",aServerFW, lFWStart, lFWStart + lFWBlocks);													//ServerFW which is decimal type converting into string

	if (mmcWriteBlock (SD_STARTING_BLOCK, Sd_buffer) != 0)
	{
		// Index write failed
		OTA_DBPRINT("FW Index sector to SD Fail:");
		sprintf(OTAFailMsg,"FWxfer2SD fail @ Index write");
		lRtv = FALSE;
	}

	return lRtv;
}



/**********************************************************************************
 * DisplaySDCardContent(unsigned long int aBlockNum)
 *
 * TODO Description
 *
 *********************************************************************************/
void DisplaySDCardContent(unsigned long int aBlockNum)
{
	//unsigned char lBuffer[514];
	unsigned long int i;
	unsigned long int j;

	memset(Sd_buffer, 0, 514);
	for(i=0; i < (aBlockNum >> 9)+2; i++)
	{
		mmcReadBlock(i + SD_STARTING_BLOCK, Sd_buffer);

		for(j=0; j<512; j++)
		{
			while(UCA2STATW&UCBUSY);
			UCA2TXBUF = Sd_buffer[j];
		}
	}
}

/**********************************************************************************
 * DisplaySramContent(void)
 *
 * TODO Description
 *
 *********************************************************************************/
void DisplaySramContent(void)
{
	char ch;
	unsigned long int i;

	i=0;
	//	 stop_address=current_address;
	//	 OTA_DBPRINT("Emeter complete firmware detected");
	ch = '1';

	while (ch != '}')
	{
		ch = read_psram_byte(i++);
		while(UCA2STATW&UCBUSY);
		UCA2TXBUF = ch;
	}

}

/**********************************************************************************
 * CheckFWCRC(unsigned long int * aFWSize)
 *
 * TODO Description
 *
 *********************************************************************************/
int CheckFWCRC(unsigned long int * aFWSize)
{
	unsigned int  local_crc;
	unsigned int  server_crc;
	unsigned long  curr_address;
	unsigned char val1;
	char strServerCRC[20];
	char* tmpstr;
	int rtv;
	char index;

	rtv = FALSE;
	curr_address = 0;

	// Find the start of the FW
	while ((read_psram_byte(curr_address++) != '@') && (curr_address < 100));

	if ((curr_address > 0) && (curr_address < 100))
	{
		// @ was found in a reasonable number of chars
		// calculate the local_crc
		local_crc = 0;
		val1 = 0;
		curr_address--;

		while ((val1 != 'q') && (curr_address <= MAX_FW_LOAD_SIZE))
		{
			val1 = read_psram_byte(curr_address);

			if ((val1!=32) && (val1!=13) && (val1!=10) && (val1!=92) && (val1!=110) && (val1!=114))
			{
				index = (char) (local_crc ^ val1);
				local_crc = (unsigned int) ((local_crc >> 8) ^ table[index]);
			}
			curr_address++;
		}

		if (curr_address <= MAX_FW_LOAD_SIZE)
		{
			*aFWSize = curr_address;
			//   Extract Server CRC
			read_psram_block(curr_address, strServerCRC, 20);

			tmpstr = strstr(strServerCRC,"crc");

			if(tmpstr != NULL)
			{
				sscanf(tmpstr, "crc\"%*[^\"]\"%x", &server_crc);
			}

			// compare the crcs
			if (server_crc == local_crc)
			{
				// Disp_Flag_msg=2 indicates the CRC is correct
				Disp_Flag_msg=2;

				//Send "UPDATE 2" message to the LCD, which indicates the CRC is verified
				Disp_Lcd_ota(Disp_Flag_msg); //E CRC PASS
				OTA_DBPRINT("CRC verified : ");
				rtv = TRUE;
			}
			else
			{
				OTA_DBPRINT("CRC Not verified: ");
				Disp_Lcd_ota(6);   //E CRC FAIL
			}
		}
	}

	return rtv;
}


/**********************************************************************************
 * DetectSDCard(void)
 *
 * TODO Description
 *
 *********************************************************************************/
/*int DetectSDCard(void)
{
	int rtv;

	rtv = mmcInit();

	if (rtv == 0x00 || rtv == 0x01) rtv = TRUE;

	return rtv;
} */



/**********************************************************************************
 * init_SD(void)
 *
 * TODO Description
 *
 *********************************************************************************/
void init_SD(void)
{
	unsigned char val;
	int i;

	for(i=0; i<4; i++)
	{
		val = mmcInit();
		if(val == 1)  break;
	}

	if (val == TRUE)
	{
		sd_msg_ok(1);
		Clear_SD_Error();
		//OTA_DBPRINT("SD Detected");
	}
	else
	{
		sd_msg_ok(2);
		Set_SD_Error();
		//OTA_DBPRINT("SD Not Detected");
	}
}

/**********************************************************************************
 * SD_CheckCRC(void)
 *
 * TODO Description
 * Handles CRC split across blocks
 *
 *********************************************************************************/
#define SD_FIND_FW_START 0
#define SD_COMPUTE_FW_CRC 1
#define SD_FIND_c 2
#define SD_FIND_CLOSED_BRACKET 3


int SD_CheckCRC(void)
{
	unsigned int  SDlocal_crc;
	//unsigned char buffertemp[514];
	unsigned char val1;
	unsigned int save_sd_end_sector;
	char* tmpstr;
	int state;
	unsigned int i;
	unsigned int j;
	unsigned int k;
	int lRtv;
	char index;
	char crcbuf[30];

	lRtv = FALSE;
	state = SD_FIND_FW_START;
    SDlocal_crc = 0;

    if(mmcInit() == TRUE)
    {}
    else
    {
		sprintf(gDbgMsg,"SD CRC Read block failed Init SD");
		OTA_DBPRINT(gDbgMsg);
    	return lRtv;
    }

    for (k=0; k<30; k++) crcbuf[k] = 0;                     // clear the crcbuf
    k = 0;                                                  // CRC buffer index
    memset(Sd_buffer, 0, 514);
	if (mmcReadBlock(SD_STARTING_BLOCK, Sd_buffer) == TRUE)
	{
		tmpstr = strstr(Sd_buffer, ",END_SD_SECTOR=");

		if (tmpstr != NULL)
		{
			sscanf(tmpstr+15, "%d", &save_sd_end_sector);

			if ((save_sd_end_sector > SD_STARTING_BLOCK+1) && (save_sd_end_sector - SD_STARTING_BLOCK+1 < MAX_FW_LOAD_SECTORS))
			{
				for (i=SD_STARTING_BLOCK+1; i<=save_sd_end_sector; i++)
				{
					if (mmcReadBlock(i, Sd_buffer) == TRUE)
					{
						for (j=0 ; j<512 ;j++)
						{
							switch (state)
							{
								case SD_FIND_FW_START:
									if (Sd_buffer[j] == '@')
									{
										j = j - 1;                  // backup 1 to include the @ in the CRC
										state = SD_COMPUTE_FW_CRC;
									}
								break;

								case SD_COMPUTE_FW_CRC:
									// compute the CRC
									val1 = Sd_buffer[j];
									if((val1 != 32) && (val1 != 13) && (val1 != 10) && (val1 != 92) && (val1 != 110) && (val1 != 114))
									{
										index = (char)(SDlocal_crc ^ val1);
										SDlocal_crc = (unsigned int) ((SDlocal_crc >> 8) ^ table[index]);
									}
									// end of the FW?
									if(Sd_buffer[j] == 'q')
									{
										 k = 0;
										 state = SD_FIND_c;              // done with crc calc
									}
								break;

								case SD_FIND_c:                    // Find the first c in crc
									if(Sd_buffer[j] == 'c')
									{
										crcbuf[k++] = Sd_buffer[j];              // accumulate the crc in a buffer
										state = SD_FIND_CLOSED_BRACKET;
									}
                                break;

								case SD_FIND_CLOSED_BRACKET:     // find the last bracket
									if(Sd_buffer[j] == '}')                                              // Bracket found
									{
									   if (SD_CompareCRC(crcbuf, SDlocal_crc) == TRUE)
									   {
										   lRtv = TRUE;       // compare the CRC's
									   }

									   // end the loops
									   j = 512;
									   i = save_sd_end_sector + 1;
									}
									else
									{
									   if (k < 30)
									   {
										   crcbuf[k++] = Sd_buffer[j];         // continue accumulating the crc in a buffer
									   }
									   else
									   {
										   // Didn't find the last bracket in a reasonable number of chars
										   // end the loops and bale
										   j = 512;
										   i = save_sd_end_sector + 1;
									   }
									}
                                break;

								default:
								break;
							}
						}
					}
					else
					{
						sprintf(gDbgMsg,"SD CRC Read block failed at %d",i);
						OTA_DBPRINT(gDbgMsg);
						//return lRtv;
					}
				}
			}
		}
	}
	else
	{
		sprintf(gDbgMsg,"SD CRC Read block failed read index");
		OTA_DBPRINT(gDbgMsg);
	}

	return lRtv;
}

/***********************************************************************************
 *  SD_CompareCRC(unsigned char* ch, unsigned int aCRC)
 *
 *  TODO Description
 *
 ***********************************************************************************/
int SD_CompareCRC(char* ch, unsigned int aCRC)
{
	unsigned int SDserver_crc;
	char* tmpstr;
	int lRtv;

	lRtv = FALSE;

	tmpstr = strstr(ch, "crc");

	if (tmpstr != NULL)
	{
		sscanf(tmpstr,"crc\"%*[^\"]\"%x", &SDserver_crc);
	}

	// compare the CRC's inside the SD card
	if (SDserver_crc == aCRC)
	{
		OTA_DBPRINT("SD CRC OK");
		Disp_Lcd_ota(4);
		lRtv = TRUE;
	}
	else
	{
		sprintf(gDbgMsg,"SD CRC MissMatch UI CRC %x , Local CRC %x",SDserver_crc,aCRC);
		OTA_DBPRINT(gDbgMsg);
		OTA_DBPRINT("SD CRC FAIL");
		Disp_Lcd_ota(5);
	}

	return lRtv;
}

int Check_For_BSL_Update(unsigned long int aSramEndFW)
{
    int lRtv;
    unsigned int i;
    char* tmpstr;
    unsigned int lBSLVerInt[9];
    unsigned char lBSLVerChar[9];
    unsigned long int lVerAdd;
    int lNumConversions;
    char lDBStr[50];

    lRtv = FALSE;

    // Search for code in BSL area (@1000) in the first block
    read_psram_block(0, (char *) Sd_buffer, 512);
    Sd_buffer[512] = '\0';

    tmpstr = strstr(Sd_buffer, "@1000\r");

    if (tmpstr != NULL)
    {
        OTA_DBPRINT("BSL Code is Present");

        // Check if it is different than the current version.
        read_psram_block(aSramEndFW - 100, (char *) Sd_buffer, 100);     // Read the end of the FW

        tmpstr = strstr(Sd_buffer, "@8bff0\r");                          // Find the BSL FW version

        if (tmpstr != NULL)
        {    // Convert to decimals
            lNumConversions = sscanf(tmpstr + 1, "%lx%x%x%x%x%x%x%x%x%x",
                                     &lVerAdd, &lBSLVerInt[0], &lBSLVerInt[1], &lBSLVerInt[2],
                                               &lBSLVerInt[3], &lBSLVerInt[4], &lBSLVerInt[5],
                                               &lBSLVerInt[6], &lBSLVerInt[7], &lBSLVerInt[8]);

            if (lNumConversions == 10)
            {
                for (i = 0; i < 9; i++) lBSLVerChar[i] = lBSLVerInt[i];  // convert to char string


                if (strncmp(BSLFwVersion, lBSLVerChar, 8) == 0)         // compare to the current version
                {
                    OTA_DBPRINT("BSL Code is Not New");
                }
                else
                {   // Different Version

                    if (BSLFwVersion[8] == 0)
                    {
                       sprintf(lDBStr,"OLD: %s -> NEW: %s", BSLFwVersion, lBSLVerChar);
                    }
                    else
                    {
                        sprintf(lDBStr,"OLD: NA -> NEW: %s", lBSLVerChar);
                    }

                    OTA_DBPRINT(lDBStr);

                    lRtv = TRUE;
                }
            }
        }
    }

return lRtv;
}

