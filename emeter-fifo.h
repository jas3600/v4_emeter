/*
 * emeter-fifo.h
 *
 *  Created on: Dec 9, 2016
 *      Author: John
 */

#ifndef EMETER_FIFO_H_
#define EMETER_FIFO_H_

// #define ENABLE_FIFO

#define SD_CARD_START_BLOCK    2048UL
#define SD_BLOCKS_FOR_FIFO     500UL
//#define SD_BLOCKS_FOR_FIFO     15625UL



/*
 * Uncomment to test data retention
 */
//#define TEST_DATA_RETENTION

#define MSG_STS_NEW  1
#define MSG_STS_GEN_TIME  2

typedef struct{
  uint16_t status;
  uint8_t  century;
  uint8_t  year;
  uint8_t  month;
  uint8_t  day;
  uint8_t  hour;
  uint8_t  minute;
  uint8_t  second;
  int32_t  power_factor; //Branch Power factor BPF (line 1)
  int32_t frequency;	//Frequency F (line 1)
  int32_t active_power; //Branch Real Power Bw
  int32_t reactive_power; //Branch Reactive Power Bvar
  int32_t temperature;	//Temprature T
  uint32_t produced_energy; //Branch Produced Kwh BPKwh
  uint32_t consumed_energy; //Branch Consumed Kwh BCKwh
  int32_t rms_voltage; //Branch Line to Line Voltage BLLV
  int32_t rms_current; //Branch Line 1 Current BL1V
//Modbus Parameters Continuations
  int32_t BL1V; //branch l1 voltage
  int32_t BL2V; //branch l2 voltage
  int32_t BL2I; //branch l2 current
  int32_t LSPf; //load side power factor
  int32_t LSw; //Load side real active power
  int32_t LSvar; //Load side Reactive power
  uint32_t LSCKwh; //Load side consumed Kwh
  uint32_t LSPKwh;//load side produced Kwh
  int32_t LSL1I; //load side l1 current
  int32_t LSL2I; //load side l2 current
}readings_t;

// Message Types
#define MSG_READINGS     1
#define MSG_GET_CONFIG   2
#define MSG_STATUS       3

typedef struct {
  uint8_t  rptstate;
  uint8_t  rptsendattemps;
  uint8_t  rpttype;
  uint8_t  rptnummsgs;
} report_t;

typedef struct {
  uint8_t  msgstate;
  uint8_t  sendattemps;
  uint8_t  msgtype;
  uint8_t  msglen;
} fifomsg_t;

void init_msg_fifo(void);
uint32_t push_msg_fifo(char * aMsg, uint8_t aType, uint8_t aLen);
uint16_t peek_msg_fifo(char *aMsg, fifomsg_t * aFifomsg, uint16_t aDepth, uint32_t * aSramLoc);
uint32_t pop_msg_fifo(void);
uint32_t pop_msg_fifo_Tail(void);
int getFIFODepth(void);
//void save_msg_fifo(void);
//void restore_msg_fifo(void);
void ClearMMCBlocks(void);
void EraseMMCBlocks(void);
void save_msg_fifo2(void);
void restore_msg_fifo2(void);
// int IsReadingValid(int gNumMessages);
int IsReadingValid(readings_t * aReading);
int FIFOTimeValid(void);

void FixFIFOTime(rtc_t old_rtc);
void Unprovision(void);
int TestDataRetention(void);
int FIFOTimeStampsValid(void);
int ZeroBlock(char * aBlock);
void setClearMMC(void);
int clearMMCQuery(void);

#endif /* EMETER_FIFO_H_ */
