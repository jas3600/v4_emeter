

#ifndef _MMCLIB_H
#define _MMCLIB_H


// macro defines
#define HIGH(a) ((a>>8)&0xFF)               // high byte from word
#define LOW(a)  (a&0xFF)                    // low byte from word

#define DUMMY 0xff

#define SD_STANDARD_CAPACITY  0
#define SD_HIGH_CAPACITY      1

// Tokens (necessary  because at NPO/IDLE (and CS active) only 0xff is on the data/command line)
#define MMC_START_DATA_BLOCK_TOKEN          0xfe   // Data token start byte, Start Single Block Read
#define MMC_START_DATA_MULTIPLE_BLOCK_READ  0xfe   // Data token start byte, Start Multiple Block Read
#define MMC_START_DATA_BLOCK_WRITE          0xfe   // Data token start byte, Start Single Block Write
#define MMC_START_DATA_MULTIPLE_BLOCK_WRITE 0xfc   // Data token start byte, Start Multiple Block Write
#define MMC_STOP_DATA_MULTIPLE_BLOCK_WRITE  0xfd   // Data toke stop byte, Stop Multiple Block Write


// an affirmative R1 response (no errors)
#define MMC_R1_RESPONSE       0x00


// this variable will be used to track the current block length
// this allows the block length to be set only when needed
// unsigned long _BlockLength = 0;

// error/success codes
#define MMC_SUCCESS           0x00
#define MMC_BLOCK_SET_ERROR   0x01
#define MMC_RESPONSE_ERROR    0x02
#define MMC_DATA_TOKEN_ERROR  0x03
#define MMC_INIT_ERROR        0x04
#define MMC_CRC_ERROR         0x10
#define MMC_WRITE_ERROR       0x11
#define MMC_OTHER_ERROR       0x12
#define MMC_TIMEOUT_ERROR     0xFF


// commands: [47] Start bit 0 , [46] transmission bit 1; [45:40] CMD-number [39:8] Argument [7:1] CRC [0] Endbit 0
                                            // Command   Response Format
#define MMC_GO_IDLE_STATE          0     //CMD0
#define MMC_SEND_OP_COND           1     //CMD1
#define MMC_SWITCH_FUNC            6     //CMD6
#define MMC_SEND_IF_COND           8     //CMD8    R7 [31:12] reserved [11:8] Supply Volts [7:0] check pattern
#define MMC_READ_CSD               9     //CMD9    R2     [31:16] RCA [15:0] stuff bits
#define MMC_SEND_CID               10     //CMD10   R2     [31:16] RCA [15:0] stuff bits
#define MMC_STOP_TRANSMISSION      12     //CMD12
#define MMC_SEND_STATUS            13     //CMD13   R1     [31:16] RCA [15:0] stuff bits
#define MMC_SET_BLOCKLEN           16     //CMD16   R1     Set block length for next read/write
#define MMC_READ_SINGLE_BLOCK      17     //CMD17   R1   Read block from memory
#define MMC_READ_MULTIPLE_BLOCK    18     //CMD18   R1
#define MMC_WRITE_BLOCK            24     //CMD24   R1
#define MMC_WRITE_MULTIPLE_BLOCK   25     //CMD25   R1
#define MMC_WRITE_CSD              27     //CMD27   R1 PROGRAM_CSD
#define MMC_SET_WRITE_PROT         28     //CMD28   R1b
#define MMC_CLR_WRITE_PROT         29     //CMD29   R1b
#define MMC_SEND_WRITE_PROT        30     //CMD30   R1
#define MMC_ERASE_WR_BLK_START_ADDR     32     //CMD32  R1
#define MMC_ERASE_WR_BLK_END_ADDR       33     //CMD33  R1
#define MMC_UNTAG_SECTOR           34     //CMD34
//#define MMC_ERASE_BLOCK_START      35     //CMD35      R1
//#define MMC_ERASE_BLOCK_END         36    //CMD36      R1
#define MMC_ERASE                  38     //CMD38      R1b
#define MMC_LOCK_UNLOCK            42     //CMD42      R1
#define MMC_APP_CMD                55     //CMD55      R1
#define MMC_READ_OCR               58     //CMD58      R3
#define MMC_CRC_ON_OFF             59     //CMD59
#define MMC_ACMD41                  41
#define MMC_ACMD22                  22   //             R1     [31:0] stuffbits
#define MMC_ACMD23                  23   //             R1     [31:23] Stuff bits  [22:0] number of blocks


unsigned char Sd_buffer[514];

// Test the SD card
int TestSDCard(void);

// mmc init
int mmcInit(void);

// read a 512 Byte size block (Block addressing)
unsigned char mmcReadBlock(unsigned long address, unsigned char *pBuffer);

// write a 512 Byte big block (Block addressing)
int mmcWriteBlock (unsigned long address, unsigned char *pBuffer);

int mmcStartWriteMultiBlock (unsigned long address, unsigned long numberofblocks);

int mmcWriteMultiBlock (unsigned char *pBuffer);

int mmcEndWriteMultiBlock (void);

// Erase from start address to end address (Block addressing)
int mmcEraseBlocks (unsigned long start_address, unsigned long end_address);

// Returns if the SD card is installed
int sdcard_installed(void);

int TestSD(const unsigned long address, int numwrites);

#endif /* _MMCLIB_H */
