/**************************************************************************
 * Telit.c
 *
 *  Created on: Jan, 2017
 *      Author: JAS
 *
 **************************************************************************/


#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <io.h>
#include <time.h>
#include <emeter-toolkit.h>

#include "Modem_Identify.h"
#include "emeter-structs.h"
#include "Telit.h"
#include "emeter-communication.h"
#include "OTAUpdate.h"
#include "emeter-http.h"
#include "Debug.h"
#include "emeter-rtc.h"
#include "emeter-display.h"
#include "emeter-boot-reset.h"
#include "emeter-fifo.h"
#include "mb.h"

//#define DB_MDM
#ifdef DB_MDM
#define MDM_DBPRINT(...) PrintDebugMessage(__VA_ARGS__)
#else
#define MDM_DBPRINT(...)
#endif

modemmode gModemMode;
int gModbusCommandFlag;

int gBaudRateToggleCnt;
uint16_t gModemTOFlag;
uint16_t gStoredResp;

unsigned int gNetwork;
unsigned int gSigQuality;
volatile unsigned int gSockSendReq;

int gMdmBufferStatus;

int gW4Response;
int gCurrentBaud;
int gFOTAflag = 0;
int FOTATimer;
char gICCID[30];
char gIMEI[30];
char gMFW[20];
char gNumber[20];

int FWDownloadComplete;

modemstate gModemState;
modemstate gPrevModemState;

int gResponse;

volatile uint16_t * gCommsRespFlag;

int gBaudRateToggleCnt;

int gSG3ACT;
uint16_t gIPA[4];

int gSocketToOpen;
int modemRetry=1;
//firmwaretype gFWFlag;
unsigned int gFWFlag = 0;

void send_mdm_command(uint16_t aCmd, uint16_t aTimeout);
void set_uart_baudrate(int);
void clear_modem_rx_buffer(void);
void mdm_set_rtc(char * tm_str);

/******************************************************************************
 *   get_modem_state(void)
 *
 *   TODO Description
 *
 *****************************************************************************/
modemstate get_modem_state(void)
{
    return gModemState;
}

/******************************************************************************
 *  modem_send_req(uint16_t * flag, uint16_t aReq)
 *
 *   TODO Description
 *
 *****************************************************************************/
modemresponse modem_send_req(uint16_t * flag, uint16_t aReq)
{
    modemresponse lRtv;

    lRtv = MDM_RESP_NOT_READY;

    *flag = NO_RESPONSE;
    gCommsRespFlag = flag;

    switch (aReq)
    {
        case HTTP_NO_REQ:

            break;
        case HTTP_REQ_UNIT_INFO:
        case HTTP_SEND_STATUS:
        case HTTP_SEND_REPORT:
        case HTTP_GETLATEST_FW:
            if (((gModemState == MDM_READY) || (gModemState == MDM_CONNECTED)) && (gSockSendReq == HTTP_NO_REQ))
            {
               gSockSendReq = aReq;
               lRtv = MDM_RESP_ACCEPT_REQUEST;
            }
            break;
        case MDM_QRY_MODE:
            if ((gModemState == MDM_READY) || (gModemState == MDM_CONNECTED)) lRtv = MDM_RESP_HTTP_MODE;
            else if((gModemState == MDM_LISTENSKT_OPEN) || (gModemState == MDM_LISTENSKT_READY)) lRtv = MDM_RESP_MODBUS_MODE;
            else lRtv = MDM_RESP_NOT_READY;
           break;
        case MDM_SWITCH_TO_MODBUS:
              MDM_DBPRINT("Switching to MODBUS MODE");
              gModemMode = MDM_MODE_MODBUS;
            break;
        case MDM_SWITCH_TO_HTTP:
              MDM_DBPRINT("Switching to HTTP MODE");
              gModemMode = MDM_MODE_HTTP;
            break;
        case MDM_REBOOT:
              MDM_DBPRINT("Rebooting MODEM");
              Telit_init_modem();
            break;
        case MDM_QRY_COMMAND_RECEIVED:
            if (gModbusCommandFlag == TRUE)
            {
                lRtv =  MDM_RESP_COMMAND_RECEIVED;
                gModbusCommandFlag = FALSE;
            }
        default:

            break;
    }

    return lRtv;
}

/******************************************************************************
 *   set_uart_baudrate(int brate)
 *
 *   TODO Description
 *
 *****************************************************************************/
void set_uart_baudrate(int brate)
{

    if (brate == B57600)
    {
        UCA0CTL1 |= UCSWRST;
        UCA0IE &= ~UCRXIE;

        UCA0BR1 = 0x00;            // 57600 @ 24MHz over sampling
        UCA0BR0 = 0x1B;
//        UCA0BR0 = 0x1B;
        UCA0MCTLW = 0x0011;       // 57600 @ 24Mhz oversampling

        UCA0CTL1 &= ~UCSWRST;
        UCA0IE |= UCRXIE;

        gCurrentBaud = B57600;
    }
    else if (brate == B115200)
    {
        UCA0CTL1 |= UCSWRST;
        UCA0IE &= ~UCRXIE;

        UCA0BR1 = 0x00;            // 115200 @ 24MHz
        UCA0BR0 = 0xD1;
        UCA0MCTLW = 0x0060;       // 115200 @ 24Mhz

        UCA0CTL1 &= ~UCSWRST;
        UCA0IE |= UCRXIE;

        gCurrentBaud = B115200;
    }
}

/*****************************************************************************
  send_modem_message(int len)

  Sends text in tx_msg buffer

*****************************************************************************/
void send_modem_message(int len)
{

    clear_modem_rx_buffer();
    tx_msg[MODEM_PORT].ptr = 0;
    tx_msg[MODEM_PORT].len = len;


//	gRespReceived = FALSE;
	sprintf(gDbgMsg,"Mdm TX: %s",tx_msg[MODEM_PORT].buf.uint8);
	MDM_DBPRINT(gDbgMsg);

    UCA0IE |= UCTXIE;
}

/*****************************************************************************
  clear_modem_rx_buffer(void)

  Clears the modem rx buffer

*****************************************************************************/
void clear_modem_rx_buffer(void)
{
    int i;

    for (i=0; i< MAX_MESSAGE_LENGTH; i++) rx_msg[MODEM_PORT].buf.uint8[i] = 0;
    rx_msg[MODEM_PORT].len = 0;
    rx_msg[MODEM_PORT].ptr = 0;
    gMdmBufferStatus = BUFFER_EMPTY;

}


/*****************************************************************************
  proc_modem_char(char ch)

  Processes a received character.

*****************************************************************************/
void proc_modem_char()
{
    int lRtv;
    char ch;

   if(gModemState == MDM_LISTENSKT_OPEN)
   {
        pxMBFrameCBByteReceived(  );
   }
   else
   {
        ch = UCA0RXBUF;

        if (gModemState == MDM_FW_DOWNLOAD)
        {

            lRtv = proc_FW_Chunks(ch);
            if (lRtv == FW_DOWNLOAD_COMPLETE)
            {
                //function call moved to telit state machine
                //UpdateFirmware(gNewFWVersion);
                FWDownloadComplete = 1;
            }
            else if (lRtv == FW_DOWNLOAD_ERROR)
            {
                gModemTOFlag = TRUE;
            }
        }
        else
        {

            if (rx_msg[MODEM_PORT].ptr < (MAX_MESSAGE_LENGTH - 1))
            {
                rx_msg[MODEM_PORT].buf.uint8[rx_msg[MODEM_PORT].ptr++] = ch;


                if (ch == '\n')
                {
                    if (rx_msg[MODEM_PORT].ptr > 2)
                    {

                        rx_msg[MODEM_PORT].len = rx_msg[MODEM_PORT].ptr;
                        rx_msg[MODEM_PORT].buf.uint8[rx_msg[MODEM_PORT].ptr] = 0;  // Tag on Null

                        gResponse = Telit_ProcessResponse();

                    }

                    rx_msg[MODEM_PORT].ptr = 0;
                }
                else
                {
                    gMdmBufferStatus = BUFFER_FILLING;
                }
            }
            else
            {
                gMdmBufferStatus = BUFFER_OVERFLOW;
            }
        }
   }
}

/******************************************************************************
 *   send_mdm_command(uint16_t aCmd, uint16_t aTimeout)
 *
 *   TODO Description
 *
 *****************************************************************************/
void send_mdm_command(modemstate aCmd, uint16_t aTimeout)
{
      int len;
      char * strcmd;
      char lCmdBuf[60];

      switch (aCmd)
      {
        case MDM_SHUT_DOWN:
            strcmd = MDM_SHUTDOWN;
            break;
        case MDM_SET_ECHO:
            strcmd = MDM_ECHO;
            break;
        case MDM_SET_IPR:
            strcmd = MDM_IPR_57600;
            break;
        case MDM_SET_VERBOSE:
            strcmd = MDM_ERR_VERBOSE;
            break;
        case MDM_SET_SKIPESC:
            strcmd = MDM_SKIPESC;
            break;
        case MDM_SET_ESCGUARDTIME:
            strcmd = MDM_ESCGT;
            break;
        case MDM_SET_FLOW:
            strcmd = MDM_FLOW_CNT_OFF;
            break;
        case MDM_SAVE_PROFILE:
            strcmd = MDM_WRITE_PROFILE;
            break;
        case MDM_SET_SELINT:
            strcmd = MDM_SELINT;
            break;
        case MDM_RD_ICCID:
            strcmd = MDM_ICCID;
            break;
        case MDM_RD_IMEI:
            strcmd = MDM_IMEI;
            break;
        case MDM_RD_MODULE:
            strcmd = MDM_MODULE;
            break;
        case MDM_RD_MFW:
            strcmd=MDM_MFW;
            break;
        case MDM_RD_SWPKGV:
            strcmd = MDM_SWPKGV;
            break;
        case MDM_RD_CSQ:
            strcmd = MDM_CSQ;
            break;
        case MDM_RD_CREG:
            strcmd = MDM_CREG;
            break;
        case MDM_NUMBER:
            strcmd = MDM_MDN;
            break;
        case MDM_QRY_FW:
            strcmd = MDM_FWQ;
            break;
        case MDM_SET_FWVZW:
            strcmd = MDM_FW_VZW;
            break;
        case MDM_SET_CLK_MODE:
            strcmd = MDM_CLKMODE;
            break;
        case MDM_RD_CLK:
            strcmd = MDM_CCLK;
            break;
        case MDM_SET_CGDCONT:
            strcmd = MDM_CGDCONT;
            break;
        case MDM_QRY_SGACT:
            strcmd = MDM_SGACTQ;
            break;
        case MDM_SET_SCFG:
            strcmd = MDM_SCFG;
            break;
        case MDM_SET_SCFGEXT:
            strcmd = MDM_SCFGEXT;
            break;
        case  MDM_ACT_SGACT:
            strcmd = MDM_SGACT;
            break;
        case MDM_ACT_RDP:
            strcmd = MDM_RDP;
            break;
        case MDM_RD_CNTXT:
            strcmd = MDM_CONTEXT;
            break;
        case MDM_DACT_SGACT:
            strcmd = MDM_DSGACT;
            break;
        case MDM_OPN_FRWL:
            strcmd = MDM_OPENFRWL;
            break;
        case MDM_CLS_FRWL:
            strcmd = MDM_CLOSEFRWL;
            break;
/*      case MDM_QRY_LIS_SKT:
            strcmd = MDM_QRYLISTCP_SKT;
            break;
        case MDM_OPN_LIS_SKT:
            strcmd = MDM_OPENLISTCPSKT;
            break;
        case MDM_CLS_LIS_SKT:
            strcmd = MDM_CLOSELISTCPSKT;
            break;                          */
        case MDM_SOCKSHUT:
             strcmd = MDM_SH;
            break;
        case MDM_HANGUP:
             strcmd = "+++\r\n";
            break;
        case MDM_QRY_SKT:
            strcmd = MDM_QRY_SL;
            break;
        case MDM_OPN_SKT:
            strcmd = MDM_OPN_SL;
            break;
        case MDM_CLS_SKT:
            strcmd = MDM_CLS_SL;
            break;
        case MDM_SOCKOPEN:
            if (gSocketToOpen == 1)
            {
                //sprintf(lCmdBuf, "AT#SD=1,0,80,\"18.233.3.81\",0,0,0\r\n");  VZW certification dummy server
               sprintf(lCmdBuf, "AT#SD=1,0,80,\"%d.%d.%d.%d\",0,0,0\r\n",
                                               nv_sys_conf.s.DNS_Address_1[0],
                                               nv_sys_conf.s.DNS_Address_1[1],
                                               nv_sys_conf.s.DNS_Address_1[2],
                                               nv_sys_conf.s.DNS_Address_1[3]);
            }
            else
            {
                //sprintf(lCmdBuf, "AT#SD=1,0,80,\"18.233.73.21\",0,0,0\r\n");  VZW certification dummy server 2
                sprintf(lCmdBuf, "AT#SD=1,0,80,\"%d.%d.%d.%d\",0,0,0\r\n",
                                               nv_sys_conf.s.DNS_Address_2[0],
                                               nv_sys_conf.s.DNS_Address_2[1],
                                               nv_sys_conf.s.DNS_Address_2[2],
                                               nv_sys_conf.s.DNS_Address_2[3]);

            }
           strcmd = lCmdBuf;
        break;

        case MDM_SET_FOTA_CONT:
            strcmd = MDM_FOTA_CONT;
            break;
        case MDM_SET_FOTA_CFG:
            strcmd = MDM_FOTA_CFG;
            break;
        case MDM_ACT_FOTA:
            strcmd = MDM_FOTA_ACT;
            break;
        case MDM_DACT_FOTA:
            strcmd = MDM_FOTA_DACT;
            break;
        case MDM_OPEN_FTP:
            strcmd = MDM_OPN_FTP;
            break;
        case MDM_SET_FTP_CFG:
            strcmd = MDM_FTP_CFG;
            break;
        case MDM_CLOSE_FTP:
            strcmd = MDM_CLS_FTP;
            break;
        case MDM_GET_OTA_FILE:
            strcmd = MDM_GET_OTA;
            break;
        case MDM_UPDATE:
            strcmd = MDM_FOTA;
            break;

      }

      gModemState = aCmd;
      gStoredResp = MDM_RESP_DISREGARD;

      strcpy((char *) (tx_msg[MODEM_PORT].buf.uint8), strcmd);
      len = strlen((char *) tx_msg[MODEM_PORT].buf.uint8);

      __delay_cycles(480000);
      start_modem_timer(&gModemTOFlag, aTimeout);
      send_modem_message(len);
}

/******************************************************************************
 *   Telit_init_modem(void)
 *
 *   TODO Description
 *
 *****************************************************************************/
void Telit_init_modem(void)
{
    P1DIR &= ~BIT0;                             //MDM POWER MON PIN
    P3DIR &= ~BIT2;                             //MDM_RESET HiZ
    comms_meter_status &= ~COMMS_STATUS_OK;
    gSockSendReq = HTTP_NO_REQ;
    set_uart_baudrate(B57600);
    clear_modem_rx_buffer();
    gW4Response = FALSE;
    MDM_DBPRINT("Init Modem:");
    //P3DIR &= ~BIT3;

    if ((P1IN & BIT0) > 0)
    {
      send_mdm_command(MDM_SET_ECHO, 2);
    }
    else
    {
        gModemState = MDM_CYCLE_POWER;
    }

    gSocketToOpen = 1;
}

/******************************************************************************
 *   Telit_modem_tick(void)
 *
 *   TODO Description
 *
 *****************************************************************************/
void Telit_modem_tick(void)
{
    eMBErrorCode lMBRtv;

    if (gModemState != gPrevModemState)   // If in the same state then probably waiting for response to command
    {
           gW4Response = FALSE;            // changed states so not waiting for a response yet
           gPrevModemState = gModemState;
    }

    switch (gModemState)
    {
/*    case MDM_CYCLE_POWER:
        P3DIR |= BIT3;     // Output
        P3OUT |= BIT3;    // Low
        start_modem_timer(&gModemTOFlag, 10);         // 10 sec
        gModemState = MDM_POWER_LOW_TO;
      break; */

/*      case MDM_SHUT_DOWN:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              gModemState = MDM_CYCLE_POWER;
          }
          break;
*/
      case MDM_CYCLE_POWER:
          P3DIR |= BIT3;
          P3OUT |= BIT3;                         // sets ON/OFF inverter transistor low.
          start_modem_timer(&gModemTOFlag, 6);
        //  P3DIR &= ~BIT3;
          gModemState = MDM_POWER_LOW_TO;

      case MDM_POWER_LOW_TO:
        if (gModemTOFlag == TRUE)      // hold low for at least 5 seconds
        {
            P3DIR &= ~BIT3;              // Take to HighZ input
            //P3OUT &= BIT3;                 // sets ON/OFF inverter transistor high.
            start_modem_timer(&gModemTOFlag, 8);       // 8 sec
            gModemState = MDM_W4_BOOT;
        }
      break;

      case MDM_W4_BOOT:
        if (gModemTOFlag == TRUE)      // hold low for at least 8 seconds
        {
          if ((P1IN & BIT0) > 0)      // If Powermon high
          {
            gBaudRateToggleCnt = 0;
            send_mdm_command(MDM_SET_ECHO, 2);
          }
          else
          {
              gModemState = MDM_CYCLE_POWER;
          }
        }
      break;

      case MDM_SET_ECHO:
          comms_meter_status &= ~COMMS_STATUS_OK;
          if (gModemTOFlag == TRUE)                  // Try a new baud rate
          {
              if(P1IN & BIT0 == BIT0)                // GSM_PWRMON is High
              {
                  if (gBaudRateToggleCnt++ < MAXBAUDTOGGLES)
                  {
                      // Toggle Baud rate
                      if ((gBaudRateToggleCnt & 1) > 0)     // first bit toggles on each count
                      {
                        set_uart_baudrate(B57600);
                      }
                      else
                      {
                        set_uart_baudrate(B115200);
                      }
                      send_mdm_command(MDM_SET_ECHO,2);
                  }
                  else
                  {
                      gModemState = MDM_CYCLE_POWER;                // Start Over
                  }
              }
              else
              {
                  gModemState = MDM_CYCLE_POWER;
              }

          }
       break;        // case MDM_SET_ECHO

       case MDM_SET_IPR:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
      break;

      case  MDM_SET_SELINT:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
               send_mdm_command(MDM_SET_ECHO,2);
          }
        break;

      case   MDM_SET_FLOW:
        if (gModemTOFlag == TRUE)                  // Timed out?
        {
            send_mdm_command(MDM_SET_ECHO,2);
        }
     break;

      case   MDM_SET_SKIPESC:
        if (gModemTOFlag == TRUE)                  // Timed out?
        {
            send_mdm_command(MDM_SET_ECHO,2);
        }
     break;

      case   MDM_SET_ESCGUARDTIME:
        if (gModemTOFlag == TRUE)                  // Timed out?
        {
            send_mdm_command(MDM_SET_ECHO,2);
        }
     break;

      case   MDM_SAVE_PROFILE:
        if (gModemTOFlag == TRUE)                  // Timed out?
        {
            send_mdm_command(MDM_SET_ECHO,2);
        }
        break;

      case   MDM_RD_ICCID:
        if (gModemTOFlag == TRUE)                  // Timed out?
        {
            send_mdm_command(MDM_SET_ECHO,2);
        }
        break;

      case   MDM_RD_IMEI:
        if (gModemTOFlag == TRUE)                  // Timed out?
        {
            send_mdm_command(MDM_SET_ECHO,2);
        }
        break;
      case   MDM_NUMBER:
        if (gModemTOFlag == TRUE)                  // Timed out?
        {
            send_mdm_command(MDM_SET_ECHO,2);
        }
      case   MDM_RD_CNTXT:
        if (gModemTOFlag == TRUE)                  // Timed out?
        {
            send_mdm_command(MDM_SET_ECHO,2);
        }
        break;
      case MDM_RD_MODULE:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
          break;
      case MDM_RD_MFW:
          if(gModemTOFlag == TRUE)
          {
              send_mdm_command(MDM_SET_ECHO,2);     //Timed out ?
          }
          break;
      case MDM_RD_SWPKGV:
          if(gModemTOFlag == TRUE)
          {
              send_mdm_command(MDM_SET_ECHO,2);     //Timed out ?
          }
          break;
      case MDM_RD_CSQ:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
      break;
      case MDM_RD_CREG:
        if (gModemTOFlag == TRUE)                  // Timed out?
         {
            send_mdm_command(MDM_SET_ECHO,2);
         }
      break;
      case MDM_SET_CLK_MODE:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
      break;
      case MDM_RD_CLK:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
      break;
      case MDM_SET_CGDCONT:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
        break;
      case MDM_QRY_FW:
          if (gModemTOFlag == TRUE)
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
          break;
      case MDM_SET_FWVZW:
          if (gModemTOFlag == TRUE)
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
          break;
      case MDM_SET_VERBOSE:
          if (gModemTOFlag == TRUE)
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
          break;
      case MDM_W8:
          if (gModemTOFlag == TRUE)
          {
              gModemState = MDM_CYCLE_POWER;
          }
          break;

#ifdef SSL
      case MDM_SSL_ENABLE:
          strcat(buff, MDM_SSLEN);
          gCurrModemState = MDM_SSL_ENABLE;
          gModemState = MDM_W4_RESP;
       break;
#endif

      case MDM_QRY_SGACT:
          // Decide what to do
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
        break;

      case MDM_SET_SCFG:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
          break;

      case MDM_SET_SCFGEXT:
          if(gModemTOFlag == TRUE)
          {
              send_mdm_command(MDM_SET_SCFGEXT,2);
          }
          break;


      case MDM_ACT_SGACT:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
        break;
      case MDM_ACT_RDP:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
        break;
#ifdef SSL
          case MDM_SET_SSLCFG:
          strcat(buff,MDM_SSLCFG);
          gCurrModemState=MDM_SET_SSLCFG;
          gModemState=MDM_W4_RESP;
          break;

        case MDM_SET_SECCFG:
            strcat(buff,MDM_SECCFG);
            gCurrModemState=MDM_SET_SECCFG;
            gModemState=MDM_W4_RESP;
            break;
#endif
      case MDM_DACT_SGACT:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
        break;

      case MDM_OPN_FRWL:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
        break;
      case MDM_CLS_FRWL:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
        break;

/*    case MDM_QRY_LIS_SKT:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
          break;
      case MDM_OPN_LIS_SKT:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
        break;
      case MDM_CLS_LIS_SKT:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
                  send_mdm_command(MDM_DACT_SGACT,5);
          }
        break;
*/

      case MDM_QRY_SKT:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
          break;
      case MDM_OPN_SKT:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
        break;
      case MDM_CLS_SKT:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
                  send_mdm_command(MDM_DACT_SGACT,5);
          }
        break;


      case MDM_LISTENSKT_READY:
          // While waiting for connection to client if need to switch to HTTP mode
            if(gModemMode == MDM_MODE_HTTP)
            {
                //send_mdm_command(MDM_CLS_LIS_SKT,5);
                send_mdm_command(MDM_CLS_SKT,5);
            }
          break;

      case MDM_LISTENSKT_OPEN:
          // MODBUS Poll Here
            if(gModemMode == MDM_MODE_MODBUS)
            {
                lMBRtv = eMBPoll();

                if (lMBRtv == MB_SWITCHTOHTTP)
                {
                    gModemMode = MDM_MODE_HTTP;
                    send_mdm_command(MDM_HANGUP,15);
                }
                else if (lMBRtv == MB_EILLSTATE)
                {
                    send_mdm_command(MDM_HANGUP,15);
                }
                else if (lMBRtv == MB_COMMAND_RECEIVED)
                {
                    gModbusCommandFlag = TRUE;
                }
            }
            else
            {
                send_mdm_command(MDM_HANGUP,15);                  // Revert to HTTP mode
            }

          break;
      case MDM_READY:
          if(gModemMode == MDM_MODE_HTTP)
          {
              if (gSockSendReq != HTTP_NO_REQ)
              {
                 // send_mdm_command(MDM_SOCKOPEN, 10);
                  send_mdm_command(MDM_RD_CSQ, 2);
              }
          }
          else
          {
              send_mdm_command(MDM_OPN_FRWL, 10);  // revert to MODBUS Mode
          }
      break;


      case MDM_SOCKOPEN:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
            if (gSocketToOpen == 1) gSocketToOpen = 2;
            else  gSocketToOpen = 1;

            //gModemState = MDM_CYCLE_POWER;
            gModemState = MDM_RETRY_DELAY;
          }
      break;

      case MDM_RETRY_DELAY:
          start_modem_timer(&gModemTOFlag, 300);
          gModemState=MDM_W8;
      break;

      case MDM_CONNECTED:
          if(gModemMode == MDM_MODE_HTTP)
          {
		      if (gSockSendReq == HTTP_REQ_UNIT_INFO)
		      {
	 			  start_modem_timer(&gModemTOFlag, 30);
	 			  gModemState = MDM_W4_HTTP_RESP;
			      Get_Config();
		      }
		      else if (gSockSendReq == HTTP_SEND_STATUS)
		      {
	 			  start_modem_timer(&gModemTOFlag, 30);
	 			  gModemState = MDM_W4_HTTP_RESP;
	 			  Send_Status();
		      }
		      else if (gSockSendReq == HTTP_SEND_REPORT)
		      {
		          gSentReadings = Send_Report();
		          if ( gSentReadings > 0)
		 		  {
			 		 start_modem_timer(&gModemTOFlag, 30);
		 		     gModemState = MDM_W4_HTTP_RESP;
		 		  }
		      }
		      else if (gSockSendReq == HTTP_GETLATEST_FW)
		      {
		    	 start_modem_timer(&gModemTOFlag, 300);  // 5 Minutes to complete

		    	 gModemState=MDM_FW_DOWNLOAD;
		    	 Set_Analog_Disable_Flag();
		    	 init_Dechunker();

            	 Disp_Lcd_ota(1);
            	 FWDownloadComplete = 0;
            	 Get_Firmware();
		      }

		      gSockSendReq = HTTP_NO_REQ;
          }
          else
          {
              send_mdm_command(MDM_HANGUP, 30);
          }
          break;

      case  MDM_W4_HTTP_RESP:
         if (gModemTOFlag == TRUE)                  // Timed out?
         {
             send_mdm_command(MDM_HANGUP, 30);
         }
      break;
      case MDM_SOCKSHUT:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
             send_mdm_command(MDM_SET_ECHO,2);
          }
      break;

      case MDM_HANGUP:
         if (gModemTOFlag == TRUE)                  // Timed out?
         {
            // gModemState =  MDM_CYCLE_POWER;          // restart
            //send_mdm_command(MDM_SET_ECHO,2);
             send_mdm_command(MDM_DACT_SGACT,4);
         }
      break;

      case MDM_FW_DOWNLOAD:
             if (gModemTOFlag == TRUE)                  // Timed out?
             {
                 send_mdm_command(MDM_HANGUP, 20);
                 MDM_DBPRINT("FW DWNLD TO");
                 *gCommsRespFlag = FW_UPDATE_FAIL;
                 init_msg_fifo(); //if Update fw fails, OTA fw will be in PSRAM in fifo location and mistakenly sent to ui as readings.
                 enable_analog();

             }
             if (FWDownloadComplete == 1)
             {
                 if (UpdateFirmware(gNewFWVersion) == TRUE)
                 {
                     Save_AND_Reset();
                 }
                 else
                 {
                     FWDownloadComplete = 0;
                     send_mdm_command(MDM_HANGUP, 20);
                     MDM_DBPRINT("FW DWNLD FAIL");
                     *gCommsRespFlag = FW_UPDATE_FAIL;
                     init_msg_fifo(); //if Update fw fails, OTA fw will be in PSRAM in fifo location and mistakenly sent to ui as readings.
                     enable_analog();
                 }
             }
          break;

      case MDM_SET_FOTA_CONT:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
             send_mdm_command(MDM_SET_ECHO,2);
          }
          break;
      case MDM_SET_FOTA_CFG:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
             send_mdm_command(MDM_SET_ECHO,2);
          }
          break;
      case MDM_ACT_FOTA:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
             send_mdm_command(MDM_SET_ECHO,2);
          }
          break;
      case MDM_DACT_FOTA:
          if(gModemTOFlag == TRUE)
          {
              send_mdm_command(MDM_SET_ECHO,2);     //Timed out ?
          }
          break;
      case MDM_OPEN_FTP:
          start_modem_timer(&gModemTOFlag, 10);
          break;
      case MDM_SET_FTP_CFG:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
             send_mdm_command(MDM_CLOSE_FTP,20);
          }
          break;
      case MDM_CLOSE_FTP:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
             send_mdm_command(MDM_SET_ECHO,2);
          }
          break;
      case MDM_GET_OTA_FILE:
          if (gModemTOFlag == TRUE)
          {
              send_mdm_command(MDM_CLOSE_FTP, 20);
          }
          break;
      case MDM_UPDATE:
          if (gModemTOFlag == TRUE)                  // Timed out?
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
          /*start_modem_timer(&gModemTOFlag, 900); // According to Telit, FOTA update should never take more than 15 minutes
          MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
          MDM_DBPRINT("Performing FOTA");
          __delay_cycles(240000);
          gModemState=MDM_W4_FOTA;*/
          break;

      case MDM_W4_FOTA:                 //Give module 15 minutes to update then reset.
          if (gModemTOFlag == TRUE)
          {
              MDM_DBPRINT("FOTA Has Stalled or Failed");
              send_mdm_command(MDM_SET_ECHO, 2);
          }
          break;

      default:
          gModemState = MDM_CYCLE_POWER;
          break;
    }

}


/******************************************************************************
 *   Telit_ProcessResponse(void)
 *
 *   TODO Description
 *
 *****************************************************************************/
int Telit_ProcessResponse(void)
{
   int lRCode;

    lRCode = MDM_RESP_DISREGARD;



	switch (gModemState)
	{
	    case MDM_SHUT_DOWN:
            MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

            if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8, "OK", 2) == 0)
            {
                gModemState = MDM_REBOOT;
            }
            break;

        case MDM_W4_HTTP_RESP:
            // Handle returned messages here
            if (rx_msg[MODEM_PORT].buf.uint8[0] == '{')
            {
                // Parse_V3_Config((char*) rx_msg[MODEM_PORT].buf.uint8);
                strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);
                *gCommsRespFlag = CONFIG_OK;
                gModemState = MDM_CONNECTED;
            }
            else if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8, "Date:", 5) == 0)
            {
                //Parse_Date((char*) rx_msg[MODEM_PORT].buf.uint8);
            }
            else if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8, "NO CARRIER", 10) == 0)
            {
                MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
                send_mdm_command(MDM_SOCKSHUT,5);
            }
            else if (strstr((char *) rx_msg[MODEM_PORT].buf.uint8, "Created") != NULL)
            {
                MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
                *gCommsRespFlag = REPORT_OK;
                gModemState = MDM_CONNECTED;
            }
        break;

        case MDM_CONNECTED:
            MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

            if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8, "NO CARRIER", 10) == 0)
            {
                send_mdm_command(MDM_SOCKSHUT,5);
            }
        break;

        case MDM_SET_SCFG:
            MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

            if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8, "OK", 2) == 0)
            {
                send_mdm_command(MDM_SET_SCFGEXT,10);
            }
            break;

        case MDM_SET_SCFGEXT:
            MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

            if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8, "OK", 2) == 0)
            {
                //send_mdm_command(MDM_ACT_SGACT,10);
                send_mdm_command(MDM_RD_CNTXT,10);
            }
            break;

        case MDM_ACT_SGACT:
            if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8,"#SGACT:", 7) == 0)
            {
                strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                gStoredResp = MDM_RESP_SG_ACTIVE;
            }
            else if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
            {

                sscanf((char *) rx_msg[SCRATCH_PAD].buf.uint8,"#SGACT:%d.%d.%d.%d", &gIPA[0],&gIPA[1],&gIPA[2],&gIPA[3]);

                strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

                if(gModemMode ==  MDM_MODE_HTTP)
                {
                    //gModemState =  MDM_READY;
                    send_mdm_command(MDM_ACT_RDP, 2);
                }
                else
                {
                    send_mdm_command(MDM_OPN_FRWL,2);
                }
            }
            else if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "ERROR", 5) == 0)
            {
                strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

                gModemState =  MDM_CYCLE_POWER;
            }
            else
            {
                send_mdm_command(MDM_ACT_RDP,2);
            }
        break;

        case MDM_ACT_RDP:
            MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
            gModemState = MDM_READY;
            break;

        case MDM_SET_ECHO:
            MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

            if ((strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0) || (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "ATE0", 4) == 0))
            {
                send_mdm_command(MDM_SET_IPR,4);
            }
            else
            {
                //send_mdm_command(MDM_SET_ECHO,2);
                gModemState = MDM_CYCLE_POWER;
            }
        break;

        case MDM_QRY_FW:

            if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8,"#FWSWITCH:", 10 ) == 0)
            {
               strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);

               gStoredResp = MDM_RESP_OK;
            }
            else
            {

                if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
                {
                    strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                    MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

                    if (gStoredResp == MDM_RESP_OK)
                    {
                        sscanf((char *) rx_msg[SCRATCH_PAD].buf.uint8,"#FWSWITCH: %d", &gFWFlag );

                           if (gFWFlag == 0)
                           {
                               send_mdm_command(MDM_SET_FWVZW,10);
                           }
                           else
                           {
                               //send_mdm_command(MDM_SET_IPR,4);
                               send_mdm_command(MDM_SET_SELINT,2);
                           }

                    }
                    else
                    {
                        gModemState = MDM_CYCLE_POWER;
                    }
                }
                else
                {
                    send_mdm_command(MDM_SET_ECHO,2);
                }
          }
            break;

        case MDM_SET_FWVZW:
            MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
            if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
            {
                //__delay_cycles(1200000); //5 seconds
                send_mdm_command(MDM_SET_ECHO,2);
            }
            else
            {
                send_mdm_command(MDM_SET_ECHO,2);
            }
            break;

        case MDM_SET_IPR:
              MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

              if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
              {
                 set_uart_baudrate(B57600);
                 send_mdm_command(MDM_QRY_FW,4);
                 //send_mdm_command(MDM_SET_VERBOSE,4);
              }
              else
              {
                  send_mdm_command(MDM_SET_ECHO,2);
              }
              break;

        case MDM_SET_SELINT:
            MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);


              if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
              {
                  send_mdm_command(MDM_SET_FLOW,2);
              }
              else
              {
                  send_mdm_command(MDM_SET_ECHO,2);
              }
        break;

        case MDM_SET_FLOW:
            MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

              if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
              {
                  send_mdm_command(MDM_SET_SKIPESC,2);
              }
              else
              {
                  send_mdm_command(MDM_SET_ECHO,2);
              }
        break;

        case MDM_SET_SKIPESC:
              MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

                if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
                {
                    send_mdm_command(MDM_SET_ESCGUARDTIME,2);
                }
                else
                {
                    send_mdm_command(MDM_SET_ECHO,2);
                }
          break;

          case MDM_SET_ESCGUARDTIME:
              MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

                if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
                {
                    //send_mdm_command(MDM_SAVE_PROFILE,2);
                    send_mdm_command(MDM_SET_VERBOSE,2);
                }
                else
                {
                    send_mdm_command(MDM_SET_ECHO,2);
                }
          break;

          case MDM_SET_VERBOSE:
              MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

              if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
              {
                 send_mdm_command(MDM_SAVE_PROFILE,2);
              }
              else
              {
                  send_mdm_command(MDM_SET_ECHO,2);
              }
              break;

        case MDM_SAVE_PROFILE:
            MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

              if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
              {
                  send_mdm_command(MDM_RD_ICCID, 4);
              }
              else
              {
                  send_mdm_command(MDM_SET_ECHO,2);
              }
        break;

        case MDM_RD_ICCID:
             if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8,"+ICCID:", 7 ) == 0)
             {
                strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                gStoredResp = MDM_RESP_OK;
             }
             else
             {
                 if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
                 {
                     strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                     MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

                      if (gStoredResp == MDM_RESP_OK)
                      {
                          sscanf((char *) rx_msg[SCRATCH_PAD].buf.uint8,"+ICCID: %s", gICCID );
                          send_mdm_command(MDM_RD_IMEI, 2);
                      }
                      else
                      {
                          send_mdm_command(MDM_SET_ECHO,2);
                      }
                 }
                 else
                 {
                     send_mdm_command(MDM_SET_ECHO,2);
                 }
              }
        break;

        case MDM_RD_IMEI:
             if (rx_msg[MODEM_PORT].len > 6)
             {
                  strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                  gStoredResp = MDM_RESP_OK;
             }
             else if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
             {
                  strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                  MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

                  if (gStoredResp == MDM_RESP_OK)
                  {
                      sscanf((char *) rx_msg[SCRATCH_PAD].buf.uint8,"%s", gIMEI );
                      //send_mdm_command(MDM_RD_MFW, 2);
                      send_mdm_command(MDM_NUMBER, 2);
                  }
                  else
                  {
                      send_mdm_command(MDM_SET_ECHO,2);
                  }
             }
             else
             {
                 send_mdm_command(MDM_SET_ECHO,2);
             }
        break;

        case MDM_NUMBER:
            if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8,"+CNUM:", 6 ) == 0)
                         {
                            strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                            gStoredResp = MDM_RESP_OK;
                         }
                         else
                         {
                             if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
                             {
                                 strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                                 MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

                                  if (gStoredResp == MDM_RESP_OK)
                                  {
                                      sscanf((char *) rx_msg[SCRATCH_PAD].buf.uint8,"+CNUM: \"Line 1\",\"+ %s", gNumber );
                                      //send_mdm_command(MDM_RD_MFW, 2);
                                      send_mdm_command(MDM_RD_MODULE, 2);
                                  }
                                  else
                                  {
                                      send_mdm_command(MDM_SET_ECHO,2);
                                  }
                             }
                             else
                             {
                                 send_mdm_command(MDM_SET_ECHO,2);
                             }
                          }
        break;

        case MDM_RD_MODULE:
            if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
            {
                strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

                if (gStoredResp == MDM_RESP_OK)
                {
                    send_mdm_command(MDM_RD_MFW, 2);
                }
                else
                {
                    send_mdm_command(MDM_SET_ECHO,2);
                }
            }
            else if (rx_msg[MODEM_PORT].len >= 4)
            {
                strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);

                gStoredResp = MDM_RESP_OK;
            }
            else
            {
                send_mdm_command(MDM_SET_ECHO,2);
            }
        break;

        case MDM_RD_MFW:
            if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
            {
                strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

				if (gStoredResp == MDM_RESP_OK)
				{
					//send_mdm_command(MDM_RD_CSQ, 2);
					send_mdm_command(MDM_RD_CREG, 2);
				}
				else
				{
					send_mdm_command(MDM_SET_ECHO,2);
				}
			}
			else if (sscanf((char *) rx_msg[MODEM_PORT].buf.uint8,"%s", gMFW ) == 1)
			{
                strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);

                gStoredResp = MDM_RESP_OK;
            }
            else
            {
                send_mdm_command(MDM_SET_ECHO,2);
            }
        break;

        case MDM_HANGUP:
            //MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

            if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
            {
                //MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);
                //send_mdm_command(MDM_SOCKSHUT,4);
                send_mdm_command(MDM_DACT_SGACT,4);
            }
            else if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8, "NO CARRIER", 10) == 0)
            {
                //MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);
                //send_mdm_command(MDM_SOCKSHUT,5);
                send_mdm_command(MDM_DACT_SGACT,4);
            }
/*            else if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8, "HTTP/1.1 400", 12) == 0)
            {
                //start_modem_timer(&gModemTOFlag, 30);
                send_mdm_command(MDM_SHUT_DOWN,4);
            }
*/
            else
            {
                //MDM_DBPRINT("lock up here?");
                //send_mdm_command(MDM_QRY_SGACT, 3);
                //send_mdm_command(MDM_DACT_SGACT, 4);
                //send_mdm_command(MDM_SHUT_DOWN, 2);
                //send_mdm_command(MDM_SOCKSHUT,5);
                //send_mdm_command(MDM_HANGUP,20);

                start_modem_timer(&gModemTOFlag, 6);
                //strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
            }
        break;

        case MDM_SOCKSHUT:
            MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

             if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
             {
                 //send_mdm_command(MDM_QRY_SGACT, 3);
                 send_mdm_command(MDM_DACT_SGACT, 2);
             }
             else
             {
                send_mdm_command(MDM_SET_ECHO,2);
             }
        break;

      case MDM_RD_CREG:

          if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8,"+CEREG:", 7 ) == 0)
          {
             strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);

             gStoredResp = MDM_RESP_CREG_OK;
          }
          else
          {

              if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
              {
                  strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                  MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

                  if (gStoredResp == MDM_RESP_CREG_OK)
                  {
                      sscanf((char *) rx_msg[SCRATCH_PAD].buf.uint8,"+CEREG: %*[^,],%d", &gNetwork );

                         if ((gNetwork == 1) || (gNetwork == 5))
                         {
                             comms_meter_status |= COMMS_STATUS_OK;  // Home or Roaming
                             send_mdm_command(MDM_SET_CLK_MODE,2);
                         }
                         else
                         {
                            comms_meter_status &= ~COMMS_STATUS_OK;
                         }

                  }
                  else
                  {
                      //send_mdm_command(MDM_SET_ECHO,2);
                      gModemState = MDM_CYCLE_POWER;
                  }
              }
              else
              {
                  send_mdm_command(MDM_SET_ECHO,2);
              }
        }

        break;

      case MDM_RD_CNTXT:
          if (rx_msg[MODEM_PORT].len >= 9)
          {
              strcat((char *) rx_msg[SCRATCH_PAD].buf.uint8, (char *) rx_msg[MODEM_PORT].buf.uint8);
          }
          else if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              strcat((char *) rx_msg[SCRATCH_PAD].buf.uint8, (char *) rx_msg[MODEM_PORT].buf.uint8);
              MDM_DBPRINT((char *) rx_msg[SCRATCH_PAD].buf.uint8);

              send_mdm_command(MDM_ACT_SGACT, 2);
          }
          else
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
      break;

      case MDM_SET_CLK_MODE:
          MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

            if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
            {
                send_mdm_command(MDM_RD_CLK, 4);
            }
            else
            {
                send_mdm_command(MDM_SET_ECHO,2);
            }
      break;

      case MDM_RD_CLK:
          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "#CCLK:", 6) == 0)
          {
              strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
              gStoredResp = MDM_RESP_OK;
          }
          else
          {
              strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
              MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

              if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
              {
                  mdm_set_rtc((char *) rx_msg[SCRATCH_PAD].buf.uint8);
                  //send_mdm_command(MDM_SET_CGDCONT,2);
                  send_mdm_command(MDM_QRY_SGACT,2);
              }
              else
              {
                  send_mdm_command(MDM_SET_ECHO,2);
              }
          }

          break;
      case MDM_RD_CSQ:

             if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8,"+CSQ:", 5) == 0)
             {
                 strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                 gStoredResp = MDM_RESP_CSQ_OK;
             }
             else
             {

                 if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
                 {
                      if (gStoredResp == MDM_RESP_CSQ_OK)
                      {
                         // send_mdm_command(MDM_RD_CREG, 2);
                          send_mdm_command(MDM_SOCKOPEN, 10);
                      }
                      else
                      {
                          send_mdm_command(MDM_SET_ECHO,2);
                      }
                 }
                 else
                 {
                     send_mdm_command(MDM_SET_ECHO,2);
                 }


             }
        break;

#ifdef SSL
     case MDM_SSL_ENABLE:
     break;
#endif
       case MDM_DACT_SGACT:
           MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

             if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
             {
                 send_mdm_command(MDM_QRY_SGACT, 2);

                 //If XDER gets modem update signal from config

                 //MDM_DBPRINT("FOTA Request Received");
                 //send_mdm_command(MDM_SET_FOTA_CONT, 2);
             }
             else // if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "ERROR", 5) == 0)
             {
                 gModemState = MDM_CYCLE_POWER;
             }
       break;

/*       case MDM_SET_CGDCONT:
           MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

             if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
             {
                  send_mdm_command(MDM_QRY_SGACT,2);
             }
             else //if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "ERROR", 5) == 0)
             {
                 send_mdm_command(MDM_DACT_SGACT,2);
             }
           break;
*/

      case MDM_QRY_SGACT:
             // Need to remember the state of context 3 even when 1,2,4 and 5 are reported.
//           if (sscanf((char *) rx_msg[MODEM_PORT].buf.uint8,"#SGACT: %d,%d", &lContext, &lActive) == 2)
             if (rx_msg[MODEM_PORT].len >= 9)
             {
                 if (rx_msg[MODEM_PORT].buf.uint8[8] == '1')
                 {
                    strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                 }
                 else
                 {
                    strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                 }

                 if (rx_msg[MODEM_PORT].buf.uint8[8] == '3')
                 {
                     if (rx_msg[MODEM_PORT].buf.uint8[10] == '1')
                     {
                        gStoredResp = MDM_RESP_SG_ACTIVE;
                     }
                     else
                     {
                        gStoredResp = MDM_RESP_SG_NOT_ACTIVE;
                     }
                 }
             }
             else if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
             {
                 strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                 MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

                  if (gStoredResp == MDM_RESP_SG_ACTIVE)
                  {
                      if (gModemMode == MDM_MODE_HTTP)
                      {
                          MDM_DBPRINT("MODEM IN HTTP MODE");
                          gModemState = MDM_READY;
                      }
                      else
                      {
                          send_mdm_command(MDM_OPN_FRWL,2);
                      }

                  }
                  else
                  {
                      send_mdm_command(MDM_SET_SCFG,2);
                  }
             }
             else
             {
                 send_mdm_command(MDM_DACT_SGACT,2);
             }
      break;

      case MDM_OPN_FRWL:
          MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              //send_mdm_command(MDM_OPN_LIS_SKT,5);
              send_mdm_command(MDM_OPN_SKT,5);
          }
          else if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "ERROR", 5) == 0)
          {
              send_mdm_command(MDM_CLS_FRWL,2);
          }

          break;

      case MDM_CLS_FRWL:
          MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              if (gModemMode == MDM_MODE_HTTP)
              {
                  // switch to http mode before getting connected from client
                  gModemState =  MDM_READY;
              }
              else // all cases open firewall in MODBUS mode or switch to modbus mode
              {
                  send_mdm_command(MDM_OPN_FRWL,2);
              }
          }
          else if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "ERROR", 5) == 0)
          {
              gModemState = MDM_CYCLE_POWER;
          }
          break;

/*    case MDM_QRY_LIS_SKT:

          if(strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "#SKTL",5) == 0 )
          {
              strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);

              if (rx_msg[MODEM_PORT].buf.uint8[7] == '1')
              {
                  gStoredResp = TRUE;
              }
              else
              {
                  gStoredResp = FALSE;
              }
          }
          else
          {
              strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
              MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

              if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
              {
                  if(gStoredResp == TRUE)
                  {
                      gModemState =  MDM_LISTENSKT_READY;
                  }
                  else
                  {
                      send_mdm_command(MDM_QRY_SGACT,2);
                  }

              }
              else
              {
                  send_mdm_command(MDM_QRY_SGACT,2);
              }
          }
          break;
      case MDM_OPN_LIS_SKT:
          MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              MDM_DBPRINT("MODEM IN MODBUS MODE");
              gModemState =  MDM_LISTENSKT_READY;
          }
          else if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "ERROR", 5) == 0)
          {
              send_mdm_command(MDM_CLS_LIS_SKT,10);
          }
          break;
      case MDM_CLS_LIS_SKT:
          MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              if (gModemMode == MDM_MODE_HTTP)
              {
                  gModemState = MDM_READY;
              }
              else
              {
                  send_mdm_command(MDM_OPN_LIS_SKT,5);
              }
          }
          else if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "ERROR", 5) == 0)
          {
              gModemState = MDM_CYCLE_POWER;
          }
          else if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8, "NO CARRIER", 10) == 0)
          {
                send_mdm_command(MDM_QRY_SGACT,2);
          }

          break;

*/

        case MDM_QRY_SKT:

              if(strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "#SL",5) == 0 )
              {
                  strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);

                  if (rx_msg[MODEM_PORT].buf.uint8[7] == '1')
                  {
                      gStoredResp = TRUE;
                  }
                  else
                  {
                      gStoredResp = FALSE;
                  }
              }
              else
              {
                  strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
                  MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

                  if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
                  {
                      if(gStoredResp == TRUE)
                      {
                          gModemState =  MDM_LISTENSKT_READY;
                      }
                      else
                      {
                          send_mdm_command(MDM_QRY_SGACT,2);
                      }

                  }
                  else
                  {
                      send_mdm_command(MDM_QRY_SGACT,2);
                  }
              }
              break;
          case MDM_OPN_SKT:
              MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

              if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
              {
                  MDM_DBPRINT("MODEM IN MODBUS MODE");
                  gModemState =  MDM_LISTENSKT_READY;
              }
              else if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "ERROR", 5) == 0)
              {
                  send_mdm_command(MDM_CLS_SKT,10);
              }
              break;
          case MDM_CLS_SKT:
              MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

              if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
              {
                  if (gModemMode == MDM_MODE_HTTP)
                  {
                      gModemState = MDM_READY;
                  }
                  else
                  {
                      send_mdm_command(MDM_OPN_SKT,5);
                  }
              }
              else if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "ERROR", 5) == 0)
              {
                  gModemState = MDM_CYCLE_POWER;
              }
              else if (strncmp((char *) rx_msg[MODEM_PORT].buf.uint8, "NO CARRIER", 10) == 0)
              {
                    send_mdm_command(MDM_QRY_SGACT,2);
              }

              break;

#ifdef SSL
          case MDM_SET_SSLCFG:
      break;

        case MDM_SET_SECCFG:
            break;
#endif

      case MDM_READY:
          break;

      case MDM_LISTENSKT_READY:
           if(strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "+CONN",5) == 0 )
           {
              strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
           }
          else
          {
              strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
       //       MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);

              if(strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "CONNECT", 7) == 0)
              {
                  gModemState =  MDM_LISTENSKT_OPEN;
              }
          }
          break;

      case MDM_LISTENSKT_OPEN:

          break;

      case MDM_SOCKOPEN:
          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "CONNECT", 7) == 0)
          {
              gModemState =  MDM_CONNECTED;

              MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
          }
          else
          {
             gModemState = MDM_RETRY_DELAY;

              // send_mdm_command(MDM_DACT_SGACT,2);     // Power cycle the modem if response as Error
              //  if (gSocketToOpen == 1) gSocketToOpen = 2;
              // else  gSocketToOpen = 1;

              //gModemState = MDM_CYCLE_POWER;
          }
      break;

      case MDM_FW_DOWNLOAD:
/*         if (UpdateFirmware() == FW_UPDATE_SUCCESS)
           {
               // reboot
           }
           else
           {
               // Back to normal ops
               gModemState =  MDM_HANGUP;
               enable_analog();
           } */
          break;
/*
 *
 *  Telit module FOTA update section
 *
 */
      case MDM_SET_FOTA_CONT:
          MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              send_mdm_command(MDM_SET_FOTA_CFG, 2);
          }
          break;
      case MDM_SET_FOTA_CFG:
          MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              send_mdm_command(MDM_ACT_FOTA, 4);
          }
          break;
      case MDM_ACT_FOTA:
          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "#SGACT:", 7) == 0)
          {
              strcpy((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
              gStoredResp = MDM_RESP_SG_ACTIVE;
          }
          else if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              sscanf((char *) rx_msg[SCRATCH_PAD].buf.uint8,"#SGACT:%d.%d.%d.%d", &gIPA[0],&gIPA[1],&gIPA[2],&gIPA[3]);

              strcat((char*) rx_msg[SCRATCH_PAD].buf.uint8, (char*) rx_msg[MODEM_PORT].buf.uint8);
              MDM_DBPRINT((char*) rx_msg[SCRATCH_PAD].buf.uint8);
              send_mdm_command(MDM_OPEN_FTP, 20);
          }
          break;
      case MDM_OPEN_FTP:
          MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              send_mdm_command(MDM_SET_FTP_CFG, 2);
          }
          break;
      case MDM_SET_FTP_CFG:
          MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              send_mdm_command(MDM_GET_OTA_FILE,600);
              MDM_DBPRINT("Downloading Module Update...");
          }
          break;

      case MDM_GET_OTA_FILE:
          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              //MDM_DBPRINT((char *)rx_msg[MODEM_PORT].buf.uint8);
              MDM_DBPRINT("Download Complete!");
              gFOTAflag = 1;
              send_mdm_command(MDM_CLOSE_FTP, 20);
          }
          else
          {
              MDM_DBPRINT((char *)rx_msg[MODEM_PORT].buf.uint8);
              send_mdm_command(MDM_CLOSE_FTP, 20);
          }
          break;

      case MDM_CLOSE_FTP:
          MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              send_mdm_command(MDM_DACT_FOTA, 2);
          }
          break;
      case MDM_DACT_FOTA:
          MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);

          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              if (gFOTAflag == 1)
              {
                  send_mdm_command(MDM_RD_SWPKGV, 10);
                  //send_mdm_command(MDM_UPDATE, 20);
              }
              else
              {
                  send_mdm_command(MDM_SET_ECHO, 2);
              }
          }
          else // if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "ERROR", 5) == 0)
          {
              gModemState = MDM_CYCLE_POWER;
          }
          break;

      case MDM_UPDATE:
          MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
          MDM_DBPRINT("Performing FOTA...");
          __delay_cycles(240000);
          start_modem_timer(&gModemTOFlag, 900);
          set_uart_baudrate(B115200);
          Set_Analog_Disable_Flag();
          gModemState=MDM_W4_FOTA;
          break;

      case MDM_W4_FOTA:
          if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "#OTAEV: Module", 14) == 0)
          {
              MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
              gFOTAflag=0;
              enable_analog();
              __delay_cycles(1200000);
              send_mdm_command(MDM_RD_SWPKGV, 2);
          }
          else
          {
              MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
              gModemState=MDM_CYCLE_POWER;
          }
          break;

      case MDM_RD_SWPKGV:
          //MDM_DBPRINT((char *) rx_msg[MODEM_PORT].buf.uint8);
          if (rx_msg[MODEM_PORT].len >= 5) //"OK" is actually 4 chars, what a f-ing headache
          {
              if (rx_msg[MODEM_PORT].buf.uint8[0] == '3')
              {
                  strcpy((char *) rx_msg[SCRATCH_PAD].buf.uint8, (char *) rx_msg[MODEM_PORT].buf.uint8);
              }
              else
              {
                  strcat((char *) rx_msg[SCRATCH_PAD].buf.uint8, (char *) rx_msg[MODEM_PORT].buf.uint8);
              }
          }
          else if (strncmp((char *) (rx_msg[MODEM_PORT].buf.uint8), "OK", 2) == 0)
          {
              strcat((char *) rx_msg[SCRATCH_PAD].buf.uint8, (char *) rx_msg[MODEM_PORT].buf.uint8);
              MDM_DBPRINT((char *) rx_msg[SCRATCH_PAD].buf.uint8);
              if (gFOTAflag == 1)
              {
                  send_mdm_command(MDM_UPDATE, 20);
              }
              else
              {
                  //gModemState = MDM_CYCLE_POWER;
                  gModemState = MDM_READY;
              }
          }
          else
          {
              send_mdm_command(MDM_SET_ECHO,2);
          }
      break;

    }

return lRCode;
}

unsigned int GetNetworkStatus()
{
    return gNetwork;
}
unsigned int GetSignalQuality()
{
    return gSigQuality;
}


void mdm_set_rtc(char * tm_str)
{
    int lNumConversions;
    int lsec,lmin,lhour,lday,lmon,lyear;
    char lDbStr[50];
    time_t old_time;
    time_t now_time;
    struct tm tm_buf;
    rtc_t old_rtc;

    lNumConversions = sscanf(tm_str,"#CCLK: \"%d/%d/%d,%d:%d:%d-", &lyear, &lmon, &lday, &lhour, &lmin, &lsec );

    old_rtc.century = rtc.century;
    old_rtc.year = rtc.year;
    old_rtc.month = rtc.month;
    old_rtc.day = rtc.day;
    old_rtc.hour = rtc.hour;
    old_rtc.minute = rtc.minute;
    old_rtc.second = rtc.second;


    if ((lyear != 80) && (lNumConversions == 6))
    {
        if( (lyear >= 17) && (lyear < 99) &&
            (lmon >= 1) && (lmon <= 12) &&
            (lday >= 1) && (lday <= 31) &&
            (lhour >= 0) && (lhour <= 24) &&
            (lmin >= 0) && (lmin <= 60) &&
            (lsec >= 0) && (lsec <= 60) )
        {

            // determine time delta
            tm_buf.tm_year = ((int) (rtc.century - 19) * 100) + (int) rtc.year;
            tm_buf.tm_mon = rtc.month - 1;
            tm_buf.tm_mday = rtc.day;
            tm_buf.tm_hour = rtc.hour;
            tm_buf.tm_min = rtc.minute;
            tm_buf.tm_sec = rtc.second;
            tm_buf.tm_isdst = -1;

            old_time = mktime(&tm_buf);

            tm_buf.tm_year = ((int) (20 - 19) * 100) + (int) lyear;
            tm_buf.tm_mon = lmon - 1;
            tm_buf.tm_mday = lday;
            tm_buf.tm_hour = lhour;
            tm_buf.tm_min = lmin;
            tm_buf.tm_sec = lsec;
            tm_buf.tm_isdst = -1;

            now_time = mktime(&tm_buf);

            if (old_time < now_time)
            {

                rtc.century = 20;
                rtc.year = lyear;
                rtc.month = lmon;
                rtc.day = lday;
                rtc.hour = lhour;
                rtc.minute = lmin;
                rtc.second = lsec;
                set_rtc_sumcheck();

                sprintf(lDbStr, "Set RTC to: %d/%d/%d,%d:%d:%d", lyear, lmon, lday, lhour, lmin, lsec);
                MDM_DBPRINT(lDbStr);

                if (FIFOTimeValid() == FALSE) FixFIFOTime(old_rtc);
            }
        }
    }

}
